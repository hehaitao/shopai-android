package top.yokey.shopai.distribu.activity;

import android.view.View;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;
import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.distribu.viewmodel.DistribuAccountVM;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zsdk.data.DistribuAccountData;

@Route(path = ARoutePath.DISTRIBU_ACCOUNT)
public class DistribuAccountActivity extends BaseActivity {

    private final DistribuAccountData data = new DistribuAccountData();
    private Toolbar mainToolbar = null;
    private AppCompatTextView accountTextView = null;
    private AppCompatTextView emailTextView = null;
    private LinearLayoutCompat typeLinearLayout = null;
    private AppCompatTextView typeTextView = null;
    private LinearLayoutCompat bankLinearLayout = null;
    private AppCompatEditText bankEditText = null;
    private AppCompatEditText payeeEditText = null;
    private AppCompatEditText receiveEditText = null;
    private AppCompatTextView modifyTextView = null;
    private DistribuAccountVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_distribu_account);
        mainToolbar = findViewById(R.id.mainToolbar);
        accountTextView = findViewById(R.id.accountTextView);
        emailTextView = findViewById(R.id.emailTextView);
        typeLinearLayout = findViewById(R.id.typeLinearLayout);
        typeTextView = findViewById(R.id.typeTextView);
        bankLinearLayout = findViewById(R.id.bankLinearLayout);
        bankEditText = findViewById(R.id.bankEditText);
        payeeEditText = findViewById(R.id.payeeEditText);
        receiveEditText = findViewById(R.id.receiveEditText);
        modifyTextView = findViewById(R.id.modifyTextView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.accountSetting);
        observeKeyborad(R.id.mainLinearLayout);
        vm = getVM(DistribuAccountVM.class);
        vm.getAsset();

    }

    @Override
    public void initEvent() {

        typeLinearLayout.setOnClickListener(view -> {
            int select = 1;
            ArrayList<String> arrayList = new ArrayList<>();
            arrayList.add(getString(R.string.alipay));
            arrayList.add(getString(R.string.bank));
            String code = data.getBillTypeCode();
            if (code.equals("alipay")) select = 0;
            DialogHelp.get().list(get(), R.string.accountSettlementType, arrayList, select, (position, content) -> {
                data.setBillTypeCode(position == 0 ? "alipay" : "bank");
                bankLinearLayout.setVisibility(position == 0 ? View.GONE : View.VISIBLE);
            }, null);
        });

        modifyTextView.setOnClickListener(view -> {
            data.setBillBankName(data.getBillTypeCode().equals("alipay") ? "" : Objects.requireNonNull(bankEditText.getText()).toString());
            data.setBillUserName(Objects.requireNonNull(payeeEditText.getText()).toString());
            data.setBillTypeNumber(Objects.requireNonNull(receiveEditText.getText()).toString());
            modifyTextView.setText(R.string.handlerIng);
            modifyTextView.setEnabled(false);
            vm.save(data);
        });

    }

    @Override
    public void initObserve() {

        vm.getAssetLiveData().observe(this, bean -> {
            accountTextView.setText(bean.getMemberName());
            emailTextView.setText(bean.getMemberEmail());
            typeTextView.setText(bean.getBillTypeCode().equals("alipay") ? R.string.alipay : R.string.bank);
            bankLinearLayout.setVisibility(bean.getBillTypeCode().equals("alipay") ? View.GONE : View.VISIBLE);
            bankEditText.setText(bean.getBillBankName());
            payeeEditText.setText(bean.getBillUserName());
            receiveEditText.setText(bean.getBillTypeNumber());
            data.setBillTypeCode(bean.getBillTypeCode());
            data.setBillBankName(bean.getBillBankName());
            data.setBillUserName(bean.getBillUserName());
            data.setBillTypeNumber(bean.getBillTypeNumber());
        });

        vm.getSaveLiveData().observe(this, string -> {
            ToastHelp.get().showSuccess();
            modifyTextView.setText(R.string.modify);
            modifyTextView.setEnabled(true);
            onReturn(false);
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getAsset());
                return;
            }
            modifyTextView.setText(R.string.modify);
            modifyTextView.setEnabled(true);
            ToastHelp.get().show(bean.getReason());
        });

    }

}
