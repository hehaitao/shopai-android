package top.yokey.shopai.distribu.activity;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.distribu.viewmodel.DistribuCashVM;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.util.VerifyUtil;

@Route(path = ARoutePath.DISTRIBU_CASH_APPLY)
public class DistribuCashApplyActivity extends BaseActivity {

    private Toolbar mainToolbar = null;
    private AppCompatTextView payeeTextView = null;
    private AppCompatTextView bankTextView = null;
    private AppCompatTextView accountTextView = null;
    private AppCompatEditText amountEditText = null;
    private AppCompatEditText payPassEditText = null;
    private AppCompatTextView applyTextView = null;
    private DistribuCashVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_distribu_cash_apply);
        mainToolbar = findViewById(R.id.mainToolbar);
        payeeTextView = findViewById(R.id.payeeTextView);
        bankTextView = findViewById(R.id.bankTextView);
        accountTextView = findViewById(R.id.accountTextView);
        amountEditText = findViewById(R.id.amountEditText);
        payPassEditText = findViewById(R.id.payPassEditText);
        applyTextView = findViewById(R.id.applyTextView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.applyCash);
        observeKeyborad(R.id.mainLinearLayout);
        vm = getVM(DistribuCashVM.class);
        vm.getAsset();

    }

    @Override
    public void initEvent() {

        applyTextView.setOnClickListener(view -> {
            hideKeyboard();
            String amount = Objects.requireNonNull(amountEditText.getText()).toString();
            String payPwd = Objects.requireNonNull(payPassEditText.getText()).toString();
            if (VerifyUtil.isEmpty(amount)) {
                ToastHelp.get().show(R.string.pleaseInputAmount);
                return;
            }
            if (VerifyUtil.isEmpty(payPwd)) {
                ToastHelp.get().show(R.string.pleaseInputPayPassword);
                return;
            }
            applyTextView.setText(R.string.handlerIng);
            applyTextView.setEnabled(false);
            vm.cash(amount, payPwd);
        });

    }

    @Override
    public void initObserve() {

        vm.getAssetLiveData().observe(this, bean -> {
            payeeTextView.setText(bean.getBillUserName());
            bankTextView.setText(bean.getBillTypeCode().equals("alipay") ? "支付宝" : bean.getBillBankName());
            accountTextView.setText(bean.getBillTypeNumber());
            amountEditText.setHint(String.format(getString(R.string.maxCashPrice), bean.getAvailableFxTrad()));
        });

        vm.getCashLiveData().observe(this, string -> {
            ToastHelp.get().showSuccess();
            applyTextView.setText(R.string.applyCash);
            applyTextView.setEnabled(true);
            onReturn(false);
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getAsset());
                return;
            }
            applyTextView.setText(R.string.applyCash);
            applyTextView.setEnabled(true);
            ToastHelp.get().show(bean.getReason());
        });

    }

}
