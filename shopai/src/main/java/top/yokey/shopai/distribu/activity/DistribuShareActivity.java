package top.yokey.shopai.distribu.activity;

import android.graphics.Bitmap;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.google.zxing.client.android.utils.ZXingUtils;

import cn.sharesdk.onekeyshare.OnekeyShare;
import top.yokey.shopai.R;
import top.yokey.shopai.distribu.viewmodel.DistribuShareVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.VerifyUtil;

@Route(path = ARoutePath.DISTRIBU_SHARE)
public class DistribuShareActivity extends BaseActivity {

    @Autowired(name = Constant.DATA_ID)
    String id;
    private Toolbar mainToolbar = null;
    private AppCompatImageView qrCodeImageView = null;
    private AppCompatTextView shareTextView = null;
    private AppCompatTextView linkTextView = null;
    private DistribuShareVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_distribu_share);
        mainToolbar = findViewById(R.id.mainToolbar);
        qrCodeImageView = findViewById(R.id.qrCodeImageView);
        shareTextView = findViewById(R.id.shareTextView);
        linkTextView = findViewById(R.id.linkTextView);

    }

    @Override
    public void initData() {

        if (VerifyUtil.isEmpty(id)) {
            ToastHelp.get().showDataError();
            onReturn(false);
            return;
        }
        observeKeyborad(R.id.mainLinearLayout);
        setToolbar(mainToolbar, R.string.promotionPoster);
        vm = getVM(DistribuShareVM.class);
        vm.add(id);

    }

    @Override
    public void initEvent() {

        qrCodeImageView.setOnClickListener(view -> {
            qrCodeImageView.buildDrawingCache(true);
            qrCodeImageView.buildDrawingCache();
            Bitmap bitmap = qrCodeImageView.getDrawingCache();
            ImageHelp.get().saveBitmap(get(), bitmap, "qr_code_" + id);
            qrCodeImageView.setDrawingCacheEnabled(false);
            ToastHelp.get().show(String.format(getString(R.string.imageSaveToPath), "qr_code" + id + ".jpg"));
        });

        shareTextView.setOnClickListener(view -> {
            OnekeyShare onekeyShare = new OnekeyShare();
            onekeyShare.disableSSOWhenAuthorize();
            onekeyShare.setTitleUrl(linkTextView.getText().toString());
            onekeyShare.setImageUrl(App.get().getMemberBean().getAvatar());
            onekeyShare.setTitle(getString(R.string.inviteProfit));
            onekeyShare.setText(getString(R.string.inviteProfit));
            onekeyShare.setUrl(linkTextView.getText().toString());
            onekeyShare.show(get());
        });

        linkTextView.setOnClickListener(view -> {
            ToastHelp.get().showSuccess();
            App.get().setClipboard(linkTextView.getText().toString());
        });

    }

    @Override
    public void initObserve() {

        vm.getAddLiveData().observe(this, bean -> {
            linkTextView.setText(bean.getFxUrl());
            qrCodeImageView.setImageBitmap(ZXingUtils.createQRCodeImage(bean.getFxUrl()));
        });

    }

}
