package top.yokey.shopai.distribu.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.GoodsFXAddBean;
import top.yokey.shopai.zsdk.bean.MemberFXAddBean;
import top.yokey.shopai.zsdk.bean.SearchShowBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.IndexController;
import top.yokey.shopai.zsdk.controller.MemberFXController;
import top.yokey.shopai.zsdk.data.GoodsDistribuData;

public class DistribuAddVM extends BaseViewModel {

    private final MutableLiveData<SearchShowBean> searchShowLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<GoodsFXAddBean>> goodsLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();
    private final MutableLiveData<MemberFXAddBean> addLiveData = new MutableLiveData<>();

    public DistribuAddVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<SearchShowBean> getSearchShowLiveData() {

        return searchShowLiveData;

    }

    public MutableLiveData<ArrayList<GoodsFXAddBean>> getGoodsLiveData() {

        return goodsLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public MutableLiveData<MemberFXAddBean> getAddLiveData() {

        return addLiveData;

    }

    public void getSearchShow() {

        IndexController.searchShow(new HttpCallBack<SearchShowBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, SearchShowBean bean) {
                searchShowLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getList(GoodsDistribuData data) {

        MemberFXController.goodsList(data, new HttpCallBack<ArrayList<GoodsFXAddBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<GoodsFXAddBean> list) {
                goodsLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void add(String id) {

        MemberFXController.fxAdd(id, new HttpCallBack<MemberFXAddBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, MemberFXAddBean bean) {
                addLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

}
