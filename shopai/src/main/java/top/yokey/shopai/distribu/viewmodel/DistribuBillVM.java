package top.yokey.shopai.distribu.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.DistribuBillListBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberFXController;

public class DistribuBillVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<DistribuBillListBean>> billLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();

    public DistribuBillVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<DistribuBillListBean>> getBillLiveData() {

        return billLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public void getBillList(String state, String keyword, String page) {

        MemberFXController.fxBill(state, keyword, page, new HttpCallBack<ArrayList<DistribuBillListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<DistribuBillListBean> list) {
                billLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
