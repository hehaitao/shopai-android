package top.yokey.shopai.distribu.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.MemberFXAssetBean;
import top.yokey.shopai.zsdk.bean.MemberFXDepositListBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberFXController;

public class DistribuDepositVM extends BaseViewModel {

    private final MutableLiveData<MemberFXAssetBean> assetLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<MemberFXDepositListBean>> logLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();

    public DistribuDepositVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<MemberFXAssetBean> getAssetLiveData() {

        return assetLiveData;

    }

    public MutableLiveData<ArrayList<MemberFXDepositListBean>> getLogLiveData() {

        return logLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public void getAsset() {

        MemberFXController.myAsset(new HttpCallBack<MemberFXAssetBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, MemberFXAssetBean bean) {
                assetLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getLog(String page) {

        MemberFXController.commissionInfo(page, new HttpCallBack<ArrayList<MemberFXDepositListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<MemberFXDepositListBean> list) {
                logLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
