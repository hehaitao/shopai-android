package top.yokey.shopai.distribu.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.OrderFXListBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberFXController;

public class DistribuOrderVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<OrderFXListBean>> orderLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();

    public DistribuOrderVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<OrderFXListBean>> getOrderLiveData() {

        return orderLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public void getOrderList(String type, String keyword, String page) {

        MemberFXController.fxOrder(type, keyword, page, new HttpCallBack<ArrayList<OrderFXListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<OrderFXListBean> list) {
                orderLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
