package top.yokey.shopai.goods.activity;

import android.content.Intent;
import android.graphics.Paint;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.listener.GSYSampleCallBack;
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;
import com.youth.banner.Banner;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Objects;

import cc.shinichi.library.ImagePreview;
import cn.sharesdk.onekeyshare.OnekeyShare;
import top.yokey.shopai.R;
import top.yokey.shopai.goods.adapter.GoodsCommendAdapter;
import top.yokey.shopai.goods.adapter.GoodsEvaluateSAdapter;
import top.yokey.shopai.goods.adapter.GoodsSpecAdapter;
import top.yokey.shopai.goods.viewmodel.GoodsVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.help.LoggerHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.FlowLayoutManager;
import top.yokey.shopai.zcom.recycler.GridDecoration;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.util.AnimUtil;
import top.yokey.shopai.zcom.util.ConvertUtil;
import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zcom.view.CenterTextView;
import top.yokey.shopai.zcom.view.CountdownTextView;
import top.yokey.shopai.zcom.view.ScrollDetailsLayout;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.EvaluateBean;
import top.yokey.shopai.zsdk.bean.GoodsCommendBean;
import top.yokey.shopai.zsdk.data.GoodsBuyData;
import top.yokey.shopai.zsdk.data.GoodsData;

@Route(path = ARoutePath.GOODS_GOODS)
public class GoodsActivity extends BaseActivity {

    private final LinearLayoutCompat[] stepsLinearLayout = new LinearLayoutCompat[3];
    private final AppCompatTextView[] stepPriceTextView = new AppCompatTextView[3];
    private final AppCompatTextView[] stepLimitTextView = new AppCompatTextView[3];
    private final AppCompatTextView[] specTextView = new AppCompatTextView[5];
    private final View[] chooseLineView = new View[5];
    private final AppCompatTextView[] chooseValueTextView = new AppCompatTextView[5];
    private final RecyclerView[] chooseValueRecyclerView = new RecyclerView[5];
    //商品Spec
    private final String[] specArray = new String[]{"", "", "", "", ""};
    private final ArrayList<HashMap<String, String>> specNameArrayList = new ArrayList<>();
    private final ArrayList<HashMap<String, String>> specValueArrayList = new ArrayList<>();
    private final ArrayList<HashMap<String, String>> goodsSpecArrayList = new ArrayList<>();
    private final ArrayList<HashMap<String, String>> specListArrayList = new ArrayList<>();
    //评价推荐
    private final ArrayList<EvaluateBean> evaluateArrayList = new ArrayList<>();
    private final GoodsEvaluateSAdapter evaluateAdapter = new GoodsEvaluateSAdapter(evaluateArrayList);
    private final ArrayList<GoodsCommendBean> commendArrayList = new ArrayList<>();
    private final GoodsCommendAdapter commendAdapter = new GoodsCommendAdapter(commendArrayList);
    //商品数据
    @Autowired(name = Constant.DATA_JSON)
    String json = "";
    //主要控件
    private ScrollDetailsLayout mainSlideDetailsLayout = null;
    private NestedScrollView mainScrollView = null;
    private WebView mainWebView = null;
    private AppCompatTextView nightTextView = null;
    //商品信息
    private RelativeLayout mediaRelativeLayout = null;
    private Banner mediaBanner = null;
    private StandardGSYVideoPlayer mediaVideoPlayer = null;
    private AppCompatImageView mediaImageView = null;
    private RelativeLayout timeRelativeLayout = null;
    private AppCompatTextView timeTypeTextView = null;
    private CountdownTextView timeTextView = null;
    private RelativeLayout pinGouRelativeLayout = null;
    private AppCompatTextView pinGouTextView = null;
    private AppCompatTextView pinGouPriceTextView = null;
    private AppCompatTextView pinGouPriceMarketTextView = null;
    private CountdownTextView pinGouTimeTextView = null;
    private AppCompatTextView nameTextView = null;
    private AppCompatTextView descTextView = null;
    private AppCompatTextView priceTextView = null;
    private AppCompatTextView priceMarketTextView = null;
    private AppCompatImageView mobileImageView = null;
    private AppCompatTextView saleTextView = null;
    private LinearLayoutCompat stepLinearLayout = null;
    //商品活动
    private LinearLayoutCompat saleTypeLinearLayout = null;
    private AppCompatTextView saleTypeTextView = null;
    private AppCompatTextView saleTypeDescTextView = null;
    private LinearLayoutCompat manSongLinearLayout = null;
    private AppCompatTextView manSongTextView = null;
    private LinearLayoutCompat zengPinLinearLayout = null;
    private AppCompatTextView zengPinTextView = null;
    private AppCompatImageView zengPinImageView = null;
    private LinearLayoutCompat voucherLinearLayout = null;
    //属性地区
    private LinearLayoutCompat specLinearLayout = null;
    private RelativeLayout areaRelativeLayout = null;
    private AppCompatTextView areaAddressTextView = null;
    private AppCompatTextView areaHaveTextView = null;
    private View areaLineView = null;
    private AppCompatTextView serviceDescTextView = null;
    private AppCompatTextView serviceSevDayTextView = null;
    private AppCompatTextView serviceQualityTextView = null;
    private AppCompatTextView serviceReissueTextView = null;
    private AppCompatTextView serviceLogisticsTextView = null;
    //商品评价
    private LinearLayoutCompat evaluateLinearLayout = null;
    private AppCompatTextView evaluateDescTextView = null;
    private AppCompatTextView evaluateNumberTextView = null;
    private RecyclerView evaluateRecyclerView = null;
    //店铺信息
    private LinearLayoutCompat storeLinearLayout = null;
    private AppCompatImageView storeAvatarImageView = null;
    private AppCompatTextView storeNameTextView = null;
    private AppCompatTextView storeOwnTextView = null;
    private AppCompatTextView storeCollectTextView = null;
    private AppCompatTextView storeDescTextView = null;
    private AppCompatTextView storeServiceTextView = null;
    private AppCompatTextView storeDeliverTextView = null;
    private CenterTextView storeCustomerTextView = null;
    private CenterTextView storeInTextView = null;
    //商品推荐
    private RecyclerView commendRecyclerView = null;
    //顶部标签
    private View topView = null;
    private Toolbar topToolbar = null;
    private View topLineView = null;
    //底部按钮
    private AppCompatTextView customerTextView = null;
    private AppCompatTextView favoriteTextView = null;
    private AppCompatTextView shareTextView = null;
    private AppCompatTextView addCartTextView = null;
    private AppCompatTextView nowBuyTextView = null;
    private AppCompatTextView signBuyTextView = null;
    private AppCompatTextView pinGouBuyTextView = null;
    private AppCompatTextView alreadyJoinTextView = null;
    //选择页面
    private RelativeLayout chooseRelativeLayout = null;
    private AppCompatImageView chooseImageView = null;
    private AppCompatTextView chooseNameTextView = null;
    private AppCompatTextView choosePriceTextView = null;
    private AppCompatTextView chooseStorageTextView = null;
    private AppCompatTextView chooseAddTextView = null;
    private AppCompatEditText chooseNumberEditText = null;
    private AppCompatTextView chooseSubTextView = null;
    private GoodsData data = null;
    //基础信息
    private String storeId = "";
    private String memberId = "";
    private String lowerLimit = "";
    private String upperLimit = "";
    private String goodsStorage = "";
    private String manSongGoodsId = "";
    //商品分享
    private String shareUrl = "";
    private String shareText = "";
    private String shareTitle = "";
    private String shareTitleUrl = "";
    private String shareImageUrl = "";
    //商品类型
    private boolean isAnim = false;
    private boolean isBack = false;
    private boolean isFCode = false;
    private boolean isVirtual = false;
    private boolean isPreSell = false;
    private boolean isFavorites = false;
    private boolean isPinGouSale = false;
    //商品数据
    private JSONObject mainJsonObject = null;
    private JSONObject contentJSONObject = null;
    private JSONObject storeJSONObject = null;
    private JSONObject evaluateJSONObject = null;
    private JSONObject hairJSONObject = null;
    private GoodsVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_goods);
        //主要控件
        mainSlideDetailsLayout = findViewById(R.id.mainSlideDetailsLayout);
        mainScrollView = findViewById(R.id.mainScrollView);
        mainWebView = findViewById(R.id.mainWebView);
        nightTextView = findViewById(R.id.nightTextView);
        //商品信息
        mediaRelativeLayout = findViewById(R.id.mediaRelativeLayout);
        mediaBanner = findViewById(R.id.mediaBanner);
        mediaVideoPlayer = findViewById(R.id.mediaVideoPlayer);
        mediaImageView = findViewById(R.id.mediaImageView);
        timeRelativeLayout = findViewById(R.id.timeRelativeLayout);
        timeTypeTextView = findViewById(R.id.timeTypeTextView);
        timeTextView = findViewById(R.id.timeTextView);
        pinGouRelativeLayout = findViewById(R.id.pinGouRelativeLayout);
        pinGouTextView = findViewById(R.id.pinGouTextView);
        pinGouPriceTextView = findViewById(R.id.pinGouPriceTextView);
        pinGouPriceMarketTextView = findViewById(R.id.pinGouPriceMarketTextView);
        pinGouTimeTextView = findViewById(R.id.pinGouTimeTextView);
        nameTextView = findViewById(R.id.nameTextView);
        descTextView = findViewById(R.id.descTextView);
        priceTextView = findViewById(R.id.priceTextView);
        priceMarketTextView = findViewById(R.id.priceMarketTextView);
        mobileImageView = findViewById(R.id.mobileImageView);
        saleTextView = findViewById(R.id.saleTextView);
        stepLinearLayout = findViewById(R.id.stepLinearLayout);
        stepsLinearLayout[0] = findViewById(R.id.step1LinearLayout);
        stepsLinearLayout[1] = findViewById(R.id.step2LinearLayout);
        stepsLinearLayout[2] = findViewById(R.id.step3LinearLayout);
        stepPriceTextView[0] = findViewById(R.id.step1PriceTextView);
        stepPriceTextView[1] = findViewById(R.id.step2PriceTextView);
        stepPriceTextView[2] = findViewById(R.id.step3PriceTextView);
        stepLimitTextView[0] = findViewById(R.id.step1LimitTextView);
        stepLimitTextView[1] = findViewById(R.id.step2LimitTextView);
        stepLimitTextView[2] = findViewById(R.id.step3LimitTextView);
        //商品活动
        saleTypeLinearLayout = findViewById(R.id.saleTypeLinearLayout);
        saleTypeTextView = findViewById(R.id.saleTypeTextView);
        saleTypeDescTextView = findViewById(R.id.saleTypeDescTextView);
        manSongLinearLayout = findViewById(R.id.manSongLinearLayout);
        manSongTextView = findViewById(R.id.manSongTextView);
        zengPinLinearLayout = findViewById(R.id.zengPinLinearLayout);
        zengPinTextView = findViewById(R.id.zengPinTextView);
        zengPinImageView = findViewById(R.id.zengPinImageView);
        voucherLinearLayout = findViewById(R.id.voucherLinearLayout);
        //属性地区
        specLinearLayout = findViewById(R.id.specLinearLayout);
        specTextView[0] = findViewById(R.id.specOneTextView);
        specTextView[1] = findViewById(R.id.specTwoTextView);
        specTextView[2] = findViewById(R.id.specThrTextView);
        specTextView[3] = findViewById(R.id.specFouTextView);
        specTextView[4] = findViewById(R.id.specFivTextView);
        specTextView[0].setVisibility(View.GONE);
        specTextView[1].setVisibility(View.GONE);
        specTextView[2].setVisibility(View.GONE);
        specTextView[3].setVisibility(View.GONE);
        specTextView[4].setVisibility(View.GONE);
        areaRelativeLayout = findViewById(R.id.areaRelativeLayout);
        areaAddressTextView = findViewById(R.id.areaAddressTextView);
        areaHaveTextView = findViewById(R.id.areaHaveTextView);
        areaLineView = findViewById(R.id.areaLineView);
        serviceDescTextView = findViewById(R.id.serviceDescTextView);
        serviceSevDayTextView = findViewById(R.id.serviceSevDayTextView);
        serviceQualityTextView = findViewById(R.id.serviceQualityTextView);
        serviceReissueTextView = findViewById(R.id.serviceReissueTextView);
        serviceLogisticsTextView = findViewById(R.id.serviceLogisticsTextView);
        //商品评价
        evaluateLinearLayout = findViewById(R.id.evaluateLinearLayout);
        evaluateDescTextView = findViewById(R.id.evaluateDescTextView);
        evaluateNumberTextView = findViewById(R.id.evaluateNumberTextView);
        evaluateRecyclerView = findViewById(R.id.evaluateRecyclerView);
        //店铺信息
        storeLinearLayout = findViewById(R.id.storeLinearLayout);
        storeAvatarImageView = findViewById(R.id.storeAvatarImageView);
        storeNameTextView = findViewById(R.id.storeNameTextView);
        storeOwnTextView = findViewById(R.id.storeOwnTextView);
        storeCollectTextView = findViewById(R.id.storeCollectTextView);
        storeDescTextView = findViewById(R.id.storeDescTextView);
        storeServiceTextView = findViewById(R.id.storeServiceTextView);
        storeDeliverTextView = findViewById(R.id.storeDeliverTextView);
        storeCustomerTextView = findViewById(R.id.storeCustomerTextView);
        storeInTextView = findViewById(R.id.storeInTextView);
        //商品推荐
        commendRecyclerView = findViewById(R.id.commendRecyclerView);
        //顶部标签
        topView = findViewById(R.id.topView);
        topToolbar = findViewById(R.id.topToolbar);
        topLineView = findViewById(R.id.topLineView);
        //底部按钮
        customerTextView = findViewById(R.id.customerTextView);
        favoriteTextView = findViewById(R.id.favoriteTextView);
        shareTextView = findViewById(R.id.shareTextView);
        addCartTextView = findViewById(R.id.addCartTextView);
        nowBuyTextView = findViewById(R.id.nowBuyTextView);
        signBuyTextView = findViewById(R.id.signBuyTextView);
        pinGouBuyTextView = findViewById(R.id.pinGouBuyTextView);
        alreadyJoinTextView = findViewById(R.id.alreadyJoinTextView);
        //选择页面
        chooseRelativeLayout = findViewById(R.id.chooseRelativeLayout);
        chooseImageView = findViewById(R.id.chooseImageView);
        chooseNameTextView = findViewById(R.id.chooseNameTextView);
        choosePriceTextView = findViewById(R.id.choosePriceTextView);
        chooseStorageTextView = findViewById(R.id.chooseStorageTextView);
        chooseLineView[0] = findViewById(R.id.chooseLineOneView);
        chooseLineView[1] = findViewById(R.id.chooseLineTwoView);
        chooseLineView[2] = findViewById(R.id.chooseLineThrView);
        chooseLineView[3] = findViewById(R.id.chooseLineFouView);
        chooseLineView[4] = findViewById(R.id.chooseLineFivView);
        chooseLineView[0].setVisibility(View.GONE);
        chooseLineView[1].setVisibility(View.GONE);
        chooseLineView[2].setVisibility(View.GONE);
        chooseLineView[3].setVisibility(View.GONE);
        chooseLineView[4].setVisibility(View.GONE);
        chooseValueTextView[0] = findViewById(R.id.chooseValueOneTextView);
        chooseValueTextView[1] = findViewById(R.id.chooseValueTwoTextView);
        chooseValueTextView[2] = findViewById(R.id.chooseValueThrTextView);
        chooseValueTextView[3] = findViewById(R.id.chooseValueFouTextView);
        chooseValueTextView[4] = findViewById(R.id.chooseValueFivTextView);
        chooseValueTextView[0].setVisibility(View.GONE);
        chooseValueTextView[1].setVisibility(View.GONE);
        chooseValueTextView[2].setVisibility(View.GONE);
        chooseValueTextView[3].setVisibility(View.GONE);
        chooseValueTextView[4].setVisibility(View.GONE);
        chooseValueRecyclerView[0] = findViewById(R.id.chooseValueOneRecyclerView);
        chooseValueRecyclerView[1] = findViewById(R.id.chooseValueTwoRecyclerView);
        chooseValueRecyclerView[2] = findViewById(R.id.chooseValueThrRecyclerView);
        chooseValueRecyclerView[3] = findViewById(R.id.chooseValueFouRecyclerView);
        chooseValueRecyclerView[4] = findViewById(R.id.chooseValueFivRecyclerView);
        chooseValueRecyclerView[0].setVisibility(View.GONE);
        chooseValueRecyclerView[1].setVisibility(View.GONE);
        chooseValueRecyclerView[2].setVisibility(View.GONE);
        chooseValueRecyclerView[3].setVisibility(View.GONE);
        chooseValueRecyclerView[4].setVisibility(View.GONE);
        chooseAddTextView = findViewById(R.id.chooseAddTextView);
        chooseNumberEditText = findViewById(R.id.chooseNumberEditText);
        chooseSubTextView = findViewById(R.id.chooseSubTextView);

    }

    @Override
    public void initData() {

        if (VerifyUtil.isEmpty(json)) {
            ToastHelp.get().showDataError();
            onReturn(false);
            return;
        }

        observeKeyborad(R.id.mainRelativeLayout);
        data = VerifyUtil.isEmpty(json) ? new GoodsData() : JsonUtil.json2Object(json, GoodsData.class);
        priceMarketTextView.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        App.get().setRecyclerView(evaluateRecyclerView, evaluateAdapter);
        evaluateRecyclerView.addItemDecoration(new LineDecoration(App.get().dp2Px(16), false));
        App.get().setRecyclerView(commendRecyclerView, commendAdapter);
        commendRecyclerView.setLayoutManager(new GridLayoutManager(get(), 4));
        commendRecyclerView.addItemDecoration(new GridDecoration(4, App.get().dp2Px(16), false));
        LinearLayoutCompat.LayoutParams layoutParams = (LinearLayoutCompat.LayoutParams) mediaRelativeLayout.getLayoutParams();
        layoutParams.height = App.get().getWidth();
        mediaRelativeLayout.setLayoutParams(layoutParams);
        layoutParams = (LinearLayoutCompat.LayoutParams) topView.getLayoutParams();
        layoutParams.height = App.get().getStatusBarHeight();
        topView.setLayoutParams(layoutParams);
        setToolbar(topToolbar, R.string.goodsDetailed);
        topToolbar.setAlpha(0);
        topView.setAlpha(0);
        topLineView.setAlpha(0);
        App.get().setWebView(mainWebView);
        vm = getVM(GoodsVM.class);
        vm.getDetailed(data.getGoodsId());

    }

    @Override
    public void initEvent() {

        mainSlideDetailsLayout.setOnSlideDetailsListener(status -> {
            switch (status) {
                case UPSTAIRS:
                    setToolbar(topToolbar, R.string.goodsDetailed);
                    break;
                case DOWNSTAIRS:
                    setToolbar(topToolbar, R.string.goodsIntroduce);
                    break;
            }
        });

        mainScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (nestedScrollView, i, i1, i2, i3) -> {
            float y = nestedScrollView.getScrollY();
            float header = App.get().getWidth() - App.get().getStatusBarHeight() - App.get().dp2Px(50);
            if (y > 0) {
                float alpha = (1 - Math.max((header - y) / header, 0f)) + 0.1f;
                topToolbar.setAlpha(alpha);
                topView.setAlpha(alpha);
                topLineView.setAlpha(alpha);
            } else {
                topToolbar.setAlpha(0);
                topView.setAlpha(0);
                topLineView.setAlpha(0);
            }
        });

        zengPinImageView.setOnClickListener(view -> {
            data.setGoodsId(manSongGoodsId);
            vm.getDetailed(data.getGoodsId());
        });

        voucherLinearLayout.setOnClickListener(view -> App.get().start(ARoutePath.STORE_VOUCHER, Constant.DATA_ID, storeId));

        specLinearLayout.setOnClickListener(view -> showChooseLayout());

        areaRelativeLayout.setOnClickListener(view -> App.get().start(get(), ARoutePath.MAIN_AREA, Constant.CODE_AREA));

        evaluateLinearLayout.setOnClickListener(view -> App.get().start(ARoutePath.GOODS_EVALUATE, Constant.DATA_ID, data.getGoodsId()));

        storeLinearLayout.setOnClickListener(view -> App.get().startStore(storeId));

        storeCustomerTextView.setOnClickListener(view -> App.get().startChat(ShopAISdk.get().getChatUrl(memberId, data.getGoodsId())));

        storeInTextView.setOnClickListener(view -> App.get().startStore(storeId));

        commendAdapter.setOnItemClickListener((position, bean) -> {
            data.setGoodsId(bean.getGoodsId());
            vm.getDetailed(data.getGoodsId());
        });

        nightTextView.setOnClickListener(view -> goneChooseLayout());

        chooseRelativeLayout.setOnClickListener(view -> LoggerHelp.get().show("click"));

        chooseSubTextView.setOnClickListener(view -> changeNumber(ConvertUtil.string2Int(Objects.requireNonNull(chooseNumberEditText.getText()).toString()) - 1));

        chooseAddTextView.setOnClickListener(view -> changeNumber(ConvertUtil.string2Int(Objects.requireNonNull(chooseNumberEditText.getText()).toString()) + 1));

        customerTextView.setOnClickListener(view -> App.get().startChat(ShopAISdk.get().getChatUrl(memberId, data.getGoodsId())));

        favoriteTextView.setOnClickListener(view -> {
            if (App.get().isMemberLogin()) {
                if (isFavorites) {
                    vm.favoritesDel(data.getGoodsId());
                } else {
                    vm.favoritesAdd(data.getGoodsId());
                }
                return;
            }
            App.get().startMemberLogin();
        });

        shareTextView.setOnClickListener(view -> {
            OnekeyShare onekeyShare = new OnekeyShare();
            onekeyShare.disableSSOWhenAuthorize();
            onekeyShare.setTitleUrl(shareTitleUrl);
            onekeyShare.setImageUrl(shareImageUrl);
            onekeyShare.setTitle(shareTitle);
            onekeyShare.setText(shareText);
            onekeyShare.setUrl(shareUrl);
            onekeyShare.show(get());
        });

        addCartTextView.setOnClickListener(view -> {
            if (App.get().isMemberLogin()) {
                if (goodsStorage.equals("0")) {
                    ToastHelp.get().show(R.string.noStock);
                    return;
                }
                if (chooseRelativeLayout.getVisibility() == View.GONE) {
                    showChooseLayout();
                } else {
                    vm.addCart(data.getGoodsId(), Objects.requireNonNull(chooseNumberEditText.getText()).toString());
                }
            }
            App.get().startMemberLogin();
        });

        nowBuyTextView.setOnClickListener(view -> buyGoods(""));

        signBuyTextView.setOnClickListener(view -> buyGoods(""));

        pinGouBuyTextView.setOnClickListener(view -> buyGoods("1"));

    }

    @Override
    public void initObserve() {

        vm.getGoodsLiveData().observe(this, string -> {
            mainJsonObject = JsonUtil.toJSONObject(string);
            contentJSONObject = JsonUtil.getJSONObject(mainJsonObject, "goods_content");
            storeJSONObject = JsonUtil.getJSONObject(mainJsonObject, "store_info");
            evaluateJSONObject = JsonUtil.getJSONObject(mainJsonObject, "goods_evaluate_info");
            hairJSONObject = JsonUtil.getJSONObject(mainJsonObject, "goods_hair_info");
            handlerInfo();
            handlerActivity();
            handlerAttr();
            handlerPinGou();
            handlerVirtual();
        });

        vm.getBodyLiveData().observe(this, string -> {
            string = string.replace("style=", "other=");
            App.get().loadHtml(mainWebView, string);
        });

        vm.getCalcLiveData().observe(this, bean -> {
            areaHaveTextView.setText(bean.getIfStoreCn());
            areaHaveTextView.append("，");
            areaHaveTextView.append(bean.getContent());
            goodsStorage = bean.isIfStore() ? "0" : goodsStorage;
        });

        vm.getFavAddLiveData().observe(this, string -> {
            isFavorites = true;
            favoriteTextView.setText(R.string.alreadyFavorite);
            favoriteTextView.setCompoundDrawablesWithIntrinsicBounds(null, App.get().getDrawables(R.drawable.ic_hint_favorite_full_accent), null, null);
        });

        vm.getFavDelLiveData().observe(this, string -> {
            isFavorites = false;
            favoriteTextView.setText(R.string.favorite);
            favoriteTextView.setCompoundDrawablesWithIntrinsicBounds(null, App.get().getDrawables(R.drawable.ic_hint_favorite_accent), null, null);
        });

        vm.getAddCartLiveData().observe(this, string -> {
            ToastHelp.get().showSuccess();
            goneChooseLayout();
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getDetailed(data.getGoodsId()));
            } else {
                ToastHelp.get().show(bean.getReason());
            }
        });

    }

    @Override
    public void onPause() {

        super.onPause();
        if (mediaVideoPlayer != null) mediaVideoPlayer.onVideoPause();

    }

    @Override
    public void onResume() {

        super.onResume();
        if (mediaVideoPlayer != null) mediaVideoPlayer.onVideoResume();

    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        GSYVideoManager.releaseAllVideos();

    }

    @Override
    public void onBackPressed() {

        if (mediaVideoPlayer != null) mediaVideoPlayer.setVideoAllCallBack(null);
        super.onBackPressed();

    }

    @Override
    public void onReturn(boolean handler) {

        if (nightTextView.getVisibility() == View.VISIBLE) {
            goneChooseLayout();
            return;
        }
        if (handler && isShowKeyboard()) {
            hideKeyboard();
            return;
        }
        finish();

    }

    @Override
    public void onActivityResult(int req, int res, @Nullable Intent intent) {

        super.onActivityResult(req, res, intent);
        if (intent != null && res == RESULT_OK && req == Constant.CODE_AREA) {
            areaAddressTextView.setText(intent.getStringExtra("area_info"));
            vm.calc(data.getGoodsId(), intent.getStringExtra("area_id"));
        }

    }

    //自定义方法

    private void refreshSpec() {

        for (int i = 0; i < specListArrayList.size(); i++) {
            String key = Objects.requireNonNull(specListArrayList.get(i).get("key"));
            if (key.contains(specArray[0]) && key.contains(specArray[1]) && key.contains(specArray[2]) && key.contains(specArray[3]) && key.contains(specArray[4])) {
                data.setGoodsId(specListArrayList.get(i).get("value"));
                break;
            }
        }
        vm.getDetailed(data.getGoodsId());

    }

    private void goneChooseLayout() {

        if (isAnim) {
            return;
        }
        if (nightTextView.getVisibility() == View.VISIBLE) {
            isAnim = true;
            AnimUtil.objectAnimator(nightTextView, AnimUtil.ALPHA, () -> {
                isAnim = false;
                nightTextView.setVisibility(View.GONE);
            }, 1.0f, 0);
        }
        if (chooseRelativeLayout.getVisibility() == View.VISIBLE) {
            isAnim = true;
            AnimUtil.objectAnimator(chooseRelativeLayout, AnimUtil.TRABSLATION_Y, () -> {
                isAnim = false;
                chooseRelativeLayout.setVisibility(View.GONE);
            }, 0, App.get().getHeight());
        }

    }

    private void showChooseLayout() {

        if (isAnim) {
            return;
        }
        if (nightTextView.getVisibility() == View.GONE) {
            isAnim = true;
            nightTextView.setVisibility(View.VISIBLE);
            AnimUtil.objectAnimator(nightTextView, AnimUtil.ALPHA, () -> isAnim = false, 0, 1.0f);
        }
        if (chooseRelativeLayout.getVisibility() == View.GONE) {
            isAnim = true;
            chooseRelativeLayout.setVisibility(View.VISIBLE);
            AnimUtil.objectAnimator(chooseRelativeLayout, AnimUtil.TRABSLATION_Y, () -> isAnim = false, App.get().getHeight(), 0);
        }

    }

    private void changeNumber(int number) {

        String temp;
        if (number <= 0) {
            chooseNumberEditText.setText("1");
            chooseNumberEditText.setSelection(1);
            return;
        }
        if (!VerifyUtil.isEmpty(upperLimit) && !upperLimit.equals("0")) {
            int upper = ConvertUtil.string2Int(upperLimit);
            if (number > upper) {
                temp = upper + "";
                chooseNumberEditText.setText(temp);
                chooseNumberEditText.setSelection(temp.length());
                ToastHelp.get().show(String.format(getString(R.string.goodsHighLimitPiece), upper + ""));
                return;
            }
        }
        if (!VerifyUtil.isEmpty(lowerLimit) && !lowerLimit.equals("0")) {
            int lower = ConvertUtil.string2Int(lowerLimit);
            if (number < lower) {
                temp = lower + "";
                chooseNumberEditText.setText(temp);
                chooseNumberEditText.setSelection(temp.length());
                ToastHelp.get().show(String.format(getString(R.string.goodsLowLimitPiece), lower + ""));
                return;
            }
        }
        if (!VerifyUtil.isEmpty(goodsStorage) && !goodsStorage.equals("0")) {
            int storage = ConvertUtil.string2Int(goodsStorage);
            if (number > storage) {
                temp = storage + "";
                chooseNumberEditText.setText(temp);
                chooseNumberEditText.setSelection(temp.length());
                return;
            }
        }
        temp = number + "";
        chooseNumberEditText.setText(temp);
        chooseNumberEditText.setSelection(temp.length());

    }

    private void buyGoods(String pinGou) {

        if (App.get().isMemberLogin()) {
            if (goodsStorage.equals("0")) {
                ToastHelp.get().show(R.string.noStock);
                return;
            }
            if (chooseRelativeLayout.getVisibility() == View.GONE) {
                showChooseLayout();
                return;
            }
            GoodsBuyData goodsBuyData = new GoodsBuyData();
            goodsBuyData.setCartId(data.getGoodsId() + "|" + Objects.requireNonNull(chooseNumberEditText.getText()).toString());
            goodsBuyData.setBuyerId(data.getBuyerId());
            goodsBuyData.setLogId(data.getLogId());
            goodsBuyData.setPingou(pinGou);
            App.get().startGoodsBuy(goodsBuyData);
            goneChooseLayout();
        }
        App.get().startMemberLogin();

    }

    //商品信息的

    private void handlerInfo() {

        String string;
        JSONArray jsonArray;
        JSONObject jsonObject;
        ArrayList<String> arrayList = new ArrayList<>();
        //视频图片
        mediaBanner.setVisibility(View.GONE);
        mediaVideoPlayer.setVisibility(View.GONE);
        mediaImageView.setVisibility(View.GONE);
        string = JsonUtil.getString(mainJsonObject, "video_path");
        if (!VerifyUtil.isEmpty(string)) {
            mediaVideoPlayer.setVisibility(View.VISIBLE);
            mediaVideoPlayer.getBackButton().setVisibility(View.GONE);
            mediaVideoPlayer.getTitleTextView().setVisibility(View.GONE);
            mediaVideoPlayer.getFullscreenButton().setVisibility(View.GONE);
            mediaVideoPlayer.setUp(string, true, "");
            mediaVideoPlayer.setVideoAllCallBack(new GSYSampleCallBack() {
                @Override
                public void onStartPrepared(String url, Object... objects) {
                    super.onStartPrepared(url, objects);
                    mediaImageView.setVisibility(View.GONE);
                }

                @Override
                public void onAutoComplete(String url, Object... objects) {
                    super.onAutoComplete(url, objects);
                    mediaImageView.setVisibility(View.VISIBLE);
                }
            });
            mediaVideoPlayer.startPlayLogic();
        }
        arrayList.clear();
        Collections.addAll(arrayList, JsonUtil.getString(mainJsonObject, "goods_image").split(","));
        App.get().setBanner(mediaBanner, 0, arrayList, position -> ImagePreview.getInstance().setContext(this).setIndex(position).setImageList(arrayList).start());
        mediaImageView.setOnClickListener(view -> ImagePreview.getInstance().setContext(this).setIndex(0).setImageList(arrayList).start());
        //基本信息
        string = JsonUtil.getString(contentJSONObject, "goods_name");
        nameTextView.setText(string);
        nameTextView.setVisibility(VerifyUtil.isEmpty(string) ? View.GONE : View.VISIBLE);
        string = JsonUtil.getString(contentJSONObject, "goods_jingle");
        descTextView.setText(string);
        descTextView.setVisibility(VerifyUtil.isEmpty(string) ? View.GONE : View.VISIBLE);
        string = "￥" + JsonUtil.getString(contentJSONObject, "goods_price");
        priceTextView.setText(string);
        string = "￥" + JsonUtil.getString(contentJSONObject, "goods_marketprice");
        priceMarketTextView.setText(string);
        saleTextView.setText(String.format(getString(R.string.goodsSaleNumber), JsonUtil.getString(contentJSONObject, "goods_salenum")));
        //批发价格
        stepLinearLayout.setVisibility(View.GONE);
        string = JsonUtil.getString(contentJSONObject, "step_prices");
        if (!string.equals("[]")) {
            stepLinearLayout.setVisibility(View.VISIBLE);
            stepsLinearLayout[0].setVisibility(View.INVISIBLE);
            stepsLinearLayout[1].setVisibility(View.INVISIBLE);
            stepsLinearLayout[2].setVisibility(View.INVISIBLE);
            jsonArray = JsonUtil.getJSONArray(contentJSONObject, "step_prices");
            for (int i = 0; i < Objects.requireNonNull(jsonArray).length(); i++) {
                jsonObject = JsonUtil.getJSONObject(jsonArray, i);
                stepsLinearLayout[i].setVisibility(View.VISIBLE);
                stepPriceTextView[i].setText("￥");
                stepPriceTextView[i].append(JsonUtil.getString(jsonObject, "step_price"));
                string = JsonUtil.getString(jsonObject, "step_h_num");
                if (string.equals("999999999")) {
                    stepLimitTextView[i].setText(String.format(getString(R.string.goodsStepLimitThan), JsonUtil.getString(jsonObject, "step_l_num")));
                } else {
                    stepLimitTextView[i].setText(String.format(getString(R.string.goodsStepLimitSection), JsonUtil.getString(jsonObject, "step_l_num"), string));
                }
            }
        }
        //送货区域
        areaAddressTextView.setText(JsonUtil.getString(hairJSONObject, "area_name"));
        areaHaveTextView.setText(JsonUtil.getString(hairJSONObject, "if_store_cn"));
        areaHaveTextView.append("，" + JsonUtil.getString(hairJSONObject, "content"));
        //服务信息
        serviceSevDayTextView.setVisibility(View.GONE);
        serviceQualityTextView.setVisibility(View.GONE);
        serviceReissueTextView.setVisibility(View.GONE);
        serviceLogisticsTextView.setVisibility(View.GONE);
        serviceDescTextView.setText(String.format(getString(R.string.goodsSaleAndSendWithService), JsonUtil.getString(storeJSONObject, "store_name")));
        if (JsonUtil.getString(contentJSONObject, "contractlist").contains("{")) {
            jsonObject = JsonUtil.getJSONObject(contentJSONObject, "contractlist");
            if (jsonObject.has("1")) {
                serviceSevDayTextView.setVisibility(View.VISIBLE);
                serviceSevDayTextView.setText(JsonUtil.getString(JsonUtil.getJSONObject(jsonObject, "1"), "cti_name"));
            }
            if (jsonObject.has("2")) {
                serviceQualityTextView.setVisibility(View.VISIBLE);
                serviceQualityTextView.setText(JsonUtil.getString(JsonUtil.getJSONObject(jsonObject, "2"), "cti_name"));
            }
            if (jsonObject.has("3")) {
                serviceReissueTextView.setVisibility(View.VISIBLE);
                serviceReissueTextView.setText(JsonUtil.getString(JsonUtil.getJSONObject(jsonObject, "3"), "cti_name"));
            }
            if (jsonObject.has("4")) {
                serviceLogisticsTextView.setVisibility(View.VISIBLE);
                serviceLogisticsTextView.setText(JsonUtil.getString(JsonUtil.getJSONObject(jsonObject, "4"), "cti_name"));
            }
        }
        //商品评价
        evaluateLinearLayout.setVisibility(View.GONE);
        if (!JsonUtil.getString(mainJsonObject, "goods_eval_list").equals("null") && !JsonUtil.getString(mainJsonObject, "goods_eval_list").equals("[]")) {
            evaluateLinearLayout.setVisibility(View.VISIBLE);
            evaluateDescTextView.setText(R.string.praiseRate);
            evaluateDescTextView.append(" " + JsonUtil.getString(evaluateJSONObject, "good_percent") + "%");
            evaluateNumberTextView.setText(JsonUtil.getString(evaluateJSONObject, "all"));
            evaluateNumberTextView.append(getString(R.string.peopleEvaluate));
            evaluateArrayList.clear();
            evaluateArrayList.addAll(Objects.requireNonNull(JsonUtil.json2ArrayList(JsonUtil.getString(mainJsonObject, "goods_eval_list"), EvaluateBean.class)));
            evaluateAdapter.setOnItemClickListener((position, bean) -> App.get().start(ARoutePath.GOODS_EVALUATE, Constant.DATA_ID, data.getGoodsId()));
            evaluateAdapter.notifyDataSetChanged();
        }
        //店铺信息
        storeId = JsonUtil.getString(storeJSONObject, "store_id");
        memberId = JsonUtil.getString(storeJSONObject, "member_id");
        ImageHelp.get().displayRadius(JsonUtil.getString(storeJSONObject, "store_avatar"), storeAvatarImageView);
        storeNameTextView.setText(JsonUtil.getString(storeJSONObject, "store_name"));
        storeOwnTextView.setVisibility(JsonUtil.getString(storeJSONObject, "is_own_shop").equals("1") ? View.VISIBLE : View.GONE);
        storeCollectTextView.setText(String.format(getString(R.string.storeGoodsAndCollect), JsonUtil.getString(storeJSONObject, "goods_count"), JsonUtil.getString(storeJSONObject, "store_collect")));
        jsonObject = JsonUtil.getJSONObject(storeJSONObject, "store_credit");
        jsonObject = JsonUtil.getJSONObject(jsonObject, "store_desccredit");
        string = JsonUtil.getString(jsonObject, "percent_text");
        string = string.equals("null") ? getString(R.string.flat) : string;
        storeDescTextView.setText(JsonUtil.getString(jsonObject, "text"));
        storeDescTextView.append("：" + JsonUtil.getString(jsonObject, "credit") + " " + string);
        jsonObject = JsonUtil.getJSONObject(storeJSONObject, "store_credit");
        jsonObject = JsonUtil.getJSONObject(jsonObject, "store_servicecredit");
        string = JsonUtil.getString(jsonObject, "percent_text");
        string = string.equals("null") ? getString(R.string.flat) : string;
        storeServiceTextView.setText(JsonUtil.getString(jsonObject, "text"));
        storeServiceTextView.append("：" + JsonUtil.getString(jsonObject, "credit") + " " + string);
        jsonObject = JsonUtil.getJSONObject(storeJSONObject, "store_credit");
        jsonObject = JsonUtil.getJSONObject(jsonObject, "store_deliverycredit");
        string = JsonUtil.getString(jsonObject, "percent_text");
        string = string.equals("null") ? getString(R.string.flat) : string;
        storeDeliverTextView.setText(JsonUtil.getString(jsonObject, "text"));
        storeDeliverTextView.append("：" + JsonUtil.getString(jsonObject, "credit") + " " + string);
        //商品推荐
        commendArrayList.clear();
        commendArrayList.addAll(Objects.requireNonNull(JsonUtil.json2ArrayList(JsonUtil.getString(mainJsonObject, "goods_commend_list"), GoodsCommendBean.class)));
        commendAdapter.notifyDataSetChanged();
        //是否收藏
        isFavorites = App.get().isMemberLogin() && JsonUtil.getBoolean(mainJsonObject, "is_favorate");
        if (isFavorites) {
            favoriteTextView.setCompoundDrawablesWithIntrinsicBounds(null, App.get().getDrawables(R.drawable.ic_hint_favorite_full_accent), null, null);
            favoriteTextView.setText(R.string.alreadyFavorite);
        } else {
            favoriteTextView.setCompoundDrawablesWithIntrinsicBounds(null, App.get().getDrawables(R.drawable.ic_hint_favorite_accent), null, null);
            favoriteTextView.setText(R.string.favorite);
        }
        //分享内容
        shareUrl = ShopAISdk.get().getGoodsUrl(data.getGoodsId());
        shareTitle = nameTextView.getText().toString();
        shareText = descTextView.getText().toString();
        shareImageUrl = arrayList.get(0);
        shareTitleUrl = shareUrl;
        //底部按钮
        addCartTextView.setVisibility(View.VISIBLE);
        nowBuyTextView.setVisibility(View.VISIBLE);
        signBuyTextView.setVisibility(View.GONE);
        pinGouBuyTextView.setVisibility(View.GONE);
        alreadyJoinTextView.setVisibility(View.GONE);
        if (JsonUtil.getInt(mainJsonObject, "IsHaveBuy") == 1) {
            addCartTextView.setVisibility(View.GONE);
            nowBuyTextView.setVisibility(View.GONE);
            alreadyJoinTextView.setVisibility(View.VISIBLE);
        }
        //选择页面
        goodsStorage = JsonUtil.getString(contentJSONObject, "goods_storage");
        ImageHelp.get().displayRadius(arrayList.get(0), chooseImageView);
        chooseNameTextView.setText(nameTextView.getText().toString());
        choosePriceTextView.setText(priceTextView.getText().toString());
        chooseStorageTextView.setText(R.string.stock);
        chooseStorageTextView.append("：");
        chooseStorageTextView.append(goodsStorage);
        if (!VerifyUtil.isEmpty(lowerLimit)) {
            chooseNumberEditText.setText(lowerLimit);
        }
        //商品详情
        vm.getBody(data.getGoodsId());

    }

    private void handlerActivity() {

        String string;
        JSONArray jsonArray;
        JSONObject jsonObject;
        mobileImageView.setVisibility(View.GONE);
        timeRelativeLayout.setVisibility(View.GONE);
        saleTypeLinearLayout.setVisibility(View.GONE);
        manSongLinearLayout.setVisibility(View.GONE);
        zengPinLinearLayout.setVisibility(View.GONE);
        voucherLinearLayout.setVisibility(View.GONE);
        //常规活动
        if (JsonUtil.has(contentJSONObject, "sale_type")) {
            saleTypeLinearLayout.setVisibility(View.VISIBLE);
            string = "￥" + JsonUtil.getString(contentJSONObject, "goods_price");
            priceMarketTextView.setText(string);
            string = "￥" + JsonUtil.getString(contentJSONObject, "sale_price");
            priceTextView.setText(string);
            saleTypeTextView.setText(JsonUtil.getString(contentJSONObject, "title"));
            switch (JsonUtil.getString(contentJSONObject, "sale_type")) {
                case "sole":
                    mobileImageView.setVisibility(View.VISIBLE);
                    string = getString(R.string.mobileVip) + getString(R.string.price) + string;
                    break;
                case "xianshi":
                    timeRelativeLayout.setVisibility(View.VISIBLE);
                    timeTypeTextView.setText(R.string.timeLimitSale);
                    lowerLimit = JsonUtil.getString(contentJSONObject, "lower_limit");
                    timeTextView.init("", JsonUtil.getLong(contentJSONObject, "xs_time"), getString(R.string.distanceEnds) + "：", "");
                    timeTextView.start(0);
                    string = String.format(getString(R.string.goodsDownPriceLimitLow), JsonUtil.getString(contentJSONObject, "down_price"), lowerLimit);
                    break;
                case "groupbuy":
                    upperLimit = JsonUtil.getString(contentJSONObject, "upper_limit");
                    string = String.format(getString(R.string.goodsDownPriceLimitLow), JsonUtil.getString(contentJSONObject, "down_price"), upperLimit);
                    break;
                case "robbuy":
                    timeRelativeLayout.setVisibility(View.VISIBLE);
                    timeTypeTextView.setText(R.string.timeLimitGroup);
                    upperLimit = JsonUtil.getString(contentJSONObject, "upper_limit");
                    timeTextView.init("", JsonUtil.getLong(contentJSONObject, "end_time"), getString(R.string.distanceEnds) + "：", "");
                    timeTextView.start(0);
                    string = String.format(getString(R.string.goodsLimitBuy), upperLimit, JsonUtil.getString(contentJSONObject, "remark"));
                    break;
            }
            saleTypeDescTextView.setText(string);
        }
        //满送活动
        string = JsonUtil.getString(mainJsonObject, "mansong_info");
        if (!string.equals("") && !string.equals("null") && !string.equals("[]")) {
            manSongLinearLayout.setVisibility(View.VISIBLE);
            jsonObject = JsonUtil.toJSONObject(string);
            jsonArray = JsonUtil.getJSONArray(jsonObject, "rules");
            jsonObject = JsonUtil.getJSONObject(Objects.requireNonNull(jsonArray), 0);
            manSongGoodsId = JsonUtil.getString(jsonObject, "goods_id");
            string = String.format(getString(R.string.goodsOrderDownPriceHaveGift), JsonUtil.getString(jsonObject, "price"), JsonUtil.getString(jsonObject, "discount"));
            manSongTextView.setText(string);
            if (JsonUtil.has(jsonObject, "goods_image_url")) {
                zengPinLinearLayout.setVisibility(View.VISIBLE);
                zengPinTextView.setText(JsonUtil.getString(jsonObject, "mansong_goods_name"));
                ImageHelp.get().display(JsonUtil.getString(jsonObject, "goods_image_url"), zengPinImageView);
            }
        }
        //店铺优惠券
        voucherLinearLayout.setVisibility(JsonUtil.has(mainJsonObject, "voucher") ? View.VISIBLE : View.GONE);

    }

    @SuppressWarnings("rawtypes")
    private void handlerAttr() {

        String string;
        JSONObject jsonObject;
        //specName
        specNameArrayList.clear();
        string = JsonUtil.getString(contentJSONObject, "spec_name");
        if (!VerifyUtil.isEmpty(string) && !string.equals("false") && !string.equals("null")) {
            jsonObject = JsonUtil.toJSONObject(string);
            Iterator iterator = jsonObject.keys();
            while (iterator.hasNext()) {
                HashMap<String, String> hashMap = new HashMap<>();
                String key = iterator.next().toString();
                String value = JsonUtil.getString(jsonObject, key);
                hashMap.put("id", key);
                hashMap.put("value", value);
                specNameArrayList.add(hashMap);
            }
            for (int i = 0; i < specNameArrayList.size(); i++) {
                if (i < 5) {
                    chooseLineView[i].setVisibility(View.VISIBLE);
                    chooseValueRecyclerView[i].setVisibility(View.VISIBLE);
                    chooseValueTextView[i].setVisibility(View.VISIBLE);
                    chooseValueTextView[i].setText(specNameArrayList.get(i).get("value"));
                }
            }
        } else {
            specTextView[0].setText(getString(R.string.defaults));
            specTextView[0].setVisibility(View.VISIBLE);
        }
        //specValue
        specValueArrayList.clear();
        string = JsonUtil.getString(contentJSONObject, "spec_value");
        if (!VerifyUtil.isEmpty(string) && !string.equals("false") && !string.equals("null")) {
            jsonObject = JsonUtil.toJSONObject(string);
            if (specNameArrayList.size() != 0) {
                for (int i = 0; i < specNameArrayList.size(); i++) {
                    String id = specNameArrayList.get(i).get("id");
                    String value = specNameArrayList.get(i).get("value");
                    JSONObject object = JsonUtil.toJSONObject(JsonUtil.getString(jsonObject, id));
                    Iterator iterator = object.keys();
                    while (iterator.hasNext()) {
                        HashMap<String, String> hashMap = new HashMap<>();
                        String key = iterator.next().toString();
                        hashMap.put("value", JsonUtil.getString(object, key));
                        hashMap.put("parent_value", value);
                        hashMap.put("parent_id", id);
                        hashMap.put("id", key);
                        specValueArrayList.add(hashMap);
                    }
                }
            }
        }
        //goodsSpec
        goodsSpecArrayList.clear();
        string = JsonUtil.getString(contentJSONObject, "goods_spec");
        if (!VerifyUtil.isEmpty(string) && !string.equals("false") && !string.equals("null")) {
            jsonObject = JsonUtil.toJSONObject(string);
            Iterator iterator = jsonObject.keys();
            while (iterator.hasNext()) {
                HashMap<String, String> hashMap = new HashMap<>();
                String key = iterator.next().toString();
                String value = JsonUtil.getString(jsonObject, key);
                for (int i = 0; i < specValueArrayList.size(); i++) {
                    String id = specValueArrayList.get(i).get("id");
                    if (key.equals(id)) {
                        String parent_value = specValueArrayList.get(i).get("parent_value");
                        hashMap.put("key", key);
                        hashMap.put("value", value);
                        hashMap.put("content", parent_value + "：" + value);
                    }
                }
                goodsSpecArrayList.add(hashMap);
            }
            for (int i = 0; i < goodsSpecArrayList.size(); i++) {
                if (i < 5) {
                    specTextView[i].setVisibility(View.VISIBLE);
                    specTextView[i].setText(goodsSpecArrayList.get(i).get("value"));
                    specArray[i] = goodsSpecArrayList.get(i).get("key");
                }
            }
        }
        //specList
        //noinspection unchecked
        ArrayList<HashMap<String, String>>[] specArrayList = new ArrayList[5];
        for (int i = 0; i < specNameArrayList.size(); i++) {
            if (i < 5) {
                specArrayList[i] = new ArrayList<>();
                String value = specNameArrayList.get(i).get("value");
                for (int j = 0; j < specValueArrayList.size(); j++) {
                    if (Objects.requireNonNull(value).equals(specValueArrayList.get(j).get("parent_value"))) {
                        HashMap<String, String> hashMap = new HashMap<>(specValueArrayList.get(j));
                        hashMap.put("default", "0");
                        for (int k = 0; k < goodsSpecArrayList.size(); k++) {
                            if (Objects.equals(goodsSpecArrayList.get(k).get("value"), hashMap.get("value"))) {
                                hashMap.put("default", "1");
                                break;
                            }
                        }
                        specArrayList[i].add(hashMap);
                    }
                }
            }
        }
        //specList
        specListArrayList.clear();
        GoodsSpecAdapter[] specAdapter = new GoodsSpecAdapter[5];
        string = JsonUtil.getString(mainJsonObject, "spec_list");
        specListArrayList.addAll(new ArrayList<>(JsonUtil.json2ArrayList(string)));
        for (int i = 0; i < specNameArrayList.size(); i++) {
            if (i < 5) {
                specArrayList[i] = new ArrayList<>();
                String value = specNameArrayList.get(i).get("value");
                for (int j = 0; j < specValueArrayList.size(); j++) {
                    if (Objects.equals(value, specValueArrayList.get(j).get("parent_value"))) {
                        HashMap<String, String> hashMap = new HashMap<>(specValueArrayList.get(j));
                        hashMap.put("default", "0");
                        for (int k = 0; k < goodsSpecArrayList.size(); k++) {
                            if (Objects.equals(goodsSpecArrayList.get(k).get("value"), hashMap.get("value"))) {
                                hashMap.put("default", "1");
                                break;
                            }
                        }
                        specArrayList[i].add(hashMap);
                    }
                }
            }
        }
        isBack = true;
        if (specArrayList[0] != null) {
            for (int i = 0; i < specArrayList[0].size(); i++) {
                String id = specArrayList[0].get(i).get("id");
                if (Objects.equals(id, specArray[0])) {
                    isBack = false;
                    break;
                }
            }
        }
        for (int i = 0; i < specArrayList.length; i++) {
            final int positionInt = i;
            specAdapter[i] = new GoodsSpecAdapter(specArrayList[i]);
            FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
            chooseValueRecyclerView[i].setLayoutManager(flowLayoutManager);
            chooseValueRecyclerView[i].setAdapter(specAdapter[i]);
            specAdapter[i].setOnItemClickListener((position, id, value) -> {
                if (isBack) {
                    if (positionInt == 1) {
                        specArray[positionInt - 1] = id;
                    } else {
                        specArray[positionInt + 1] = id;
                    }
                } else {
                    specArray[positionInt] = id;
                }
                refreshSpec();
            });
        }

    }

    private void handlerPinGou() {

        isPinGouSale = JsonUtil.getString(contentJSONObject, "pingou_sale").equals("1");
        if (isPinGouSale) {
            timeRelativeLayout.setVisibility(View.GONE);
            pinGouRelativeLayout.setVisibility(View.VISIBLE);
            pinGouTextView.setText(String.format(getString(R.string.goodsPinGouPeople), JsonUtil.getString(contentJSONObject, "pingou_min_num")));
            pinGouPriceTextView.setText("￥");
            pinGouPriceTextView.append(JsonUtil.getString(contentJSONObject, "pingou_price"));
            pinGouPriceMarketTextView.setText("￥");
            pinGouPriceMarketTextView.append(JsonUtil.getString(contentJSONObject, "goods_price"));
            pinGouPriceMarketTextView.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            pinGouTimeTextView.init("", JsonUtil.getLong(contentJSONObject, "pingou_end_time"), getString(R.string.distanceEnds) + "：", "");
            pinGouTimeTextView.start(0);
            signBuyTextView.setText(String.format(getString(R.string.goodsSingleBuy), JsonUtil.getString(contentJSONObject, "goods_price")));
            pinGouBuyTextView.setText(String.format(getString(R.string.goodsPinGouBuy), JsonUtil.getString(contentJSONObject, "pingou_price")));
            addCartTextView.setVisibility(View.GONE);
            nowBuyTextView.setVisibility(View.GONE);
            signBuyTextView.setVisibility(View.VISIBLE);
            pinGouBuyTextView.setVisibility(View.VISIBLE);
        } else {
            pinGouRelativeLayout.setVisibility(View.GONE);
        }

    }

    private void handlerVirtual() {

        isVirtual = !JsonUtil.getString(contentJSONObject, "is_virtual").equals("0");
        areaRelativeLayout.setVisibility(isVirtual ? View.GONE : View.VISIBLE);
        areaLineView.setVisibility(isVirtual ? View.GONE : View.VISIBLE);
        addCartTextView.setVisibility(isVirtual ? View.GONE : addCartTextView.getVisibility());

    }

}
