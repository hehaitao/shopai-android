package top.yokey.shopai.goods.activity;

import android.content.Intent;
import android.view.View;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.goods.adapter.GoodsCateBrandAdapter;
import top.yokey.shopai.goods.adapter.GoodsCateOneAdapter;
import top.yokey.shopai.goods.adapter.GoodsCateTwoAdapter;
import top.yokey.shopai.goods.viewmodel.GoodsCateVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.BrandListBean;
import top.yokey.shopai.zsdk.bean.GoodsClassBean;

@Route(path = ARoutePath.GOODS_CATE)
public class GoodsCateActivity extends BaseActivity {

    private final ArrayList<GoodsClassBean> oneArrayList = new ArrayList<>();
    private final GoodsCateOneAdapter oneAdapter = new GoodsCateOneAdapter(oneArrayList);
    private final ArrayList<BrandListBean> brandArrayList = new ArrayList<>();
    private final GoodsCateBrandAdapter brandAdapter = new GoodsCateBrandAdapter(brandArrayList);
    private final ArrayList<GoodsClassBean.GcListBean> classArrayList = new ArrayList<>();
    private final GoodsCateTwoAdapter classAdapter = new GoodsCateTwoAdapter(classArrayList);
    private Toolbar mainToolbar = null;
    private PullRefreshView onePullRefreshView = null;
    private PullRefreshView brandPullRefreshView = null;
    private PullRefreshView classPullRefreshView = null;
    private GoodsCateVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_goods_cate);
        mainToolbar = findViewById(R.id.mainToolbar);
        onePullRefreshView = findViewById(R.id.onePullRefreshView);
        brandPullRefreshView = findViewById(R.id.brandPullRefreshView);
        classPullRefreshView = findViewById(R.id.classPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.cate);
        observeKeyborad(R.id.mainLinearLayout);
        onePullRefreshView.setAdapter(oneAdapter);
        onePullRefreshView.setCanRefresh(false);
        onePullRefreshView.setCanLoadMore(false);
        onePullRefreshView.setLoad();
        brandPullRefreshView.setLayoutManager(new GridLayoutManager(get(), 3));
        brandPullRefreshView.setAdapter(brandAdapter);
        brandPullRefreshView.setCanRefresh(false);
        brandPullRefreshView.setCanLoadMore(false);
        brandPullRefreshView.setLoad();
        brandPullRefreshView.setVisibility(View.VISIBLE);
        classPullRefreshView.setAdapter(classAdapter);
        classPullRefreshView.setCanRefresh(false);
        classPullRefreshView.setCanLoadMore(false);
        classPullRefreshView.setLoad();
        classPullRefreshView.setVisibility(View.GONE);
        vm = getVM(GoodsCateVM.class);
        vm.getGoodsClass();
        vm.getBrand();

    }

    @Override
    public void initEvent() {

        onePullRefreshView.setOnClickListener(view -> {
            if (onePullRefreshView.isError() || oneArrayList.size() == 0) {
                vm.getGoodsClass();
            }
        });

        oneAdapter.setOnItemClickListener((position, bean) -> {
            for (int i = 0; i < oneArrayList.size(); i++) {
                oneArrayList.get(i).setSelect(false);
            }
            oneArrayList.get(position).setSelect(true);
            oneAdapter.notifyDataSetChanged();
            if (position == 0) {
                brandPullRefreshView.setVisibility(View.VISIBLE);
                classPullRefreshView.setVisibility(View.GONE);
                return;
            }
            classArrayList.clear();
            classArrayList.addAll(oneArrayList.get(position).getGcList());
            classPullRefreshView.setComplete();
            brandPullRefreshView.setVisibility(View.GONE);
            classPullRefreshView.setVisibility(View.VISIBLE);
        });

        brandAdapter.setOnItemClickListener((position, bean) -> {
            Intent intent = new Intent();
            intent.putExtra(Constant.DATA_BID, bean.getBrandId());
            intent.putExtra(Constant.DATA_GCID, "");
            App.get().finishOk(get(), intent);
        });

        classAdapter.setOnItemClickListener(new GoodsCateTwoAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position, GoodsClassBean.GcListBean bean) {
                Intent intent = new Intent();
                intent.putExtra(Constant.DATA_BID, "");
                intent.putExtra(Constant.DATA_GCID, bean.getGcId());
                App.get().finishOk(get(), intent);
            }

            @Override
            public void onClickItem(int position, int positionItem, GoodsClassBean.GcListBean.ChildBean bean) {
                Intent intent = new Intent();
                intent.putExtra(Constant.DATA_BID, "");
                intent.putExtra(Constant.DATA_GCID, bean.getGcId());
                App.get().finishOk(get(), intent);
            }
        });

    }

    @Override
    public void initObserve() {

        vm.getBrandLiveData().observe(this, list -> {
            brandArrayList.clear();
            brandArrayList.addAll(list);
            brandPullRefreshView.setComplete();
        });

        vm.getGoodsClassLiveData().observe(this, list -> {
            oneArrayList.clear();
            GoodsClassBean goodsClassBean = new GoodsClassBean();
            goodsClassBean.setGcName(getString(R.string.recommendBrand));
            oneArrayList.add(goodsClassBean);
            oneArrayList.addAll(list);
            oneArrayList.get(0).setSelect(true);
            onePullRefreshView.setComplete();
        });

        vm.getErrorLiveData().observe(this, bean -> {
            switch (bean.getCode()) {
                case 1:
                    DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getBrand());
                    break;
                case 2:
                    DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getGoodsClass());
                    break;
            }
        });

    }

}
