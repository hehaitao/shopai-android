package top.yokey.shopai.goods.adapter;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zsdk.bean.BuyStoreBean;

public class GoodsBuyStoreAdapter extends RecyclerView.Adapter<GoodsBuyStoreAdapter.ViewHolder> {

    private final ArrayList<BuyStoreBean> arrayList;

    public GoodsBuyStoreAdapter(ArrayList<BuyStoreBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        BuyStoreBean bean = arrayList.get(position);
        GoodsBuyAdapter adapter = new GoodsBuyAdapter(bean.getGoodsList());
        App.get().setRecyclerView(holder.mainRecyclerView, adapter);
        holder.nameTextView.setText(bean.getStoreName());
        holder.logisticsTextView.setText("￥");
        holder.logisticsTextView.append(bean.getLogistics());
        holder.totalTextView.setText("￥");
        holder.totalTextView.append(bean.getTotal());
        holder.voucherLineView.setVisibility(View.GONE);
        holder.voucherLinearLayout.setVisibility(View.GONE);
        if (bean.getStoreVoucherInfo() != null) {
            holder.voucherLineView.setVisibility(View.VISIBLE);
            holder.voucherLinearLayout.setVisibility(View.VISIBLE);
            holder.voucherTextView.setText(String.format(App.get().getString(R.string.goodsBuySave), bean.getStoreVoucherInfo().getVoucherPrice()));
        }
        if (bean.getStoreMansongRuleList() == null) {
            holder.manSongLinearLayout.setVisibility(View.GONE);
            holder.zengPinLinearLayout.setVisibility(View.GONE);
        } else {
            holder.manSongLinearLayout.setVisibility(View.VISIBLE);
            holder.manSongTextView.setText(bean.getStoreMansongRuleList().getDesc().getDesc());
            if (bean.getStoreMansongRuleList().getDesc().getGoodsName() == null || bean.getStoreMansongRuleList().getDesc().getUrl() == null) {
                holder.zengPinLinearLayout.setVisibility(View.GONE);
            } else {
                holder.zengPinLinearLayout.setVisibility(View.VISIBLE);
                holder.zengPinTextView.setText(bean.getStoreMansongRuleList().getDesc().getGoodsName());
                ImageHelp.get().displayRadius(bean.getStoreMansongRuleList().getDesc().getUrl(), holder.zengPinImageView);
            }
        }

        holder.messageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                arrayList.get(position).setMessage(Objects.requireNonNull(holder.messageEditText.getText()).toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_goods_buy_store, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final AppCompatTextView nameTextView;
        private final LinearLayoutCompat manSongLinearLayout;
        private final AppCompatTextView manSongTextView;
        private final LinearLayoutCompat zengPinLinearLayout;
        private final AppCompatTextView zengPinTextView;
        private final AppCompatImageView zengPinImageView;
        private final RecyclerView mainRecyclerView;
        private final View voucherLineView;
        private final LinearLayoutCompat voucherLinearLayout;
        private final AppCompatTextView voucherTextView;
        private final AppCompatTextView logisticsTextView;
        private final AppCompatTextView totalTextView;
        private final AppCompatEditText messageEditText;

        private ViewHolder(View view) {

            super(view);
            nameTextView = view.findViewById(R.id.nameTextView);
            manSongLinearLayout = view.findViewById(R.id.manSongLinearLayout);
            manSongTextView = view.findViewById(R.id.manSongTextView);
            zengPinLinearLayout = view.findViewById(R.id.zengPinLinearLayout);
            zengPinTextView = view.findViewById(R.id.zengPinTextView);
            zengPinImageView = view.findViewById(R.id.zengPinImageView);
            mainRecyclerView = view.findViewById(R.id.mainRecyclerView);
            voucherLineView = view.findViewById(R.id.voucherLineView);
            voucherLinearLayout = view.findViewById(R.id.voucherLinearLayout);
            voucherTextView = view.findViewById(R.id.voucherTextView);
            logisticsTextView = view.findViewById(R.id.logisticsTextView);
            totalTextView = view.findViewById(R.id.totalTextView);
            messageEditText = view.findViewById(R.id.messageEditText);

        }

    }

}
