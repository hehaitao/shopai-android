package top.yokey.shopai.goods.adapter;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zsdk.bean.GoodsPinGouListBean;

public class GoodsPinGouAdapter extends RecyclerView.Adapter<GoodsPinGouAdapter.ViewHolder> {

    private final ArrayList<GoodsPinGouListBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public GoodsPinGouAdapter(ArrayList<GoodsPinGouListBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        GoodsPinGouListBean bean = arrayList.get(position);
        ImageHelp.get().displayRadius(bean.getGoodsImage(), holder.mainImageView);
        holder.nameTextView.setText(bean.getGoodsName());
        holder.saleTextView.setText(String.format(App.get().getString(R.string.goodsSaleNumber), bean.getGoods().getGoodsSalenum()));
        holder.saleTextView.append("，");
        holder.priceMarketTextView.setText("￥");
        holder.priceMarketTextView.append(bean.getGoodsPrice());
        holder.priceMarketTextView.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.priceTextView.setText("￥");
        holder.priceTextView.append(bean.getPingouPrice());
        holder.priceTextView.append("，");
        holder.priceTextView.append(String.format(App.get().getString(R.string.goodsPinGouPeople), bean.getMinNum() + ""));

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_goods_pin_gou, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, GoodsPinGouListBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatImageView mainImageView;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView saleTextView;
        private final AppCompatTextView priceMarketTextView;
        private final AppCompatTextView priceTextView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainImageView = view.findViewById(R.id.mainImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            saleTextView = view.findViewById(R.id.saleTextView);
            priceMarketTextView = view.findViewById(R.id.priceMarketTextView);
            priceTextView = view.findViewById(R.id.priceTextView);

        }

    }

}
