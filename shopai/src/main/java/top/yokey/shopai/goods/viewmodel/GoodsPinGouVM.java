package top.yokey.shopai.goods.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.GoodsClassBean;
import top.yokey.shopai.zsdk.bean.GoodsPinGouListBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.GoodsClassController;
import top.yokey.shopai.zsdk.controller.PinGouController;

public class GoodsPinGouVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<GoodsClassBean>> cateLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<GoodsPinGouListBean>> goodsLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();

    public GoodsPinGouVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<GoodsClassBean>> getCateLiveData() {

        return cateLiveData;

    }

    public MutableLiveData<ArrayList<GoodsPinGouListBean>> getGoodsLiveData() {

        return goodsLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public void getCate() {

        GoodsClassController.getChildAllList(new HttpCallBack<ArrayList<GoodsClassBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<GoodsClassBean> list) {
                cateLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getGoods(String gcId, String page) {

        PinGouController.pingouList(gcId, page, new HttpCallBack<ArrayList<GoodsPinGouListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<GoodsPinGouListBean> list) {
                goodsLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
