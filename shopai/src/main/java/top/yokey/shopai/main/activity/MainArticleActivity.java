package top.yokey.shopai.main.activity;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;
import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.main.adapter.MainArticleClassAdapter;
import top.yokey.shopai.main.adapter.MainArticleListAdapter;
import top.yokey.shopai.main.viewmodel.MainArticleVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.ArticleClassBean;
import top.yokey.shopai.zsdk.bean.ArticleListBean;

@Route(path = ARoutePath.MAIN_ARTICLE)
public class MainArticleActivity extends BaseActivity {

    private final ArrayList<ArticleClassBean> classArrayList = new ArrayList<>();
    private final MainArticleClassAdapter classAdapter = new MainArticleClassAdapter(classArrayList);
    private final ArrayList<ArticleListBean> listArrayList = new ArrayList<>();
    private final MainArticleListAdapter listAdapter = new MainArticleListAdapter(listArrayList);
    private Toolbar mainToolbar = null;
    private AppCompatEditText toolbarEditText = null;
    private AppCompatImageView toolbarImageView = null;
    private RecyclerView mainRecyclerView = null;
    private PullRefreshView mainPullRefreshView = null;
    private int position = 0;
    private MainArticleVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_main_article);
        mainToolbar = findViewById(R.id.mainToolbar);
        toolbarEditText = findViewById(R.id.toolbarEditText);
        toolbarImageView = findViewById(R.id.toolbarImageView);
        mainRecyclerView = findViewById(R.id.mainRecyclerView);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar);
        observeKeyborad(R.id.mainLinearLayout);
        App.get().setRecyclerView(mainRecyclerView, classAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(get());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mainRecyclerView.setLayoutManager(linearLayoutManager);
        mainPullRefreshView.setItemDecoration(new LineDecoration(1, false));
        mainPullRefreshView.setAdapter(listAdapter);
        mainPullRefreshView.setCanLoadMore(false);
        mainPullRefreshView.setLoad();
        toolbarEditText.setHint(R.string.pleaseInputKeyword);
        vm = getVM(MainArticleVM.class);
        vm.getArticleClass();

    }

    @Override
    public void initEvent() {

        toolbarImageView.setOnClickListener(view -> getArticle());

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || listArrayList.size() == 0) {
                getArticle();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getArticle();
            }

            @Override
            public void onLoadMore() {

            }
        });

        classAdapter.setOnItemClickListener((position, bean) -> {
            MainArticleActivity.this.position = position;
            getArticle();
        });

        listAdapter.setOnItemClickListener((position, bean) -> App.get().startArticle(bean.getArticleId()));

    }

    @Override
    public void initObserve() {

        vm.getClassLiveData().observe(this, list -> {
            classArrayList.clear();
            classArrayList.addAll(list);
            classAdapter.notifyDataSetChanged();
            position = 0;
            getArticle();
        });

        vm.getArticleLiveData().observe(this, list -> {
            listArrayList.clear();
            listArrayList.addAll(list);
            listAdapter.notifyDataSetChanged();
            mainPullRefreshView.setComplete();
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.confirmSelection, bean.getReason(), view -> onReturn(false), view -> vm.getArticleClass());
                return;
            }
            if (bean.getCode() == 2) {
                mainPullRefreshView.setError(bean.getReason());
            }
        });

    }

    //自定义方法

    private void getArticle() {

        mainPullRefreshView.setLoad();
        for (int i = 0; i < classArrayList.size(); i++) {
            classArrayList.get(i).setSelect(false);
        }
        classArrayList.get(position).setSelect(true);
        classAdapter.notifyDataSetChanged();
        vm.getArticle(classArrayList.get(position).getAcId(), Objects.requireNonNull(toolbarEditText.getText()).toString());

    }

}
