package top.yokey.shopai.main.activity;

import android.webkit.WebView;

import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;

import top.yokey.shopai.R;
import top.yokey.shopai.main.viewmodel.MainArticleVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.other.Constant;

@Route(path = ARoutePath.MAIN_ARTICLE_DETAIL)
public class MainArticleDetailActivity extends BaseActivity {

    @Autowired(name = Constant.DATA_ID)
    String id = "";
    private Toolbar mainToolbar = null;
    private WebView mainWebView = null;
    private MainArticleVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_main_article_detail);
        mainToolbar = findViewById(R.id.mainToolbar);
        mainWebView = findViewById(R.id.mainWebView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.loadIng);
        vm = getVM(MainArticleVM.class);
        vm.getArticleDetail(id);

    }

    @Override
    public void initEvent() {

    }

    @Override
    public void initObserve() {

        vm.getArticleDetailLiveData().observe(this, bean -> {
            setToolbar(mainToolbar, bean.getArticleTitle());
            App.get().loadHtml(mainWebView, bean.getArticleContent());
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.confirmSelection, bean.getReason(), view -> onReturn(false), view -> vm.getArticleDetail(id));
            }
        });

    }

}
