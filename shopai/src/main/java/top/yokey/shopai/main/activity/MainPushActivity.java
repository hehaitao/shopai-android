package top.yokey.shopai.main.activity;

import android.net.Uri;
import android.os.Bundle;

import java.util.Objects;

import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;

public class MainPushActivity extends BaseActivity {

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    public void initEvent() {

    }

    @Override
    public void initObserve() {

    }

    @Override
    public void onCreate(Bundle bundle) {

        super.onCreate(bundle);
        Uri uri = getIntent().getData();
        if (uri != null) {
            String type = uri.getQueryParameter("type");
            String value = uri.getQueryParameter("value");
            App.get().start(ARoutePath.MAIN_MAIN);
            App.get().startTypeValue(Objects.requireNonNull(type), value);
            onReturn(false);
        }

    }

}
