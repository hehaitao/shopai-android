package top.yokey.shopai.main.activity;

import android.view.View;
import android.view.inputmethod.EditorInfo;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.main.adapter.MainSearchKeyAdapter;
import top.yokey.shopai.main.viewmodel.MainSearchVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.SharedHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.other.CountDown;
import top.yokey.shopai.zcom.recycler.FlowLayoutManager;
import top.yokey.shopai.zcom.recycler.SpaceDecoration;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zcom.voice.XFVoiceDialog;
import top.yokey.shopai.zsdk.data.GoodsSearchData;

@Route(path = ARoutePath.MAIN_SEARCH)
public class MainSearchActivity extends BaseActivity {

    private final GoodsSearchData data = new GoodsSearchData();
    private final ArrayList<String> arrayList = new ArrayList<>();
    private final MainSearchKeyAdapter adapter = new MainSearchKeyAdapter(arrayList);
    private final ArrayList<String> hotArrayList = new ArrayList<>();
    private final MainSearchKeyAdapter hotAdapter = new MainSearchKeyAdapter(hotArrayList);
    private Toolbar mainToolbar = null;
    private AppCompatEditText toolbarEditText = null;
    private AppCompatImageView toolbarImageView = null;
    private AppCompatImageView clearImageView = null;
    private AppCompatTextView recentTextView = null;
    private RecyclerView recentRecyclerView = null;
    private AppCompatTextView hotTextView = null;
    private RecyclerView hotRecyclerView = null;
    private FloatingActionButton voiceButton = null;
    private MainSearchVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_main_search);
        mainToolbar = findViewById(R.id.mainToolbar);
        toolbarEditText = findViewById(R.id.toolbarEditText);
        toolbarImageView = findViewById(R.id.toolbarImageView);
        clearImageView = findViewById(R.id.clearImageView);
        recentTextView = findViewById(R.id.recentTextView);
        recentRecyclerView = findViewById(R.id.recentRecyclerView);
        hotTextView = findViewById(R.id.hotTextView);
        hotRecyclerView = findViewById(R.id.hotRecyclerView);
        voiceButton = findViewById(R.id.voiceButton);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar);
        observeKeyborad(R.id.mainLinearLayout);
        toolbarEditText.setHint(R.string.tipsSearchHint);
        App.get().setFocus(toolbarEditText);
        App.get().setRecyclerView(recentRecyclerView, adapter);
        recentRecyclerView.setLayoutManager(new FlowLayoutManager());
        recentRecyclerView.addItemDecoration(new SpaceDecoration(App.get().dp2Px(4)));
        App.get().setRecyclerView(hotRecyclerView, hotAdapter);
        hotRecyclerView.setLayoutManager(new FlowLayoutManager());
        hotRecyclerView.addItemDecoration(new SpaceDecoration(App.get().dp2Px(4)));
        vm = getVM(MainSearchVM.class);
        vm.getSearch();

    }

    @Override
    public void initEvent() {

        toolbarEditText.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                data.setKeyword(Objects.requireNonNull(toolbarEditText.getText()).toString());
                gotoList();
            }
            return false;
        });

        toolbarImageView.setOnClickListener(view -> {
            data.setKeyword(Objects.requireNonNull(toolbarEditText.getText()).toString());
            gotoList();
        });

        clearImageView.setOnClickListener(view -> {
            SharedHelp.get().putString(Constant.SHARED_SEARCH_KEYWORD, "");
            recentTextView.setVisibility(View.GONE);
            clearImageView.setVisibility(View.GONE);
            recentRecyclerView.setVisibility(View.GONE);
        });

        adapter.setOnItemClickListener((position, bean) -> {
            data.setKeyword(bean);
            gotoList();
        });

        hotAdapter.setOnItemClickListener((position, bean) -> {
            data.setKeyword(bean);
            gotoList();
        });

        voiceButton.setOnClickListener(view -> XFVoiceDialog.get().show(get(), result -> {
            if (!VerifyUtil.isEmpty(result)) {
                data.setKeyword(result);
                gotoList();
            }
        }));

    }

    @Override
    public void initObserve() {

        vm.getSearchLiveData().observe(this, bean -> {
            hotArrayList.clear();
            hotArrayList.addAll(bean.getList());
            hotAdapter.notifyDataSetChanged();
            if (hotArrayList.size() == 0) {
                hotTextView.setVisibility(View.GONE);
                hotRecyclerView.setVisibility(View.GONE);
            } else {
                hotTextView.setVisibility(View.VISIBLE);
                hotRecyclerView.setVisibility(View.VISIBLE);
            }
        });

        vm.getErrorLiveData().observe(this, bean -> {
            hotTextView.setVisibility(View.GONE);
            hotRecyclerView.setVisibility(View.GONE);
        });

    }

    @Override
    public void onResume() {

        super.onResume();
        arrayList.clear();
        String searchKeyword = SharedHelp.get().getString(Constant.SHARED_SEARCH_KEYWORD);
        clearImageView.setVisibility(VerifyUtil.isEmpty(searchKeyword) ? View.GONE : View.VISIBLE);
        recentTextView.setVisibility(VerifyUtil.isEmpty(searchKeyword) ? View.GONE : View.VISIBLE);
        recentRecyclerView.setVisibility(VerifyUtil.isEmpty(searchKeyword) ? View.GONE : View.VISIBLE);
        if (!VerifyUtil.isEmpty(searchKeyword)) {
            String[] keyArray = searchKeyword.split(",");
            Collections.addAll(arrayList, keyArray);
            adapter.notifyDataSetChanged();
        }

    }

    //自定义方法

    private void gotoList() {

        hideKeyboard();
        String keyword = data.getKeyword();
        if (!VerifyUtil.isEmpty(keyword)) {
            String searchKey = SharedHelp.get().getString(Constant.SHARED_SEARCH_KEYWORD);
            if (VerifyUtil.isEmpty(searchKey)) {
                searchKey = keyword;
            } else if (!searchKey.contains(",")) {
                if (!searchKey.equals(keyword)) {
                    searchKey += "," + keyword;
                }
            } else {
                if (!searchKey.contains("," + keyword + ",") && !searchKey.contains("," + keyword) && !searchKey.contains(keyword + ",")) {
                    searchKey += "," + keyword;
                }
            }
            SharedHelp.get().putString(Constant.SHARED_SEARCH_KEYWORD, searchKey);
        }
        App.get().startGoodsList(data);
        new CountDown(500, 500) {
            @Override
            public void onFinish() {
                super.onFinish();
                onReturn(false);
            }
        }.start();

    }

}
