package top.yokey.shopai.main.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zsdk.bean.AreaListBean;

public class MainAreaAdapter extends RecyclerView.Adapter<MainAreaAdapter.ViewHolder> {

    private final ArrayList<AreaListBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public MainAreaAdapter(ArrayList<AreaListBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        AreaListBean bean = arrayList.get(position);
        holder.mainTextView.setText(bean.getAreaName());
        if (bean.isSelect()) {
            holder.mainTextView.setTextColor(App.get().getColors(R.color.accent));
            holder.mainTextView.setBackgroundResource(R.color.primaryDark);
        } else {
            holder.mainTextView.setTextColor(App.get().getColors(R.color.text));
            holder.mainTextView.setBackgroundResource(R.drawable.selector_primary);
        }

        holder.mainTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_main_area, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, AreaListBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final AppCompatTextView mainTextView;

        private ViewHolder(View view) {

            super(view);
            mainTextView = view.findViewById(R.id.mainTextView);

        }

    }

}
