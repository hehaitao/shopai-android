package top.yokey.shopai.main.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zsdk.bean.ArticleClassBean;

public class MainArticleClassAdapter extends RecyclerView.Adapter<MainArticleClassAdapter.ViewHolder> {

    private final ArrayList<ArticleClassBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public MainArticleClassAdapter(ArrayList<ArticleClassBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        ArticleClassBean bean = arrayList.get(position);
        holder.mainTextView.setText(bean.getAcName());
        holder.mainTextView.setTextColor(bean.isSelect() ? App.get().getColors(R.color.accent) : App.get().getColors(R.color.textTwo));
        holder.mainTextView.setBackgroundResource(bean.isSelect() ? R.drawable.selector_primary_16dp : R.color.transparent);

        holder.mainTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_main_article_class, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, ArticleClassBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final AppCompatTextView mainTextView;

        private ViewHolder(View view) {

            super(view);
            mainTextView = view.findViewById(R.id.mainTextView);

        }

    }

}
