package top.yokey.shopai.main.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zsdk.bean.GoodsClassBean;

public class MainCateOneAdapter extends RecyclerView.Adapter<MainCateOneAdapter.ViewHolder> {

    private final ArrayList<GoodsClassBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public MainCateOneAdapter(ArrayList<GoodsClassBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        GoodsClassBean bean = arrayList.get(position);
        holder.mainTextView.setText(bean.getGcName());
        if (bean.isSelect()) {
            holder.lineView.setVisibility(View.VISIBLE);
            holder.mainTextView.setTextColor(App.get().getColors(R.color.accent));
        } else {
            holder.lineView.setVisibility(View.GONE);
            holder.mainTextView.setTextColor(App.get().getColors(R.color.textTwo));
        }

        holder.mainTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

        holder.mainLinearLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_main_cate_one, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, GoodsClassBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayoutCompat mainLinearLayout;
        private final View lineView;
        private final AppCompatTextView mainTextView;

        private ViewHolder(View view) {

            super(view);
            mainLinearLayout = view.findViewById(R.id.mainLinearLayout);
            lineView = view.findViewById(R.id.lineView);
            mainTextView = view.findViewById(R.id.mainTextView);

        }

    }

}
