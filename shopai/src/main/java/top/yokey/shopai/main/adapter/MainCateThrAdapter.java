package top.yokey.shopai.main.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zsdk.bean.GoodsClassBean;

class MainCateThrAdapter extends RecyclerView.Adapter<MainCateThrAdapter.ViewHolder> {

    private final ArrayList<GoodsClassBean.GcListBean.ChildBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    MainCateThrAdapter(ArrayList<GoodsClassBean.GcListBean.ChildBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        GoodsClassBean.GcListBean.ChildBean bean = arrayList.get(position);
        ImageHelp.get().display(bean.getAppImageUrl(), holder.mainImageView);
        holder.mainTextView.setText(bean.getGcName());
        LinearLayoutCompat.LayoutParams layoutParams = (LinearLayoutCompat.LayoutParams) holder.mainImageView.getLayoutParams();
        layoutParams.height = (App.get().getWidth() - App.get().dp2Px(160)) / 5;
        holder.mainImageView.setLayoutParams(layoutParams);

        holder.mainLinearLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_main_cate_thr, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, GoodsClassBean.GcListBean.ChildBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayoutCompat mainLinearLayout;
        private final AppCompatImageView mainImageView;
        private final AppCompatTextView mainTextView;

        private ViewHolder(View view) {

            super(view);
            mainLinearLayout = view.findViewById(R.id.mainLinearLayout);
            mainImageView = view.findViewById(R.id.mainImageView);
            mainTextView = view.findViewById(R.id.mainTextView);

        }

    }

}
