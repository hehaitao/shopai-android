package top.yokey.shopai.main.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sunfusheng.marqueeview.MarqueeView;
import com.youth.banner.Banner;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.other.CountDown;
import top.yokey.shopai.zcom.recycler.GridDecoration;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zsdk.bean.ArticleListBean;
import top.yokey.shopai.zsdk.bean.SpecialBean;

public class MainSpecialAdapter extends RecyclerView.Adapter<MainSpecialAdapter.ViewHolder> {

    private final ArrayList<SpecialBean> arrayList;
    private final GridDecoration gridDecoration = new GridDecoration(2, App.get().dp2Px(8), false);
    private final GridDecoration home3Decoration = new GridDecoration(2, 1, App.get().getColors(R.color.divider), false);
    private CountDown countDown = null;

    public MainSpecialAdapter(ArrayList<SpecialBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        SpecialBean bean = arrayList.get(position);
        holder.showListBanner.setVisibility(View.GONE);
        holder.articleLinearLayout.setVisibility(View.GONE);
        holder.home1ImageView.setVisibility(View.GONE);
        holder.home2LinearLayout.setVisibility(View.GONE);
        holder.home3LinearLayout.setVisibility(View.GONE);
        holder.home4LinearLayout.setVisibility(View.GONE);
        holder.home5LinearLayout.setVisibility(View.GONE);
        holder.home6LinearLayout.setVisibility(View.GONE);
        holder.home7RelativeLayout.setVisibility(View.GONE);
        holder.home8LinearLayout.setVisibility(View.GONE);
        holder.commonLinearLayout.setVisibility(View.GONE);
        holder.commonRecyclerView.removeItemDecoration(gridDecoration);
        if (bean.getShowList() != null) showList(holder, bean.getShowList());
        if (bean.getArticleList().size() != 0) article(holder, bean.getArticleList());
        if (bean.getHome1() != null) home1(holder, bean.getHome1());
        if (bean.getHome2() != null) home2(holder, bean.getHome2());
        if (bean.getHome3() != null) home3(holder, bean.getHome3());
        if (bean.getHome4() != null) home4(holder, bean.getHome4());
        if (bean.getHome5() != null) home5(holder, bean.getHome5());
        if (bean.getHome6() != null) home6(holder, bean.getHome6());
        if (bean.getHome7() != null) home7(holder, bean.getHome7());
        if (bean.getHome8() != null) home8(holder, bean.getHome8());
        if (bean.getGoods1() != null) goods1(holder, bean.getGoods1());
        if (bean.getGoods2() != null) goods2(holder, bean.getGoods2());
        if (bean.getGoods() != null) goods(holder, bean.getGoods());

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_main_special, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    private void showList(ViewHolder holder, SpecialBean.ShowListBean showListBean) {

        holder.showListBanner.setVisibility(View.VISIBLE);
        ViewGroup.LayoutParams layoutParams = holder.showListBanner.getLayoutParams();
        layoutParams.height = (int) (App.get().getWidth() / 2.2);
        holder.showListBanner.setLayoutParams(layoutParams);
        List<String> image = new ArrayList<>();
        List<String> type = new ArrayList<>();
        List<String> data = new ArrayList<>();
        for (int i = 0; i < showListBean.getItem().size(); i++) {
            image.add(showListBean.getItem().get(i).getImage());
            type.add(showListBean.getItem().get(i).getType());
            data.add(showListBean.getItem().get(i).getData());
        }
        App.get().setBanner(holder.showListBanner, ImageHelp.get().getHome(), image, position -> App.get().startTypeValue(type.get(position), data.get(position)));

    }

    //自定义方法

    private void article(ViewHolder holder, ArrayList<ArticleListBean> arrayList) {

        holder.articleLinearLayout.setVisibility(View.VISIBLE);
        holder.articleMarqueeView.setOnItemClickListener(null);
        List<String> id = new ArrayList<>();
        List<String> list = new ArrayList<>();
        for (int i = 0; i < arrayList.size(); i++) {
            id.add(arrayList.get(i).getArticleId());
            list.add(arrayList.get(i).getArticleTitle());
        }
        holder.articleMarqueeView.startWithList(list);
        holder.articleTextView.setOnClickListener(view -> App.get().start(ARoutePath.MAIN_ARTICLE));
        holder.articleMarqueeView.setOnItemClickListener((position, textView) -> App.get().start(ARoutePath.MAIN_ARTICLE_DETAIL, Constant.DATA_ID, id.get(position)));

    }

    private void home1(ViewHolder holder, SpecialBean.Home1Bean home1Bean) {

        holder.home1ImageView.setVisibility(View.VISIBLE);
        LinearLayoutCompat.LayoutParams layoutParams = (LinearLayoutCompat.LayoutParams) holder.home1ImageView.getLayoutParams();
        layoutParams.height = (int) (App.get().getWidth() / 2.8);
        holder.home1ImageView.setLayoutParams(layoutParams);
        ImageHelp.get().displayRadius(home1Bean.getImage(), holder.home1ImageView, ImageHelp.get().getHome());
        holder.home1ImageView.setOnClickListener(view -> App.get().startTypeValue(home1Bean.getType(), home1Bean.getData()));

    }

    private void home2(ViewHolder holder, SpecialBean.Home2Bean home2Bean) {

        holder.home2LinearLayout.setVisibility(View.VISIBLE);
        LinearLayoutCompat.LayoutParams layoutParams = (LinearLayoutCompat.LayoutParams) holder.home20LinearLayout.getLayoutParams();
        layoutParams.height = (int) (App.get().getWidth() / 2.6);
        holder.home20LinearLayout.setLayoutParams(layoutParams);
        holder.home2TextView.setText(home2Bean.getTitle());
        ImageHelp.get().displayRadius(home2Bean.getSquareImage(), holder.home20ImageView, ImageHelp.get().getHome());
        ImageHelp.get().displayRadius(home2Bean.getRectangle1Image(), holder.home21ImageView, ImageHelp.get().getHome());
        ImageHelp.get().displayRadius(home2Bean.getRectangle2Image(), holder.home22ImageView, ImageHelp.get().getHome());
        holder.home20ImageView.setOnClickListener(view -> App.get().startTypeValue(home2Bean.getSquareType(), home2Bean.getSquareData()));
        holder.home21ImageView.setOnClickListener(view -> App.get().startTypeValue(home2Bean.getRectangle1Type(), home2Bean.getRectangle1Data()));
        holder.home22ImageView.setOnClickListener(view -> App.get().startTypeValue(home2Bean.getRectangle2Type(), home2Bean.getRectangle2Data()));
        holder.home2TextView.setVisibility(VerifyUtil.isEmpty(home2Bean.getTitle()) ? View.GONE : View.VISIBLE);

    }

    private void home3(ViewHolder holder, SpecialBean.Home3Bean home3Bean) {

        holder.home3LinearLayout.setVisibility(View.VISIBLE);
        holder.home3TextView.setText(home3Bean.getTitle());
        App.get().setRecyclerView(holder.home3RecyclerView, new MainSpecialHome3Adapter(home3Bean.getItem()));
        holder.home3RecyclerView.setLayoutManager(new GridLayoutManager(App.get(), 2));
        holder.home3RecyclerView.removeItemDecoration(home3Decoration);
        holder.home3RecyclerView.addItemDecoration(home3Decoration);
        holder.home3TextView.setVisibility(VerifyUtil.isEmpty(home3Bean.getTitle()) ? View.GONE : View.VISIBLE);

    }

    private void home4(ViewHolder holder, SpecialBean.Home4Bean home4Bean) {

        holder.home4LinearLayout.setVisibility(View.VISIBLE);
        LinearLayoutCompat.LayoutParams layoutParams = (LinearLayoutCompat.LayoutParams) holder.home40LinearLayout.getLayoutParams();
        layoutParams.height = (int) (App.get().getWidth() / 2.6);
        holder.home40LinearLayout.setLayoutParams(layoutParams);
        holder.home4TextView.setText(home4Bean.getTitle());
        ImageHelp.get().displayRadius(home4Bean.getSquareImage(), holder.home40ImageView, ImageHelp.get().getHome());
        ImageHelp.get().displayRadius(home4Bean.getRectangle1Image(), holder.home41ImageView, ImageHelp.get().getHome());
        ImageHelp.get().displayRadius(home4Bean.getRectangle2Image(), holder.home42ImageView, ImageHelp.get().getHome());
        holder.home40ImageView.setOnClickListener(view -> App.get().startTypeValue(home4Bean.getSquareType(), home4Bean.getSquareData()));
        holder.home41ImageView.setOnClickListener(view -> App.get().startTypeValue(home4Bean.getRectangle1Type(), home4Bean.getRectangle1Data()));
        holder.home42ImageView.setOnClickListener(view -> App.get().startTypeValue(home4Bean.getRectangle2Type(), home4Bean.getRectangle2Data()));
        holder.home4TextView.setVisibility(VerifyUtil.isEmpty(home4Bean.getTitle()) ? View.GONE : View.VISIBLE);

    }

    private void home5(ViewHolder holder, SpecialBean.Home5Bean home5Bean) {

        holder.home5LinearLayout.setVisibility(View.VISIBLE);
        LinearLayoutCompat.LayoutParams layoutParams = (LinearLayoutCompat.LayoutParams) holder.home50LinearLayout.getLayoutParams();
        layoutParams.height = App.get().getWidth() / 2;
        holder.home50LinearLayout.setLayoutParams(layoutParams);
        holder.home5TextView.setText(home5Bean.getTitle());
        holder.home51TextView.setText(home5Bean.getStitle());
        ImageHelp.get().displayRadius(home5Bean.getSquareImage(), holder.home50ImageView, ImageHelp.get().getHome());
        ImageHelp.get().displayRadius(home5Bean.getRectangle1Image(), holder.home51ImageView, ImageHelp.get().getHome());
        ImageHelp.get().displayRadius(home5Bean.getRectangle2Image(), holder.home52ImageView, ImageHelp.get().getHome());
        ImageHelp.get().displayRadius(home5Bean.getRectangle3Image(), holder.home53ImageView, ImageHelp.get().getHome());
        holder.home50ImageView.setOnClickListener(view -> App.get().startTypeValue(home5Bean.getSquareType(), home5Bean.getSquareData()));
        holder.home51ImageView.setOnClickListener(view -> App.get().startTypeValue(home5Bean.getRectangle1Type(), home5Bean.getRectangle1Data()));
        holder.home52ImageView.setOnClickListener(view -> App.get().startTypeValue(home5Bean.getRectangle2Type(), home5Bean.getRectangle2Data()));
        holder.home53ImageView.setOnClickListener(view -> App.get().startTypeValue(home5Bean.getRectangle3Type(), home5Bean.getRectangle3Data()));
        holder.home5TextView.setVisibility(VerifyUtil.isEmpty(home5Bean.getTitle()) ? View.GONE : View.VISIBLE);
        holder.home51TextView.setVisibility(VerifyUtil.isEmpty(home5Bean.getStitle()) ? View.GONE : View.VISIBLE);

    }

    private void home6(ViewHolder holder, SpecialBean.Home6Bean home6Bean) {

        holder.home6LinearLayout.setVisibility(View.VISIBLE);
        LinearLayoutCompat.LayoutParams layoutParams = (LinearLayoutCompat.LayoutParams) holder.home60LinearLayout.getLayoutParams();
        layoutParams.height = (int) (App.get().getWidth() / 2.6);
        holder.home60LinearLayout.setLayoutParams(layoutParams);
        holder.home6TextView.setText(home6Bean.getTitle());
        ImageHelp.get().displayRadius(home6Bean.getSquareImage(), holder.home60ImageView, ImageHelp.get().getHome());
        ImageHelp.get().displayRadius(home6Bean.getRectangle1Image(), holder.home61ImageView, ImageHelp.get().getHome());
        ImageHelp.get().displayRadius(home6Bean.getRectangle2Image(), holder.home62ImageView, ImageHelp.get().getHome());
        holder.home60ImageView.setOnClickListener(view -> App.get().startTypeValue(home6Bean.getSquareType(), home6Bean.getSquareData()));
        holder.home61ImageView.setOnClickListener(view -> App.get().startTypeValue(home6Bean.getRectangle1Type(), home6Bean.getRectangle1Data()));
        holder.home62ImageView.setOnClickListener(view -> App.get().startTypeValue(home6Bean.getRectangle2Type(), home6Bean.getRectangle2Data()));
        holder.home6TextView.setVisibility(VerifyUtil.isEmpty(home6Bean.getTitle()) ? View.GONE : View.VISIBLE);

    }

    private void home7(ViewHolder holder, SpecialBean.Home7Bean home7Bean) {

        holder.home7RelativeLayout.setVisibility(View.VISIBLE);
        holder.home70TextView.setText(home7Bean.getSquareIcoName());
        holder.home71TextView.setText(home7Bean.getRectangle1IcoName());
        holder.home72TextView.setText(home7Bean.getRectangle2IcoName());
        holder.home73TextView.setText(home7Bean.getRectangle3IcoName());
        holder.home74TextView.setText(home7Bean.getRectangle4IcoName());
        holder.home75TextView.setText(home7Bean.getRectangle5IcoName());
        holder.home76TextView.setText(home7Bean.getRectangle6IcoName());
        holder.home77TextView.setText(home7Bean.getRectangle7IcoName());
        holder.home78TextView.setText(home7Bean.getRectangle8IcoName());
        holder.home79TextView.setText(home7Bean.getRectangle9IcoName());
        ImageHelp.get().display(home7Bean.getSquareImage(), App.get().dp2Px(24), App.get().dp2Px(24), holder.home70ImageView);
        ImageHelp.get().display(home7Bean.getRectangle1Image(), App.get().dp2Px(24), App.get().dp2Px(24), holder.home71ImageView);
        ImageHelp.get().display(home7Bean.getRectangle2Image(), App.get().dp2Px(24), App.get().dp2Px(24), holder.home72ImageView);
        ImageHelp.get().display(home7Bean.getRectangle3Image(), App.get().dp2Px(24), App.get().dp2Px(24), holder.home73ImageView);
        ImageHelp.get().display(home7Bean.getRectangle4Image(), App.get().dp2Px(24), App.get().dp2Px(24), holder.home74ImageView);
        ImageHelp.get().display(home7Bean.getRectangle5Image(), App.get().dp2Px(24), App.get().dp2Px(24), holder.home75ImageView);
        ImageHelp.get().display(home7Bean.getRectangle6Image(), App.get().dp2Px(24), App.get().dp2Px(24), holder.home76ImageView);
        ImageHelp.get().display(home7Bean.getRectangle7Image(), App.get().dp2Px(24), App.get().dp2Px(24), holder.home77ImageView);
        ImageHelp.get().display(home7Bean.getRectangle8Image(), App.get().dp2Px(24), App.get().dp2Px(24), holder.home78ImageView);
        ImageHelp.get().display(home7Bean.getRectangle9Image(), App.get().dp2Px(24), App.get().dp2Px(24), holder.home79ImageView);
        holder.home70ImageView.setBackgroundDrawable(App.get().getGradientDrawable(App.get().dp2Px(24), Color.parseColor(home7Bean.getSquareIcoColor())));
        holder.home71ImageView.setBackgroundDrawable(App.get().getGradientDrawable(App.get().dp2Px(24), Color.parseColor(home7Bean.getRectangle1IcoColor())));
        holder.home72ImageView.setBackgroundDrawable(App.get().getGradientDrawable(App.get().dp2Px(24), Color.parseColor(home7Bean.getRectangle2IcoColor())));
        holder.home73ImageView.setBackgroundDrawable(App.get().getGradientDrawable(App.get().dp2Px(24), Color.parseColor(home7Bean.getRectangle3IcoColor())));
        holder.home74ImageView.setBackgroundDrawable(App.get().getGradientDrawable(App.get().dp2Px(24), Color.parseColor(home7Bean.getRectangle4IcoColor())));
        holder.home75ImageView.setBackgroundDrawable(App.get().getGradientDrawable(App.get().dp2Px(24), Color.parseColor(home7Bean.getRectangle5IcoColor())));
        holder.home76ImageView.setBackgroundDrawable(App.get().getGradientDrawable(App.get().dp2Px(24), Color.parseColor(home7Bean.getRectangle6IcoColor())));
        holder.home77ImageView.setBackgroundDrawable(App.get().getGradientDrawable(App.get().dp2Px(24), Color.parseColor(home7Bean.getRectangle7IcoColor())));
        holder.home78ImageView.setBackgroundDrawable(App.get().getGradientDrawable(App.get().dp2Px(24), Color.parseColor(home7Bean.getRectangle8IcoColor())));
        holder.home79ImageView.setBackgroundDrawable(App.get().getGradientDrawable(App.get().dp2Px(24), Color.parseColor(home7Bean.getRectangle9IcoColor())));
        holder.home70LinearLayout.setOnClickListener(view -> App.get().startTypeValue(home7Bean.getSquareType(), home7Bean.getSquareData()));
        holder.home71LinearLayout.setOnClickListener(view -> App.get().startTypeValue(home7Bean.getRectangle1Type(), home7Bean.getRectangle1Data()));
        holder.home72LinearLayout.setOnClickListener(view -> App.get().startTypeValue(home7Bean.getRectangle2Type(), home7Bean.getRectangle2Data()));
        holder.home73LinearLayout.setOnClickListener(view -> App.get().startTypeValue(home7Bean.getRectangle3Type(), home7Bean.getRectangle3Data()));
        holder.home74LinearLayout.setOnClickListener(view -> App.get().startTypeValue(home7Bean.getRectangle4Type(), home7Bean.getRectangle4Data()));
        holder.home75LinearLayout.setOnClickListener(view -> App.get().startTypeValue(home7Bean.getRectangle5Type(), home7Bean.getRectangle5Data()));
        holder.home76LinearLayout.setOnClickListener(view -> App.get().startTypeValue(home7Bean.getRectangle6Type(), home7Bean.getRectangle6Data()));
        holder.home77LinearLayout.setOnClickListener(view -> App.get().startTypeValue(home7Bean.getRectangle7Type(), home7Bean.getRectangle7Data()));
        holder.home78LinearLayout.setOnClickListener(view -> App.get().startTypeValue(home7Bean.getRectangle8Type(), home7Bean.getRectangle8Data()));
        holder.home79LinearLayout.setOnClickListener(view -> App.get().startTypeValue(home7Bean.getRectangle9Type(), home7Bean.getRectangle9Data()));

    }

    private void home8(ViewHolder holder, SpecialBean.Home8Bean home8Bean) {

        holder.home8LinearLayout.setVisibility(View.VISIBLE);
        LinearLayoutCompat.LayoutParams layoutParams = (LinearLayoutCompat.LayoutParams) holder.home80LinearLayout.getLayoutParams();
        layoutParams.height = App.get().getWidth() / 3;
        holder.home80LinearLayout.setLayoutParams(layoutParams);
        holder.home8TextView.setText(home8Bean.getTitle());
        ImageHelp.get().displayRadiusCenter(home8Bean.getSquareImage(), holder.home80ImageView, ImageHelp.get().getHome());
        ImageHelp.get().displayRadiusCenter(home8Bean.getRectangle1Image(), holder.home81ImageView, ImageHelp.get().getHome());
        ImageHelp.get().displayRadiusCenter(home8Bean.getRectangle2Image(), holder.home82ImageView, ImageHelp.get().getHome());
        ImageHelp.get().displayRadiusCenter(home8Bean.getRectangle3Image(), holder.home83ImageView, ImageHelp.get().getHome());
        holder.home80ImageView.setOnClickListener(view -> App.get().startTypeValue(home8Bean.getSquareType(), home8Bean.getSquareData()));
        holder.home81ImageView.setOnClickListener(view -> App.get().startTypeValue(home8Bean.getRectangle1Type(), home8Bean.getRectangle1Data()));
        holder.home82ImageView.setOnClickListener(view -> App.get().startTypeValue(home8Bean.getRectangle2Type(), home8Bean.getRectangle2Data()));
        holder.home83ImageView.setOnClickListener(view -> App.get().startTypeValue(home8Bean.getRectangle3Type(), home8Bean.getRectangle3Data()));
        holder.home8TextView.setVisibility(VerifyUtil.isEmpty(home8Bean.getTitle()) ? View.GONE : View.VISIBLE);

    }

    private void goods1(ViewHolder holder, SpecialBean.Goods1Bean goods1Bean) {

        holder.commonLinearLayout.setVisibility(View.VISIBLE);
        holder.commonTextView.setText(goods1Bean.getTitle());
        App.get().setRecyclerView(holder.commonRecyclerView, new MainSpecialGoods1Adapter(goods1Bean.getItem()));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(App.get());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        holder.commonRecyclerView.setLayoutManager(linearLayoutManager);
        holder.commonTextView.setVisibility(VerifyUtil.isEmpty(goods1Bean.getTitle()) ? View.GONE : View.VISIBLE);
        scrollShopCircle(holder, 0);

    }

    private void goods2(ViewHolder holder, SpecialBean.Goods2Bean goods2Bean) {

        holder.commonLinearLayout.setVisibility(View.VISIBLE);
        holder.commonTextView.setText(goods2Bean.getTitle());
        App.get().setRecyclerView(holder.commonRecyclerView, new MainSpecialGoods2Adapter(goods2Bean.getItem()));
        GridLayoutManager gridLayoutManager = new GridLayoutManager(App.get(), 2);
        holder.commonRecyclerView.setLayoutManager(gridLayoutManager);
        holder.commonRecyclerView.addItemDecoration(gridDecoration);
        holder.commonTextView.setVisibility(VerifyUtil.isEmpty(goods2Bean.getTitle()) ? View.GONE : View.VISIBLE);

    }

    private void goods(ViewHolder holder, SpecialBean.GoodsBean goodsBean) {

        holder.commonLinearLayout.setVisibility(View.VISIBLE);
        holder.commonTextView.setText(goodsBean.getTitle());
        App.get().setRecyclerView(holder.commonRecyclerView, new MainSpecialGoodsAdapter(goodsBean.getItem()));
        GridLayoutManager gridLayoutManager = new GridLayoutManager(App.get(), 2);
        holder.commonRecyclerView.setLayoutManager(gridLayoutManager);
        holder.commonRecyclerView.addItemDecoration(gridDecoration);
        holder.commonTextView.setVisibility(VerifyUtil.isEmpty(goodsBean.getTitle()) ? View.GONE : View.VISIBLE);

    }

    private void scrollShopCircle(ViewHolder holder, int position) {

        if (countDown != null) countDown.cancel();
        countDown = new CountDown(Constant.TIME_DELAY, Constant.TIME_TICK) {
            @Override
            public void onFinish() {
                super.onFinish();
                if (position > ((Objects.requireNonNull(holder.commonRecyclerView.getLayoutManager())).getChildCount())) {
                    holder.commonRecyclerView.smoothScrollToPosition(0);
                    scrollShopCircle(holder, 0);
                } else {
                    holder.commonRecyclerView.smoothScrollToPosition(position + 1);
                    scrollShopCircle(holder, position + 1);
                }
            }
        };
        countDown.start();

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        //ShowList
        private final Banner showListBanner;
        //Article
        private final LinearLayoutCompat articleLinearLayout;
        private final MarqueeView articleMarqueeView;
        private final AppCompatTextView articleTextView;
        //Home1
        private final AppCompatImageView home1ImageView;
        //Home2
        private final LinearLayoutCompat home2LinearLayout;
        private final AppCompatTextView home2TextView;
        private final LinearLayoutCompat home20LinearLayout;
        private final AppCompatImageView home20ImageView;
        private final AppCompatImageView home21ImageView;
        private final AppCompatImageView home22ImageView;
        //Home3
        private final LinearLayoutCompat home3LinearLayout;
        private final AppCompatTextView home3TextView;
        private final RecyclerView home3RecyclerView;
        //Home4
        private final LinearLayoutCompat home4LinearLayout;
        private final AppCompatTextView home4TextView;
        private final LinearLayoutCompat home40LinearLayout;
        private final AppCompatImageView home40ImageView;
        private final AppCompatImageView home41ImageView;
        private final AppCompatImageView home42ImageView;
        //Home5
        private final LinearLayoutCompat home5LinearLayout;
        private final AppCompatTextView home5TextView;
        private final AppCompatTextView home51TextView;
        private final LinearLayoutCompat home50LinearLayout;
        private final AppCompatImageView home50ImageView;
        private final AppCompatImageView home51ImageView;
        private final AppCompatImageView home52ImageView;
        private final AppCompatImageView home53ImageView;
        //Home6
        private final LinearLayoutCompat home6LinearLayout;
        private final AppCompatTextView home6TextView;
        private final LinearLayoutCompat home60LinearLayout;
        private final AppCompatImageView home60ImageView;
        private final AppCompatImageView home61ImageView;
        private final AppCompatImageView home62ImageView;
        //Home7
        private final RelativeLayout home7RelativeLayout;
        private final LinearLayoutCompat home70LinearLayout;
        private final LinearLayoutCompat home71LinearLayout;
        private final LinearLayoutCompat home72LinearLayout;
        private final LinearLayoutCompat home73LinearLayout;
        private final LinearLayoutCompat home74LinearLayout;
        private final LinearLayoutCompat home75LinearLayout;
        private final LinearLayoutCompat home76LinearLayout;
        private final LinearLayoutCompat home77LinearLayout;
        private final LinearLayoutCompat home78LinearLayout;
        private final LinearLayoutCompat home79LinearLayout;
        private final AppCompatImageView home70ImageView;
        private final AppCompatImageView home71ImageView;
        private final AppCompatImageView home72ImageView;
        private final AppCompatImageView home73ImageView;
        private final AppCompatImageView home74ImageView;
        private final AppCompatImageView home75ImageView;
        private final AppCompatImageView home76ImageView;
        private final AppCompatImageView home77ImageView;
        private final AppCompatImageView home78ImageView;
        private final AppCompatImageView home79ImageView;
        private final AppCompatTextView home70TextView;
        private final AppCompatTextView home71TextView;
        private final AppCompatTextView home72TextView;
        private final AppCompatTextView home73TextView;
        private final AppCompatTextView home74TextView;
        private final AppCompatTextView home75TextView;
        private final AppCompatTextView home76TextView;
        private final AppCompatTextView home77TextView;
        private final AppCompatTextView home78TextView;
        private final AppCompatTextView home79TextView;
        //Home8
        private final LinearLayoutCompat home8LinearLayout;
        private final AppCompatTextView home8TextView;
        private final LinearLayoutCompat home80LinearLayout;
        private final AppCompatImageView home80ImageView;
        private final AppCompatImageView home81ImageView;
        private final AppCompatImageView home82ImageView;
        private final AppCompatImageView home83ImageView;
        //Common
        private final LinearLayoutCompat commonLinearLayout;
        private final AppCompatTextView commonTextView;
        private final RecyclerView commonRecyclerView;

        private ViewHolder(View view) {

            super(view);
            //ShowList
            showListBanner = view.findViewById(R.id.showListBanner);
            //Article
            articleLinearLayout = view.findViewById(R.id.articleLinearLayout);
            articleMarqueeView = view.findViewById(R.id.articleMarqueeView);
            articleTextView = view.findViewById(R.id.articleTextView);
            //Home1
            home1ImageView = view.findViewById(R.id.home1ImageView);
            //Home2
            home2LinearLayout = view.findViewById(R.id.home2LinearLayout);
            home2TextView = view.findViewById(R.id.home2TextView);
            home20LinearLayout = view.findViewById(R.id.home20LinearLayout);
            home20ImageView = view.findViewById(R.id.home20ImageView);
            home21ImageView = view.findViewById(R.id.home21ImageView);
            home22ImageView = view.findViewById(R.id.home22ImageView);
            //Home3
            home3LinearLayout = view.findViewById(R.id.home3LinearLayout);
            home3TextView = view.findViewById(R.id.home3TextView);
            home3RecyclerView = view.findViewById(R.id.home3RecyclerView);
            //Home4
            home4LinearLayout = view.findViewById(R.id.home4LinearLayout);
            home4TextView = view.findViewById(R.id.home4TextView);
            home40LinearLayout = view.findViewById(R.id.home40LinearLayout);
            home40ImageView = view.findViewById(R.id.home40ImageView);
            home41ImageView = view.findViewById(R.id.home41ImageView);
            home42ImageView = view.findViewById(R.id.home42ImageView);
            //Home5
            home5LinearLayout = view.findViewById(R.id.home5LinearLayout);
            home5TextView = view.findViewById(R.id.home5TextView);
            home51TextView = view.findViewById(R.id.home51TextView);
            home50LinearLayout = view.findViewById(R.id.home50LinearLayout);
            home50ImageView = view.findViewById(R.id.home50ImageView);
            home51ImageView = view.findViewById(R.id.home51ImageView);
            home52ImageView = view.findViewById(R.id.home52ImageView);
            home53ImageView = view.findViewById(R.id.home53ImageView);
            //Home6
            home6LinearLayout = view.findViewById(R.id.home6LinearLayout);
            home6TextView = view.findViewById(R.id.home6TextView);
            home60LinearLayout = view.findViewById(R.id.home60LinearLayout);
            home60ImageView = view.findViewById(R.id.home60ImageView);
            home61ImageView = view.findViewById(R.id.home61ImageView);
            home62ImageView = view.findViewById(R.id.home62ImageView);
            //Home7
            home7RelativeLayout = view.findViewById(R.id.home7RelativeLayout);
            home70LinearLayout = view.findViewById(R.id.home70LinearLayout);
            home71LinearLayout = view.findViewById(R.id.home71LinearLayout);
            home72LinearLayout = view.findViewById(R.id.home72LinearLayout);
            home73LinearLayout = view.findViewById(R.id.home73LinearLayout);
            home74LinearLayout = view.findViewById(R.id.home74LinearLayout);
            home75LinearLayout = view.findViewById(R.id.home75LinearLayout);
            home76LinearLayout = view.findViewById(R.id.home76LinearLayout);
            home77LinearLayout = view.findViewById(R.id.home77LinearLayout);
            home78LinearLayout = view.findViewById(R.id.home78LinearLayout);
            home79LinearLayout = view.findViewById(R.id.home79LinearLayout);
            home70ImageView = view.findViewById(R.id.home70ImageView);
            home71ImageView = view.findViewById(R.id.home71ImageView);
            home72ImageView = view.findViewById(R.id.home72ImageView);
            home73ImageView = view.findViewById(R.id.home73ImageView);
            home74ImageView = view.findViewById(R.id.home74ImageView);
            home75ImageView = view.findViewById(R.id.home75ImageView);
            home76ImageView = view.findViewById(R.id.home76ImageView);
            home77ImageView = view.findViewById(R.id.home77ImageView);
            home78ImageView = view.findViewById(R.id.home78ImageView);
            home79ImageView = view.findViewById(R.id.home79ImageView);
            home70TextView = view.findViewById(R.id.home70TextView);
            home71TextView = view.findViewById(R.id.home71TextView);
            home72TextView = view.findViewById(R.id.home72TextView);
            home73TextView = view.findViewById(R.id.home73TextView);
            home74TextView = view.findViewById(R.id.home74TextView);
            home75TextView = view.findViewById(R.id.home75TextView);
            home76TextView = view.findViewById(R.id.home76TextView);
            home77TextView = view.findViewById(R.id.home77TextView);
            home78TextView = view.findViewById(R.id.home78TextView);
            home79TextView = view.findViewById(R.id.home79TextView);
            //Home8
            home8LinearLayout = view.findViewById(R.id.home8LinearLayout);
            home8TextView = view.findViewById(R.id.home8TextView);
            home80LinearLayout = view.findViewById(R.id.home80LinearLayout);
            home80ImageView = view.findViewById(R.id.home80ImageView);
            home81ImageView = view.findViewById(R.id.home81ImageView);
            home82ImageView = view.findViewById(R.id.home82ImageView);
            home83ImageView = view.findViewById(R.id.home83ImageView);
            //Common
            commonLinearLayout = view.findViewById(R.id.commonLinearLayout);
            commonTextView = view.findViewById(R.id.commonTextView);
            commonRecyclerView = view.findViewById(R.id.commonRecyclerView);

        }

    }

}
