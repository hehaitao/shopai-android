package top.yokey.shopai.main.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zsdk.bean.SpecialBean;

class MainSpecialHome3Adapter extends RecyclerView.Adapter<MainSpecialHome3Adapter.ViewHolder> {

    private final ArrayList<SpecialBean.Home3Bean.ItemBean> arrayList;

    MainSpecialHome3Adapter(ArrayList<SpecialBean.Home3Bean.ItemBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        SpecialBean.Home3Bean.ItemBean bean = arrayList.get(position);
        ImageHelp.get().displayRadius(bean.getImage(), holder.mainImageView, ImageHelp.get().getHome());
        ViewGroup.LayoutParams layoutParams = holder.mainImageView.getLayoutParams();
        layoutParams.height = App.get().getWidth() / 5;
        holder.mainImageView.setLayoutParams(layoutParams);

        holder.mainImageView.setOnClickListener(view -> App.get().startTypeValue(bean.getType(), bean.getData()));

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_main_special_home3, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final AppCompatImageView mainImageView;

        private ViewHolder(View view) {

            super(view);
            mainImageView = view.findViewById(R.id.mainImageView);

        }

    }

}
