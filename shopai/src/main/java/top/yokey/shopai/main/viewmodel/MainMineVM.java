package top.yokey.shopai.main.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.GoodsListBean;
import top.yokey.shopai.zsdk.bean.MemberAssetBean;
import top.yokey.shopai.zsdk.bean.MemberBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.GoodsController;
import top.yokey.shopai.zsdk.controller.MemberAccountController;
import top.yokey.shopai.zsdk.controller.MemberCartController;
import top.yokey.shopai.zsdk.controller.MemberFavoritesController;
import top.yokey.shopai.zsdk.controller.MemberIndexController;
import top.yokey.shopai.zsdk.data.GoodsSearchData;

public class MainMineVM extends BaseViewModel {

    private final MutableLiveData<MemberBean> memberLiveData = new MutableLiveData<>();
    private final MutableLiveData<MemberAssetBean> assetLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<GoodsListBean>> goodsLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> addCartLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> favoriteLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> xingeTokenLiveData = new MutableLiveData<>();

    public MainMineVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<MemberBean> getMemberLiveData() {

        return memberLiveData;

    }

    public MutableLiveData<MemberAssetBean> getAssetLiveData() {

        return assetLiveData;

    }

    public MutableLiveData<ArrayList<GoodsListBean>> getGoodsLiveData() {

        return goodsLiveData;

    }

    public MutableLiveData<String> getAddCartLiveData() {

        return addCartLiveData;

    }

    public MutableLiveData<String> getFavoriteLiveData() {

        return favoriteLiveData;

    }

    public void getMember() {

        MemberIndexController.index(new HttpCallBack<MemberBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, MemberBean bean) {
                memberLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getAsset() {

        MemberIndexController.myAsset(new HttpCallBack<MemberAssetBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, MemberAssetBean bean) {
                assetLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void getGoods() {

        GoodsController.goodsList(new GoodsSearchData(), new HttpCallBack<ArrayList<GoodsListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<GoodsListBean> list) {
                goodsLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

    public void addCart(String goodsId, String quantity) {

        MemberCartController.cartAdd(goodsId, quantity, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                addCartLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(4, reason));
            }
        });

    }

    public void favorite(String goodsId) {

        MemberFavoritesController.favoritesAdd(goodsId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                favoriteLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(5, reason));
            }
        });

    }

    public void updateXingeToken(String token) {

        MemberAccountController.updateXingeToken(token, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                xingeTokenLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(6, reason));
            }
        });

    }

}
