package top.yokey.shopai.main.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.SpecialBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.IndexController;

public class MainSpecialVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<SpecialBean>> specialLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> titleLiveData = new MutableLiveData<>();

    public MainSpecialVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<SpecialBean>> getSpecialLiveData() {

        return specialLiveData;

    }

    public MutableLiveData<String> getTitleLiveData() {

        return titleLiveData;

    }

    public void getSpecial(String id) {

        IndexController.special(id, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                String name;
                JSONObject jsonObject;
                SpecialBean specialBean;
                ArrayList<SpecialBean> arrayList = new ArrayList<>();
                JSONArray jsonArray = JsonUtil.toJSONArray(string);
                for (int i = 0; i < jsonArray.length(); i++) {
                    jsonObject = JsonUtil.getJSONObject(jsonArray, i);
                    name = "show_list";
                    if (jsonObject.has("show_list")) {
                        specialBean = new SpecialBean();
                        specialBean.setShowList(JsonUtil.json2Object(JsonUtil.getString(jsonObject, name), SpecialBean.ShowListBean.class));
                        arrayList.add(specialBean);
                    }
                    name = "home1";
                    if (jsonObject.has(name)) {
                        specialBean = new SpecialBean();
                        specialBean.setHome1(JsonUtil.json2Object(JsonUtil.getString(jsonObject, name), SpecialBean.Home1Bean.class));
                        arrayList.add(specialBean);
                    }
                    name = "home2";
                    if (jsonObject.has(name)) {
                        specialBean = new SpecialBean();
                        specialBean.setHome2(JsonUtil.json2Object(JsonUtil.getString(jsonObject, name), SpecialBean.Home2Bean.class));
                        arrayList.add(specialBean);
                    }
                    name = "home3";
                    if (jsonObject.has(name)) {
                        specialBean = new SpecialBean();
                        specialBean.setHome3(JsonUtil.json2Object(JsonUtil.getString(jsonObject, name), SpecialBean.Home3Bean.class));
                        arrayList.add(specialBean);
                    }
                    name = "home4";
                    if (jsonObject.has(name)) {
                        specialBean = new SpecialBean();
                        specialBean.setHome4(JsonUtil.json2Object(JsonUtil.getString(jsonObject, name), SpecialBean.Home4Bean.class));
                        arrayList.add(specialBean);
                    }
                    name = "home5";
                    if (jsonObject.has(name)) {
                        specialBean = new SpecialBean();
                        specialBean.setHome5(JsonUtil.json2Object(JsonUtil.getString(jsonObject, name), SpecialBean.Home5Bean.class));
                        arrayList.add(specialBean);
                    }
                    name = "home6";
                    if (jsonObject.has(name)) {
                        specialBean = new SpecialBean();
                        specialBean.setHome6(JsonUtil.json2Object(JsonUtil.getString(jsonObject, name), SpecialBean.Home6Bean.class));
                        arrayList.add(specialBean);
                    }
                    name = "home7";
                    if (jsonObject.has(name)) {
                        specialBean = new SpecialBean();
                        specialBean.setHome7(JsonUtil.json2Object(JsonUtil.getString(jsonObject, name), SpecialBean.Home7Bean.class));
                        arrayList.add(specialBean);
                    }
                    name = "home8";
                    if (jsonObject.has(name)) {
                        specialBean = new SpecialBean();
                        specialBean.setHome8(JsonUtil.json2Object(JsonUtil.getString(jsonObject, name), SpecialBean.Home8Bean.class));
                        arrayList.add(specialBean);
                    }
                    name = "goods";
                    if (jsonObject.has(name)) {
                        specialBean = new SpecialBean();
                        specialBean.setGoods(JsonUtil.json2Object(JsonUtil.getString(jsonObject, name), SpecialBean.GoodsBean.class));
                        arrayList.add(specialBean);
                    }
                    name = "goods1";
                    if (jsonObject.has(name)) {
                        specialBean = new SpecialBean();
                        specialBean.setGoods1(JsonUtil.json2Object(JsonUtil.getString(jsonObject, name), SpecialBean.Goods1Bean.class));
                        arrayList.add(specialBean);
                    }
                    name = "goods2";
                    if (jsonObject.has(name)) {
                        specialBean = new SpecialBean();
                        specialBean.setGoods2(JsonUtil.json2Object(JsonUtil.getString(jsonObject, name), SpecialBean.Goods2Bean.class));
                        arrayList.add(specialBean);
                    }
                }
                specialLiveData.setValue(arrayList);
                titleLiveData.setValue(JsonUtil.getString(baseBean.getDatas(), "special_desc"));
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
