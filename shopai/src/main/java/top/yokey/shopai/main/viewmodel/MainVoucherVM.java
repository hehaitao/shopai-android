package top.yokey.shopai.main.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.CouponListBean;
import top.yokey.shopai.zsdk.bean.VoucherListBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.CouponController;
import top.yokey.shopai.zsdk.controller.MemberCouponController;
import top.yokey.shopai.zsdk.controller.MemberVoucherController;
import top.yokey.shopai.zsdk.controller.VoucherController;

public class MainVoucherVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<CouponListBean>> couponLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<VoucherListBean>> voucherLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> couponFreeexLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> voucherFreeexLiveData = new MutableLiveData<>();

    public MainVoucherVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<CouponListBean>> getCouponLiveData() {

        return couponLiveData;

    }

    public MutableLiveData<ArrayList<VoucherListBean>> getVoucherLiveData() {

        return voucherLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public MutableLiveData<String> getCouponFreeexLiveData() {

        return couponFreeexLiveData;

    }

    public MutableLiveData<String> getVoucherFreeexLiveData() {

        return voucherFreeexLiveData;

    }

    public void getCoupon(String page) {

        CouponController.couponList(page, new HttpCallBack<ArrayList<CouponListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<CouponListBean> list) {
                couponLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getVoucher(String page) {

        VoucherController.voucherList(page, new HttpCallBack<ArrayList<VoucherListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<VoucherListBean> list) {
                voucherLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void couponFreeex(String tid) {

        MemberCouponController.getcoupon(tid, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                couponFreeexLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

    public void voucherFreeex(String tid) {

        MemberVoucherController.voucherFreeex(tid, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                voucherFreeexLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(4, reason));
            }
        });

    }

}
