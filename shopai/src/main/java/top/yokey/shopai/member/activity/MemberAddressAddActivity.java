package top.yokey.shopai.member.activity;

import android.content.Intent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.member.viewmodel.MemberAddressAddVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zsdk.data.AddressData;

@Route(path = ARoutePath.MEMBER_ADDRESS_ADD)
public class MemberAddressAddActivity extends BaseActivity {

    private final AddressData data = new AddressData();
    private Toolbar mainToolbar = null;
    private AppCompatEditText nameEditText = null;
    private AppCompatEditText mobileEditText = null;
    private LinearLayoutCompat areaLinearLayout = null;
    private AppCompatEditText areaEditText = null;
    private AppCompatEditText addressEditText = null;
    private AppCompatCheckBox defaultCheckBox = null;
    private AppCompatTextView addTextView = null;
    private MemberAddressAddVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_member_address_add);
        mainToolbar = findViewById(R.id.mainToolbar);
        nameEditText = findViewById(R.id.nameEditText);
        mobileEditText = findViewById(R.id.mobileEditText);
        areaLinearLayout = findViewById(R.id.areaLinearLayout);
        areaEditText = findViewById(R.id.areaEditText);
        addressEditText = findViewById(R.id.addressEditText);
        defaultCheckBox = findViewById(R.id.defaultCheckBox);
        addTextView = findViewById(R.id.addTextView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.addAddress);
        observeKeyborad(R.id.mainLinearLayout);
        vm = getVM(MemberAddressAddVM.class);

    }

    @Override
    public void initEvent() {

        View.OnClickListener onClickListener = view -> App.get().start(get(), ARoutePath.MAIN_AREA, Constant.CODE_AREA);

        areaLinearLayout.setOnClickListener(onClickListener);

        areaEditText.setOnClickListener(onClickListener);

        addTextView.setOnClickListener(view -> {
            data.setTrueName(Objects.requireNonNull(nameEditText.getText()).toString());
            data.setMobPhone(Objects.requireNonNull(mobileEditText.getText()).toString());
            data.setAddress(Objects.requireNonNull(addressEditText.getText()).toString());
            data.setIsDefault(defaultCheckBox.isChecked() ? Constant.COMMON_ENABLE : "0");
            hideKeyboard();
            vm.add(data);
        });

    }

    @Override
    public void initObserve() {

        vm.getAddLiveData().observe(this, string -> {
            ToastHelp.get().show(R.string.addressAddSuccess);
            onReturn(false);
        });

        vm.getErrorLiveData().observe(this, bean -> ToastHelp.get().show(bean.getReason()));

    }

    @Override
    public void onActivityResult(int req, int res, @Nullable Intent intent) {

        super.onActivityResult(req, res, intent);
        if (intent != null && res == RESULT_OK && req == Constant.CODE_AREA) {
            areaEditText.setText(intent.getStringExtra("area_info"));
            data.setAreaInfo(intent.getStringExtra("area_info"));
            data.setCityId(intent.getStringExtra("city_id"));
            data.setAreaId(intent.getStringExtra("area_id"));
        }

    }

}
