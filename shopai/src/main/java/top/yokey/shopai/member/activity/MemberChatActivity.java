package top.yokey.shopai.member.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.Nullable;
import androidx.core.widget.ContentLoadingProgressBar;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.entity.LocalMedia;

import java.io.File;
import java.util.List;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.AnimUtil;
import top.yokey.shopai.zcom.util.ConvertUtil;
import top.yokey.shopai.zsdk.ShopAISdk;

@Route(path = ARoutePath.MEMBER_CHAT)
public class MemberChatActivity extends BaseActivity {

    @Autowired(name = Constant.DATA_URL)
    String chatUrl = "";
    private WebView mainWebView = null;
    private ContentLoadingProgressBar mainProgressBar = null;
    private boolean success = false;
    private ValueCallback<Uri[]> valueCallback;

    @Override
    public void initView() {

        setContentView(R.layout.activity_member_chat);
        mainWebView = findViewById(R.id.mainWebView);
        mainProgressBar = findViewById(R.id.mainProgressBar);

    }

    @Override
    public void initData() {

        setStateBarWap();
        App.get().setWebView(mainWebView);
        ShopAISdk.get().setSupportWebView(get());
        mainWebView.loadUrl(ShopAISdk.get().getFeedbackUrl());
        observeKeyborad(R.id.mainRelativeLayout);

    }

    @Override
    public void initEvent() {

        mainWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if (success && url.equals(ShopAISdk.get().getFeedbackUrl())) {
                    onReturn(false);
                    return;
                }
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (url.equals(ShopAISdk.get().getFeedbackUrl())) {
                    mainWebView.loadUrl(chatUrl);
                }
                if (url.contains(ShopAISdk.get().getChatUrl())) {
                    success = true;
                }
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (success && url.contains("goods_id")) {
                    App.get().startUrl(url);
                } else {
                    mainWebView.loadUrl(url);
                }
                return true;
            }
        });

        mainWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int progress) {
                mainProgressBar.setProgress(progress);
                if (progress == 100) {
                    if (mainProgressBar.getVisibility() == View.VISIBLE) {
                        AnimUtil.objectAnimator(mainProgressBar, AnimUtil.ALPHA, 1.0f, 0f);
                    }
                    mainProgressBar.setVisibility(View.GONE);
                } else {
                    if (mainProgressBar.getVisibility() == View.GONE) {
                        AnimUtil.objectAnimator(mainProgressBar, AnimUtil.ALPHA, 0f, 1.0f);
                    }
                    mainProgressBar.setVisibility(View.VISIBLE);
                }
                super.onProgressChanged(view, progress);
            }

            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                if (valueCallback != null) {
                    valueCallback.onReceiveValue(null);
                }
                valueCallback = filePathCallback;
                App.get().startAlbum(get(), 1);
                return true;
            }
        });

    }

    @Override
    public void initObserve() {

    }

    @Override
    public void onActivityResult(int req, int res, @Nullable Intent intent) {

        super.onActivityResult(req, res, intent);
        if (res == RESULT_OK && req == Constant.CODE_ALBUM && intent != null) {
            List<LocalMedia> list = PictureSelector.obtainMultipleResult(intent);
            if (valueCallback == null) {
                return;
            }
            valueCallback.onReceiveValue(new Uri[]{ConvertUtil.file2Uri(get(), new File(list.get(0).getCompressPath()))});
            valueCallback = null;
        }

    }

}
