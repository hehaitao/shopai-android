package top.yokey.shopai.member.activity;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.member.adapter.MemberFavoriteGoodsAdapter;
import top.yokey.shopai.member.adapter.MemberFavoriteStoreAdapter;
import top.yokey.shopai.member.viewmodel.MemberFavoriteVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.recycler.SpaceDecoration;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.GoodsFavoriteBean;
import top.yokey.shopai.zsdk.bean.StoreFavoriteBean;
import top.yokey.shopai.zsdk.data.GoodsData;

@Route(path = ARoutePath.MEMBER_FAVORITE)
public class MemberFavoriteActivity extends BaseActivity {

    private final ArrayList<GoodsFavoriteBean> arrayList = new ArrayList<>();
    private final MemberFavoriteGoodsAdapter adapter = new MemberFavoriteGoodsAdapter(arrayList);
    private final ArrayList<StoreFavoriteBean> storeArrayList = new ArrayList<>();
    private final MemberFavoriteStoreAdapter storeAdapter = new MemberFavoriteStoreAdapter(storeArrayList);
    private final LineDecoration lineDecoration = new LineDecoration(App.get().dp2Px(12), true);
    private final SpaceDecoration spaceDecoration = new SpaceDecoration(App.get().dp2Px(4));
    private Toolbar mainToolbar = null;
    private AppCompatTextView leftTextView = null;
    private AppCompatTextView rightTextView = null;
    private PullRefreshView mainPullRefreshView = null;
    private int page = 1;
    private boolean isGoods = true;
    private MemberFavoriteVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_switch_refresh_view);
        mainToolbar = findViewById(R.id.mainToolbar);
        leftTextView = findViewById(R.id.leftTextView);
        rightTextView = findViewById(R.id.rightTextView);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar);
        leftTextView.setText(R.string.goods);
        rightTextView.setText(R.string.store);
        vm = getVM(MemberFavoriteVM.class);
        observeKeyborad(R.id.mainLinearLayout);
        clickLeft();

    }

    @Override
    public void initEvent() {

        leftTextView.setOnClickListener(view -> clickLeft());

        rightTextView.setOnClickListener(view -> clickRight());

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || arrayList.size() == 0) {
                page = 1;
                getData();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getData();
            }

            @Override
            public void onLoadMore() {
                getData();
            }
        });

        adapter.setOnItemClickListener(new MemberFavoriteGoodsAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position, GoodsFavoriteBean bean) {
                App.get().startGoods(new GoodsData(bean.getGoodsId()));
            }

            @Override
            public void onDelete(int position, GoodsFavoriteBean bean) {
                vm.delGoods(bean.getFavId());
            }
        });

        storeAdapter.setOnItemClickListener(new MemberFavoriteStoreAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position, StoreFavoriteBean bean) {
                App.get().startStore(bean.getStoreId());
            }

            @Override
            public void onDelete(int position, StoreFavoriteBean bean) {
                vm.delStore(bean.getStoreId());
            }
        });

    }

    @Override
    public void initObserve() {

        vm.getGoodsLiveData().observe(this, list -> {
            if (page == 1) arrayList.clear();
            page = page + 1;
            arrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getStoreLiveData().observe(this, list -> {
            if (page == 1) storeArrayList.clear();
            page = page + 1;
            storeArrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getHasMoreLiveData().observe(this, bool -> mainPullRefreshView.setCanLoadMore(bool));

        vm.getDelLiveData().observe(this, string -> {
            ToastHelp.get().showSuccess();
            page = 1;
            getData();
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                if (arrayList.size() == 0) {
                    mainPullRefreshView.setError(bean.getReason());
                } else {
                    ToastHelp.get().show(bean.getReason());
                }
                return;
            }
            ToastHelp.get().show(bean.getReason());
        });

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView.setLoad();
        if (isGoods) {
            vm.getGoods(page + "");
            return;
        }
        vm.getStore(page + "");

    }

    private void clickLeft() {

        leftTextView.setTextColor(App.get().getColors(R.color.primary));
        leftTextView.setBackgroundResource(R.drawable.selector_switch_left_press);
        rightTextView.setTextColor(App.get().getColors(R.color.accent));
        rightTextView.setBackgroundResource(R.drawable.selector_switch_right);
        mainPullRefreshView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        mainPullRefreshView.getRecyclerView().setPadding(App.get().dp2Px(4), 0, App.get().dp2Px(4), 0);
        mainPullRefreshView.removeItemDecoration(lineDecoration);
        mainPullRefreshView.removeItemDecoration(spaceDecoration);
        mainPullRefreshView.setItemDecoration(spaceDecoration);
        mainPullRefreshView.setAdapter(adapter);
        mainPullRefreshView.setComplete();
        isGoods = true;
        page = 1;
        getData();

    }

    private void clickRight() {

        leftTextView.setTextColor(App.get().getColors(R.color.accent));
        leftTextView.setBackgroundResource(R.drawable.selector_switch_left);
        rightTextView.setTextColor(App.get().getColors(R.color.primary));
        rightTextView.setBackgroundResource(R.drawable.selector_switch_right_press);
        mainPullRefreshView.setLayoutManager(new LinearLayoutManager(get()));
        mainPullRefreshView.getRecyclerView().setPadding(0, 0, 0, 0);
        mainPullRefreshView.removeItemDecoration(lineDecoration);
        mainPullRefreshView.removeItemDecoration(spaceDecoration);
        mainPullRefreshView.setItemDecoration(lineDecoration);
        mainPullRefreshView.setAdapter(storeAdapter);
        mainPullRefreshView.setComplete();
        isGoods = false;
        page = 1;
        getData();

    }

}
