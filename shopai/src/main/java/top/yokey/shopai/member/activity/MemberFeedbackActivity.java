package top.yokey.shopai.member.activity;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.member.viewmodel.MemberFeedbackVM;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.util.VerifyUtil;

@Route(path = ARoutePath.MEMBER_FEEDBACK)
public class MemberFeedbackActivity extends BaseActivity {

    private Toolbar mainToolbar = null;
    private AppCompatEditText contentEditText = null;
    private AppCompatTextView submitTextView = null;

    private MemberFeedbackVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_member_feedback);
        mainToolbar = findViewById(R.id.mainToolbar);
        contentEditText = findViewById(R.id.contentEditText);
        submitTextView = findViewById(R.id.submitTextView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.feedback);
        observeKeyborad(R.id.mainLinearLayout);
        vm = getVM(MemberFeedbackVM.class);

    }

    @Override
    public void initEvent() {

        submitTextView.setOnClickListener(view -> {
            hideKeyboard();
            String content = Objects.requireNonNull(contentEditText.getText()).toString();
            if (VerifyUtil.isEmpty(content)) {
                ToastHelp.get().show(R.string.pleaseDescribeTheProblemsYouEncounter);
                return;
            }
            submitTextView.setEnabled(false);
            submitTextView.setText(R.string.handlerIng);
            vm.feedback(content);
        });

    }

    @Override
    public void initObserve() {

        vm.getFeedbackLiveData().observe(this, string -> {
            ToastHelp.get().showSuccess();
            onReturn(false);
        });

        vm.getErrorLiveData().observe(this, bean -> {
            submitTextView.setEnabled(true);
            submitTextView.setText(R.string.submit);
            ToastHelp.get().show(bean.getReason());
        });

    }

}
