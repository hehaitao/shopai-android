package top.yokey.shopai.member.activity;

import android.text.Editable;
import android.text.TextWatcher;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.member.viewmodel.MemberMobileUntyingVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.other.CountDown;
import top.yokey.shopai.zcom.util.ConvertUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;

@Route(path = ARoutePath.MEMBER_MOBILE_UNTYING)
public class MemberMobileUntyingActivity extends BaseActivity {

    private Toolbar mainToolbar = null;
    private AppCompatEditText captchaEditText = null;
    private AppCompatTextView captchaTextView = null;
    private AppCompatTextView untyingTextView = null;

    private MemberMobileUntyingVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_member_mobile_untying);
        mainToolbar = findViewById(R.id.mainToolbar);
        captchaEditText = findViewById(R.id.captchaEditText);
        captchaTextView = findViewById(R.id.captchaTextView);
        untyingTextView = findViewById(R.id.untyingTextView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.mobileUntying);
        observeKeyborad(R.id.mainLinearLayout);
        vm = getVM(MemberMobileUntyingVM.class);
        canUntying();

    }

    @Override
    public void initEvent() {

        captchaEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (VerifyUtil.isEmpty(Objects.requireNonNull(captchaEditText.getText()).toString())) {
                    captchaEditText.setCompoundDrawablesRelativeWithIntrinsicBounds(App.get().getDrawables(R.drawable.ic_hint_code), null, null, null);
                } else {
                    captchaEditText.setCompoundDrawablesRelativeWithIntrinsicBounds(App.get().getDrawables(R.drawable.ic_hint_code_accent), null, null, null);
                }
                canUntying();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        captchaTextView.setOnClickListener(view -> {
            captchaTextView.setEnabled(false);
            captchaTextView.setText(R.string.handlerIng);
            vm.step2();
        });

        untyingTextView.setOnClickListener(view -> {
            captchaEditText.setEnabled(false);
            captchaTextView.setEnabled(false);
            untyingTextView.setEnabled(false);
            untyingTextView.setText(R.string.tipsCheckCaptcha);
            untyingTextView.setTextColor(App.get().getColors(R.color.textThr));
            String captcha = Objects.requireNonNull(captchaEditText.getText()).toString();
            vm.step3(captcha);
        });

    }

    @Override
    public void initObserve() {

        vm.getGetCaptchaLiveData().observe(this, string -> {
            ToastHelp.get().show(R.string.captchaSendSuccess);
            final int time = ConvertUtil.string2Int(string);
            new CountDown(time * 1000, Constant.TIME_TICK) {
                int count = time;

                @Override
                public void onTick(long millisUntilFinished) {
                    super.onTick(millisUntilFinished);
                    count--;
                    String temp = count + " S";
                    captchaTextView.setText(temp);
                    captchaTextView.setTextColor(App.get().getColors(R.color.textThr));
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    captchaTextView.setEnabled(true);
                    captchaTextView.setText(R.string.get);
                    captchaTextView.setTextColor(App.get().getColors(R.color.accent));
                }
            }.start();
        });

        vm.getUntyingLiveData().observe(this, string -> {
            App.get().getMemberBean().getMemberInfoAll().setMemberMobile("");
            App.get().getMemberBean().getMemberInfoAll().setMemberMobileBind("0");
            ToastHelp.get().show(R.string.mobileUntyingSuccess);
            onReturn(false);
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                ToastHelp.get().show(bean.getReason());
                captchaTextView.setText(R.string.get);
                captchaTextView.setEnabled(true);
            } else {
                ToastHelp.get().show(bean.getReason());
                captchaEditText.setEnabled(true);
                captchaTextView.setEnabled(true);
                untyingTextView.setEnabled(true);
                untyingTextView.setTextColor(App.get().getColors(R.color.accent));
                untyingTextView.setText(R.string.untying);
            }
        });

    }

    //自定义方法

    private void canUntying() {

        if (!VerifyUtil.isEmpty(Objects.requireNonNull(captchaEditText.getText()).toString())) {
            untyingTextView.setEnabled(true);
            untyingTextView.setTextColor(App.get().getColors(R.color.accent));
        } else {
            untyingTextView.setEnabled(false);
            untyingTextView.setTextColor(App.get().getColors(R.color.textThr));
        }

    }

}
