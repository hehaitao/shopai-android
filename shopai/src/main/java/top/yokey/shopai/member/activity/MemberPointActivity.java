package top.yokey.shopai.member.activity;

import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.member.adapter.MemberPointLogAdapter;
import top.yokey.shopai.member.viewmodel.MemberPointVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.util.AnimUtil;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.PointLogBean;

@Route(path = ARoutePath.MEMBER_POINT)
public class MemberPointActivity extends BaseActivity {

    private final ArrayList<PointLogBean> arrayList = new ArrayList<>();
    private final MemberPointLogAdapter adapter = new MemberPointLogAdapter(arrayList);
    @Autowired(name = Constant.DATA_ID)
    String id;
    private Toolbar mainToolbar = null;
    private AppCompatTextView priceTextView = null;
    private AppCompatTextView signTextView = null;
    private AppCompatTextView shopTextView = null;
    private AppCompatTextView detailedTextView = null;
    private AppCompatTextView nightTextView = null;
    private LinearLayoutCompat logLinearLayout = null;
    private PullRefreshView logPullRefreshView = null;
    private int page = 1;
    private boolean isAnim = false;
    private MemberPointVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_member_point);
        mainToolbar = findViewById(R.id.mainToolbar);
        priceTextView = findViewById(R.id.priceTextView);
        signTextView = findViewById(R.id.signTextView);
        shopTextView = findViewById(R.id.shopTextView);
        detailedTextView = findViewById(R.id.detailedTextView);
        nightTextView = findViewById(R.id.nightTextView);
        logLinearLayout = findViewById(R.id.logLinearLayout);
        logPullRefreshView = findViewById(R.id.logPullRefreshView);

    }

    @Override
    public void initData() {

        if (id != null && id.equals(Constant.COMMON_ENABLE)) showLog();
        setToolbar(mainToolbar, R.string.point);
        observeKeyborad(R.id.mainRelativeLayout);
        logPullRefreshView.setAdapter(adapter);
        logPullRefreshView.setItemDecoration(new LineDecoration(1, App.get().getColors(R.color.divider), true));
        vm = getVM(MemberPointVM.class);
        vm.getAsset();
        getLog();

    }

    @Override
    public void initEvent() {

        signTextView.setOnClickListener(view -> App.get().start(ARoutePath.MAIN_SIGN));

        shopTextView.setOnClickListener(view -> App.get().start(ARoutePath.POINT_LIST));

        detailedTextView.setOnClickListener(view -> showLog());

        nightTextView.setOnClickListener(view -> goneAll());

        logPullRefreshView.setOnClickListener(view -> {
            if (logPullRefreshView.isError() || arrayList.size() == 0) {
                getLog();
            }
        });

        logPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getLog();
            }

            @Override
            public void onLoadMore() {
                getLog();
            }
        });

    }

    @Override
    public void initObserve() {

        vm.getAssetLiveData().observe(this, bean -> priceTextView.setText(bean.getPoint()));

        vm.getLogLiveData().observe(this, list -> {
            if (page == 1) arrayList.clear();
            page = page + 1;
            arrayList.addAll(list);
            logPullRefreshView.setComplete();
        });

        vm.getHasMoreLiveData().observe(this, bool -> logPullRefreshView.setCanLoadMore(bool));

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getAsset());
                return;
            }
            ToastHelp.get().show(bean.getReason());
        });

    }

    @Override
    public void onReturn(boolean handler) {

        if (nightTextView.getVisibility() == View.VISIBLE) {
            goneAll();
            return;
        }
        if (handler && isShowKeyboard()) {
            hideKeyboard();
            return;
        }
        finish();

    }

    //自定义方法

    private void getLog() {

        logPullRefreshView.setLoad();
        vm.getLog(page + "");

    }

    private void goneAll() {

        if (isAnim) {
            return;
        }
        if (nightTextView.getVisibility() == View.VISIBLE) {
            isAnim = true;
            AnimUtil.objectAnimator(nightTextView, AnimUtil.ALPHA, () -> {
                isAnim = false;
                nightTextView.setVisibility(View.GONE);
            }, 1.0f, 0);
        }
        if (logLinearLayout.getVisibility() == View.VISIBLE) {
            isAnim = true;
            AnimUtil.objectAnimator(logLinearLayout, AnimUtil.TRABSLATION_Y, () -> {
                isAnim = false;
                logLinearLayout.setVisibility(View.GONE);
            }, 0, App.get().getHeight());
        }

    }

    private void showLog() {

        if (isAnim) {
            return;
        }
        if (nightTextView.getVisibility() == View.GONE) {
            isAnim = true;
            nightTextView.setVisibility(View.VISIBLE);
            AnimUtil.objectAnimator(nightTextView, AnimUtil.ALPHA, () -> isAnim = false, 0, 1.0f);
        }
        if (logLinearLayout.getVisibility() == View.GONE) {
            isAnim = true;
            logLinearLayout.setVisibility(View.VISIBLE);
            AnimUtil.objectAnimator(logLinearLayout, AnimUtil.TRABSLATION_Y, () -> isAnim = false, App.get().getHeight(), 0);
        }

    }

}
