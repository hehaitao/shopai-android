package top.yokey.shopai.member.activity;

import android.text.InputType;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import top.yokey.shopai.R;
import top.yokey.shopai.member.viewmodel.MemberPreDepositVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.VerifyUtil;

@Route(path = ARoutePath.MEMBER_PRE_DEPOSIT)
public class MemberPreDepositActivity extends BaseActivity {

    private Toolbar mainToolbar = null;
    private AppCompatTextView priceTextView = null;
    private AppCompatTextView cashTextView = null;
    private AppCompatTextView rechargeTextView = null;
    private AppCompatTextView detailedTextView = null;

    private MemberPreDepositVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_member_pre_deposit);
        mainToolbar = findViewById(R.id.mainToolbar);
        priceTextView = findViewById(R.id.priceTextView);
        cashTextView = findViewById(R.id.cashTextView);
        rechargeTextView = findViewById(R.id.rechargeTextView);
        detailedTextView = findViewById(R.id.detailedTextView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.preDeposit);
        observeKeyborad(R.id.mainLinearLayout);
        vm = getVM(MemberPreDepositVM.class);

    }

    @Override
    public void initEvent() {

        cashTextView.setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_PRE_DEPOSIT_CASH));

        rechargeTextView.setOnClickListener(view -> DialogHelp.get().input(get(), InputType.TYPE_CLASS_TEXT, R.string.pleaseInputAmount, "", null, content -> {
            if (!VerifyUtil.isEmpty(content)) {
                vm.index(content);
                return;
            }
            ToastHelp.get().show(R.string.pleaseInputAmount);
        }));

        detailedTextView.setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_PRE_DEPOSIT_LOG));

    }

    @Override
    public void initObserve() {

        vm.getAssetLiveData().observe(this, bean -> {
            priceTextView.setText("￥");
            priceTextView.append(bean.getPredepoit());
        });

        vm.getIndexLiveData().observe(this, string -> App.get().start(ARoutePath.MEMBER_PRE_DEPOSIT_PAY, Constant.DATA_SN, string));

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getAsset());
                return;
            }
            ToastHelp.get().show(bean.getReason());
        });

    }

    @Override
    public void onResume() {

        super.onResume();
        vm.getAsset();

    }

}
