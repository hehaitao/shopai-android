package top.yokey.shopai.member.activity;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;

import top.yokey.shopai.R;
import top.yokey.shopai.member.viewmodel.MemberPreDepositCashDetailedVM;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.VerifyUtil;

@Route(path = ARoutePath.MEMBER_PRE_DEPOSIT_CASH_DETAILED)
public class MemberPreDepositCashDetailedActivity extends BaseActivity {

    @Autowired(name = Constant.DATA_ID)
    String id;
    private Toolbar mainToolbar;
    private AppCompatTextView snTextView;
    private AppCompatTextView amountTextView;
    private AppCompatTextView methodTextView;
    private AppCompatTextView accountTextView;
    private AppCompatTextView nameTextView;
    private AppCompatTextView timeTextView;
    private AppCompatTextView stateTextView;
    private MemberPreDepositCashDetailedVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_member_pre_deposit_cash_detailed);
        mainToolbar = findViewById(R.id.mainToolbar);
        snTextView = findViewById(R.id.snTextView);
        amountTextView = findViewById(R.id.amountTextView);
        methodTextView = findViewById(R.id.methodTextView);
        accountTextView = findViewById(R.id.accountTextView);
        nameTextView = findViewById(R.id.nameTextView);
        timeTextView = findViewById(R.id.timeTextView);
        stateTextView = findViewById(R.id.stateTextView);

    }

    @Override
    public void initData() {

        if (VerifyUtil.isEmpty(id)) {
            ToastHelp.get().showDataError();
            onReturn(false);
            return;
        }
        setToolbar(mainToolbar, R.string.cashDetailed);
        observeKeyborad(R.id.mainLinearLayout);
        vm = getVM(MemberPreDepositCashDetailedVM.class);
        vm.getCash(id);

    }

    @Override
    public void initEvent() {


    }

    @Override
    public void initObserve() {

        vm.getCashLiveData().observe(this, bean -> {
            snTextView.setText(bean.getPdcSn());
            amountTextView.setText(bean.getPdcAmount());
            methodTextView.setText(bean.getPdcBankName());
            accountTextView.setText(bean.getPdcBankNo());
            nameTextView.setText(bean.getPdcBankUser());
            timeTextView.setText(bean.getPdcAddTimeText());
            stateTextView.setText(bean.getPdcPaymentStateText());
        });

    }

}
