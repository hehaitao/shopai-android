package top.yokey.shopai.member.activity;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alipay.sdk.app.PayTask;
import com.jeremyliao.liveeventbus.LiveEventBus;

import top.yokey.shopai.R;
import top.yokey.shopai.member.viewmodel.MemberPreDepositPayVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zsdk.ShopAISdk;

@Route(path = ARoutePath.MEMBER_PRE_DEPOSIT_PAY)
public class MemberPreDepositPayActivity extends BaseActivity {

    @SuppressLint("HandlerLeak")
    @SuppressWarnings("deprecation")
    private final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    LiveEventBus.get(Constant.DATA_REFRESH).post(true);
                    ToastHelp.get().show(R.string.paySuccess);
                    onReturn(false);
                    break;
                case 2:
                    ToastHelp.get().show(R.string.payFailure);
                    break;
            }
        }
    };
    @Autowired(name = Constant.DATA_SN)
    String paySn;
    private Toolbar mainToolbar;
    private AppCompatTextView priceTextView = null;
    private AppCompatTextView snTextView = null;
    private LinearLayoutCompat alipayLinearLayout = null;
    private AppCompatCheckBox alipayCheckBox = null;
    private LinearLayoutCompat wechatLinearLayout = null;
    private AppCompatCheckBox wechatCheckBox = null;
    private AppCompatTextView payTextView = null;
    private WebView mainWebView = null;
    private MemberPreDepositPayVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_member_pre_deposit_pay);
        mainToolbar = findViewById(R.id.mainToolbar);
        priceTextView = findViewById(R.id.priceTextView);
        snTextView = findViewById(R.id.snTextView);
        alipayLinearLayout = findViewById(R.id.alipayLinearLayout);
        alipayCheckBox = findViewById(R.id.alipayCheckBox);
        wechatLinearLayout = findViewById(R.id.wechatLinearLayout);
        wechatCheckBox = findViewById(R.id.wechatCheckBox);
        payTextView = findViewById(R.id.payTextView);
        mainWebView = findViewById(R.id.mainWebView);

    }

    @Override
    public void initData() {

        if (VerifyUtil.isEmpty(paySn)) {
            ToastHelp.get().showDataError();
            onReturn(false);
            return;
        }
        App.get().setWebView(mainWebView);
        setToolbar(mainToolbar, R.string.orderPay);
        observeKeyborad(R.id.mainLinearLayout);
        snTextView.setText(String.format(getString(R.string.orderPaySn), paySn));
        vm = getVM(MemberPreDepositPayVM.class);
        vm.order(paySn);

    }

    @Override
    public void initEvent() {

        alipayLinearLayout.setOnClickListener(view -> {
            alipayCheckBox.setChecked(true);
            wechatCheckBox.setChecked(false);
        });

        wechatLinearLayout.setOnClickListener(view -> {
            alipayCheckBox.setChecked(false);
            wechatCheckBox.setChecked(true);
        });

        payTextView.setOnClickListener(view -> pay());

        mainWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                DialogHelp.get().dismiss();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (!(url.startsWith("http") || url.startsWith("https"))) {
                    return true;
                }
                PayTask task = new PayTask(MemberPreDepositPayActivity.this);
                boolean success = task.payInterceptorWithUrl(url, true, result -> {
                    String returnUrl = result.getReturnUrl();
                    String resultCode = result.getResultCode();
                    if (!VerifyUtil.isEmpty(returnUrl)) {
                        MemberPreDepositPayActivity.this.runOnUiThread(() -> view.loadUrl(returnUrl));
                    }
                    if (resultCode.equals("9000")) {
                        Message msg = new Message();
                        msg.what = 1;
                        mHandler.sendMessage(msg);
                    } else {
                        Message msg = new Message();
                        msg.what = 2;
                        mHandler.sendMessage(msg);
                    }
                });
                if (!success) {
                    view.loadUrl(url);
                }
                return true;
            }
        });

    }

    @Override
    public void initObserve() {

        vm.getOrderLiveData().observe(this, bean -> {
            priceTextView.setText("￥");
            priceTextView.append(bean.getPdinfo().getPdrAmount());
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.order(paySn));
                return;
            }
            ToastHelp.get().show(bean.getReason());
        });

    }

    //自定义方法

    private void pay() {

        //支付宝支付
        if (alipayCheckBox.isChecked()) {
            DialogHelp.get().progress(get(), R.string.handlerIng);
            mainWebView.loadUrl(ShopAISdk.get().getPreDepositOrderAlipayUrl(paySn));
            return;
        }

        ToastHelp.get().show(R.string.pleaseChoosePayment);

    }

}
