package top.yokey.shopai.member.activity;

import android.text.TextUtils;
import android.view.View;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;
import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.member.adapter.MemberVoucherAdapter;
import top.yokey.shopai.member.viewmodel.MemberVoucherVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.util.AnimUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.VoucherBean;
import top.yokey.shopai.zsdk.data.GoodsSearchData;

@Route(path = ARoutePath.MEMBER_VOUCHER)
public class MemberVoucherActivity extends BaseActivity {

    private final ArrayList<VoucherBean> enableArrayList = new ArrayList<>();
    private final ArrayList<VoucherBean> disableArrayList = new ArrayList<>();
    private final MemberVoucherAdapter enableAdapter = new MemberVoucherAdapter(enableArrayList);
    private final MemberVoucherAdapter disableAdapter = new MemberVoucherAdapter(disableArrayList);
    private Toolbar mainToolbar = null;
    private AppCompatImageView toolbarImageView;
    private NestedScrollView mainScrollView;
    private RecyclerView enableRecyclerView;
    private RecyclerView disableRecyclerView;
    private AppCompatTextView tipsTextView;
    private AppCompatTextView nightTextView = null;
    private LinearLayoutCompat voucherLinearLayout = null;
    private AppCompatEditText numberEditText = null;
    private AppCompatEditText captchaEditText = null;
    private AppCompatImageView captchaImageView = null;
    private AppCompatTextView submitTextView = null;
    private String codeKey = "";
    private boolean isAnim = false;
    private MemberVoucherVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_member_voucher);
        mainToolbar = findViewById(R.id.mainToolbar);
        toolbarImageView = findViewById(R.id.toolbarImageView);
        mainScrollView = findViewById(R.id.mainScrollView);
        enableRecyclerView = findViewById(R.id.enableRecyclerView);
        disableRecyclerView = findViewById(R.id.disableRecyclerView);
        tipsTextView = findViewById(R.id.tipsTextView);
        nightTextView = findViewById(R.id.nightTextView);
        voucherLinearLayout = findViewById(R.id.voucherLinearLayout);
        numberEditText = findViewById(R.id.numberEditText);
        captchaEditText = findViewById(R.id.captchaEditText);
        captchaImageView = findViewById(R.id.captchaImageView);
        submitTextView = findViewById(R.id.submitTextView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.voucher);
        observeKeyborad(R.id.mainRelativeLayout);
        toolbarImageView.setImageResource(R.drawable.ic_action_plus);
        App.get().setRecyclerView(enableRecyclerView, enableAdapter);
        App.get().setRecyclerView(disableRecyclerView, disableAdapter);
        enableRecyclerView.addItemDecoration(new LineDecoration(App.get().dp2Px(12), true));
        disableRecyclerView.addItemDecoration(new LineDecoration(App.get().dp2Px(12), true));
        vm = getVM(MemberVoucherVM.class);
        vm.getVoucher();
        vm.getCodeKey();

    }

    @Override
    public void initEvent() {

        toolbarImageView.setOnClickListener(view -> showVoucher());

        enableAdapter.setOnItemClickListener((position, bean) -> App.get().startGoodsList(new GoodsSearchData()));

        nightTextView.setOnClickListener(view -> goneAll());

        submitTextView.setOnClickListener(view -> {
            String number = Objects.requireNonNull(numberEditText.getText()).toString();
            String captcha = Objects.requireNonNull(captchaEditText.getText()).toString();
            if (TextUtils.isEmpty(number)) {
                ToastHelp.get().show(R.string.pleaseInputVoucherPassword);
                return;
            }
            if (TextUtils.isEmpty(captcha)) {
                ToastHelp.get().show(R.string.pleaseInputVerifyCode);
                return;
            }
            submitTextView.setEnabled(false);
            submitTextView.setText(R.string.handlerIng);
            vm.add(number, captcha, codeKey);
        });

    }

    @Override
    public void initObserve() {

        vm.getVoucherLiveData().observe(this, list -> {
            enableArrayList.clear();
            disableArrayList.clear();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getVoucherState().equals(Constant.COMMON_ENABLE)) {
                    enableArrayList.add(list.get(i));
                } else {
                    disableArrayList.add(list.get(i));
                }
            }
            if (enableArrayList.size() == 0 && disableArrayList.size() == 0) {
                mainScrollView.setVisibility(View.GONE);
                tipsTextView.setVisibility(View.VISIBLE);
            } else {
                mainScrollView.setVisibility(View.VISIBLE);
                tipsTextView.setVisibility(View.GONE);
            }
            enableAdapter.notifyDataSetChanged();
            disableAdapter.notifyDataSetChanged();
        });

        vm.getCodeKeyLiveData().observe(this, string -> {
            codeKey = string;
            captchaEditText.setText("");
            ImageHelp.get().display(ShopAISdk.get().getCaptchaUrl(string), captchaImageView);
        });

        vm.getAddLiveData().observe(this, string -> {
            numberEditText.setText("");
            captchaEditText.setText("");
            submitTextView.setEnabled(true);
            submitTextView.setText(R.string.submit);
            ToastHelp.get().showSuccess();
            vm.getCodeKey();
            vm.getVoucher();
            hideKeyboard();
            goneAll();
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getVoucher());
                return;
            }
            vm.getCodeKey();
            submitTextView.setEnabled(true);
            submitTextView.setText(R.string.submit);
            ToastHelp.get().show(bean.getReason());
        });

    }

    @Override
    public void onReturn(boolean handler) {

        if (nightTextView.getVisibility() == View.VISIBLE) {
            goneAll();
            return;
        }
        if (handler && isShowKeyboard()) {
            hideKeyboard();
            return;
        }
        finish();

    }

    //自定义方法

    private void goneAll() {

        if (isAnim) {
            return;
        }
        if (nightTextView.getVisibility() == View.VISIBLE) {
            isAnim = true;
            AnimUtil.objectAnimator(nightTextView, AnimUtil.ALPHA, () -> {
                isAnim = false;
                nightTextView.setVisibility(View.GONE);
            }, 1.0f, 0);
        }
        if (voucherLinearLayout.getVisibility() == View.VISIBLE) {
            isAnim = true;
            AnimUtil.objectAnimator(voucherLinearLayout, AnimUtil.TRABSLATION_Y, () -> {
                isAnim = false;
                voucherLinearLayout.setVisibility(View.GONE);
            }, 0, App.get().getHeight());
        }

    }

    private void showVoucher() {

        if (isAnim) {
            return;
        }
        if (nightTextView.getVisibility() == View.GONE) {
            isAnim = true;
            nightTextView.setVisibility(View.VISIBLE);
            AnimUtil.objectAnimator(nightTextView, AnimUtil.ALPHA, () -> isAnim = false, 0, 1.0f);
        }
        if (voucherLinearLayout.getVisibility() == View.GONE) {
            isAnim = true;
            voucherLinearLayout.setVisibility(View.VISIBLE);
            AnimUtil.objectAnimator(voucherLinearLayout, AnimUtil.TRABSLATION_Y, () -> isAnim = false, App.get().getHeight(), 0);
        }

    }

}
