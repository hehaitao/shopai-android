package top.yokey.shopai.member.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zsdk.bean.GoodsFavoriteBean;

public class MemberFavoriteGoodsAdapter extends RecyclerView.Adapter<MemberFavoriteGoodsAdapter.ViewHolder> {

    private final ArrayList<GoodsFavoriteBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public MemberFavoriteGoodsAdapter(ArrayList<GoodsFavoriteBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        GoodsFavoriteBean bean = arrayList.get(position);
        ImageHelp.get().displayRadius(bean.getGoodsImageUrl(), holder.mainImageView);
        int height = (App.get().getWidth() - 32) / 2;
        ViewGroup.LayoutParams layoutParams = holder.mainImageView.getLayoutParams();
        layoutParams.height = height;
        holder.mainImageView.setLayoutParams(layoutParams);
        holder.nameTextView.setText(bean.getGoodsName());
        holder.priceTextView.setText("￥");
        holder.priceTextView.append(bean.getGoodsPrice());

        holder.deleteImageView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onDelete(position, bean);
            }
        });

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_member_favorite_goods, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, GoodsFavoriteBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onDelete(int position, GoodsFavoriteBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatImageView mainImageView;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView priceTextView;
        private final AppCompatImageView deleteImageView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainImageView = view.findViewById(R.id.mainImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            priceTextView = view.findViewById(R.id.priceTextView);
            deleteImageView = view.findViewById(R.id.deleteImageView);

        }

    }

}
