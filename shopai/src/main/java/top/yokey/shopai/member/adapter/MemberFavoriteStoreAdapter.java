package top.yokey.shopai.member.adapter;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zsdk.bean.StoreFavoriteBean;

public class MemberFavoriteStoreAdapter extends RecyclerView.Adapter<MemberFavoriteStoreAdapter.ViewHolder> {

    private final ArrayList<StoreFavoriteBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public MemberFavoriteStoreAdapter(ArrayList<StoreFavoriteBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        StoreFavoriteBean bean = arrayList.get(position);
        ImageHelp.get().displayRadius(bean.getStoreAvatarUrl(), holder.mainImageView);
        holder.nameTextView.setText(bean.getStoreName());
        holder.favoriteTextView.setText(Html.fromHtml(App.get().handlerHtml(String.format(App.get().getString(R.string.htmlFavoriteFans), bean.getStoreCollect()), "#FF0000")));
        holder.goodsTextView.setText(Html.fromHtml(App.get().handlerHtml(String.format(App.get().getString(R.string.htmlFavoriteGoods), bean.getGoodsCount()), "#FF0000")));

        holder.deleteImageView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onDelete(position, bean);
            }
        });

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_member_favorite_store, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, StoreFavoriteBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onDelete(int position, StoreFavoriteBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatImageView mainImageView;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView favoriteTextView;
        private final AppCompatTextView goodsTextView;
        private final AppCompatImageView deleteImageView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainImageView = view.findViewById(R.id.mainImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            favoriteTextView = view.findViewById(R.id.favoriteTextView);
            goodsTextView = view.findViewById(R.id.goodsTextView);
            deleteImageView = view.findViewById(R.id.deleteImageView);

        }

    }

}
