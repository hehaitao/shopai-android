package top.yokey.shopai.member.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zsdk.bean.MemberInviteIncomeBean;

public class MemberInviteIncomeAdapter extends RecyclerView.Adapter<MemberInviteIncomeAdapter.ViewHolder> {

    private final ArrayList<MemberInviteIncomeBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public MemberInviteIncomeAdapter(ArrayList<MemberInviteIncomeBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        MemberInviteIncomeBean bean = arrayList.get(position);
        holder.nameTextView.setText(bean.getMemberName());
        holder.numberTextView.setText(String.format(App.get().getString(R.string.distribuBuyNumber), bean.getInviteNum()));
        holder.pointTextView.setText(bean.getInviteAmount());

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_member_invite_income, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, MemberInviteIncomeBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickDelete(int position, MemberInviteIncomeBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView numberTextView;
        private final AppCompatTextView pointTextView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            nameTextView = view.findViewById(R.id.nameTextView);
            numberTextView = view.findViewById(R.id.numberTextView);
            pointTextView = view.findViewById(R.id.pointTextView);

        }

    }

}
