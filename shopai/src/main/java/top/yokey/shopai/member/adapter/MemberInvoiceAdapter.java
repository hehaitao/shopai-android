package top.yokey.shopai.member.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zsdk.bean.InvoiceListBean;

public class MemberInvoiceAdapter extends RecyclerView.Adapter<MemberInvoiceAdapter.ViewHolder> {

    private final ArrayList<InvoiceListBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public MemberInvoiceAdapter(ArrayList<InvoiceListBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        InvoiceListBean bean = arrayList.get(position);
        holder.nameTextView.setText(bean.getInvTitle());
        holder.nameTextView.append(" " + bean.getInvContent());

        holder.deleteImageView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickDelete(position, bean);
            }
        });

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_member_invoice, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, InvoiceListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickDelete(int position, InvoiceListBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatTextView nameTextView;
        private final AppCompatImageView deleteImageView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            nameTextView = view.findViewById(R.id.nameTextView);
            deleteImageView = view.findViewById(R.id.deleteImageView);

        }

    }

}
