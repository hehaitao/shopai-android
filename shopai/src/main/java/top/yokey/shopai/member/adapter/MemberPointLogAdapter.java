package top.yokey.shopai.member.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zsdk.bean.PointLogBean;

public class MemberPointLogAdapter extends RecyclerView.Adapter<MemberPointLogAdapter.ViewHolder> {

    private final ArrayList<PointLogBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public MemberPointLogAdapter(ArrayList<PointLogBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        PointLogBean bean = arrayList.get(position);
        holder.mainTextView.setText(bean.getStagetext());
        if (bean.getPlStage().equals("pointstomoney")) {
            holder.mainTextView.setText(R.string.pointConsumption);
        }
        holder.pointTextView.setText("+");
        holder.pointTextView.append(bean.getPlPoints());
        holder.descTextView.setText(bean.getPlDesc());
        holder.timeTextView.setText(bean.getAddtimetext());

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_member_point_log, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, PointLogBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatTextView mainTextView;
        private final AppCompatTextView pointTextView;
        private final AppCompatTextView descTextView;
        private final AppCompatTextView timeTextView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainTextView = view.findViewById(R.id.mainTextView);
            pointTextView = view.findViewById(R.id.pointTextView);
            descTextView = view.findViewById(R.id.descTextView);
            timeTextView = view.findViewById(R.id.timeTextView);

        }

    }

}
