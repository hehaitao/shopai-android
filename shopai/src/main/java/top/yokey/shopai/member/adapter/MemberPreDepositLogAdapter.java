package top.yokey.shopai.member.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zsdk.bean.PreDepositLogBean;

public class MemberPreDepositLogAdapter extends RecyclerView.Adapter<MemberPreDepositLogAdapter.ViewHolder> {

    private final ArrayList<PreDepositLogBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public MemberPreDepositLogAdapter(ArrayList<PreDepositLogBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        PreDepositLogBean bean = arrayList.get(position);
        if (bean.getLgAvAmount().contains("-")) {
            holder.priceTextView.setTextColor(App.get().getColors(R.color.text));
            holder.priceTextView.setText(bean.getLgAvAmount());
        } else {
            holder.priceTextView.setTextColor(App.get().getColors(R.color.red));
            holder.priceTextView.setText("+");
            holder.priceTextView.append(bean.getLgAvAmount());
        }
        holder.mainTextView.setText(bean.getLgDesc());
        holder.timeTextView.setText(bean.getLgAddTimeText());
        switch (bean.getLgType()) {
            case "refund":
                holder.typeTextView.setText(R.string.refund);
                break;
            case "order_pay":
                holder.typeTextView.setText(R.string.consumption);
                break;
            case "recharge":
                holder.typeTextView.setText(R.string.recharge);
                break;
            case "mb_redpacket":
                holder.typeTextView.setText(R.string.redPacket);
                break;
            default:
                holder.typeTextView.setText(R.string.other);
                break;
        }

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_member_pre_deposit_log, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, PreDepositLogBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatTextView mainTextView;
        private final AppCompatTextView priceTextView;
        private final AppCompatTextView typeTextView;
        private final AppCompatTextView timeTextView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainTextView = view.findViewById(R.id.mainTextView);
            priceTextView = view.findViewById(R.id.priceTextView);
            typeTextView = view.findViewById(R.id.typeTextView);
            timeTextView = view.findViewById(R.id.timeTextView);

        }

    }

}
