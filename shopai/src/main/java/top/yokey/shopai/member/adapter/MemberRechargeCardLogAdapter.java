package top.yokey.shopai.member.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zsdk.bean.RechargeCardLogBean;

public class MemberRechargeCardLogAdapter extends RecyclerView.Adapter<MemberRechargeCardLogAdapter.ViewHolder> {

    private final ArrayList<RechargeCardLogBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public MemberRechargeCardLogAdapter(ArrayList<RechargeCardLogBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        RechargeCardLogBean bean = arrayList.get(position);
        holder.mainTextView.setText(bean.getDescription());
        if (bean.getAvailableAmount().contains("-")) {
            holder.priceTextView.setText(bean.getAvailableAmount());
        } else {
            holder.priceTextView.setText("+");
            holder.priceTextView.append(bean.getAvailableAmount());
        }
        holder.timeTextView.setText(bean.getAddTimeText());

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_member_recharge_card_log, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, RechargeCardLogBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatTextView mainTextView;
        private final AppCompatTextView priceTextView;
        private final AppCompatTextView timeTextView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainTextView = view.findViewById(R.id.mainTextView);
            priceTextView = view.findViewById(R.id.priceTextView);
            timeTextView = view.findViewById(R.id.timeTextView);

        }

    }

}
