package top.yokey.shopai.member.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zsdk.bean.RedPacketListBean;
import yyydjk.com.library.CouponView;

public class MemberRedPacketAdapter extends RecyclerView.Adapter<MemberRedPacketAdapter.ViewHolder> {

    private final ArrayList<RedPacketListBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public MemberRedPacketAdapter(ArrayList<RedPacketListBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        RedPacketListBean bean = arrayList.get(position);
        holder.titleTextView.setText(bean.getPacketName());
        holder.timeTextView.setText(String.format(App.get().getString(R.string.redPacketGetTime), bean.getAddTime()));
        holder.priceTextView.setText("￥");
        holder.priceTextView.append(bean.getPacketPrice());
        if (bean.getIsUse().equals(Constant.COMMON_DISABLE)) {
            holder.disableTextView.setVisibility(View.GONE);
        } else {
            holder.disableTextView.setVisibility(View.VISIBLE);
        }

        holder.mainCouponView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_member_red_packet, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, RedPacketListBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final CouponView mainCouponView;
        private final AppCompatTextView titleTextView;
        private final AppCompatTextView timeTextView;
        private final AppCompatTextView priceTextView;
        private final AppCompatTextView disableTextView;

        private ViewHolder(View view) {

            super(view);
            mainCouponView = view.findViewById(R.id.mainCouponView);
            titleTextView = view.findViewById(R.id.titleTextView);
            timeTextView = view.findViewById(R.id.timeTextView);
            priceTextView = view.findViewById(R.id.priceTextView);
            disableTextView = view.findViewById(R.id.disableTextView);

        }

    }

}
