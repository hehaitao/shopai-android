package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.LogoutController;
import top.yokey.shopai.zsdk.controller.MemberAccountController;

public class MemberCenterVM extends BaseViewModel {

    private final MutableLiveData<String> modifyLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> logoutLiveData = new MutableLiveData<>();

    public MemberCenterVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<String> getModifyLiveData() {

        return modifyLiveData;

    }

    public MutableLiveData<String> getLogoutLiveData() {

        return logoutLiveData;

    }

    public void modifyInfo(String item, String value) {

        MemberAccountController.updateInfo(item, value, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                modifyLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void modifyArea(String provinceId, String cityId, String areaId, String areaInfo) {

        MemberAccountController.updateArea(provinceId, cityId, areaId, areaInfo, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                modifyLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void modifyAvatar(String img) {

        MemberAccountController.updateAvatar(img, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                modifyLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void logout(String username) {

        LogoutController.index(username, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                logoutLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
