package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberFeedbackController;

public class MemberFeedbackVM extends BaseViewModel {

    private final MutableLiveData<String> feedbackLiveData = new MutableLiveData<>();

    public MemberFeedbackVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<String> getFeedbackLiveData() {

        return feedbackLiveData;

    }

    public void feedback(String feedback) {

        MemberFeedbackController.feedbackAdd(feedback, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                feedbackLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
