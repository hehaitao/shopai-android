package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.MemberInviteBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberInviteController;

public class MemberInviteVM extends BaseViewModel {

    private final MutableLiveData<MemberInviteBean> inviteLiveData = new MutableLiveData<>();

    public MemberInviteVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<MemberInviteBean> getInviteLiveData() {

        return inviteLiveData;

    }

    public void getInvite() {

        MemberInviteController.index(new HttpCallBack<MemberInviteBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, MemberInviteBean bean) {
                inviteLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
