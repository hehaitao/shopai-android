package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberAccountController;

public class MemberPayPasswordVM extends BaseViewModel {

    private final MutableLiveData<String> step2LiveData = new MutableLiveData<>();
    private final MutableLiveData<String> step3LiveData = new MutableLiveData<>();
    private final MutableLiveData<String> step4LiveData = new MutableLiveData<>();
    private final MutableLiveData<String> step5LiveData = new MutableLiveData<>();

    public MemberPayPasswordVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<String> getStep2LiveData() {

        return step2LiveData;

    }

    public MutableLiveData<String> getStep3LiveData() {

        return step3LiveData;

    }

    public MutableLiveData<String> getStep4LiveData() {

        return step4LiveData;

    }

    public MutableLiveData<String> getStep5LiveData() {

        return step5LiveData;

    }

    public void step2() {

        MemberAccountController.modifyPaypwdStep2(new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                step2LiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void step3(String captcha) {

        MemberAccountController.modifyPaypwdStep3(captcha, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                step3LiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void step4() {

        MemberAccountController.modifyPaypwdStep4(new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                step4LiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void step5(String password) {

        MemberAccountController.modifyPaypwdStep5(password, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                step5LiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
