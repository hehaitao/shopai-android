package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.RechargeController;
import top.yokey.shopai.zsdk.data.PreDepositCashData;

public class MemberPreDepositCashVM extends BaseViewModel {

    private final MutableLiveData<String> submitLiveData = new MutableLiveData<>();

    public MemberPreDepositCashVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<String> getSubmitLiveData() {

        return submitLiveData;

    }

    public void submit(PreDepositCashData data) {

        RechargeController.pdCashAdd(data, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                submitLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
