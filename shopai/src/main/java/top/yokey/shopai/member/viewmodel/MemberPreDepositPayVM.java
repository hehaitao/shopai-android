package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.RechargeOrderBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.RechargeController;

public class MemberPreDepositPayVM extends BaseViewModel {

    private final MutableLiveData<RechargeOrderBean> orderLiveData = new MutableLiveData<>();

    public MemberPreDepositPayVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<RechargeOrderBean> getOrderLiveData() {

        return orderLiveData;

    }

    public void order(String paySn) {

        RechargeController.rechargeOrder(paySn, new HttpCallBack<RechargeOrderBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, RechargeOrderBean bean) {
                orderLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
