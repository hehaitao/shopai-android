package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.RedPacketListBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberPacketController;

public class MemberRedPacketVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<RedPacketListBean>> redPacketLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();

    public MemberRedPacketVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<RedPacketListBean>> getRedPacketLiveData() {

        return redPacketLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public void getRedPacket(String page) {

        MemberPacketController.redPacketList(page, new HttpCallBack<ArrayList<RedPacketListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<RedPacketListBean> list) {
                redPacketLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
