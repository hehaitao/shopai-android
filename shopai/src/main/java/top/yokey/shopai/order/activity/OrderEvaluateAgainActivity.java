package top.yokey.shopai.order.activity;

import android.content.Intent;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.jeremyliao.liveeventbus.LiveEventBus;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.entity.LocalMedia;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import top.yokey.shopai.R;
import top.yokey.shopai.order.adapter.OrderEvaluateAgainGoodsAdapter;
import top.yokey.shopai.order.viewmodel.OrderEvaluateAgainVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zsdk.bean.OrderEvaluateAgainBean;

@Route(path = ARoutePath.ORDER_EVALUATE_AGAIN)
public class OrderEvaluateAgainActivity extends BaseActivity {

    private final ArrayList<OrderEvaluateAgainBean.EvaluateGoodsBean> arrayList = new ArrayList<>();
    private final OrderEvaluateAgainGoodsAdapter adapter = new OrderEvaluateAgainGoodsAdapter(arrayList);
    @Autowired(name = Constant.DATA_ID)
    String orderId;
    private Toolbar mainToolbar = null;
    private RecyclerView mainRecyclerView = null;
    private AppCompatTextView evaluateTextView = null;
    private int pos = 0;
    private int posImg = 0;
    private OrderEvaluateAgainVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_order_evaluate_again);
        mainToolbar = findViewById(R.id.mainToolbar);
        mainRecyclerView = findViewById(R.id.mainRecyclerView);
        evaluateTextView = findViewById(R.id.evaluateTextView);

    }

    @Override
    public void initData() {

        if (VerifyUtil.isEmpty(orderId)) {
            ToastHelp.get().showDataError();
            onReturn(false);
            return;
        }
        setToolbar(mainToolbar, R.string.appendEvaluate);
        observeKeyborad(R.id.mainLinearLayout);
        App.get().setRecyclerView(mainRecyclerView, adapter);
        mainRecyclerView.addItemDecoration(new LineDecoration(App.get().dp2Px(16), false));
        vm = getVM(OrderEvaluateAgainVM.class);
        vm.getEvaluate(orderId);

    }

    @Override
    public void initEvent() {

        adapter.setOnItemClickListener(new OrderEvaluateAgainGoodsAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position, OrderEvaluateAgainBean.EvaluateGoodsBean bean) {

            }

            @Override
            public void onClickImage(int position, int positionImage, OrderEvaluateAgainBean.EvaluateGoodsBean bean) {
                pos = position;
                posImg = positionImage;
                App.get().startAlbum(get(), 1);
            }
        });

        evaluateTextView.setOnClickListener(view -> {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("order_id", orderId);
            OrderEvaluateAgainBean.EvaluateGoodsBean bean;
            for (int i = 0; i < arrayList.size(); i++) {
                bean = arrayList.get(i);
                hashMap.put("goods[" + bean.getGevalId() + "][comment]", bean.getEvaluateContent());
                hashMap.put("goods[" + bean.getGevalId() + "][evaluate_image][0]", bean.getEvaluateImage0Name());
                hashMap.put("goods[" + bean.getGevalId() + "][evaluate_image][1]", bean.getEvaluateImage1Name());
                hashMap.put("goods[" + bean.getGevalId() + "][evaluate_image][2]", bean.getEvaluateImage2Name());
                hashMap.put("goods[" + bean.getGevalId() + "][evaluate_image][3]", bean.getEvaluateImage3Name());
                hashMap.put("goods[" + bean.getGevalId() + "][evaluate_image][4]", bean.getEvaluateImage4Name());
            }
            evaluateTextView.setEnabled(false);
            evaluateTextView.setText(R.string.handlerIng);
            vm.save(hashMap);
        });

    }

    @Override
    public void initObserve() {

        vm.getEvaluateLiveData().observe(this, bean -> {
            arrayList.clear();
            arrayList.addAll(bean.getEvaluateGoods());
            adapter.notifyDataSetChanged();
        });

        vm.getFileUploadLiveData().observe(this, bean -> {
            switch (posImg) {
                case 0:
                    arrayList.get(pos).setEvaluateImage0(bean.getFileUrl());
                    arrayList.get(pos).setEvaluateImage0Name(bean.getFileName());
                    break;
                case 1:
                    arrayList.get(pos).setEvaluateImage1(bean.getFileUrl());
                    arrayList.get(pos).setEvaluateImage1Name(bean.getFileName());
                    break;
                case 2:
                    arrayList.get(pos).setEvaluateImage2(bean.getFileUrl());
                    arrayList.get(pos).setEvaluateImage2Name(bean.getFileName());
                    break;
                case 3:
                    arrayList.get(pos).setEvaluateImage3(bean.getFileUrl());
                    arrayList.get(pos).setEvaluateImage3Name(bean.getFileName());
                    break;
                case 4:
                    arrayList.get(pos).setEvaluateImage4(bean.getFileUrl());
                    arrayList.get(pos).setEvaluateImage4Name(bean.getFileName());
                    break;
            }
            adapter.notifyDataSetChanged();
        });

        vm.getSaveLiveData().observe(this, string -> {
            LiveEventBus.get(Constant.DATA_REFRESH).post(true);
            ToastHelp.get().showSuccess();
            onReturn(false);
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getEvaluate(orderId));
                return;
            }
            evaluateTextView.setEnabled(true);
            evaluateTextView.setText(R.string.appendEvaluate);
            ToastHelp.get().show(bean.getReason());
        });

    }

    @Override
    public void onActivityResult(int req, int res, @Nullable Intent intent) {

        super.onActivityResult(req, res, intent);
        if (res == RESULT_OK && req == Constant.CODE_ALBUM && intent != null) {
            List<LocalMedia> list = PictureSelector.obtainMultipleResult(intent);
            vm.fileUpload(new File(list.get(0).getCompressPath()));
        }

    }

}
