package top.yokey.shopai.order.activity;

import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import cn.sharesdk.onekeyshare.OnekeyShare;
import top.yokey.shopai.R;
import top.yokey.shopai.order.adapter.OrderPinGouLogAdapter;
import top.yokey.shopai.order.viewmodel.OrderPinGouVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.util.ConvertUtil;
import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zcom.view.CountdownTextView;
import top.yokey.shopai.zsdk.bean.OrderPinGouBean;
import top.yokey.shopai.zsdk.data.GoodsData;
import top.yokey.shopai.zsdk.data.OrderPinGouData;

@Route(path = ARoutePath.ORDER_PIN_GOU)
public class OrderPinGouActivity extends BaseActivity {

    private final ArrayList<OrderPinGouBean.LogListBean> arrayList = new ArrayList<>();
    private final OrderPinGouLogAdapter adapter = new OrderPinGouLogAdapter(arrayList);
    @Autowired(name = Constant.DATA_JSON)
    String json = "";
    private Toolbar mainToolbar = null;
    private AppCompatImageView mainImageView = null;
    private AppCompatTextView nameTextView = null;
    private AppCompatTextView priceTextView = null;
    private AppCompatTextView pricePinGouTextView = null;
    private AppCompatTextView goTextView = null;
    private AppCompatTextView peopleTextView = null;
    private CountdownTextView timeTextView = null;
    private AppCompatTextView tipsTextView = null;
    private RecyclerView mainRecyclerView = null;
    private RelativeLayout bottomRelativeLayout = null;
    private AppCompatTextView inviteTextView = null;
    private OrderPinGouVM vm = null;
    private OrderPinGouData data = null;
    private OrderPinGouBean bean = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_order_pin_gou);
        mainToolbar = findViewById(R.id.mainToolbar);
        mainImageView = findViewById(R.id.mainImageView);
        nameTextView = findViewById(R.id.nameTextView);
        priceTextView = findViewById(R.id.priceTextView);
        pricePinGouTextView = findViewById(R.id.pricePinGouTextView);
        goTextView = findViewById(R.id.goTextView);
        peopleTextView = findViewById(R.id.peopleTextView);
        timeTextView = findViewById(R.id.timeTextView);
        tipsTextView = findViewById(R.id.tipsTextView);
        mainRecyclerView = findViewById(R.id.mainRecyclerView);
        bottomRelativeLayout = findViewById(R.id.bottomRelativeLayout);
        inviteTextView = findViewById(R.id.inviteTextView);

    }

    @Override
    public void initData() {

        if (VerifyUtil.isEmpty(json)) {
            ToastHelp.get().showDataError();
            onReturn(false);
            return;
        }
        observeKeyborad(R.id.mainRelativeLayout);
        setToolbar(mainToolbar, R.string.pinGouDetail);
        data = JsonUtil.json2Object(json, OrderPinGouData.class);
        App.get().setRecyclerView(mainRecyclerView, adapter);
        mainRecyclerView.addItemDecoration(new LineDecoration(1, false));
        vm = getVM(OrderPinGouVM.class);
        vm.getOrder(data);

    }

    @Override
    public void initEvent() {

        goTextView.setOnClickListener(view -> {
            GoodsData goodsData = new GoodsData();
            goodsData.setGoodsId(bean.getGoodsId());
            goodsData.setBuyerId(bean.getBuyerId());
            goodsData.setLogId(bean.getLogId());
            App.get().startGoods(goodsData);
        });

        tipsTextView.setOnClickListener(view -> App.get().start(ARoutePath.MAIN_BROWSER, Constant.DATA_URL, Constant.URL_PINGOU_DETAIL));

        inviteTextView.setOnClickListener(view -> {
            OnekeyShare onekeyShare = new OnekeyShare();
            onekeyShare.disableSSOWhenAuthorize();
            onekeyShare.setTitleUrl("一起来拼团吧");
            onekeyShare.setImageUrl(bean.getGoodsImageUrl());
            onekeyShare.setTitle("一起来拼团吧");
            onekeyShare.setText("一起来拼团吧");
            onekeyShare.setUrl(Constant.URL_PINGOU_SHARE + data.getPinGouId() + "&buyer_id=" + bean.getBuyerId());
            onekeyShare.show(get());
        });

    }

    @Override
    public void initObserve() {

        vm.getOrderLiveData().observe(this, bean -> {
            OrderPinGouActivity.this.bean = bean;
            ImageHelp.get().displayRadius(bean.getGoodsImageUrl(), mainImageView);
            nameTextView.setText(bean.getGoodsName());
            priceTextView.setText("￥");
            priceTextView.append(bean.getGoodsPrice());
            pricePinGouTextView.setText("￥");
            pricePinGouTextView.append(bean.getPingouPrice());
            peopleTextView.setText(String.format(getString(R.string.pinGouStartNeedSuccessPeople), bean.getNum()));
            timeTextView.init("", ConvertUtil.string2Long(bean.getPingouEndTime()), "剩", "自动结束");
            timeTextView.start(0);
            arrayList.clear();
            arrayList.addAll(bean.getLogList());
            adapter.notifyDataSetChanged();
            if (bean.getPingouEndTime().equals("0")) {
                goTextView.setVisibility(View.GONE);
                peopleTextView.setText(R.string.activityIsEnd);
                timeTextView.setVisibility(View.GONE);
                bottomRelativeLayout.setVisibility(View.GONE);
            }
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.confirmSelection, bean.getReason(), view -> onReturn(false), view -> vm.getOrder(data));
            }
        });

    }

}
