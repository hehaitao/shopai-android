package top.yokey.shopai.order.adapter;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zsdk.bean.OrderBean;

class OrderListStoreAdapter extends RecyclerView.Adapter<OrderListStoreAdapter.ViewHolder> {

    private final ArrayList<OrderBean.OrderListBean> arrayList;
    private final LineDecoration lineDecoration = new LineDecoration(App.get().dp2Px(12), true);
    private OnItemClickListener onItemClickListener = null;

    OrderListStoreAdapter(ArrayList<OrderBean.OrderListBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        OrderBean.OrderListBean bean = arrayList.get(position);
        OrderListGoodsAdapter adapter = new OrderListGoodsAdapter(bean.getExtendOrderGoods());
        App.get().setRecyclerView(holder.mainRecyclerView, adapter);
        holder.mainRecyclerView.removeItemDecoration(lineDecoration);
        holder.mainRecyclerView.addItemDecoration(lineDecoration);
        if (bean.getZengpinList().size() == 0) {
            holder.zengPinLinearLayout.setVisibility(View.GONE);
        } else {
            holder.zengPinLinearLayout.setVisibility(View.VISIBLE);
            holder.zengPinTextView.setText(bean.getZengpinList().get(0).getGoodsName());
            ImageHelp.get().displayRadius(bean.getZengpinList().get(0).getGoodsImageUrl(), holder.zengPinImageView);
        }
        holder.nameTextView.setText(bean.getStoreName());
        holder.descTextView.setText(bean.getStateDesc());
        int count = 0;
        for (int i = 0; i < bean.getExtendOrderGoods().size(); i++) {
            count += Integer.parseInt(bean.getExtendOrderGoods().get(i).getGoodsNum());
        }
        String temp;
        if (!bean.getShippingFee().equals("0.00") && !bean.getPointsNumber().equals("0")) {
            temp = String.format(App.get().getString(R.string.htmlOrderSettlementShippingFeePoint), count + "", bean.getOrderAmount(), bean.getShippingFee(), bean.getPointsNumber(), bean.getPointsMoney());
        } else if (bean.getShippingFee().equals("0.00") && !bean.getPointsNumber().equals("0")) {
            temp = String.format(App.get().getString(R.string.htmlOrderSettlementPoint), count + "", bean.getOrderAmount(), bean.getPointsNumber(), bean.getPointsMoney());
        } else if (!bean.getShippingFee().equals("0.00") && bean.getPointsNumber().equals("0")) {
            temp = String.format(App.get().getString(R.string.htmlOrderSettlementShippingFee), count + "", bean.getOrderAmount(), bean.getShippingFee());
        } else {
            temp = String.format(App.get().getString(R.string.htmlOrderSettlementNormal), count + "", bean.getOrderAmount());
        }
        holder.totalTextView.setText(Html.fromHtml(App.get().handlerHtml(temp, "#EE0000")));
        holder.optionTextView.setVisibility(View.GONE);
        holder.operaTextView.setVisibility(View.VISIBLE);
        switch (bean.getOrderState()) {
            case "0":
                holder.operaTextView.setText(R.string.deleteOrder);
                break;
            case "10":
                holder.operaTextView.setText(R.string.cancelOrder);
                break;
            case "20":
                if (bean.getLockState() == null || bean.getLockState().equals("0")) {
                    holder.operaTextView.setText(R.string.applyRefund);
                } else {
                    holder.descTextView.setText(R.string.refundReturnIng);
                    holder.operaTextView.setText(R.string.orderDetailed);
                }
                break;
            case "30":
                if (bean.getLockState() == null || bean.getLockState().equals("0")) {
                    holder.optionTextView.setVisibility(View.VISIBLE);
                    holder.optionTextView.setText(R.string.seeLogistics);
                    holder.operaTextView.setText(R.string.confirmReceive);
                } else {
                    holder.descTextView.setText(R.string.refundReturnIng);
                    holder.operaTextView.setText(R.string.orderDetailed);
                }
                break;
            case "40":
                if (bean.isIfEvaluation()) {
                    holder.optionTextView.setVisibility(View.VISIBLE);
                    holder.optionTextView.setText(R.string.deleteOrder);
                    holder.operaTextView.setText(R.string.orderEvaluate);
                }
                if (bean.isIfEvaluationAgain()) {
                    holder.optionTextView.setVisibility(View.VISIBLE);
                    holder.optionTextView.setText(R.string.deleteOrder);
                    holder.operaTextView.setText(R.string.appendEvaluate);
                }
                if (!bean.isIfEvaluation() && !bean.isIfEvaluationAgain()) {
                    holder.optionTextView.setVisibility(View.VISIBLE);
                    holder.optionTextView.setText(R.string.deleteOrder);
                    if (bean.isIfDeliver()) {
                        holder.operaTextView.setText(R.string.seeLogistics);
                    } else {
                        holder.operaTextView.setText(R.string.orderDetailed);
                    }
                }
                break;
        }

        if (bean.getPinGouInfo() != null && bean.getOrderState().equals("20")) {
            holder.optionTextView.setVisibility(View.GONE);
            holder.operaTextView.setVisibility(View.VISIBLE);
            holder.operaTextView.setText(R.string.pinGouDetail);
        }

        holder.optionTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onOption(position, bean);
            }
        });

        holder.operaTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onOpera(position, bean);
            }
        });

        holder.mainLinearLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

        adapter.setOnItemClickListener((position1, bean1) -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickGoods(position, position1, bean1);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_order_list_store, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, OrderBean.OrderListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onOption(int position, OrderBean.OrderListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onOpera(int position, OrderBean.OrderListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickGoods(int position, int itemPosition, OrderBean.OrderListBean.ExtendOrderGoodsBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayoutCompat mainLinearLayout;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView descTextView;
        private final RecyclerView mainRecyclerView;
        private final LinearLayoutCompat zengPinLinearLayout;
        private final AppCompatTextView zengPinTextView;
        private final AppCompatImageView zengPinImageView;
        private final AppCompatTextView totalTextView;
        private final AppCompatTextView optionTextView;
        private final AppCompatTextView operaTextView;

        private ViewHolder(View view) {

            super(view);
            mainLinearLayout = view.findViewById(R.id.mainLinearLayout);
            nameTextView = view.findViewById(R.id.nameTextView);
            descTextView = view.findViewById(R.id.descTextView);
            mainRecyclerView = view.findViewById(R.id.mainRecyclerView);
            zengPinLinearLayout = view.findViewById(R.id.zengPinLinearLayout);
            zengPinTextView = view.findViewById(R.id.zengPinTextView);
            zengPinImageView = view.findViewById(R.id.zengPinImageView);
            totalTextView = view.findViewById(R.id.totalTextView);
            optionTextView = view.findViewById(R.id.optionTextView);
            operaTextView = view.findViewById(R.id.operaTextView);

        }

    }

}
