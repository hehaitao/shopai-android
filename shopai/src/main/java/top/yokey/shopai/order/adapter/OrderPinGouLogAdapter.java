package top.yokey.shopai.order.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zsdk.bean.OrderPinGouBean;

public class OrderPinGouLogAdapter extends RecyclerView.Adapter<OrderPinGouLogAdapter.ViewHolder> {

    private final ArrayList<OrderPinGouBean.LogListBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public OrderPinGouLogAdapter(ArrayList<OrderPinGouBean.LogListBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        OrderPinGouBean.LogListBean bean = arrayList.get(position);

        ImageHelp.get().displayRadius(bean.getAvatar(), holder.mainImageView);
        holder.nameTextView.setText(bean.getBuyerName());
        holder.timeTextView.setText(bean.getTimeText());
        holder.timeTextView.append(" ");
        holder.timeTextView.append(bean.getTypeText());

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_order_pin_gou_log, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, OrderPinGouBean.LogListBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatImageView mainImageView;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView timeTextView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainImageView = view.findViewById(R.id.mainImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            timeTextView = view.findViewById(R.id.timeTextView);

        }

    }

}
