package top.yokey.shopai.order.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.OrderInfoBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberOrderController;

public class OrderDetailedVM extends BaseViewModel {

    private final MutableLiveData<OrderInfoBean> infoLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> currentLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> cancelLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> deleteLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> receiveLiveData = new MutableLiveData<>();

    public OrderDetailedVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<OrderInfoBean> getInfoLiveData() {

        return infoLiveData;

    }

    public MutableLiveData<String> getCurrentLiveData() {

        return currentLiveData;

    }

    public MutableLiveData<String> getCancelLiveData() {

        return cancelLiveData;

    }

    public MutableLiveData<String> getDeleteLiveData() {

        return deleteLiveData;

    }

    public MutableLiveData<String> getReceiveLiveData() {

        return receiveLiveData;

    }

    public void getInfo(String orderId) {

        MemberOrderController.orderInfo(orderId, new HttpCallBack<OrderInfoBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, OrderInfoBean bean) {
                infoLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getCurrent(String orderId) {

        MemberOrderController.getCurrentDeliver(orderId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                currentLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void cancelOrder(String orderId) {

        MemberOrderController.orderCancel(orderId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                cancelLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

    public void deleteOrder(String orderId) {

        MemberOrderController.orderDelete(orderId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                deleteLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(4, reason));
            }
        });

    }

    public void receiveOrder(String orderId) {

        MemberOrderController.orderReceive(orderId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                receiveLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(5, reason));
            }
        });

    }

}
