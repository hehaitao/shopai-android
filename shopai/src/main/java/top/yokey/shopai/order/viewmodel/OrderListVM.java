package top.yokey.shopai.order.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.OrderBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberOrderController;

public class OrderListVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<OrderBean>> orderLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> cancelLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> deleteLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> receiveLiveData = new MutableLiveData<>();

    public OrderListVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<OrderBean>> getOrderLiveData() {

        return orderLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public MutableLiveData<String> getCancelLiveData() {

        return cancelLiveData;

    }

    public MutableLiveData<String> getDeleteLiveData() {

        return deleteLiveData;

    }

    public MutableLiveData<String> getReceiveLiveData() {

        return receiveLiveData;

    }

    public void getOrderList(String type, String keyword, String page) {

        MemberOrderController.orderList(type, keyword, page, new HttpCallBack<ArrayList<OrderBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<OrderBean> list) {
                orderLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void cancelOrder(String orderId) {

        MemberOrderController.orderCancel(orderId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                cancelLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void deleteOrder(String orderId) {

        MemberOrderController.orderDelete(orderId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                deleteLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

    public void receiveOrder(String orderId) {

        MemberOrderController.orderReceive(orderId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                receiveLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(4, reason));
            }
        });

    }

}
