package top.yokey.shopai.point.activity;

import android.graphics.Paint;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.point.viewmodel.PointVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.help.LoggerHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.AnimUtil;
import top.yokey.shopai.zcom.util.ConvertUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;

@Route(path = ARoutePath.POINT_POINT)
public class PointActivity extends BaseActivity {

    @Autowired(name = Constant.DATA_ID)
    String id = "";
    private Toolbar mainToolbar = null;
    private AppCompatImageView mainImageView = null;
    private AppCompatTextView nameTextView = null;
    private AppCompatTextView pointTextView = null;
    private AppCompatTextView priceTextView = null;
    private AppCompatTextView stockTextView = null;
    private WebView mainWebView = null;
    private AppCompatTextView exchangeTextView = null;
    private AppCompatTextView nightTextView = null;
    private RelativeLayout chooseRelativeLayout = null;
    private AppCompatImageView chooseImageView = null;
    private AppCompatTextView chooseNameTextView = null;
    private AppCompatTextView choosePointTextView = null;
    private AppCompatTextView chooseStorageTextView = null;
    private AppCompatTextView chooseAddTextView = null;
    private AppCompatEditText chooseNumberEditText = null;
    private AppCompatTextView chooseSubTextView = null;
    private PointVM pointVM = null;
    private boolean isAnim = false;
    private String goodsStorage = "";

    @Override
    public void initView() {

        setContentView(R.layout.activity_point);
        mainToolbar = findViewById(R.id.mainToolbar);
        mainImageView = findViewById(R.id.mainImageView);
        nameTextView = findViewById(R.id.nameTextView);
        pointTextView = findViewById(R.id.pointTextView);
        priceTextView = findViewById(R.id.priceTextView);
        stockTextView = findViewById(R.id.stockTextView);
        mainWebView = findViewById(R.id.mainWebView);
        exchangeTextView = findViewById(R.id.exchangeTextView);
        nightTextView = findViewById(R.id.nightTextView);
        chooseRelativeLayout = findViewById(R.id.chooseRelativeLayout);
        chooseImageView = findViewById(R.id.chooseImageView);
        chooseNameTextView = findViewById(R.id.chooseNameTextView);
        choosePointTextView = findViewById(R.id.choosePointTextView);
        chooseStorageTextView = findViewById(R.id.chooseStorageTextView);
        chooseAddTextView = findViewById(R.id.chooseAddTextView);
        chooseNumberEditText = findViewById(R.id.chooseNumberEditText);
        chooseSubTextView = findViewById(R.id.chooseSubTextView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.goodsDetailed);
        observeKeyborad(R.id.mainRelativeLayout);
        App.get().setWebView(mainWebView);
        LinearLayoutCompat.LayoutParams layoutParams = (LinearLayoutCompat.LayoutParams) mainImageView.getLayoutParams();
        layoutParams.height = App.get().getWidth();
        mainImageView.setLayoutParams(layoutParams);
        priceTextView.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        pointVM = getVM(PointVM.class);
        pointVM.getGoods(id);

    }

    @Override
    public void initEvent() {

        exchangeTextView.setOnClickListener(view -> {
            if (goodsStorage.equals("0")) {
                ToastHelp.get().show(R.string.noStock);
                return;
            }
            if (chooseRelativeLayout.getVisibility() == View.GONE) {
                showChooseLayout();
                return;
            }
            pointVM.addCart(id, Objects.requireNonNull(chooseNumberEditText.getText()).toString());
        });

        nightTextView.setOnClickListener(view -> goneChooseLayout());

        chooseRelativeLayout.setOnClickListener(view -> LoggerHelp.get().show("click"));

        chooseSubTextView.setOnClickListener(view -> changeNumber(ConvertUtil.string2Int(Objects.requireNonNull(chooseNumberEditText.getText()).toString()) - 1));

        chooseAddTextView.setOnClickListener(view -> changeNumber(ConvertUtil.string2Int(Objects.requireNonNull(chooseNumberEditText.getText()).toString()) + 1));

    }

    @Override
    public void initObserve() {

        pointVM.getGoodsLiveData().observe(this, bean -> {
            goodsStorage = bean.getPgoodsStorage();
            ImageHelp.get().display(bean.getPgoodsImageMax(), mainImageView);
            ImageHelp.get().display(bean.getPgoodsImageMax(), chooseImageView);
            nameTextView.setText(bean.getPgoodsName());
            chooseNameTextView.setText(bean.getPgoodsName());
            pointTextView.setText(R.string.exchangePoint);
            pointTextView.append("：" + bean.getPgoodsPoints());
            choosePointTextView.setText(pointTextView.getText());
            priceTextView.setText("￥");
            priceTextView.append(bean.getPgoodsPrice());
            stockTextView.setText(R.string.stock);
            stockTextView.append("：" + bean.getPgoodsStorage());
            chooseStorageTextView.setText(stockTextView.getText());
            App.get().loadHtml(mainWebView, bean.getPgoodsBody());
        });

        pointVM.getAddCartLiveData().observe(this, string -> {
            goneChooseLayout();
            App.get().start(ARoutePath.POINT_BUY);
        });

        pointVM.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> pointVM.getGoods(id));
                return;
            }
            ToastHelp.get().show(bean.getReason());
        });

    }

    //自定义方法

    private void goneChooseLayout() {

        if (isAnim) {
            return;
        }
        if (nightTextView.getVisibility() == View.VISIBLE) {
            isAnim = true;
            AnimUtil.objectAnimator(nightTextView, AnimUtil.ALPHA, () -> {
                isAnim = false;
                nightTextView.setVisibility(View.GONE);
            }, 1.0f, 0);
        }
        if (chooseRelativeLayout.getVisibility() == View.VISIBLE) {
            isAnim = true;
            AnimUtil.objectAnimator(chooseRelativeLayout, AnimUtil.TRABSLATION_Y, () -> {
                isAnim = false;
                chooseRelativeLayout.setVisibility(View.GONE);
            }, 0, App.get().getHeight());
        }

    }

    private void showChooseLayout() {

        if (isAnim) {
            return;
        }
        if (nightTextView.getVisibility() == View.GONE) {
            isAnim = true;
            nightTextView.setVisibility(View.VISIBLE);
            AnimUtil.objectAnimator(nightTextView, AnimUtil.ALPHA, () -> isAnim = false, 0, 1.0f);
        }
        if (chooseRelativeLayout.getVisibility() == View.GONE) {
            isAnim = true;
            chooseRelativeLayout.setVisibility(View.VISIBLE);
            AnimUtil.objectAnimator(chooseRelativeLayout, AnimUtil.TRABSLATION_Y, () -> isAnim = false, App.get().getHeight(), 0);
        }

    }

    private void changeNumber(int number) {

        String temp;
        if (number <= 0) {
            chooseNumberEditText.setText("1");
            chooseNumberEditText.setSelection(1);
            return;
        }
        if (!VerifyUtil.isEmpty(goodsStorage) && !goodsStorage.equals("0")) {
            int storage = ConvertUtil.string2Int(goodsStorage);
            if (number > storage) {
                temp = storage + "";
                chooseNumberEditText.setText(temp);
                chooseNumberEditText.setSelection(temp.length());
                return;
            }
        }
        temp = number + "";
        chooseNumberEditText.setText(temp);
        chooseNumberEditText.setSelection(temp.length());

    }

}
