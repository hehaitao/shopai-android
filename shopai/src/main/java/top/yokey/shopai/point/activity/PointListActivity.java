package top.yokey.shopai.point.activity;

import android.view.View;
import android.view.inputmethod.EditorInfo;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;
import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.point.adapter.PointListAdapter;
import top.yokey.shopai.point.viewmodel.PointListVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.recycler.SpaceDecoration;
import top.yokey.shopai.zcom.util.AnimUtil;
import top.yokey.shopai.zcom.util.ConvertUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zcom.voice.XFVoiceDialog;
import top.yokey.shopai.zsdk.bean.PointGoodsListBean;
import top.yokey.shopai.zsdk.data.PointSearchData;

@Route(path = ARoutePath.POINT_LIST)
public class PointListActivity extends BaseActivity {

    private final PointSearchData data = new PointSearchData();
    private final ArrayList<PointGoodsListBean> arrayList = new ArrayList<>();
    private final PointListAdapter adapter = new PointListAdapter(arrayList, true);
    private final LineDecoration lineDecoration = new LineDecoration(1, App.get().getColors(R.color.divider), false);
    private final SpaceDecoration spaceDecoration = new SpaceDecoration(App.get().dp2Px(4));
    private Toolbar mainToolbar = null;
    private AppCompatEditText toolbarEditText = null;
    private AppCompatImageView voiceImageView = null;
    private AppCompatImageView toolbarImageView = null;
    private AppCompatTextView screenOrderTextView = null;
    private AppCompatTextView screenICanExchangeTextView = null;
    private AppCompatTextView screenScreenTextView = null;
    private AppCompatImageView screenOrientationImageView = null;
    private AppCompatTextView screenNightTextView = null;
    private LinearLayoutCompat screenOrderLinearLayout = null;
    private AppCompatTextView screenOrderDefaultTextView = null;
    private AppCompatTextView screenOrderHighTextView = null;
    private AppCompatTextView screenOrderLowTextView = null;
    private AppCompatTextView screenOrderNewTextView = null;
    private LinearLayoutCompat screenConditionLinearLayout = null;
    private AppCompatEditText screenPointFromEditText = null;
    private AppCompatEditText screenPointToEditText = null;
    private AppCompatTextView screenConfirmTextView = null;
    private AppCompatTextView screenResetTextView = null;
    private PullRefreshView mainPullRefreshView = null;
    private boolean isGrid = true;
    private PointListVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_point_list);
        mainToolbar = findViewById(R.id.mainToolbar);
        toolbarEditText = findViewById(R.id.toolbarEditText);
        voiceImageView = findViewById(R.id.voiceImageView);
        toolbarImageView = findViewById(R.id.toolbarImageView);
        screenOrderTextView = findViewById(R.id.screenOrderTextView);
        screenICanExchangeTextView = findViewById(R.id.screenICanExchangeTextView);
        screenScreenTextView = findViewById(R.id.screenScreenTextView);
        screenOrientationImageView = findViewById(R.id.screenOrientationImageView);
        screenNightTextView = findViewById(R.id.screenNightTextView);
        screenOrderLinearLayout = findViewById(R.id.screenOrderLinearLayout);
        screenOrderDefaultTextView = findViewById(R.id.screenOrderDefaultTextView);
        screenOrderHighTextView = findViewById(R.id.screenOrderHighTextView);
        screenOrderLowTextView = findViewById(R.id.screenOrderLowTextView);
        screenOrderNewTextView = findViewById(R.id.screenOrderNewTextView);
        screenConditionLinearLayout = findViewById(R.id.screenConditionLinearLayout);
        screenPointFromEditText = findViewById(R.id.screenPointFromEditText);
        screenPointToEditText = findViewById(R.id.screenPointToEditText);
        screenConfirmTextView = findViewById(R.id.screenConfirmTextView);
        screenResetTextView = findViewById(R.id.screenResetTextView);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar);
        observeKeyborad(R.id.mainRelativeLayout);
        toolbarEditText.setText(data.getKeyword());
        toolbarEditText.setSelection(VerifyUtil.isEmail(data.getKeyword()) ? 0 : data.getKeyword().length());
        toolbarEditText.setHint(R.string.pleaseInputKeyword);
        vm = getVM(PointListVM.class);
        data.setPage("1");
        setGridModel();
        getData();

    }

    @Override
    public void initEvent() {

        toolbarEditText.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                data.setKeyword(Objects.requireNonNull(toolbarEditText.getText()).toString());
                data.setPage("1");
                hideKeyboard();
                getData();
            }
            return false;
        });

        voiceImageView.setOnClickListener(view -> XFVoiceDialog.get().show(get(), result -> {
            toolbarEditText.setText(result);
            toolbarEditText.setSelection(result.length());
            data.setKeyword(result);
            data.setPage("1");
            getData();
        }));

        toolbarImageView.setOnClickListener(view -> {
            data.setKeyword(Objects.requireNonNull(toolbarEditText.getText()).toString());
            data.setPage("1");
            hideKeyboard();
            getData();
        });

        screenOrderTextView.setOnClickListener(view -> {
            screenConditionLinearLayout.setVisibility(View.GONE);
            screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
            if (screenOrderLinearLayout.getVisibility() == View.VISIBLE) {
                screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
                goneScreenCondition();
            } else {
                screenNightTextView.setVisibility(View.VISIBLE);
                screenOrderLinearLayout.setVisibility(View.VISIBLE);
                screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_up, 0);
                AnimUtil.objectAnimator(screenOrderLinearLayout, AnimUtil.TRABSLATION_Y, -screenOrderLinearLayout.getHeight(), 0);
                AnimUtil.objectAnimator(screenNightTextView, AnimUtil.ALPHA, 0, 1.0f);
            }
        });

        screenICanExchangeTextView.setOnClickListener(view -> order("2", "1"));

        screenScreenTextView.setOnClickListener(view -> {
            screenOrderLinearLayout.setVisibility(View.GONE);
            screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
            if (screenConditionLinearLayout.getVisibility() == View.VISIBLE) {
                screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
                goneScreenCondition();
            } else {
                screenNightTextView.setVisibility(View.VISIBLE);
                screenConditionLinearLayout.setVisibility(View.VISIBLE);
                screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_up, 0);
                AnimUtil.objectAnimator(screenConditionLinearLayout, AnimUtil.TRABSLATION_X, App.get().getWidth(), 0);
                AnimUtil.objectAnimator(screenNightTextView, AnimUtil.ALPHA, 0, 1.0f);
            }
        });

        screenOrientationImageView.setOnClickListener(view -> {
            if (isGrid) {
                setVerModel();
                return;
            }
            setGridModel();
        });

        screenNightTextView.setOnClickListener(view -> goneScreenCondition());

        screenOrderDefaultTextView.setOnClickListener(view -> {
            screenOrderDefaultTextView.setTextColor(App.get().getColors(R.color.accent));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderNewTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderTextView.setText(R.string.synthesizeSort);
            order("", "");
        });

        screenOrderHighTextView.setOnClickListener(view -> {
            screenOrderDefaultTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.accent));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderNewTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderTextView.setText(R.string.priceHighToLow);
            order("pointsdesc", "");
        });

        screenOrderLowTextView.setOnClickListener(view -> {
            screenOrderDefaultTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.accent));
            screenOrderNewTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderTextView.setText(R.string.priceLowToHigh);
            order("pointsasc", "");
        });

        screenOrderNewTextView.setOnClickListener(view -> {
            screenOrderDefaultTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderNewTextView.setTextColor(App.get().getColors(R.color.accent));
            screenOrderTextView.setText(R.string.popularitySort);
            order("stimedesc", "");
        });

        screenConfirmTextView.setOnClickListener(view -> {
            screenOrderDefaultTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderNewTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderTextView.setText(R.string.synthesizeSort);
            order("", "");
        });

        screenResetTextView.setOnClickListener(view -> {
            data.setPointsMin("");
            data.setPointsMax("");
            screenPointFromEditText.setText("");
            screenPointToEditText.setText("");
            ToastHelp.get().show(R.string.success);
        });

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || arrayList.size() == 0) {
                data.setPage("1");
                getData();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                data.setPage("1");
                getData();
            }

            @Override
            public void onLoadMore() {
                getData();
            }
        });

    }

    @Override
    public void initObserve() {

        vm.getGoodsLiveData().observe(this, list -> {
            if (data.getPage().equals("1")) arrayList.clear();
            data.setPage((ConvertUtil.string2Int(data.getPage()) + 1) + "");
            arrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getHasMoreLiveData().observe(this, bool -> mainPullRefreshView.setCanLoadMore(bool));

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                if (arrayList.size() == 0) {
                    mainPullRefreshView.setError(bean.getReason());
                } else {
                    ToastHelp.get().show(bean.getReason());
                }
                return;
            }
            ToastHelp.get().show(bean.getReason());
        });

    }

    @Override
    public void onReturn(boolean handler) {

        if (screenNightTextView.getVisibility() == View.VISIBLE) {
            goneScreenCondition();
            return;
        }
        if (handler && isShowKeyboard()) {
            hideKeyboard();
            return;
        }
        finish();

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView.setLoad();
        vm.getList(data);

    }

    private void setVerModel() {

        isGrid = false;
        screenOrientationImageView.setImageResource(R.mipmap.ic_orientation_ver);
        mainPullRefreshView.setLayoutManager(new LinearLayoutManager(get()));
        mainPullRefreshView.getRecyclerView().setPadding(0, 0, 0, 0);
        mainPullRefreshView.removeItemDecoration(lineDecoration);
        mainPullRefreshView.removeItemDecoration(spaceDecoration);
        mainPullRefreshView.setItemDecoration(lineDecoration);
        adapter.setGrid(isGrid);
        mainPullRefreshView.setAdapter(adapter);
        mainPullRefreshView.setComplete();
        adapter.setOnItemClickListener((position, bean) -> App.get().start(ARoutePath.POINT_POINT, Constant.DATA_ID, bean.getPgoodsId()));

    }

    private void setGridModel() {

        isGrid = true;
        screenOrientationImageView.setImageResource(R.mipmap.ic_orientation_grid);
        mainPullRefreshView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        mainPullRefreshView.getRecyclerView().setPadding(App.get().dp2Px(4), 0, App.get().dp2Px(4), 0);
        mainPullRefreshView.removeItemDecoration(lineDecoration);
        mainPullRefreshView.removeItemDecoration(spaceDecoration);
        mainPullRefreshView.setItemDecoration(spaceDecoration);
        adapter.setGrid(isGrid);
        mainPullRefreshView.setAdapter(adapter);
        mainPullRefreshView.setComplete();
        adapter.setOnItemClickListener((position, bean) -> App.get().start(ARoutePath.POINT_POINT, Constant.DATA_ID, bean.getPgoodsId()));

    }

    private void goneScreenCondition() {

        if (screenNightTextView.getVisibility() == View.VISIBLE) {
            AnimUtil.objectAnimator(screenNightTextView, AnimUtil.ALPHA, () -> screenNightTextView.setVisibility(View.GONE), 1.0f, 0);
        }
        if (screenOrderLinearLayout.getVisibility() == View.VISIBLE) {
            screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
            AnimUtil.objectAnimator(screenOrderLinearLayout, AnimUtil.TRABSLATION_Y, () -> screenOrderLinearLayout.setVisibility(View.GONE), 0, -screenOrderLinearLayout.getHeight());
        }
        if (screenConditionLinearLayout.getVisibility() == View.VISIBLE) {
            screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
            AnimUtil.objectAnimator(screenConditionLinearLayout, AnimUtil.TRABSLATION_X, () -> screenConditionLinearLayout.setVisibility(View.GONE), 0, App.get().getWidth());
        }

    }

    private void order(String orderBy, String isAble) {

        hideKeyboard();
        goneScreenCondition();
        data.setPage("1");
        data.setOrderby(orderBy);
        data.setIsable(isAble);
        data.setPointsMin(Objects.requireNonNull(screenPointToEditText.getText()).toString());
        data.setPointsMax(Objects.requireNonNull(screenPointFromEditText.getText()).toString());
        screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
        screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
        if (VerifyUtil.isEmpty(isAble)) {
            screenOrderTextView.setTextColor(App.get().getColors(R.color.accent));
            screenICanExchangeTextView.setTextColor(App.get().getColors(R.color.textTwo));
        } else {
            screenOrderTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenICanExchangeTextView.setTextColor(App.get().getColors(R.color.accent));
            screenOrderDefaultTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderNewTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderTextView.setText(R.string.defaultSort);
        }
        if (!VerifyUtil.isEmpty(data.getPointsMin()) || !VerifyUtil.isEmpty(data.getPointsMax())) {
            screenScreenTextView.setTextColor(App.get().getColors(R.color.accentDark));
        } else {
            screenScreenTextView.setTextColor(App.get().getColors(R.color.textTwo));
        }
        getData();

    }

}
