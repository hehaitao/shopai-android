package top.yokey.shopai.point.adapter;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zsdk.bean.PointOrderListBean;

public class PointsOrderListAdapter extends RecyclerView.Adapter<PointsOrderListAdapter.ViewHolder> {

    private final ArrayList<PointOrderListBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public PointsOrderListAdapter(ArrayList<PointOrderListBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final int positionInt = position;
        final PointOrderListBean bean = arrayList.get(position);

        holder.snTextView.setText(String.format(App.get().getString(R.string.orderPaySn), bean.getPointOrdersn()));
        holder.descTextView.setText(bean.getPointOrderstatetext());
        ImageHelp.get().displayRadius(bean.getProdlist().get(0).getPointGoodsimage(), holder.goodsImageView);
        holder.nameTextView.setText(bean.getProdlist().get(0).getPointGoodsname());
        holder.pointTextView.setText(R.string.exchangePoint);
        holder.pointTextView.append("：" + bean.getProdlist().get(0).getPointGoodspoints());
        holder.numberTextView.setText("x");
        holder.numberTextView.append(bean.getProdlist().get(0).getPointGoodsnum());
        String temp = String.format(App.get().getString(R.string.htmlPointOrderSettlement), bean.getProdlist().size() + "", bean.getPointAllpoint());
        holder.totalTextView.setText(Html.fromHtml(App.get().handlerHtml(temp, "#EE0000")));
        switch (bean.getPointOrderstate()) {
            case "2":
                holder.operaTextView.setText(R.string.orderDetailed);
                break;
            case "20":
                holder.operaTextView.setText(R.string.cancelExchange);
                break;
            case "30":
                holder.operaTextView.setText(R.string.confirmReceive);
                break;
            case "40":
                holder.operaTextView.setText(R.string.orderDetailed);
                break;
        }

        holder.operaTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onOpera(positionInt, bean);
            }
        });

        holder.mainLinearLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(positionInt, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_point_order_list, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, PointOrderListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onOpera(int position, PointOrderListBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayoutCompat mainLinearLayout;
        private final AppCompatTextView snTextView;
        private final AppCompatTextView descTextView;
        private final AppCompatImageView goodsImageView;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView pointTextView;
        private final AppCompatTextView numberTextView;
        private final AppCompatTextView totalTextView;
        private final AppCompatTextView operaTextView;

        private ViewHolder(View view) {

            super(view);
            mainLinearLayout = view.findViewById(R.id.mainLinearLayout);
            snTextView = view.findViewById(R.id.snTextView);
            descTextView = view.findViewById(R.id.descTextView);
            goodsImageView = view.findViewById(R.id.goodsImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            pointTextView = view.findViewById(R.id.pointTextView);
            numberTextView = view.findViewById(R.id.numberTextView);
            totalTextView = view.findViewById(R.id.totalTextView);
            operaTextView = view.findViewById(R.id.operaTextView);

        }

    }

}
