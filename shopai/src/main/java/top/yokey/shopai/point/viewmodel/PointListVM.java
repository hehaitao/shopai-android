package top.yokey.shopai.point.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.PointGoodsListBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.PointprodController;
import top.yokey.shopai.zsdk.data.PointSearchData;

public class PointListVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<PointGoodsListBean>> goodsLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();

    public PointListVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<PointGoodsListBean>> getGoodsLiveData() {

        return goodsLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public void getList(PointSearchData data) {

        PointprodController.index(data, new HttpCallBack<ArrayList<PointGoodsListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<PointGoodsListBean> list) {
                goodsLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
