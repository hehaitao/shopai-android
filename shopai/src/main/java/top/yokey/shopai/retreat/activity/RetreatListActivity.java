package top.yokey.shopai.retreat.activity;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.retreat.adapter.RetreatRefundListAdapter;
import top.yokey.shopai.retreat.adapter.RetreatReturnListAdapter;
import top.yokey.shopai.retreat.viewmodel.RetreatListVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.RetreatRefundListBean;
import top.yokey.shopai.zsdk.bean.RetreatReturnListBean;
import top.yokey.shopai.zsdk.data.GoodsData;

@Route(path = ARoutePath.RETREAT_LIST)
public class RetreatListActivity extends BaseActivity {

    private final ArrayList<RetreatRefundListBean> arrayList = new ArrayList<>();
    private final RetreatRefundListAdapter adapter = new RetreatRefundListAdapter(arrayList);
    private final ArrayList<RetreatReturnListBean> returnArrayList = new ArrayList<>();
    private final RetreatReturnListAdapter returnAdapter = new RetreatReturnListAdapter(returnArrayList);
    private Toolbar mainToolbar = null;
    private AppCompatTextView leftTextView = null;
    private AppCompatTextView rightTextView = null;
    private PullRefreshView mainPullRefreshView = null;
    private int page = 1;
    private boolean isRefund = true;
    private RetreatListVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_switch_refresh_view);
        mainToolbar = findViewById(R.id.mainToolbar);
        leftTextView = findViewById(R.id.leftTextView);
        rightTextView = findViewById(R.id.rightTextView);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar);
        observeKeyborad(R.id.mainLinearLayout);
        leftTextView.setText(R.string.refund);
        rightTextView.setText(R.string.returnGoods);
        mainPullRefreshView.setItemDecoration(new LineDecoration(App.get().dp2Px(16), true));
        vm = getVM(RetreatListVM.class);
        clickLeft();

    }

    @Override
    public void initEvent() {

        leftTextView.setOnClickListener(view -> clickLeft());

        rightTextView.setOnClickListener(view -> clickRight());

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || arrayList.size() == 0) {
                page = 1;
                getData();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getData();
            }

            @Override
            public void onLoadMore() {
                getData();
            }
        });

        adapter.setOnItemClickListener(new RetreatRefundListAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position, RetreatRefundListBean bean) {
                App.get().start(ARoutePath.RETREAT_REFUND_INFO, Constant.DATA_ID, bean.getRefundId());
            }

            @Override
            public void onClickGoods(int position, int itemPosition, RetreatRefundListBean bean, RetreatRefundListBean.GoodsListBean bean1) {
                App.get().startGoods(new GoodsData(bean1.getGoodsId()));
            }
        });

        returnAdapter.setOnItemClickListener(new RetreatReturnListAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position, RetreatReturnListBean bean) {
                App.get().start(ARoutePath.RETREAT_RETURN_INFO, Constant.DATA_ID, bean.getRefundId());
            }

            @Override
            public void onOpera(int position, RetreatReturnListBean bean) {
                if (bean.getShipState().equals("1")) {
                    App.get().start(ARoutePath.RETREAT_RETURN_DELIVER, Constant.DATA_ID, bean.getRefundId());
                } else {
                    App.get().start(ARoutePath.RETREAT_RETURN_INFO, Constant.DATA_ID, bean.getRefundId());
                }
            }

            @Override
            public void onClickGoods(int position, RetreatReturnListBean bean) {
                App.get().startGoods(new GoodsData(bean.getGoodsId()));
            }
        });

    }

    @Override
    public void initObserve() {

        vm.getRefundLiveData().observe(this, list -> {
            if (page == 1) arrayList.clear();
            page = page + 1;
            arrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getReturnLiveData().observe(this, list -> {
            if (page == 1) returnArrayList.clear();
            page = page + 1;
            returnArrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getHasMoreLiveData().observe(this, bool -> mainPullRefreshView.setCanLoadMore(bool));

        vm.getErrorLiveData().observe(this, bean -> {
            if (arrayList.size() == 0) {
                mainPullRefreshView.setError(bean.getReason());
            } else {
                ToastHelp.get().show(bean.getReason());
            }
        });

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView.setLoad();
        if (isRefund) {
            vm.getRefund(page + "");
            return;
        }
        vm.getReturn(page + "");

    }

    private void clickLeft() {

        leftTextView.setTextColor(App.get().getColors(R.color.primary));
        leftTextView.setBackgroundResource(R.drawable.selector_switch_left_press);
        rightTextView.setTextColor(App.get().getColors(R.color.accent));
        rightTextView.setBackgroundResource(R.drawable.selector_switch_right);
        mainPullRefreshView.setAdapter(adapter);
        isRefund = true;
        page = 1;
        getData();

    }

    private void clickRight() {

        leftTextView.setTextColor(App.get().getColors(R.color.accent));
        leftTextView.setBackgroundResource(R.drawable.selector_switch_left);
        rightTextView.setTextColor(App.get().getColors(R.color.primary));
        rightTextView.setBackgroundResource(R.drawable.selector_switch_right_press);
        mainPullRefreshView.setAdapter(returnAdapter);
        isRefund = false;
        page = 1;
        getData();

    }

}
