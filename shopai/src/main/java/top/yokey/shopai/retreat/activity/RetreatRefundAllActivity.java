package top.yokey.shopai.retreat.activity;

import android.content.Intent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.jeremyliao.liveeventbus.LiveEventBus;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.entity.LocalMedia;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.retreat.adapter.RetreatRefundGoodsAdapter;
import top.yokey.shopai.retreat.viewmodel.RetreatRefundAllVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zsdk.bean.RetreatRefundAllBean;
import top.yokey.shopai.zsdk.data.GoodsData;
import top.yokey.shopai.zsdk.data.RetreatData;

@Route(path = ARoutePath.RETREAT_REFUND_ALL)
public class RetreatRefundAllActivity extends BaseActivity {

    private final AppCompatImageView[] picImageView = new AppCompatImageView[3];
    private final ArrayList<RetreatRefundAllBean.GoodsListBean> arrayList = new ArrayList<>();
    private final RetreatRefundGoodsAdapter adapter = new RetreatRefundGoodsAdapter(arrayList);
    @Autowired(name = Constant.DATA_JSON)
    String json;
    private Toolbar mainToolbar;
    private AppCompatTextView nameTextView;
    private RecyclerView mainRecyclerView;
    private LinearLayoutCompat zengPinLinearLayout;
    private AppCompatTextView zengPinTextView;
    private AppCompatImageView zengPinImageView;
    private AppCompatTextView priceTextView;
    private AppCompatEditText messageEditText;
    private AppCompatTextView submitTextView;
    private int position = 0;
    private String storeId = "";
    private RetreatData data = null;
    private RetreatRefundAllVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_retreat_refund_all);
        mainToolbar = findViewById(R.id.mainToolbar);
        nameTextView = findViewById(R.id.nameTextView);
        mainRecyclerView = findViewById(R.id.mainRecyclerView);
        zengPinLinearLayout = findViewById(R.id.zengPinLinearLayout);
        zengPinTextView = findViewById(R.id.zengPinTextView);
        zengPinImageView = findViewById(R.id.zengPinImageView);
        priceTextView = findViewById(R.id.priceTextView);
        messageEditText = findViewById(R.id.messageEditText);
        submitTextView = findViewById(R.id.submitTextView);
        picImageView[0] = findViewById(R.id.zeroImageView);
        picImageView[1] = findViewById(R.id.oneImageView);
        picImageView[2] = findViewById(R.id.twoImageView);

    }

    @Override
    public void initData() {

        if (VerifyUtil.isEmpty(json)) {
            ToastHelp.get().showDataError();
            onReturn(false);
            return;
        }
        position = 0;
        data = JsonUtil.json2Object(json, RetreatData.class);
        vm = getVM(RetreatRefundAllVM.class);
        App.get().setRecyclerView(mainRecyclerView, adapter);
        mainRecyclerView.addItemDecoration(new LineDecoration(App.get().dp2Px(12), true));
        setToolbar(mainToolbar, R.string.orderRefund);
        observeKeyborad(R.id.mainLinearLayout);
        vm.form(data);

    }

    @Override
    public void initEvent() {

        nameTextView.setOnClickListener(view -> App.get().startStore(storeId));

        adapter.setOnItemClickListener((position, goodsListBean) -> App.get().startGoods(new GoodsData(goodsListBean.getGoodsId())));

        picImageView[0].setOnClickListener(view -> {
            position = 0;
            App.get().startAlbum(get(), 1);
        });

        picImageView[1].setOnClickListener(view -> {
            position = 1;
            App.get().startAlbum(get(), 1);
        });

        picImageView[2].setOnClickListener(view -> {
            position = 2;
            App.get().startAlbum(get(), 1);
        });

        submitTextView.setOnClickListener(view -> {
            String message = Objects.requireNonNull(messageEditText.getText()).toString();
            if (VerifyUtil.isEmpty(message)) {
                ToastHelp.get().show(R.string.pleaseInputRefundExplain);
                return;
            }
            data.setBuyerMessage(message);
            submitTextView.setEnabled(false);
            submitTextView.setText(R.string.handlerIng);
            hideKeyboard();
            vm.submit(data);
        });

    }

    @Override
    public void initObserve() {

        vm.getFormLiveData().observe(this, bean -> {
            storeId = bean.getOrder().getStoreId();
            nameTextView.setText(bean.getOrder().getStoreName());
            arrayList.clear();
            arrayList.addAll(bean.getGoodsList());
            adapter.notifyDataSetChanged();
            priceTextView.setText("￥");
            priceTextView.append(bean.getOrder().getAllowRefundAmount());
            if (bean.getGiftList().size() == 0) {
                zengPinLinearLayout.setVisibility(View.GONE);
            } else {
                zengPinLinearLayout.setVisibility(View.VISIBLE);
                zengPinTextView.setText(bean.getGiftList().get(0).getGoodsName());
                zengPinTextView.append(" x" + bean.getGiftList().get(0).getGoodsNum());
                ImageHelp.get().display(bean.getGiftList().get(0).getGoodsImg360(), zengPinImageView);
            }
        });

        vm.getPicLiveData().observe(this, bean -> {
            switch (position) {
                case 0:
                    data.setRefundPic0(bean.getFileName());
                    ImageHelp.get().display(bean.getPic(), picImageView[0]);
                    break;
                case 1:
                    data.setRefundPic1(bean.getFileName());
                    ImageHelp.get().display(bean.getPic(), picImageView[1]);
                    break;
                case 2:
                    data.setRefundPic2(bean.getFileName());
                    ImageHelp.get().display(bean.getPic(), picImageView[2]);
                    break;
            }
        });

        vm.getSubmitLiveData().observe(this, string -> {
            LiveEventBus.get(Constant.DATA_REFRESH).post(true);
            ToastHelp.get().show(R.string.refundSuccess);
            onReturn(false);
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.form(data));
            } else {
                submitTextView.setEnabled(true);
                submitTextView.setText(R.string.submit);
                ToastHelp.get().show(bean.getReason());
            }
        });

    }

    @Override
    public void onActivityResult(int req, int res, @Nullable Intent intent) {

        super.onActivityResult(req, res, intent);
        if (res == RESULT_OK && req == Constant.CODE_ALBUM && intent != null) {
            List<LocalMedia> list = PictureSelector.obtainMultipleResult(intent);
            vm.uploadPic(new File(list.get(0).getCompressPath()));
        }

    }

}
