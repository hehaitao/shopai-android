package top.yokey.shopai.retreat.activity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.Objects;
import java.util.Vector;

import top.yokey.shopai.R;
import top.yokey.shopai.retreat.viewmodel.RetreatReturnDeliverVM;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.VerifyUtil;

@Route(path = ARoutePath.RETREAT_RETURN_DELIVER)
public class RetreatReturnDeliverActivity extends BaseActivity {

    private final Vector<String> idVector = new Vector<>();
    private final Vector<String> nameVector = new Vector<>();
    @Autowired(name = Constant.DATA_ID)
    String returnId;
    private Toolbar mainToolbar;
    private AppCompatSpinner logisticsSpinner;
    private AppCompatEditText numberEditText;
    private AppCompatTextView submitTextView;
    private String logisticsId = "";
    private RetreatReturnDeliverVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_retreat_return_deliver);
        mainToolbar = findViewById(R.id.mainToolbar);
        logisticsSpinner = findViewById(R.id.logisticsSpinner);
        numberEditText = findViewById(R.id.numberEditText);
        submitTextView = findViewById(R.id.submitTextView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.returnDeliver);
        observeKeyborad(R.id.mainLinearLayout);
        vm = getVM(RetreatReturnDeliverVM.class);
        vm.getDeliver(returnId);

    }

    @Override
    public void initEvent() {

        logisticsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                logisticsId = idVector.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        submitTextView.setOnClickListener(view -> {
            String number = Objects.requireNonNull(numberEditText.getText()).toString();
            if (VerifyUtil.isEmpty(number)) {
                ToastHelp.get().show(R.string.pleaseInputWaybillNumber);
                return;
            }
            submitTextView.setEnabled(false);
            submitTextView.setText(R.string.handlerIng);
            vm.post(returnId, logisticsId, number);
        });

    }

    @Override
    public void initObserve() {

        vm.getDeliverLiveData().observe(this, bean -> {
            idVector.clear();
            nameVector.clear();
            for (int i = 0; i < bean.getExpressList().size(); i++) {
                idVector.add(bean.getExpressList().get(i).getExpressId());
                nameVector.add(bean.getExpressList().get(i).getExpressName());
            }
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(get(), R.layout.spinner_simple, nameVector);
            arrayAdapter.setDropDownViewResource(R.layout.spinner_item);
            logisticsSpinner.setAdapter(arrayAdapter);
        });

        vm.getPostLiveData().observe(this, string -> {
            submitTextView.setEnabled(true);
            submitTextView.setText(R.string.submit);
            ToastHelp.get().showSuccess();
            onReturn(false);
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getDeliver(returnId));
                return;
            }
            submitTextView.setEnabled(true);
            submitTextView.setText(R.string.submit);
            ToastHelp.get().show(bean.getReason());
        });

    }

}
