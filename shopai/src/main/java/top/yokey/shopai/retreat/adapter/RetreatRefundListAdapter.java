package top.yokey.shopai.retreat.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zsdk.bean.RetreatRefundListBean;

public class RetreatRefundListAdapter extends RecyclerView.Adapter<RetreatRefundListAdapter.ViewHolder> {

    private final ArrayList<RetreatRefundListBean> arrayList;
    private final LineDecoration lineDecoration = new LineDecoration(App.get().dp2Px(12), true);
    private OnItemClickListener onItemClickListener = null;

    public RetreatRefundListAdapter(ArrayList<RetreatRefundListBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        RetreatRefundListBean bean = arrayList.get(position);
        RetreatRefundListGoodsAdapter adapter = new RetreatRefundListGoodsAdapter(bean.getGoodsList());
        App.get().setRecyclerView(holder.mainRecyclerView, adapter);
        holder.mainRecyclerView.removeItemDecoration(lineDecoration);
        holder.mainRecyclerView.addItemDecoration(lineDecoration);
        holder.nameTextView.setText(bean.getStoreName());
        holder.timeTextView.setText(bean.getAddTime());
        holder.priceTextView.setText("￥");
        holder.priceTextView.append(bean.getRefundAmount());
        if (bean.getAdminState().equals("无")) {
            holder.descTextView.setText(R.string.seller);
            holder.descTextView.append("：" + bean.getSellerState());
        } else {
            holder.descTextView.setText(R.string.platform);
            holder.descTextView.append("：" + bean.getAdminState());
        }

        adapter.setOnItemClickListener((position1, bean1) -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickGoods(position, position1, bean, bean1);
            }
        });

        holder.mainLinearLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_retreat_refund_list, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, RetreatRefundListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickGoods(int position, int itemPosition, RetreatRefundListBean bean, RetreatRefundListBean.GoodsListBean goodsBean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayoutCompat mainLinearLayout;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView descTextView;
        private final RecyclerView mainRecyclerView;
        private final AppCompatTextView timeTextView;
        private final AppCompatTextView priceTextView;

        private ViewHolder(View view) {

            super(view);
            mainLinearLayout = view.findViewById(R.id.mainLinearLayout);
            nameTextView = view.findViewById(R.id.nameTextView);
            descTextView = view.findViewById(R.id.descTextView);
            mainRecyclerView = view.findViewById(R.id.mainRecyclerView);
            timeTextView = view.findViewById(R.id.timeTextView);
            priceTextView = view.findViewById(R.id.priceTextView);

        }

    }

}
