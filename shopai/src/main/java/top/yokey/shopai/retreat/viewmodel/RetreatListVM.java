package top.yokey.shopai.retreat.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.RetreatRefundListBean;
import top.yokey.shopai.zsdk.bean.RetreatReturnListBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberRefundController;
import top.yokey.shopai.zsdk.controller.MemberReturnController;

public class RetreatListVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<RetreatRefundListBean>> refundLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<RetreatReturnListBean>> returnLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();

    public RetreatListVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<RetreatRefundListBean>> getRefundLiveData() {

        return refundLiveData;

    }

    public MutableLiveData<ArrayList<RetreatReturnListBean>> getReturnLiveData() {

        return returnLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public void getRefund(String page) {

        MemberRefundController.getRefundList(page, new HttpCallBack<ArrayList<RetreatRefundListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<RetreatRefundListBean> list) {
                refundLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getReturn(String page) {

        MemberReturnController.getReturnList(page, new HttpCallBack<ArrayList<RetreatReturnListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<RetreatReturnListBean> list) {
                returnLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
