package top.yokey.shopai.retreat.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.io.File;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.RetreatRefundAllBean;
import top.yokey.shopai.zsdk.bean.RetreatUploadPicBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberRefundController;
import top.yokey.shopai.zsdk.data.RetreatData;

public class RetreatRefundAllVM extends BaseViewModel {

    private final MutableLiveData<RetreatRefundAllBean> formLiveData = new MutableLiveData<>();
    private final MutableLiveData<RetreatUploadPicBean> picLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> submitLiveData = new MutableLiveData<>();

    public RetreatRefundAllVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<RetreatRefundAllBean> getFormLiveData() {

        return formLiveData;

    }

    public MutableLiveData<RetreatUploadPicBean> getPicLiveData() {

        return picLiveData;

    }

    public MutableLiveData<String> getSubmitLiveData() {

        return submitLiveData;

    }

    public void form(RetreatData data) {

        MemberRefundController.refundAllForm(data, new HttpCallBack<RetreatRefundAllBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, RetreatRefundAllBean bean) {
                formLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void uploadPic(File file) {

        MemberRefundController.uploadPic(file, new HttpCallBack<RetreatUploadPicBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, RetreatUploadPicBean bean) {
                picLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void submit(RetreatData data) {

        MemberRefundController.refundAllPost(data, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                submitLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

}
