package top.yokey.shopai.retreat.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.io.File;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.RetreatRefundBean;
import top.yokey.shopai.zsdk.bean.RetreatUploadPicBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberRefundController;
import top.yokey.shopai.zsdk.data.RetreatData;

public class RetreatRefundVM extends BaseViewModel {

    private final MutableLiveData<RetreatRefundBean> formLiveData = new MutableLiveData<>();
    private final MutableLiveData<RetreatUploadPicBean> picLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> submitLiveData = new MutableLiveData<>();

    public RetreatRefundVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<RetreatRefundBean> getFormLiveData() {

        return formLiveData;

    }

    public MutableLiveData<RetreatUploadPicBean> getPicLiveData() {

        return picLiveData;

    }

    public MutableLiveData<String> getSubmitLiveData() {

        return submitLiveData;

    }

    public void form(RetreatData data) {

        MemberRefundController.refundForm(data, new HttpCallBack<RetreatRefundBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, RetreatRefundBean bean) {
                formLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void uploadPic(File file) {

        MemberRefundController.uploadPic(file, new HttpCallBack<RetreatUploadPicBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, RetreatUploadPicBean bean) {
                picLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void submit(RetreatData data) {

        MemberRefundController.refundPost(data, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                submitLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

}
