package top.yokey.shopai.seller.activity;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.seller.adapter.SellerExpressAdapter;
import top.yokey.shopai.seller.viewmodel.SellerExpressVM;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.SellerExpressListBean;

@Route(path = ARoutePath.SELLER_EXPRESS)
public class SellerExpressActivity extends BaseActivity {

    private final ArrayList<SellerExpressListBean> arrayList = new ArrayList<>();
    private final SellerExpressAdapter adapter = new SellerExpressAdapter(arrayList);
    private Toolbar mainToolbar = null;
    private AppCompatImageView toolbarImageView = null;
    private PullRefreshView mainPullRefreshView = null;
    private SellerExpressVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_pull_refresh_view);
        mainToolbar = findViewById(R.id.mainToolbar);
        toolbarImageView = findViewById(R.id.toolbarImageView);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.logisticsCompany);
        observeKeyborad(R.id.mainLinearLayout);
        mainPullRefreshView.setAdapter(adapter);
        mainPullRefreshView.setCanLoadMore(false);
        vm = getVM(SellerExpressVM.class);
        getData();

    }

    @Override
    public void initEvent() {

        toolbarImageView.setOnClickListener(view -> {
            StringBuilder ids = new StringBuilder();
            for (int i = 0; i < arrayList.size(); i++) {
                if (arrayList.get(i).getIsChecke().equals(Constant.COMMON_ENABLE)) {
                    ids.append(arrayList.get(i).getId()).append(",");
                }
            }
            if (ids.length() != 0) {
                ids = new StringBuilder(ids.substring(0, ids.length() - 1));
            }
            ToastHelp.get().show(R.string.handlerIng);
            vm.save(ids.toString());
        });

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || arrayList.size() == 0) {
                getData();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }

            @Override
            public void onLoadMore() {
                getData();
            }
        });

        adapter.setOnItemClickListener((position, bean) -> {
            arrayList.get(position).setIsChecke(bean.getIsChecke().equals(Constant.COMMON_ENABLE) ? Constant.COMMON_DISABLE : Constant.COMMON_ENABLE);
            adapter.notifyItemChanged(position);
        });

    }

    @Override
    public void initObserve() {

        vm.getExpressLiveData().observe(this, list -> {
            arrayList.clear();
            arrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getSaveLiveData().observe(this, string -> ToastHelp.get().showSuccess());

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                if (arrayList.size() != 0) {
                    ToastHelp.get().show(bean.getReason());
                } else {
                    mainPullRefreshView.setError(bean.getReason());
                }
                return;
            }
            ToastHelp.get().show(bean.getReason());
        });

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView.setLoad();
        vm.getExpress();

    }

}
