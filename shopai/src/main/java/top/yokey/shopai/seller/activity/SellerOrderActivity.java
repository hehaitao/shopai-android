package top.yokey.shopai.seller.activity;

import android.text.InputType;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.seller.adapter.SellerOrderAdapter;
import top.yokey.shopai.seller.viewmodel.SellerOrderVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.adapter.ViewPagerAdapter;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.util.ConvertUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.OrderSellerListBean;
import top.yokey.shopai.zsdk.data.GoodsData;

@Route(path = ARoutePath.SELLER_ORDER)
public class SellerOrderActivity extends BaseActivity {

    private final int[] page = new int[6];
    private final String[] type = new String[6];
    @SuppressWarnings("unchecked")
    private final ArrayList<OrderSellerListBean>[] arrayList = new ArrayList[6];
    private final SellerOrderAdapter[] adapter = new SellerOrderAdapter[6];
    private final PullRefreshView[] mainPullRefreshView = new PullRefreshView[6];
    @Autowired(name = Constant.DATA_POSITION)
    int position;
    private Toolbar mainToolbar = null;
    private AppCompatEditText toolbarEditText = null;
    private AppCompatImageView toolbarImageView = null;
    private TabLayout mainTabLayout = null;
    private ViewPager mainViewPager = null;
    private String keyword = "";
    private SellerOrderVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_seller_order);
        mainToolbar = findViewById(R.id.mainToolbar);
        toolbarEditText = findViewById(R.id.toolbarEditText);
        toolbarImageView = findViewById(R.id.toolbarImageView);
        mainTabLayout = findViewById(R.id.mainTabLayout);
        mainViewPager = findViewById(R.id.mainViewPager);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar);
        observeKeyborad(R.id.mainLinearLayout);
        position = getIntent().getIntExtra(Constant.DATA_POSITION, 0);
        toolbarEditText.setHint(R.string.pleaseInputOrderSnOrTitle);
        toolbarImageView.setImageResource(R.drawable.ic_action_search);
        type[0] = "";
        type[1] = "state_new";
        type[2] = "state_pay";
        type[3] = "state_send";
        type[4] = "state_success";
        type[5] = "state_cancel";
        List<String> titleList = new ArrayList<>();
        titleList.add(getString(R.string.all));
        titleList.add(getString(R.string.waitPay));
        titleList.add(getString(R.string.waitDeliver));
        titleList.add(getString(R.string.waitReceive));
        titleList.add(getString(R.string.alreadyComplete));
        titleList.add(getString(R.string.alreadyCancel));
        List<View> viewList = new ArrayList<>();
        viewList.add(getLayoutInflater().inflate(R.layout.include_pull_refresh_view, null));
        viewList.add(getLayoutInflater().inflate(R.layout.include_pull_refresh_view, null));
        viewList.add(getLayoutInflater().inflate(R.layout.include_pull_refresh_view, null));
        viewList.add(getLayoutInflater().inflate(R.layout.include_pull_refresh_view, null));
        viewList.add(getLayoutInflater().inflate(R.layout.include_pull_refresh_view, null));
        viewList.add(getLayoutInflater().inflate(R.layout.include_pull_refresh_view, null));
        for (int i = 0; i < viewList.size(); i++) {
            page[i] = 1;
            arrayList[i] = new ArrayList<>();
            adapter[i] = new SellerOrderAdapter(arrayList[i]);
            mainPullRefreshView[i] = viewList.get(i).findViewById(R.id.mainPullRefreshView);
            mainPullRefreshView[i].setItemDecoration(new LineDecoration(App.get().dp2Px(12), true));
            mainTabLayout.addTab(mainTabLayout.newTab().setText(titleList.get(i)));
            mainPullRefreshView[i].setAdapter(adapter[i]);
        }
        App.get().setTabLayout(mainTabLayout, mainViewPager, new ViewPagerAdapter(viewList, titleList));
        mainTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        mainViewPager.setCurrentItem(position);
        vm = getVM(SellerOrderVM.class);
        page[position] = 1;
        getData();

    }

    @Override
    public void initEvent() {

        toolbarImageView.setOnClickListener(view -> {
            keyword = Objects.requireNonNull(toolbarEditText.getText()).toString();
            page[position] = 1;
            hideKeyboard();
            getData();
        });

        toolbarEditText.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                keyword = Objects.requireNonNull(toolbarEditText.getText()).toString();
                page[position] = 1;
                hideKeyboard();
                getData();
            }
            return false;
        });

        mainViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position1) {
                position = position1;
                if (arrayList[position].size() == 0) {
                    page[position] = 1;
                    getData();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        for (int i = 0; i < mainPullRefreshView.length; i++) {
            final int pos = i;
            mainPullRefreshView[pos].setOnClickListener(view -> {
                if (mainPullRefreshView[pos].isError() || arrayList[pos].size() == 0) {
                    page[pos] = 1;
                    getData();
                }
            });
        }

        for (PullRefreshView pullRefreshView : mainPullRefreshView) {
            pullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    page[position] = 1;
                    getData();
                }

                @Override
                public void onLoadMore() {
                    getData();
                }
            });
        }

        for (SellerOrderAdapter adapter : adapter) {
            adapter.setOnItemClickListener(new SellerOrderAdapter.OnItemClickListener() {
                @Override
                public void onClick(int position, OrderSellerListBean bean) {
                    orderDetail(bean);
                }

                @Override
                public void onOption(int position, OrderSellerListBean bean) {
                    switch (bean.getOrderState()) {
                        case "10":
                            orderCancel(bean);
                            break;
                        case "20":
                            orderDetail(bean);
                            break;
                    }
                }

                @Override
                public void onOpera(int position, OrderSellerListBean bean) {
                    switch (bean.getOrderState()) {
                        case "10":
                            modifyPrice(bean);
                            break;
                        case "20":
                            orderDeliver(bean);
                            break;
                    }
                }

                @Override
                public void onClickGoods(int position, int itemPosition, OrderSellerListBean.GoodsListBean bean) {
                    App.get().startGoods(new GoodsData(bean.getGoodsId()));
                }
            });
        }

    }

    @Override
    public void initObserve() {

        vm.getOrderLiveData().observe(this, list -> {
            if (page[position] == 1) arrayList[position].clear();
            page[position] = page[position] + 1;
            arrayList[position].addAll(list);
            mainPullRefreshView[position].setComplete();
        });

        vm.getHasMoreLiveData().observe(this, bool -> mainPullRefreshView[position].setCanLoadMore(bool));

        vm.getCancelLiveData().observe(this, string -> showSuccess());

        vm.getModifyLiveData().observe(this, string -> showSuccess());

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                if (arrayList[position] != null && arrayList[position].size() != 0) {
                    ToastHelp.get().show(bean.getReason());
                } else {
                    mainPullRefreshView[position].setError(bean.getReason());
                }
            } else {
                ToastHelp.get().show(bean.getReason());
            }
        });

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView[position].setLoad();
        vm.getOrderList(type[position], keyword, page[position] + "");

    }

    private void showSuccess() {

        ToastHelp.get().showSuccess();
        page[position] = 1;
        getData();

    }

    private void orderDetail(OrderSellerListBean bean) {

    }

    private void modifyPrice(OrderSellerListBean bean) {

        DialogHelp.get().input(get(), InputType.TYPE_CLASS_NUMBER, R.string.modifyPrice, bean.getOrderAmount(), null, content -> {
            if (VerifyUtil.isEmpty(content)) {
                ToastHelp.get().show(R.string.pleaseInputAmount);
                return;
            }
            if (ConvertUtil.string2Float(content) <= 0) {
                ToastHelp.get().show(R.string.pleaseInputAmount);
                return;
            }
            vm.modifyPrice(bean.getOrderId(), content);
        });

    }

    private void orderCancel(OrderSellerListBean bean) {

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(getString(R.string.unableToPrepareGoods));
        arrayList.add(getString(R.string.notAValidOrder));
        arrayList.add(getString(R.string.buyerInitiative));
        arrayList.add(getString(R.string.otherReason));
        DialogHelp.get().list(get(), R.string.cancelOrder, arrayList, 0, (position, content) -> vm.orderCancel(bean.getOrderId(), content), null);

    }

    private void orderDeliver(OrderSellerListBean bean) {


    }

}
