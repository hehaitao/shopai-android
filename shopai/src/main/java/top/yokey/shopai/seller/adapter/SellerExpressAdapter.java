package top.yokey.shopai.seller.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zsdk.bean.SellerExpressListBean;

public class SellerExpressAdapter extends RecyclerView.Adapter<SellerExpressAdapter.ViewHolder> {

    private final ArrayList<SellerExpressListBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public SellerExpressAdapter(ArrayList<SellerExpressListBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        SellerExpressListBean bean = arrayList.get(position);

        holder.mainCheckBox.setText(bean.geteName());
        holder.mainCheckBox.setChecked(bean.getIsChecke().equals(Constant.COMMON_ENABLE));

        holder.mainCheckBox.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

        holder.mainLinearLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_seller_express, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, SellerExpressListBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayoutCompat mainLinearLayout;
        private final AppCompatCheckBox mainCheckBox;

        private ViewHolder(View view) {

            super(view);
            mainLinearLayout = view.findViewById(R.id.mainLinearLayout);
            mainCheckBox = view.findViewById(R.id.mainCheckBox);

        }

    }

}
