package top.yokey.shopai.seller.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.SellerExpressListBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.SellerExpressController;

public class SellerExpressVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<SellerExpressListBean>> expressLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> saveLiveData = new MutableLiveData<>();

    public SellerExpressVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<SellerExpressListBean>> getExpressLiveData() {

        return expressLiveData;

    }

    public MutableLiveData<String> getSaveLiveData() {

        return saveLiveData;

    }

    public void getExpress() {

        SellerExpressController.getList(new HttpCallBack<ArrayList<SellerExpressListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<SellerExpressListBean> list) {
                expressLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void save(String id) {

        SellerExpressController.saveDefault(id, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                saveLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
