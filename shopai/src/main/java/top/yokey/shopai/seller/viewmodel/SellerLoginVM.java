package top.yokey.shopai.seller.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.SellerLoginBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.SellerLoginController;

public class SellerLoginVM extends BaseViewModel {

    private final MutableLiveData<SellerLoginBean> loginLiveData = new MutableLiveData<>();

    public SellerLoginVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<SellerLoginBean> getLoginLiveData() {

        return loginLiveData;

    }

    public void login(String username, String password) {

        SellerLoginController.index(username, password, new HttpCallBack<SellerLoginBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, SellerLoginBean bean) {
                loginLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
