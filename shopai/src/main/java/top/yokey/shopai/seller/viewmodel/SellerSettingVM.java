package top.yokey.shopai.seller.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.SellerStoreBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.SellerStoreController;
import top.yokey.shopai.zsdk.data.SellerStoreData;

public class SellerSettingVM extends BaseViewModel {

    private final MutableLiveData<SellerStoreBean> storeLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> modifyLiveData = new MutableLiveData<>();

    public SellerSettingVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<SellerStoreBean> getStoreLiveData() {

        return storeLiveData;

    }

    public MutableLiveData<String> getModifyLiveData() {

        return modifyLiveData;

    }

    public void getStore() {

        SellerStoreController.storeInfo(new HttpCallBack<SellerStoreBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, SellerStoreBean bean) {
                storeLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void modify(SellerStoreData data) {

        SellerStoreController.storeEdit(data, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                modifyLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
