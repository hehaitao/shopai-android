package top.yokey.shopai.store.activity;

import android.content.Intent;

import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.store.adapter.StoreClassAdapter;
import top.yokey.shopai.store.viewmodel.StoreClassVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.StoreClassBean;

@Route(path = ARoutePath.STORE_CLASS)
public class StoreClassActivity extends BaseActivity {

    private final ArrayList<StoreClassBean> arrayList = new ArrayList<>();
    private final StoreClassAdapter adapter = new StoreClassAdapter(arrayList);
    private Toolbar mainToolbar = null;
    private PullRefreshView mainPullRefreshView = null;
    private StoreClassVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_pull_refresh_view);
        mainToolbar = findViewById(R.id.mainToolbar);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.storeClass);
        observeKeyborad(R.id.mainLinearLayout);
        mainPullRefreshView.setAdapter(adapter);
        mainPullRefreshView.setCanLoadMore(false);
        mainPullRefreshView.setItemDecoration(new LineDecoration(1, App.get().getColors(R.color.divider), false));
        vm = getVM(StoreClassVM.class);
        vm.getStoreClass();

    }

    @Override
    public void initEvent() {

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || arrayList.size() == 0) {
                mainPullRefreshView.setLoad();
                vm.getStoreClass();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mainPullRefreshView.setLoad();
                vm.getStoreClass();
            }

            @Override
            public void onLoadMore() {

            }
        });

        adapter.setOnItemClickListener((position, bean) -> {
            Intent intent = new Intent();
            intent.putExtra(Constant.DATA_ID, bean.getScId());
            intent.putExtra(Constant.DATA_CONTENT, bean.getScName());
            App.get().finishOk(get(), intent);
        });

    }

    @Override
    public void initObserve() {

        vm.getStoreClassLiveData().observe(this, list -> {
            arrayList.clear();
            arrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getErrorLiveData().observe(this, bean -> DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getStoreClass()));

    }

}
