package top.yokey.shopai.store.activity;

import android.view.View;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.google.android.material.tabs.TabLayout;
import com.jeremyliao.liveeventbus.LiveEventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.store.fragment.StoreActivityFragment;
import top.yokey.shopai.store.fragment.StoreGoodsFragment;
import top.yokey.shopai.store.fragment.StoreIndexFragment;
import top.yokey.shopai.store.fragment.StoreNewsFragment;
import top.yokey.shopai.store.viewmodel.StoreVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.adapter.FragmentTitleAdapter;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.ConvertUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.StoreBean;

@Route(path = ARoutePath.STORE_INDEX)
public class StoreIndexActivity extends BaseActivity {

    @Autowired(name = Constant.DATA_ID)
    String storeId;
    private Toolbar mainToolbar = null;
    private AppCompatEditText toolbarEditText = null;
    private AppCompatImageView toolbarImageView = null;
    private TabLayout mainTabLayout = null;
    private ViewPager mainViewPager = null;
    private AppCompatImageView avatarImageView = null;
    private AppCompatTextView nameTextView = null;
    private AppCompatTextView favoriteTextView = null;
    private AppCompatTextView numberTextView = null;
    private AppCompatTextView infoTextView = null;
    private AppCompatTextView voucherTextView = null;
    private AppCompatTextView customerTextView = null;
    private StoreBean bean = null;
    private StoreVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_store_index);
        mainToolbar = findViewById(R.id.mainToolbar);
        toolbarEditText = findViewById(R.id.toolbarEditText);
        toolbarImageView = findViewById(R.id.toolbarImageView);
        mainTabLayout = findViewById(R.id.mainTabLayout);
        mainViewPager = findViewById(R.id.mainViewPager);
        avatarImageView = findViewById(R.id.avatarImageView);
        nameTextView = findViewById(R.id.nameTextView);
        favoriteTextView = findViewById(R.id.favoriteTextView);
        numberTextView = findViewById(R.id.numberTextView);
        infoTextView = findViewById(R.id.infoTextView);
        voucherTextView = findViewById(R.id.voucherTextView);
        customerTextView = findViewById(R.id.customerTextView);

    }

    @Override
    public void initData() {

        if (VerifyUtil.isEmpty(storeId)) {
            ToastHelp.get().showDataError();
            onReturn(false);
            return;
        }
        setToolbar(mainToolbar);
        observeKeyborad(R.id.mainLinearLayout);
        toolbarEditText.setHint(R.string.pleaseInputKeyword);
        toolbarEditText.setFocusable(false);
        toolbarImageView.setImageResource(R.drawable.ic_action_menu);
        List<String> stringList = new ArrayList<>();
        stringList.add(getString(R.string.storeIndex));
        stringList.add(getString(R.string.allGoods));
        stringList.add(getString(R.string.goodsNew));
        stringList.add(getString(R.string.storeActivity));
        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(new StoreIndexFragment());
        fragmentList.add(new StoreGoodsFragment());
        fragmentList.add(new StoreNewsFragment());
        fragmentList.add(new StoreActivityFragment());
        mainTabLayout.addTab(mainTabLayout.newTab().setText(stringList.get(0)).setIcon(R.mipmap.ic_store_home));
        mainTabLayout.addTab(mainTabLayout.newTab().setText(stringList.get(1)).setIcon(R.mipmap.ic_store_goods));
        mainTabLayout.addTab(mainTabLayout.newTab().setText(stringList.get(2)).setIcon(R.mipmap.ic_store_news));
        mainTabLayout.addTab(mainTabLayout.newTab().setText(stringList.get(3)).setIcon(R.mipmap.ic_store_activity));
        mainTabLayout.setSelectedTabIndicatorColor(App.get().getColors(R.color.transparent));
        mainTabLayout.setTabTextColors(App.get().getColors(R.color.textThr), App.get().getColors(R.color.red));
        mainViewPager.setAdapter(new FragmentTitleAdapter(getSupportFragmentManager(), fragmentList, stringList));
        mainViewPager.setOffscreenPageLimit(fragmentList.size());
        mainTabLayout.setupWithViewPager(mainViewPager);
        mainTabLayout.setTabMode(TabLayout.MODE_FIXED);
        mainViewPager.setCurrentItem(0);
        updateNav(0);
        vm = getVM(StoreVM.class);
        vm.getInfo(storeId);

    }

    @Override
    public void initEvent() {

        final View.OnClickListener onClickListener = view -> App.get().start(ARoutePath.STORE_SEARCH, Constant.DATA_ID, storeId);

        toolbarEditText.setOnClickListener(onClickListener);

        toolbarImageView.setOnClickListener(onClickListener);

        favoriteTextView.setOnClickListener(view -> {
            if (App.get().isMemberLogin()) {
                if (bean.getStoreInfo().isIsFavorate()) {
                    vm.favDel(storeId);
                    return;
                }
                vm.favAdd(storeId);
                return;
            }
            App.get().startMemberLogin();
        });

        mainTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                updateNav(mainTabLayout.getSelectedTabPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mainViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                updateNav(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        infoTextView.setOnClickListener(view -> App.get().start(ARoutePath.STORE_INFO, Constant.DATA_ID, storeId));

        voucherTextView.setOnClickListener(view -> App.get().start(ARoutePath.STORE_VOUCHER, Constant.DATA_ID, storeId));

        customerTextView.setOnClickListener(view -> App.get().startChat(ShopAISdk.get().getChatUrl(bean.getStoreInfo().getMemberId(), "")));

    }

    @Override
    public void initObserve() {

        vm.getStoreLiveData().observe(this, bean -> {
            StoreIndexActivity.this.bean = bean;
            ImageHelp.get().displayCircle(bean.getStoreInfo().getStoreAvatar(), avatarImageView);
            nameTextView.setText(bean.getStoreInfo().getStoreName());
            numberTextView.setText(String.format(getString(R.string.storeFans), bean.getStoreInfo().getStoreCollect() + ""));
            favoriteTextView.setText(bean.getStoreInfo().isIsFavorate() ? R.string.alreadyFavorite : R.string.favorite);
            LiveEventBus.get(Constant.DATA_BEAN).post(bean);
        });

        vm.getFavAddLiveData().observe(this, string -> {
            favoriteTextView.setText(R.string.alreadyFavorite);
            bean.getStoreInfo().setStoreCollect((ConvertUtil.string2Int(bean.getStoreInfo().getStoreCollect()) + 1) + "");
            bean.getStoreInfo().setIsFavorate(true);
            numberTextView.setText(String.format(getString(R.string.storeFans), bean.getStoreInfo().getStoreCollect() + ""));
        });

        vm.getFavDelLiveData().observe(this, string -> {
            favoriteTextView.setText(R.string.favorite);
            bean.getStoreInfo().setStoreCollect((ConvertUtil.string2Int(bean.getStoreInfo().getStoreCollect()) - 1) + "");
            bean.getStoreInfo().setIsFavorate(false);
            numberTextView.setText(String.format(getString(R.string.storeFans), bean.getStoreInfo().getStoreCollect() + ""));
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getInfo(storeId));
                return;
            }
            ToastHelp.get().show(bean.getReason());
        });

    }

    //自定义方法

    private void updateNav(int position) {

        Objects.requireNonNull(mainTabLayout.getTabAt(0)).setIcon(R.mipmap.ic_store_home);
        Objects.requireNonNull(mainTabLayout.getTabAt(1)).setIcon(R.mipmap.ic_store_goods);
        Objects.requireNonNull(mainTabLayout.getTabAt(2)).setIcon(R.mipmap.ic_store_news);
        Objects.requireNonNull(mainTabLayout.getTabAt(3)).setIcon(R.mipmap.ic_store_activity);

        switch (position) {
            case 0:
                Objects.requireNonNull(mainTabLayout.getTabAt(0)).setIcon(R.mipmap.ic_store_home_press);
                break;
            case 1:
                Objects.requireNonNull(mainTabLayout.getTabAt(1)).setIcon(R.mipmap.ic_store_goods_press);
                break;
            case 2:
                Objects.requireNonNull(mainTabLayout.getTabAt(2)).setIcon(R.mipmap.ic_store_news_press);
                break;
            case 3:
                Objects.requireNonNull(mainTabLayout.getTabAt(3)).setIcon(R.mipmap.ic_store_activity_press);
                break;
        }

    }

}
