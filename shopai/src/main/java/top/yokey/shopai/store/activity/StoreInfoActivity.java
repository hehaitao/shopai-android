package top.yokey.shopai.store.activity;

import android.webkit.WebView;

import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zsdk.ShopAISdk;

@Route(path = ARoutePath.STORE_INFO)
public class StoreInfoActivity extends BaseActivity {

    @Autowired(name = Constant.DATA_ID)
    String storeId = "";
    private Toolbar mainToolbar = null;
    private WebView mainWebView = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_store_info);
        mainToolbar = findViewById(R.id.mainToolbar);
        mainWebView = findViewById(R.id.mainWebView);

    }

    @Override
    public void initData() {

        if (VerifyUtil.isEmpty(storeId)) {
            ToastHelp.get().showDataError();
            onReturn(false);
            return;
        }
        setToolbar(mainToolbar, R.string.storeInfo);
        observeKeyborad(R.id.mainRelativeLayout);
        App.get().setWebView(mainWebView);
        ShopAISdk.get().setSupportWebView(get());
        mainWebView.loadUrl(ShopAISdk.get().getStoreInfoUrl(storeId));

    }

    @Override
    public void initEvent() {

    }

    @Override
    public void initObserve() {

    }

}
