package top.yokey.shopai.store.activity;

import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.store.adapter.StoreVoucherAdapter;
import top.yokey.shopai.store.viewmodel.StoreVoucherVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.VoucherTplListBean;

@Route(path = ARoutePath.STORE_VOUCHER)
public class StoreVoucherActivity extends BaseActivity {

    private final ArrayList<VoucherTplListBean> arrayList = new ArrayList<>();
    private final StoreVoucherAdapter adapter = new StoreVoucherAdapter(arrayList);
    @Autowired(name = Constant.DATA_ID)
    String storeId;
    private Toolbar mainToolbar = null;
    private PullRefreshView mainPullRefreshView = null;
    private StoreVoucherVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_pull_refresh_view);
        mainToolbar = findViewById(R.id.mainToolbar);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        if (VerifyUtil.isEmpty(storeId)) {
            ToastHelp.get().showDataError();
            onReturn(false);
            return;
        }
        setToolbar(mainToolbar, R.string.storeVoucher);
        observeKeyborad(R.id.mainLinearLayout);
        mainPullRefreshView.setCanLoadMore(false);
        mainPullRefreshView.setAdapter(adapter);
        mainPullRefreshView.setItemDecoration(new LineDecoration(App.get().dp2Px(16), true));
        vm = getVM(StoreVoucherVM.class);
        mainPullRefreshView.setLoad();
        vm.getVoucher(storeId);

    }

    @Override
    public void initEvent() {

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || arrayList.size() == 0) {
                mainPullRefreshView.setLoad();
                vm.getVoucher(storeId);
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mainPullRefreshView.setLoad();
                vm.getVoucher(storeId);
            }

            @Override
            public void onLoadMore() {

            }
        });

        adapter.setOnItemClickListener((position, bean) -> vm.voucherFreex(bean.getVoucherTId()));

    }

    @Override
    public void initObserve() {

        vm.getVoucherLiveData().observe(this, list -> {
            arrayList.clear();
            arrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getFreexLiveData().observe(this, string -> ToastHelp.get().showSuccess());

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                mainPullRefreshView.setError(bean.getReason());
            } else {
                ToastHelp.get().show(bean.getReason());
            }
        });

    }

}
