package top.yokey.shopai.store.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zsdk.bean.StoreClassBean;

public class StoreClassAdapter extends RecyclerView.Adapter<StoreClassAdapter.ViewHolder> {

    private final ArrayList<StoreClassBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public StoreClassAdapter(ArrayList<StoreClassBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        StoreClassBean bean = arrayList.get(position);
        holder.nameTextView.setText(bean.getScName());

        holder.nameTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {
        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_store_class, group, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, StoreClassBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final AppCompatTextView nameTextView;

        private ViewHolder(View view) {

            super(view);
            nameTextView = view.findViewById(R.id.nameTextView);

        }

    }

}
