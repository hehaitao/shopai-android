package top.yokey.shopai.store.fragment;

import android.view.LayoutInflater;
import android.view.View;

import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.jeremyliao.liveeventbus.LiveEventBus;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.store.adapter.StoreGoodsAdapter;
import top.yokey.shopai.store.viewmodel.StoreNewsVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.base.BaseFragment;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.SpaceDecoration;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.GoodsListBean;
import top.yokey.shopai.zsdk.bean.StoreBean;
import top.yokey.shopai.zsdk.data.GoodsData;

public class StoreNewsFragment extends BaseFragment {

    private final ArrayList<GoodsListBean> arrayList = new ArrayList<>();
    private final StoreGoodsAdapter adapter = new StoreGoodsAdapter(arrayList, true);
    private PullRefreshView mainPullRefreshView;
    private int page = 1;
    private String storeId = "";
    private StoreNewsVM vm = null;

    @Override
    public View initView() {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.include_pull_refresh_view, null);
        mainPullRefreshView = view.findViewById(R.id.mainPullRefreshView);
        return view;

    }

    @Override
    public void initData() {

        mainPullRefreshView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        mainPullRefreshView.getRecyclerView().setPadding(App.get().dp2Px(4), 0, App.get().dp2Px(4), 0);
        mainPullRefreshView.setItemDecoration(new SpaceDecoration(App.get().dp2Px(4)));
        mainPullRefreshView.setAdapter(adapter);
        vm = getVM(StoreNewsVM.class);

    }

    @Override
    public void initEvent() {

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || arrayList.size() == 0) {
                page = 1;
                getData();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getData();
            }

            @Override
            public void onLoadMore() {
                getData();
            }
        });

        adapter.setOnItemClickListener(new StoreGoodsAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position, GoodsListBean bean) {
                App.get().startGoods(new GoodsData(bean.getGoodsId()));
            }

            @Override
            public void onClickCart(int position, GoodsListBean bean) {
                if (App.get().isMemberLogin()) {
                    vm.addCart(bean.getGoodsId(), "1");
                } else {
                    App.get().startMemberLogin();
                }
            }

            @Override
            public void onClickFavorate(int position, GoodsListBean bean) {
                if (App.get().isMemberLogin()) {
                    vm.favorite(bean.getGoodsId());
                } else {
                    App.get().startMemberLogin();
                }
            }

            @Override
            public void onClickStore(int position, GoodsListBean bean) {
                App.get().startStore(bean.getStoreId());
            }
        });

    }

    @Override
    public void initObserve() {

        vm.getGoodsLiveData().observe(this, list -> {
            if (page == 1) arrayList.clear();
            page++;
            arrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getHasMoreLiveData().observe(this, bool -> mainPullRefreshView.setCanLoadMore(bool));

        vm.getAddCartLiveData().observe(this, string -> ToastHelp.get().show(R.string.tipsAlreadyAddCart));

        vm.getFavoriteLiveData().observe(this, string -> ToastHelp.get().show(R.string.tipsAlreadyFavorite));

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                if (arrayList.size() == 0) {
                    mainPullRefreshView.setError(bean.getReason());
                } else {
                    ToastHelp.get().show(bean.getReason());
                }
            } else {
                ToastHelp.get().show(bean.getReason());
            }
        });

        LiveEventBus.get(Constant.DATA_BEAN, StoreBean.class).observe(this, bean -> {
            storeId = bean.getStoreInfo().getStoreId();
            page = 1;
            getData();
        });

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView.setLoad();
        vm.getList(storeId, page + "");

    }

}
