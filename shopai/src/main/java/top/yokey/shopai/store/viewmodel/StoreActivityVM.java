package top.yokey.shopai.store.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.StoreController;

public class StoreActivityVM extends BaseViewModel {

    private final MutableLiveData<String> saleClassLiveData = new MutableLiveData<>();

    public StoreActivityVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<String> getSaleClassLiveData() {

        return saleClassLiveData;

    }

    public void getStoreSale(String storeId) {

        StoreController.storeSale(storeId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                saleClassLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
