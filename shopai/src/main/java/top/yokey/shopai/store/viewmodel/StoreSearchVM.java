package top.yokey.shopai.store.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.StoreGoodsClassBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.StoreController;

public class StoreSearchVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<StoreGoodsClassBean>> classLiveData = new MutableLiveData<>();

    public StoreSearchVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<StoreGoodsClassBean>> getClassLiveData() {

        return classLiveData;

    }

    public void getData(String storeId) {

        StoreController.storeGoodsClass(storeId, new HttpCallBack<ArrayList<StoreGoodsClassBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<StoreGoodsClassBean> list) {
                classLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
