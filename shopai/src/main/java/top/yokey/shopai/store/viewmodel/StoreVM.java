package top.yokey.shopai.store.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.StoreBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberFavoritesStoreController;
import top.yokey.shopai.zsdk.controller.StoreController;

public class StoreVM extends BaseViewModel {

    private final MutableLiveData<StoreBean> storeLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> favAddLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> favDelLiveData = new MutableLiveData<>();

    public StoreVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<StoreBean> getStoreLiveData() {

        return storeLiveData;

    }

    public MutableLiveData<String> getFavAddLiveData() {

        return favAddLiveData;

    }

    public MutableLiveData<String> getFavDelLiveData() {

        return favDelLiveData;

    }

    public void getInfo(String storeId) {

        StoreController.storeInfo(storeId, new HttpCallBack<StoreBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, StoreBean bean) {
                storeLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void favAdd(String storeId) {

        MemberFavoritesStoreController.favoritesAdd(storeId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                favAddLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void favDel(String storeId) {

        MemberFavoritesStoreController.favoritesDel(storeId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                favDelLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

}
