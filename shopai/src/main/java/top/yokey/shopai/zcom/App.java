package top.yokey.shopai.zcom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import androidx.multidex.MultiDex;

import com.alibaba.android.arouter.launcher.ARouter;
import com.github.anzewei.parallaxbacklayout.ParallaxHelper;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechUtility;
import com.jeremyliao.liveeventbus.LiveEventBus;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.style.PictureParameterStyle;
import com.luck.picture.lib.tools.PictureFileUtils;
import com.mob.MobSDK;
import com.tencent.bugly.crashreport.CrashReport;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseApplication;
import top.yokey.shopai.zcom.help.AesHelp;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.help.LoggerHelp;
import top.yokey.shopai.zcom.help.SharedHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.other.GlideEngine;
import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zcom.view.PullRefreshViewHelper;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.AppAdvertBean;
import top.yokey.shopai.zsdk.bean.MemberBean;
import top.yokey.shopai.zsdk.bean.SellerIndexBean;
import top.yokey.shopai.zsdk.data.GoodsBuyData;
import top.yokey.shopai.zsdk.data.GoodsData;
import top.yokey.shopai.zsdk.data.GoodsSearchData;
import top.yokey.shopai.zsdk.data.OrderPinGouData;


@SuppressWarnings("ALL")
public class App extends BaseApplication {

    private static App instance = null;
    private String xingeToken = "";
    private MemberBean memberBean = null;
    private SellerIndexBean sellerBean = null;
    private AppAdvertBean appAdvertBean = null;

    public static App get() {

        return instance;

    }

    @Override
    public void onCreate() {

        super.onCreate();
        instance = this;
        //帮助类的
        AesHelp.get().init(Constant.AES_KEY);
        LoggerHelp.get().init(Constant.LOG_TAG);
        DialogHelp.get().init(getString(R.string.cancel), getString(R.string.confirm), getString(R.string.cancel));
        ToastHelp.get().init(this, getString(R.string.success), getString(R.string.dataError));
        SharedHelp.get().init(getSharedPreferences(Constant.SHARED_NAME, MODE_PRIVATE));
        ImageHelp.get().init(this, Constant.APP_NAME, App.get().dp2Px(4), App.get().dp2Px(12));
        //第三方库
        ARouter.openLog();
        ARouter.openDebug();
        MobSDK.init(this);
        ARouter.init(this);
        registerActivityLifecycleCallbacks(ParallaxHelper.getInstance());
        PictureFileUtils.deleteCacheDirFile(get(), PictureMimeType.ofAll());
        CrashReport.initCrashReport(getApplicationContext(), Constant.BUGLY_APP_ID, false);
        SpeechUtility.createUtility(this, SpeechConstant.APPID + "=" + Constant.XFYUN_APP_ID);
        LiveEventBus.config().lifecycleObserverAlwaysActive(true).autoClear(false);
        ShopAISdk.get().init(this, Constant.URL, Constant.URL_API, Constant.URL_WAP, Constant.URL_ACT, Constant.URL_OP, Constant.PAGE_NUMBER, getMemberKey());
        //下拉刷新
        PullRefreshViewHelper.get().init(14);
        PullRefreshViewHelper.get().setColors(getColors(R.color.accent), getColors(R.color.red), getColors(R.color.accentDark), getColors(R.color.redDark));
        PullRefreshViewHelper.get().setLoadStyle(PullRefreshViewHelper.LOAD_STYLE_THREE);
        PullRefreshViewHelper.get().setLoadColor(getColors(R.color.accent));
        PullRefreshViewHelper.get().setLoadText(getString(R.string.loadIng));
        PullRefreshViewHelper.get().setEmptyImage(R.mipmap.ic_pull_refresh_empty);
        PullRefreshViewHelper.get().setEmptyColor(Color.GRAY);
        PullRefreshViewHelper.get().setEmptyText(getString(R.string.noData));
        PullRefreshViewHelper.get().setErrorImage(R.mipmap.ic_pull_refresh_error);
        PullRefreshViewHelper.get().setErrorColor(Color.GRAY);
        PullRefreshViewHelper.get().setErrorText(getString(R.string.loadFailure));

    }

    @Override
    public void attachBaseContext(Context context) {

        super.attachBaseContext(context);
        MultiDex.install(this);

    }

    //用户信息

    public String getMemberKey() {

        return SharedHelp.get().getString(Constant.SHARED_MEMBER_KEY);

    }

    public String getSellerKey() {

        return SharedHelp.get().getString(Constant.SHARED_SELLER_KEY);

    }

    public boolean isMemberLogin() {

        return !VerifyUtil.isEmpty(getMemberKey());

    }

    public boolean isSellerLogin() {

        return !VerifyUtil.isEmpty(getSellerKey());

    }

    public String getXingeToken() {

        return xingeToken;

    }

    public void setXingeToken(String xingeToken) {

        this.xingeToken = xingeToken;

    }

    public MemberBean getMemberBean() {

        return memberBean;

    }

    public void setMemberBean(MemberBean memberBean) {

        this.memberBean = memberBean;

    }

    public SellerIndexBean getSellerBean() {

        return sellerBean;

    }

    public void setSellerBean(SellerIndexBean sellerBean) {

        this.sellerBean = sellerBean;

    }

    public AppAdvertBean getAppAdvertBean() {

        return appAdvertBean;

    }

    public void setAppAdvertBean(AppAdvertBean appAdvertBean) {

        this.appAdvertBean = appAdvertBean;

    }

    public PictureParameterStyle getPictureParameterStyle() {

        PictureParameterStyle pictureParameterStyle = new PictureParameterStyle();
        pictureParameterStyle.isChangeStatusBarFontColor = true;
        pictureParameterStyle.pictureStatusBarColor = Color.parseColor("#F3F3F3");
        pictureParameterStyle.pictureTitleBarBackgroundColor = Color.parseColor("#F3F3F3");
        pictureParameterStyle.pictureLeftBackIcon = R.drawable.ic_action_back;
        pictureParameterStyle.pictureTitleTextColor = getColors(R.color.text);
        pictureParameterStyle.pictureTitleUpResId = R.drawable.icon_arrow_up;
        pictureParameterStyle.pictureTitleDownResId = R.drawable.icon_arrow_down;
        pictureParameterStyle.pictureCancelTextColor = getColors(R.color.accent);
        pictureParameterStyle.pictureBottomBgColor = getColors(R.color.accent);
        pictureParameterStyle.picturePreviewTextColor = getColors(R.color.primary);
        pictureParameterStyle.pictureUnPreviewTextColor = getColors(R.color.primaryDark);
        pictureParameterStyle.pictureCompleteTextColor = getColors(R.color.primary);
        pictureParameterStyle.pictureUnCompleteTextColor = getColors(R.color.primary);
        pictureParameterStyle.picturePreviewBottomBgColor = getColors(R.color.accent);
        return pictureParameterStyle;

    }

    //应用内跳转

    public void startAlbumSignleCrop(Activity activity) {

        PictureSelector.create(activity)
                .openGallery(PictureMimeType.ofImage())
                .setPictureStyle(getPictureParameterStyle())
                .loadImageEngine(GlideEngine.createGlideEngine())
                .maxSelectNum(1)
                .minSelectNum(1)
                .imageSpanCount(4)
                .selectionMode(PictureConfig.SINGLE)
                .previewImage(true)
                .isCamera(true)
                .imageFormat(PictureMimeType.PNG)
                .isZoomAnim(true)
                .sizeMultiplier(0.5f)
                .enableCrop(true)
                .compress(true)
                .withAspectRatio(1, 1)
                .isGif(true)
                .freeStyleCropEnabled(true)
                .circleDimmedLayer(true)
                .showCropFrame(false)
                .showCropGrid(false)
                .previewEggs(true)
                .cropCompressQuality(90)
                .cutOutQuality(90)
                .minimumCompressSize(100)
                .compressQuality(100)
                .synOrAsy(true)
                .cropWH(1000, 1000)
                .rotateEnabled(true)
                .scaleEnabled(true)
                .isDragFrame(true)
                .forResult(Constant.CODE_ALBUM);

    }

    public void startAlbum(Activity activity, int number) {

        PictureSelector.create(activity)
                .openGallery(PictureMimeType.ofImage())
                .setPictureStyle(getPictureParameterStyle())
                .loadImageEngine(GlideEngine.createGlideEngine())
                .maxSelectNum(number)
                .minSelectNum(1)
                .imageSpanCount(4)
                .selectionMode(PictureConfig.MULTIPLE)
                .previewImage(true)
                .isCamera(true)
                .imageFormat(PictureMimeType.PNG)
                .isZoomAnim(true)
                .compress(true)
                .isGif(true)
                .minimumCompressSize(100)
                .compressQuality(100)
                .synOrAsy(true)
                .forResult(Constant.CODE_ALBUM);

    }

    //Arouter跳转

    public void start(String path) {

        ARouter.getInstance().build(path).navigation();

    }

    public void start(String path, String key, int value) {

        ARouter.getInstance().build(path).withInt(key, value).navigation();

    }

    public void start(String path, String key, String value) {

        ARouter.getInstance().build(path).withString(key, value).navigation();

    }

    public void start(Activity activity, String path, int code) {

        ARouter.getInstance().build(path).navigation(activity, code);

    }

    public void start(Activity activity, String path, String key, String value, int code) {

        ARouter.getInstance().build(path).withString(key, value).navigation(activity, code);

    }

    //页面跳转

    public void startMemberLogin() {

        if (!isMemberLogin()) {
            start(ARoutePath.MEMBER_LOGIN);
        }

    }

    public void startSellerLogin() {

        if (!isSellerLogin()) {
            start(ARoutePath.SELLER_LOGIN);
        }

    }

    public void startUrl(String url) {

        if (url.contains("gc_id=")) {
            GoodsSearchData data = new GoodsSearchData();
            data.setGcId(url.substring(url.lastIndexOf("=") + 1));
            startGoodsList(data);
            return;
        }
        if (url.contains("goods_id=")) {
            startGoods(new GoodsData(url.substring(url.lastIndexOf("=") + 1)));
            return;
        }
        if (url.contains("store_id=")) {
            startStore(url.substring(url.lastIndexOf("=") + 1));
            return;
        }
        if (url.contains("article_id=")) {
            startArticle(url.substring(url.lastIndexOf("=") + 1));
            return;
        }
        if (url.contains("special_id=")) {
            startSpecial(url.substring(url.lastIndexOf("=") + 1));
            return;
        }
        if (url.contains("red_packet.html?id=")) {
            String id = url.substring(url.lastIndexOf("=") + 1);
            startRedPacket(id);
            return;
        }
        if (url.contains("chat_info.html?t_id=")) {
            String id = url.substring(url.lastIndexOf("=") + 1);
            startChat(ShopAISdk.get().getChatUrl(id, ""));
            return;
        }
        if (url.contains("shop.html")) {
            start(ARoutePath.STORE_LIST);
            return;
        }
        if (url.contains("signin.html")) {
            start(ARoutePath.MAIN_SIGN);
            return;
        }
        if (url.contains("pingou_list.html")) {
            start(ARoutePath.GOODS_PIN_GOU);
            return;
        }
        if (url.contains("product_list.html")) {
            startGoodsList(new GoodsSearchData());
            return;
        }
        if (url.contains("product_dazhe.html")) {
            start(ARoutePath.GOODS_ROBBUY, Constant.DATA_POSITION, 1);
            return;
        }
        if (url.contains("product_robbuy.html")) {
            start(ARoutePath.GOODS_ROBBUY, Constant.DATA_POSITION, 2);
            return;
        }
        if (url.contains("coupon_list.html")) {
            start(ARoutePath.MAIN_VOUCHER, Constant.DATA_POSITION, 1);
            return;
        }
        if (url.contains("voucher_list.html")) {
            start(ARoutePath.MAIN_VOUCHER, Constant.DATA_POSITION, 2);
            return;
        }
        if (url.contains("pointspro_list.html")) {
            App.get().start(ARoutePath.POINT_LIST);
            return;
        }
        start(ARoutePath.MAIN_BROWSER, Constant.DATA_URL, url);

    }

    public void startChat(String url) {

        start(ARoutePath.MEMBER_CHAT, Constant.DATA_URL, url);

    }

    public void startMain(String url) {

        if (!VerifyUtil.isEmpty(url)) {
            start(ARoutePath.MAIN_MAIN, Constant.DATA_URL, url);
        } else {
            start(ARoutePath.MAIN_MAIN);
        }

    }

    public void startStore(String id) {

        start(ARoutePath.STORE_INDEX, Constant.DATA_ID, id);

    }

    public void startGoods(GoodsData data) {

        start(ARoutePath.GOODS_GOODS, Constant.DATA_JSON, JsonUtil.toJson(data));

    }

    public void startSpecial(String id) {

        start(ARoutePath.MAIN_SPECIAL, Constant.DATA_ID, id);

    }

    public void startArticle(String id) {

        start(ARoutePath.MAIN_ARTICLE_DETAIL, Constant.DATA_ID, id);

    }

    public void startRedPacket(String id) {

        start(ARoutePath.MAIN_RED_PACKET, Constant.DATA_ID, id);

    }

    public void startGoodsBuy(GoodsBuyData data) {

        start(ARoutePath.GOODS_BUY, Constant.DATA_JSON, JsonUtil.toJson(data));

    }

    public void startGoodsList(GoodsSearchData data) {

        start(ARoutePath.GOODS_LIST, Constant.DATA_JSON, JsonUtil.toJson(data));

    }

    public void startOrderPinGou(OrderPinGouData data) {

        start(ARoutePath.ORDER_PIN_GOU, Constant.DATA_JSON, JsonUtil.toJson(data));

    }

    public void startTypeValue(String type, String value) {

        switch (type) {
            case "2":
                startGoods(new GoodsData(value));
                break;
            case "url":
                startUrl(value);
                break;
            case "goods":
                startGoods(new GoodsData(value));
                break;
            case "store":
                startStore(value);
                break;
            case "article":
                startArticle(value);
                break;
            case "special":
                startSpecial(value);
                break;
            case "keyword":
                GoodsSearchData data = new GoodsSearchData();
                data.setKeyword(value);
                startGoodsList(data);
                break;
        }

    }

    //页面结束

    public void finishOk(Activity activity) {

        activity.setResult(activity.RESULT_OK);
        activity.finish();

    }

    public void finishOk(Activity activity, Intent intent) {

        activity.setResult(activity.RESULT_OK, intent);
        activity.finish();

    }

}
