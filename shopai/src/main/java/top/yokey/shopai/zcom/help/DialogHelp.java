package top.yokey.shopai.zcom.help;

import android.app.Activity;
import android.view.View;

import java.util.ArrayList;

import top.yokey.miuidialog.MiuiInputDialog;
import top.yokey.miuidialog.MiuiInputListener;
import top.yokey.miuidialog.MiuiListDialog;
import top.yokey.miuidialog.MiuiListListener;
import top.yokey.miuidialog.MiuiLoadDialog;
import top.yokey.miuidialog.MiuiQueryDialog;

@SuppressWarnings("ALL")
public class DialogHelp {

    private static volatile DialogHelp instance = null;
    private MiuiLoadDialog miuiLoadDialog = null;
    private String negative = "", positive = "", list = "";

    public static DialogHelp get() {

        if (instance == null) {
            synchronized (DialogHelp.class) {
                if (instance == null) {
                    instance = new DialogHelp();
                }
            }
        }
        return instance;

    }

    public void init(String negative, String positive, String list) {

        this.negative = negative;
        this.positive = positive;
        this.list = list;

    }

    public void query(Activity activity, int title, int content, View.OnClickListener negative, View.OnClickListener positive) {

        new MiuiQueryDialog.Builder(activity)
                .setCancelable(false)
                .setTitle(title)
                .setContent(content)
                .setNegativeButton(this.negative, negative)
                .setPositiveButton(this.positive, positive)
                .show();

    }

    public void query(Activity activity, int title, String content, View.OnClickListener negative, View.OnClickListener positive) {

        new MiuiQueryDialog.Builder(activity)
                .setCancelable(false)
                .setTitle(title)
                .setContent(content)
                .setNegativeButton(this.negative, negative)
                .setPositiveButton(this.positive, positive)
                .show();

    }

    public void query(Activity activity, String title, int content, View.OnClickListener negative, View.OnClickListener positive) {

        new MiuiQueryDialog.Builder(activity)
                .setCancelable(false)
                .setTitle(title)
                .setContent(content)
                .setNegativeButton(this.negative, negative)
                .setPositiveButton(this.positive, positive)
                .show();

    }

    public void query(Activity activity, String title, String content, View.OnClickListener negative, View.OnClickListener positive) {

        new MiuiQueryDialog.Builder(activity)
                .setCancelable(false)
                .setTitle(title)
                .setContent(content)
                .setNegativeButton(this.negative, negative)
                .setPositiveButton(this.positive, positive)
                .show();

    }

    public void input(Activity activity, int type, int title, int content, MiuiInputListener negative, MiuiInputListener positive) {

        new MiuiInputDialog.Builder(activity)
                .setCancelable(false)
                .setInputType(type)
                .setTitle(title)
                .setContent(content)
                .setNegativeButton(this.negative, negative)
                .setPositiveButton(this.positive, positive)
                .show();

    }

    public void input(Activity activity, int type, int title, String content, MiuiInputListener negative, MiuiInputListener positive) {

        new MiuiInputDialog.Builder(activity)
                .setCancelable(false)
                .setInputType(type)
                .setTitle(title)
                .setContent(content)
                .setNegativeButton(this.negative, negative)
                .setPositiveButton(this.positive, positive)
                .show();

    }

    public void input(Activity activity, int type, String title, int content, MiuiInputListener negative, MiuiInputListener positive) {

        new MiuiInputDialog.Builder(activity)
                .setCancelable(false)
                .setInputType(type)
                .setTitle(title)
                .setContent(content)
                .setNegativeButton(this.negative, negative)
                .setPositiveButton(this.positive, positive)
                .show();

    }

    public void input(Activity activity, int type, String title, String content, MiuiInputListener negative, MiuiInputListener positive) {

        new MiuiInputDialog.Builder(activity)
                .setCancelable(false)
                .setInputType(type)
                .setTitle(title)
                .setContent(content)
                .setNegativeButton(this.negative, negative)
                .setPositiveButton(this.positive, positive)
                .show();

    }

    public void list(Activity activity, int title, ArrayList<String> arrayList, int select, MiuiListListener listListener, MiuiListListener positive) {

        new MiuiListDialog.Builder(activity)
                .setCancelable(false)
                .setTitle(title)
                .setList(arrayList)
                .setSelector(select)
                .setListListener(listListener)
                .setPositiveButton(this.list, positive)
                .show();

    }

    public void list(Activity activity, String title, ArrayList<String> arrayList, int select, MiuiListListener listListener, MiuiListListener positive) {

        new MiuiListDialog.Builder(activity)
                .setCancelable(false)
                .setTitle(title)
                .setList(arrayList)
                .setSelector(select)
                .setListListener(listListener)
                .setPositiveButton(this.list, positive)
                .show();

    }

    public void progress(Activity activity, int content) {

        if (miuiLoadDialog != null) {
            miuiLoadDialog.dismiss();
        }
        miuiLoadDialog = new MiuiLoadDialog(activity);
        miuiLoadDialog.setCancelable(false);
        miuiLoadDialog.setContent(activity.getString(content));
        miuiLoadDialog.show();

    }

    public void progress(Activity activity, String content) {

        if (miuiLoadDialog != null) {
            miuiLoadDialog.dismiss();
        }
        miuiLoadDialog = new MiuiLoadDialog(activity);
        miuiLoadDialog.setCancelable(false);
        miuiLoadDialog.setContent(content);
        miuiLoadDialog.show();

    }

    public void dismiss() {

        if (miuiLoadDialog != null) {
            miuiLoadDialog.dismiss();
        }

    }

}
