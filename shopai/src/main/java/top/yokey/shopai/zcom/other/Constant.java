package top.yokey.shopai.zcom.other;

@SuppressWarnings("ALL")
public class Constant {

    public static final int CODE_AREA = 1000;
    public static final int CODE_ALBUM = 1001;

    public static final int CODE_CATE = 2000;
    public static final int CODE_ADDRESS = 2001;
    public static final int CODE_INVOICE = 2002;

    public static final long TIME_TICK = 1000L;
    public static final long TIME_COUNT = 2000L;
    public static final long TIME_DELAY = 5000L;

    public static final String DATA_ID = "data_id";
    public static final String DATA_SN = "data_sn";
    public static final String DATA_URL = "data_url";
    public static final String DATA_BID = "data_bid";
    public static final String DATA_GCID = "data_gcid";
    public static final String DATA_JSON = "data_json";
    public static final String DATA_BEAN = "data_bean";
    public static final String DATA_ADVERT = "data_advert";
    public static final String DATA_REFRESH = "data_refresh";
    public static final String DATA_CONTENT = "data_content";
    public static final String DATA_POSITION = "data_position";
    public static final String DATA_MSG_COUNT = "data_msg_count";

    public static final String APP_NAME = "ShopAI";
    public static final String SHARED_NAME = "shared_shopai";
    public static final String SHARED_MEMBER_KEY = "shared_member_key";
    public static final String SHARED_SELLER_KEY = "shared_seller_key";
    public static final String SHARED_SEARCH_KEYWORD = "shared_search_keyword";

    public static final String COMMON_ENABLE = "1";
    public static final String COMMON_DISABLE = "2";

    public static final String URL_OP = "t";
    public static final String URL_ACT = "w";
    public static final String PAGE_NUMBER = "20";
    public static final String LOG_TAG = "yokey_tag";
    public static final String AES_KEY = "shopaislocalkeys";

    public static final String URL = "https://shopai.yokey.top/";
    public static final String URL_API = "api/mobile/index.php";
    public static final String URL_WAP = "mobile/";
    public static final String URL_PINGOU_DETAIL = URL + URL_WAP + "html/pingou_document.html";
    public static final String URL_PINGOU_SHARE = URL + URL_WAP + "html/pingou_info.html?pingou_id=";

    //厂商平台的
    public static final String XIAOMI_APP_ID = "2882303761517977121";
    public static final String XIAOMI_APP_KEY = "5671797747121";
    public static final String MEIZU_APP_ID = "123";
    public static final String MEIZU_APP_KEY = "456";

    //第三方应用
    public static final String XFYUN_APP_ID = "5cc2c96d";
    public static final String BUGLY_APP_ID = "c46f5de388";

}
