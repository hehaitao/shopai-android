package top.yokey.shopai.zcom.other;

import android.content.Context;
import android.widget.ImageView;

import com.youth.banner.loader.ImageLoader;

import top.yokey.shopai.zcom.help.ImageHelp;

@SuppressWarnings("ALL")
public class UBLImageLoader extends ImageLoader {

    private int radius = 0;

    public UBLImageLoader(int radius) {

        this.radius = radius;

    }

    @Override
    public void displayImage(Context context, Object path, ImageView imageView) {

        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        if (radius == 0) {
            ImageHelp.get().display(path.toString(), imageView);
        } else {
            ImageHelp.get().displayRadius(path.toString(), imageView, radius);
        }

    }

}
