package top.yokey.shopai.zcom.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.viewpager.widget.ViewPager;

@SuppressWarnings("ALL")
public class FixedViewPager extends ViewPager {

    private boolean isCanScroll = false;

    public FixedViewPager(Context context) {

        super(context);

    }

    public FixedViewPager(Context context, AttributeSet attrs) {

        super(context, attrs);

    }

    public void setScanScroll(boolean isCanScroll) {

        this.isCanScroll = isCanScroll;

    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        return isCanScroll && super.onInterceptTouchEvent(ev);

    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        return isCanScroll && super.onTouchEvent(ev);

    }

}
