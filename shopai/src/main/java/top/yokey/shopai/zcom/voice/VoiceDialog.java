package top.yokey.shopai.zcom.voice;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import top.yokey.shopai.R;

@SuppressWarnings("ALL")
public class VoiceDialog {

    private AppCompatTextView labelTextView = null;
    private AppCompatImageView iconImageView = null;
    private AppCompatImageView voiceImageView = null;

    private Dialog dialog = null;
    private Context context = null;

    public VoiceDialog(Context context) {

        this.context = context;

    }

    public void showRecordingDialog() {

        dialog = new Dialog(context, R.style.Theme_Voice);
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_voice, null);
        dialog.setContentView(view);
        iconImageView = view.findViewById(R.id.iconImageView);
        voiceImageView = view.findViewById(R.id.voiceImageView);
        labelTextView = view.findViewById(R.id.labelTextView);
        dialog.show();

    }

    public void setLevel(int level) {

        if (dialog != null && dialog.isShowing()) {
            int resId = context.getResources().getIdentifier("ic_voice_level_" + level, "mipmap", context.getPackageName());
            voiceImageView.setImageResource(resId);
        }

    }

    public void want2Cancel() {

        if (dialog != null && dialog.isShowing()) {
            iconImageView.setVisibility(View.VISIBLE);
            voiceImageView.setVisibility(View.GONE);
            labelTextView.setVisibility(View.VISIBLE);
            iconImageView.setImageResource(R.mipmap.ic_voice_cancel);
            labelTextView.setText("松开手指，取消发送");
        }

    }

    public void recording() {

        if (dialog != null && dialog.isShowing()) {
            iconImageView.setVisibility(View.VISIBLE);
            voiceImageView.setVisibility(View.VISIBLE);
            labelTextView.setVisibility(View.VISIBLE);
            iconImageView.setImageResource(R.mipmap.ic_voice_normal);
            labelTextView.setText("手指上划，取消发送");
        }

    }

    public void tooShort() {

        if (dialog != null && dialog.isShowing()) {
            iconImageView.setVisibility(View.VISIBLE);
            voiceImageView.setVisibility(View.GONE);
            labelTextView.setVisibility(View.VISIBLE);
            iconImageView.setImageResource(R.mipmap.ic_voice_short);
            labelTextView.setText("录音时间过短");
        }

    }

    public void dismiss() {

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }

    }

}
