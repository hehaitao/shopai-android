package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class AddressBean implements Serializable {

    @SerializedName("address_id")
    private String addressId;
    @SerializedName("member_id")
    private String memberId;
    @SerializedName("true_name")
    private String trueName;
    @SerializedName("area_id")
    private String areaId;
    @SerializedName("city_id")
    private String cityId;
    @SerializedName("area_info")
    private String areaInfo;
    @SerializedName("address")
    private String address;
    @SerializedName("tel_phone")
    private String telPhone;
    @SerializedName("mob_phone")
    private String mobPhone;
    @SerializedName("is_default")
    private String isDefault;
    @SerializedName("dlyp_id")
    private String dlypId;

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getAreaInfo() {
        return areaInfo;
    }

    public void setAreaInfo(String areaInfo) {
        this.areaInfo = areaInfo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelPhone() {
        return telPhone;
    }

    public void setTelPhone(String telPhone) {
        this.telPhone = telPhone;
    }

    public String getMobPhone() {
        return mobPhone;
    }

    public void setMobPhone(String mobPhone) {
        this.mobPhone = mobPhone;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public String getDlypId() {
        return dlypId;
    }

    public void setDlypId(String dlypId) {
        this.dlypId = dlypId;
    }

}
