package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class BuyCouponBean implements Serializable {

    @SerializedName("coupon_price")
    private String couponPrice;
    @SerializedName("coupon_limit")
    private String couponLimit;
    @SerializedName("coupon_t_id")
    private String couponTId;
    @SerializedName("desc")
    private String desc;

    public String getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(String couponPrice) {
        this.couponPrice = couponPrice;
    }

    public String getCouponLimit() {
        return couponLimit;
    }

    public void setCouponLimit(String couponLimit) {
        this.couponLimit = couponLimit;
    }

    public String getCouponTId() {
        return couponTId;
    }

    public void setCouponTId(String couponTId) {
        this.couponTId = couponTId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
