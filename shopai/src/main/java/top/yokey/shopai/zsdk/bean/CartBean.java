package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


@SuppressWarnings("ALL")
public class CartBean implements Serializable {

    @SerializedName("store_id")
    private String storeId = "";
    @SerializedName("store_name")
    private String storeName = "";
    @SerializedName("goods")
    private ArrayList<GoodsBean> goods = new ArrayList<>();
    @SerializedName("mansong")
    private ArrayList<MansongBean> mansong = new ArrayList<>();
    @SerializedName("free_freight")
    private String freeFreight = "";
    private boolean select = true;

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public ArrayList<GoodsBean> getGoods() {
        return goods;
    }

    public void setGoods(ArrayList<GoodsBean> goods) {
        this.goods = goods;
    }

    public ArrayList<MansongBean> getMansong() {
        return mansong;
    }

    public void setMansong(ArrayList<MansongBean> mansong) {
        this.mansong = mansong;
    }

    public String getFreeFreight() {
        return freeFreight;
    }

    public void setFreeFreight(String freeFreight) {
        this.freeFreight = freeFreight;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public static class GoodsBean {

        @SerializedName("cart_id")
        private String cartId = "";
        @SerializedName("buyer_id")
        private String buyerId = "";
        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("store_name")
        private String storeName = "";
        @SerializedName("goods_id")
        private String goodsId = "";
        @SerializedName("goods_name")
        private String goodsName = "";
        @SerializedName("goods_price")
        private String goodsPrice = "";
        @SerializedName("goods_num")
        private String goodsNum = "";
        @SerializedName("goods_image")
        private String goodsImage = "";
        @SerializedName("bl_id")
        private String blId = "";
        @SerializedName("state")
        private boolean state = false;
        @SerializedName("storage_state")
        private boolean storageState = false;
        @SerializedName("goods_commonid")
        private String goodsCommonid = "";
        @SerializedName("gc_id")
        private String gcId = "";
        @SerializedName("transport_id")
        private String transportId = "";
        @SerializedName("goods_freight")
        private String goodsFreight = "";
        @SerializedName("goods_trans_v")
        private String goodsTransV = "";
        @SerializedName("goods_vat")
        private String goodsVat = "";
        @SerializedName("goods_storage")
        private String goodsStorage = "";
        @SerializedName("goods_storage_alarm")
        private String goodsStorageAlarm = "";
        @SerializedName("is_fcode")
        private String isFcode = "";
        @SerializedName("have_gift")
        private String haveGift = "";
        @SerializedName("is_book")
        private String isBook = "";
        @SerializedName("book_down_payment")
        private String bookDownPayment = "";
        @SerializedName("book_final_payment")
        private String bookFinalPayment = "";
        @SerializedName("book_down_time")
        private String bookDownTime = "";
        @SerializedName("is_chain")
        private String isChain = "";
        @SerializedName("ifsole")
        private boolean ifsole = false;
        @SerializedName("ifxianshi")
        private boolean ifxianshi = false;
        @SerializedName("ifgroupbuy")
        private boolean ifgroupbuy = false;
        @SerializedName("goods_image_url")
        private String goodsImageUrl = "";
        @SerializedName("goods_total")
        private String goodsTotal = "";
        @SerializedName("goods_spec")
        private String goodsSpec;
        private boolean select = true;

        public String getCartId() {
            return cartId;
        }

        public void setCartId(String cartId) {
            this.cartId = cartId;
        }

        public String getBuyerId() {
            return buyerId;
        }

        public void setBuyerId(String buyerId) {
            this.buyerId = buyerId;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(String goodsId) {
            this.goodsId = goodsId;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public String getGoodsPrice() {
            return goodsPrice;
        }

        public void setGoodsPrice(String goodsPrice) {
            this.goodsPrice = goodsPrice;
        }

        public String getGoodsNum() {
            return goodsNum;
        }

        public void setGoodsNum(String goodsNum) {
            this.goodsNum = goodsNum;
        }

        public String getGoodsImage() {
            return goodsImage;
        }

        public void setGoodsImage(String goodsImage) {
            this.goodsImage = goodsImage;
        }

        public String getBlId() {
            return blId;
        }

        public void setBlId(String blId) {
            this.blId = blId;
        }

        public boolean isState() {
            return state;
        }

        public void setState(boolean state) {
            this.state = state;
        }

        public boolean isStorageState() {
            return storageState;
        }

        public void setStorageState(boolean storageState) {
            this.storageState = storageState;
        }

        public String getGoodsCommonid() {
            return goodsCommonid;
        }

        public void setGoodsCommonid(String goodsCommonid) {
            this.goodsCommonid = goodsCommonid;
        }

        public String getGcId() {
            return gcId;
        }

        public void setGcId(String gcId) {
            this.gcId = gcId;
        }

        public String getTransportId() {
            return transportId;
        }

        public void setTransportId(String transportId) {
            this.transportId = transportId;
        }

        public String getGoodsFreight() {
            return goodsFreight;
        }

        public void setGoodsFreight(String goodsFreight) {
            this.goodsFreight = goodsFreight;
        }

        public String getGoodsTransV() {
            return goodsTransV;
        }

        public void setGoodsTransV(String goodsTransV) {
            this.goodsTransV = goodsTransV;
        }

        public String getGoodsVat() {
            return goodsVat;
        }

        public void setGoodsVat(String goodsVat) {
            this.goodsVat = goodsVat;
        }

        public String getGoodsStorage() {
            return goodsStorage;
        }

        public void setGoodsStorage(String goodsStorage) {
            this.goodsStorage = goodsStorage;
        }

        public String getGoodsStorageAlarm() {
            return goodsStorageAlarm;
        }

        public void setGoodsStorageAlarm(String goodsStorageAlarm) {
            this.goodsStorageAlarm = goodsStorageAlarm;
        }

        public String getIsFcode() {
            return isFcode;
        }

        public void setIsFcode(String isFcode) {
            this.isFcode = isFcode;
        }

        public String getHaveGift() {
            return haveGift;
        }

        public void setHaveGift(String haveGift) {
            this.haveGift = haveGift;
        }

        public String getIsBook() {
            return isBook;
        }

        public void setIsBook(String isBook) {
            this.isBook = isBook;
        }

        public String getBookDownPayment() {
            return bookDownPayment;
        }

        public void setBookDownPayment(String bookDownPayment) {
            this.bookDownPayment = bookDownPayment;
        }

        public String getBookFinalPayment() {
            return bookFinalPayment;
        }

        public void setBookFinalPayment(String bookFinalPayment) {
            this.bookFinalPayment = bookFinalPayment;
        }

        public String getBookDownTime() {
            return bookDownTime;
        }

        public void setBookDownTime(String bookDownTime) {
            this.bookDownTime = bookDownTime;
        }

        public String getIsChain() {
            return isChain;
        }

        public void setIsChain(String isChain) {
            this.isChain = isChain;
        }

        public boolean isIfsole() {
            return ifsole;
        }

        public void setIfsole(boolean ifsole) {
            this.ifsole = ifsole;
        }

        public boolean isIfxianshi() {
            return ifxianshi;
        }

        public void setIfxianshi(boolean ifxianshi) {
            this.ifxianshi = ifxianshi;
        }

        public boolean isIfgroupbuy() {
            return ifgroupbuy;
        }

        public void setIfgroupbuy(boolean ifgroupbuy) {
            this.ifgroupbuy = ifgroupbuy;
        }

        public String getGoodsImageUrl() {
            return goodsImageUrl;
        }

        public void setGoodsImageUrl(String goodsImageUrl) {
            this.goodsImageUrl = goodsImageUrl;
        }

        public String getGoodsTotal() {
            return goodsTotal;
        }

        public void setGoodsTotal(String goodsTotal) {
            this.goodsTotal = goodsTotal;
        }

        public String getGoodsSpec() {
            return goodsSpec;
        }

        public void setGoodsSpec(String goodsSpec) {
            this.goodsSpec = goodsSpec;
        }

        public boolean isSelect() {
            return select;
        }

        public void setSelect(boolean select) {
            this.select = select;
        }

    }

    public static class MansongBean {

        @SerializedName("desc")
        private String desc = "";
        @SerializedName("goods_name")
        private String goodsName = "";
        @SerializedName("url")
        private String url = "";

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

    }

}
