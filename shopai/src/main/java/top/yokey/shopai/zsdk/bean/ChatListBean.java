package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class ChatListBean implements Serializable {

    @SerializedName("u_id")
    private String uId;
    @SerializedName("u_name")
    private String uName;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("recent")
    private String recent;
    @SerializedName("m_id")
    private String mId;
    @SerializedName("time")
    private String time;
    @SerializedName("r_state")
    private String rState;
    @SerializedName("t_msg")
    private String tMsg;

    public String getUId() {
        return uId;
    }

    public void setUId(String uId) {
        this.uId = uId;
    }

    public String getUName() {
        return uName;
    }

    public void setUName(String uName) {
        this.uName = uName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getRecent() {
        return recent;
    }

    public void setRecent(String recent) {
        this.recent = recent;
    }

    public String getMId() {
        return mId;
    }

    public void setMId(String mId) {
        this.mId = mId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRState() {
        return rState;
    }

    public void setRState(String rState) {
        this.rState = rState;
    }

    public String getTMsg() {
        return tMsg;
    }

    public void setTMsg(String tMsg) {
        this.tMsg = tMsg;
    }

}
