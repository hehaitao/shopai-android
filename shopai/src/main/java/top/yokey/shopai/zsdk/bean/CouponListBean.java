package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class CouponListBean implements Serializable {

    @SerializedName("coupon_t_id")
    private String couponTId = "";
    @SerializedName("coupon_t_title")
    private String couponTTitle = "";
    @SerializedName("coupon_t_desc")
    private String couponTDesc = "";
    @SerializedName("coupon_t_start_date")
    private String couponTStartDate = "";
    @SerializedName("coupon_t_end_date")
    private String couponTEndDate = "";
    @SerializedName("coupon_t_price")
    private String couponTPrice = "";
    @SerializedName("coupon_t_limit")
    private String couponTLimit = "";
    @SerializedName("coupon_t_adminid")
    private String couponTAdminid = "";
    @SerializedName("coupon_t_state")
    private String couponTState = "";
    @SerializedName("coupon_t_total")
    private String couponTTotal = "";
    @SerializedName("coupon_t_giveout")
    private String couponTGiveout = "";
    @SerializedName("coupon_t_used")
    private String couponTUsed = "";
    @SerializedName("coupon_t_updatetime")
    private String couponTUpdatetime = "";
    @SerializedName("coupon_t_points")
    private String couponTPoints = "";
    @SerializedName("coupon_t_eachlimit")
    private String couponTEachlimit = "";
    @SerializedName("coupon_t_customimg")
    private String couponTCustomimg = "";
    @SerializedName("coupon_t_recommend")
    private String couponTRecommend = "";
    @SerializedName("coupon_t_gettype")
    private String couponTGettype = "";
    @SerializedName("coupon_t_isbuild")
    private String couponTIsbuild = "";
    @SerializedName("coupon_t_mgradelimit")
    private String couponTMgradelimit = "";
    @SerializedName("coupon_t_customimg_url")
    private String couponTCustomimgUrl = "";
    @SerializedName("coupon_t_gettype_key")
    private String couponTGettypeKey = "";
    @SerializedName("coupon_t_gettype_text")
    private String couponTGettypeText = "";
    @SerializedName("coupon_t_state_text")
    private String couponTStateText = "";
    @SerializedName("coupon_t_mgradelimittext")
    private String couponTMgradelimittext = "";
    @SerializedName("coupon_t_progress")
    private String couponTProgress = "";
    @SerializedName("end_date")
    private String endDate = "";

    public String getCouponTId() {
        return couponTId;
    }

    public void setCouponTId(String couponTId) {
        this.couponTId = couponTId;
    }

    public String getCouponTTitle() {
        return couponTTitle;
    }

    public void setCouponTTitle(String couponTTitle) {
        this.couponTTitle = couponTTitle;
    }

    public String getCouponTDesc() {
        return couponTDesc;
    }

    public void setCouponTDesc(String couponTDesc) {
        this.couponTDesc = couponTDesc;
    }

    public String getCouponTStartDate() {
        return couponTStartDate;
    }

    public void setCouponTStartDate(String couponTStartDate) {
        this.couponTStartDate = couponTStartDate;
    }

    public String getCouponTEndDate() {
        return couponTEndDate;
    }

    public void setCouponTEndDate(String couponTEndDate) {
        this.couponTEndDate = couponTEndDate;
    }

    public String getCouponTPrice() {
        return couponTPrice;
    }

    public void setCouponTPrice(String couponTPrice) {
        this.couponTPrice = couponTPrice;
    }

    public String getCouponTLimit() {
        return couponTLimit;
    }

    public void setCouponTLimit(String couponTLimit) {
        this.couponTLimit = couponTLimit;
    }

    public String getCouponTAdminid() {
        return couponTAdminid;
    }

    public void setCouponTAdminid(String couponTAdminid) {
        this.couponTAdminid = couponTAdminid;
    }

    public String getCouponTState() {
        return couponTState;
    }

    public void setCouponTState(String couponTState) {
        this.couponTState = couponTState;
    }

    public String getCouponTTotal() {
        return couponTTotal;
    }

    public void setCouponTTotal(String couponTTotal) {
        this.couponTTotal = couponTTotal;
    }

    public String getCouponTGiveout() {
        return couponTGiveout;
    }

    public void setCouponTGiveout(String couponTGiveout) {
        this.couponTGiveout = couponTGiveout;
    }

    public String getCouponTUsed() {
        return couponTUsed;
    }

    public void setCouponTUsed(String couponTUsed) {
        this.couponTUsed = couponTUsed;
    }

    public String getCouponTUpdatetime() {
        return couponTUpdatetime;
    }

    public void setCouponTUpdatetime(String couponTUpdatetime) {
        this.couponTUpdatetime = couponTUpdatetime;
    }

    public String getCouponTPoints() {
        return couponTPoints;
    }

    public void setCouponTPoints(String couponTPoints) {
        this.couponTPoints = couponTPoints;
    }

    public String getCouponTEachlimit() {
        return couponTEachlimit;
    }

    public void setCouponTEachlimit(String couponTEachlimit) {
        this.couponTEachlimit = couponTEachlimit;
    }

    public String getCouponTCustomimg() {
        return couponTCustomimg;
    }

    public void setCouponTCustomimg(String couponTCustomimg) {
        this.couponTCustomimg = couponTCustomimg;
    }

    public String getCouponTRecommend() {
        return couponTRecommend;
    }

    public void setCouponTRecommend(String couponTRecommend) {
        this.couponTRecommend = couponTRecommend;
    }

    public String getCouponTGettype() {
        return couponTGettype;
    }

    public void setCouponTGettype(String couponTGettype) {
        this.couponTGettype = couponTGettype;
    }

    public String getCouponTIsbuild() {
        return couponTIsbuild;
    }

    public void setCouponTIsbuild(String couponTIsbuild) {
        this.couponTIsbuild = couponTIsbuild;
    }

    public String getCouponTMgradelimit() {
        return couponTMgradelimit;
    }

    public void setCouponTMgradelimit(String couponTMgradelimit) {
        this.couponTMgradelimit = couponTMgradelimit;
    }

    public String getCouponTCustomimgUrl() {
        return couponTCustomimgUrl;
    }

    public void setCouponTCustomimgUrl(String couponTCustomimgUrl) {
        this.couponTCustomimgUrl = couponTCustomimgUrl;
    }

    public String getCouponTGettypeKey() {
        return couponTGettypeKey;
    }

    public void setCouponTGettypeKey(String couponTGettypeKey) {
        this.couponTGettypeKey = couponTGettypeKey;
    }

    public String getCouponTGettypeText() {
        return couponTGettypeText;
    }

    public void setCouponTGettypeText(String couponTGettypeText) {
        this.couponTGettypeText = couponTGettypeText;
    }

    public String getCouponTStateText() {
        return couponTStateText;
    }

    public void setCouponTStateText(String couponTStateText) {
        this.couponTStateText = couponTStateText;
    }

    public String getCouponTMgradelimittext() {
        return couponTMgradelimittext;
    }

    public void setCouponTMgradelimittext(String couponTMgradelimittext) {
        this.couponTMgradelimittext = couponTMgradelimittext;
    }

    public String getCouponTProgress() {
        return couponTProgress;
    }

    public void setCouponTProgress(String couponTProgress) {
        this.couponTProgress = couponTProgress;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

}
