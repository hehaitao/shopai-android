package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class DistribuBillListBean implements Serializable {

    @SerializedName("order_sn")
    private String orderSn = "";
    @SerializedName("goods_id")
    private String goodsId = "";
    @SerializedName("goods_name")
    private String goodsName = "";
    @SerializedName("pay_goods_amount")
    private String payGoodsAmount = "";
    @SerializedName("refund_amount")
    private String refundAmount = "";
    @SerializedName("fx_commis_rate")
    private String fxCommisRate = "";
    @SerializedName("goods_image_url")
    private String goodsImageUrl = "";
    @SerializedName("fx_pay_amount")
    private String fxPayAmount = "";
    @SerializedName("fx_pay_time")
    private String fxPayTime = "";
    @SerializedName("bill_state")
    private String billState = "";
    @SerializedName("bill_state_txt")
    private String billStateTxt = "";
    @SerializedName("fx_pay_time_txt")
    private String fxPayTimeTxt = "";

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getPayGoodsAmount() {
        return payGoodsAmount;
    }

    public void setPayGoodsAmount(String payGoodsAmount) {
        this.payGoodsAmount = payGoodsAmount;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getFxCommisRate() {
        return fxCommisRate;
    }

    public void setFxCommisRate(String fxCommisRate) {
        this.fxCommisRate = fxCommisRate;
    }

    public String getGoodsImageUrl() {
        return goodsImageUrl;
    }

    public void setGoodsImageUrl(String goodsImageUrl) {
        this.goodsImageUrl = goodsImageUrl;
    }

    public String getFxPayAmount() {
        return fxPayAmount;
    }

    public void setFxPayAmount(String fxPayAmount) {
        this.fxPayAmount = fxPayAmount;
    }

    public String getFxPayTime() {
        return fxPayTime;
    }

    public void setFxPayTime(String fxPayTime) {
        this.fxPayTime = fxPayTime;
    }

    public String getBillState() {
        return billState;
    }

    public void setBillState(String billState) {
        this.billState = billState;
    }

    public String getBillStateTxt() {
        return billStateTxt;
    }

    public void setBillStateTxt(String billStateTxt) {
        this.billStateTxt = billStateTxt;
    }

    public String getFxPayTimeTxt() {
        return fxPayTimeTxt;
    }

    public void setFxPayTimeTxt(String fxPayTimeTxt) {
        this.fxPayTimeTxt = fxPayTimeTxt;
    }

}
