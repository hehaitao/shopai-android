package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class GoodsBrowseBean implements Serializable {

    @SerializedName("goods_id")
    private String goodsId = "";
    @SerializedName("goods_name")
    private String goodsName = "";
    @SerializedName("goods_sale_price")
    private String goodsSalePrice = "";
    @SerializedName("goods_sale_type")
    private String goodsSaleType = "";
    @SerializedName("goods_marketprice")
    private String goodsMarketprice = "";
    @SerializedName("goods_image")
    private String goodsImage = "";
    @SerializedName("store_id")
    private String storeId = "";
    @SerializedName("gc_id")
    private String gcId = "";
    @SerializedName("gc_id_1")
    private String gcId1 = "";
    @SerializedName("gc_id_2")
    private String gcId2 = "";
    @SerializedName("gc_id_3")
    private String gcId3 = "";
    @SerializedName("browsetime")
    private String browsetime = "";
    @SerializedName("goods_image_url")
    private String goodsImageUrl = "";
    @SerializedName("browsetime_day")
    private String browsetimeDay = "";
    @SerializedName("browsetime_text")
    private String browsetimeText = "";

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsSalePrice() {
        return goodsSalePrice;
    }

    public void setGoodsSalePrice(String goodsSalePrice) {
        this.goodsSalePrice = goodsSalePrice;
    }

    public String getGoodsSaleType() {
        return goodsSaleType;
    }

    public void setGoodsSaleType(String goodsSaleType) {
        this.goodsSaleType = goodsSaleType;
    }

    public String getGoodsMarketprice() {
        return goodsMarketprice;
    }

    public void setGoodsMarketprice(String goodsMarketprice) {
        this.goodsMarketprice = goodsMarketprice;
    }

    public String getGoodsImage() {
        return goodsImage;
    }

    public void setGoodsImage(String goodsImage) {
        this.goodsImage = goodsImage;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getGcId() {
        return gcId;
    }

    public void setGcId(String gcId) {
        this.gcId = gcId;
    }

    public String getGcId1() {
        return gcId1;
    }

    public void setGcId1(String gcId1) {
        this.gcId1 = gcId1;
    }

    public String getGcId2() {
        return gcId2;
    }

    public void setGcId2(String gcId2) {
        this.gcId2 = gcId2;
    }

    public String getGcId3() {
        return gcId3;
    }

    public void setGcId3(String gcId3) {
        this.gcId3 = gcId3;
    }

    public String getBrowsetime() {
        return browsetime;
    }

    public void setBrowsetime(String browsetime) {
        this.browsetime = browsetime;
    }

    public String getGoodsImageUrl() {
        return goodsImageUrl;
    }

    public void setGoodsImageUrl(String goodsImageUrl) {
        this.goodsImageUrl = goodsImageUrl;
    }

    public String getBrowsetimeDay() {
        return browsetimeDay;
    }

    public void setBrowsetimeDay(String browsetimeDay) {
        this.browsetimeDay = browsetimeDay;
    }

    public String getBrowsetimeText() {
        return browsetimeText;
    }

    public void setBrowsetimeText(String browsetimeText) {
        this.browsetimeText = browsetimeText;
    }

}
