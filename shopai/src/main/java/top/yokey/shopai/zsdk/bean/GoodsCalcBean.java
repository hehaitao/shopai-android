package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class GoodsCalcBean implements Serializable {

    @SerializedName("content")
    private String content = "";
    @SerializedName("if_store_cn")
    private String ifStoreCn = "";
    @SerializedName("if_store")
    private boolean ifStore = false;
    @SerializedName("area_name")
    private String areaName = "";

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIfStoreCn() {
        return ifStoreCn;
    }

    public void setIfStoreCn(String ifStoreCn) {
        this.ifStoreCn = ifStoreCn;
    }

    public boolean isIfStore() {
        return ifStore;
    }

    public void setIfStore(boolean ifStore) {
        this.ifStore = ifStore;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

}
