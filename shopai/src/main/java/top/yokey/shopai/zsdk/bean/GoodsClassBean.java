package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class GoodsClassBean implements Serializable {

    @SerializedName("gc_id")
    private String gcId = "";
    @SerializedName("gc_name")
    private String gcName = "";
    @SerializedName("gc_parent_id")
    private String gcParentId = "";
    @SerializedName("gc_sort")
    private String gcSort = "";
    @SerializedName("deep")
    private String deep = "";
    @SerializedName("app_image")
    private String appImage = "";
    @SerializedName("app_image_url")
    private String appImageUrl = "";
    @SerializedName("is_show")
    private String isShow = "";
    @SerializedName("gc_list")
    private ArrayList<GcListBean> gcList = new ArrayList<>();

    private boolean select = false;

    public String getGcId() {
        return gcId;
    }

    public void setGcId(String gcId) {
        this.gcId = gcId;
    }

    public String getGcName() {
        return gcName;
    }

    public void setGcName(String gcName) {
        this.gcName = gcName;
    }

    public String getGcParentId() {
        return gcParentId;
    }

    public void setGcParentId(String gcParentId) {
        this.gcParentId = gcParentId;
    }

    public String getGcSort() {
        return gcSort;
    }

    public void setGcSort(String gcSort) {
        this.gcSort = gcSort;
    }

    public String getDeep() {
        return deep;
    }

    public void setDeep(String deep) {
        this.deep = deep;
    }

    public String getAppImage() {
        return appImage;
    }

    public void setAppImage(String appImage) {
        this.appImage = appImage;
    }

    public String getAppImageUrl() {
        return appImageUrl;
    }

    public void setAppImageUrl(String appImageUrl) {
        this.appImageUrl = appImageUrl;
    }

    public String getIsShow() {
        return isShow;
    }

    public void setIsShow(String isShow) {
        this.isShow = isShow;
    }

    public ArrayList<GcListBean> getGcList() {
        return gcList;
    }

    public void setGcList(ArrayList<GcListBean> gcList) {
        this.gcList = gcList;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public static class GcListBean {

        @SerializedName("gc_id")
        private String gcId = "";
        @SerializedName("gc_name")
        private String gcName = "";
        @SerializedName("gc_parent_id")
        private String gcParentId = "";
        @SerializedName("gc_sort")
        private String gcSort = "";
        @SerializedName("deep")
        private String deep = "";
        @SerializedName("app_image")
        private String appImage = "";
        @SerializedName("app_image_url")
        private String appImageUrl = "";
        @SerializedName("is_show")
        private String isShow = "";
        @SerializedName("child")
        private ArrayList<ChildBean> child = new ArrayList<>();

        public String getGcId() {
            return gcId;
        }

        public void setGcId(String gcId) {
            this.gcId = gcId;
        }

        public String getGcName() {
            return gcName;
        }

        public void setGcName(String gcName) {
            this.gcName = gcName;
        }

        public String getGcParentId() {
            return gcParentId;
        }

        public void setGcParentId(String gcParentId) {
            this.gcParentId = gcParentId;
        }

        public String getGcSort() {
            return gcSort;
        }

        public void setGcSort(String gcSort) {
            this.gcSort = gcSort;
        }

        public String getDeep() {
            return deep;
        }

        public void setDeep(String deep) {
            this.deep = deep;
        }

        public String getAppImage() {
            return appImage;
        }

        public void setAppImage(String appImage) {
            this.appImage = appImage;
        }

        public String getAppImageUrl() {
            return appImageUrl;
        }

        public void setAppImageUrl(String appImageUrl) {
            this.appImageUrl = appImageUrl;
        }

        public String getIsShow() {
            return isShow;
        }

        public void setIsShow(String isShow) {
            this.isShow = isShow;
        }

        public ArrayList<ChildBean> getChild() {
            return child;
        }

        public void setChild(ArrayList<ChildBean> child) {
            this.child = child;
        }

        public static class ChildBean {

            @SerializedName("gc_id")
            private String gcId = "";
            @SerializedName("gc_name")
            private String gcName = "";
            @SerializedName("gc_parent_id")
            private String gcParentId = "";
            @SerializedName("gc_sort")
            private String gcSort = "";
            @SerializedName("deep")
            private String deep = "";
            @SerializedName("app_image")
            private String appImage = "";
            @SerializedName("app_image_url")
            private String appImageUrl = "";
            @SerializedName("is_show")
            private String isShow = "";

            public String getGcId() {
                return gcId;
            }

            public void setGcId(String gcId) {
                this.gcId = gcId;
            }

            public String getGcName() {
                return gcName;
            }

            public void setGcName(String gcName) {
                this.gcName = gcName;
            }

            public String getGcParentId() {
                return gcParentId;
            }

            public void setGcParentId(String gcParentId) {
                this.gcParentId = gcParentId;
            }

            public String getGcSort() {
                return gcSort;
            }

            public void setGcSort(String gcSort) {
                this.gcSort = gcSort;
            }

            public String getDeep() {
                return deep;
            }

            public void setDeep(String deep) {
                this.deep = deep;
            }

            public String getAppImage() {
                return appImage;
            }

            public void setAppImage(String appImage) {
                this.appImage = appImage;
            }

            public String getAppImageUrl() {
                return appImageUrl;
            }

            public void setAppImageUrl(String appImageUrl) {
                this.appImageUrl = appImageUrl;
            }

            public String getIsShow() {
                return isShow;
            }

            public void setIsShow(String isShow) {
                this.isShow = isShow;
            }

        }

    }

}
