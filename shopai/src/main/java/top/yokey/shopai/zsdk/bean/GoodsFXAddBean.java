package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class GoodsFXAddBean implements Serializable {

    @SerializedName("goods_commonid")
    private String goodsCommonid = "";
    @SerializedName("goods_name")
    private String goodsName = "";
    @SerializedName("goods_jingle")
    private String goodsJingle = "";
    @SerializedName("gc_id")
    private String gcId = "";
    @SerializedName("store_id")
    private String storeId = "";
    @SerializedName("store_name")
    private String storeName = "";
    @SerializedName("goods_price")
    private String goodsPrice = "";
    @SerializedName("goods_image")
    private String goodsImage = "";
    @SerializedName("sale_count")
    private String saleCount = "";
    @SerializedName("click_count")
    private String clickCount = "";
    @SerializedName("gc_id_3")
    private String gcId3 = "";
    @SerializedName("gc_id_1")
    private String gcId1 = "";
    @SerializedName("gc_id_2")
    private String gcId2 = "";
    @SerializedName("goods_verify")
    private String goodsVerify = "";
    @SerializedName("goods_state")
    private String goodsState = "";
    @SerializedName("is_own_shop")
    private String isOwnShop = "";
    @SerializedName("areaid_1")
    private String areaid1 = "";
    @SerializedName("fx_commis_rate")
    private String fxCommisRate = "";
    @SerializedName("sole_flag")
    private boolean soleFlag = false;
    @SerializedName("group_flag")
    private boolean groupFlag = false;
    @SerializedName("xianshi_flag")
    private boolean xianshiFlag = false;
    @SerializedName("goods_image_url")
    private String goodsImageUrl = "";
    @SerializedName("fx_commis_m")
    private String fxCommisM = "";
    @SerializedName("goods_id")
    private String goodsId = "";

    public String getGoodsCommonid() {
        return goodsCommonid;
    }

    public void setGoodsCommonid(String goodsCommonid) {
        this.goodsCommonid = goodsCommonid;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsJingle() {
        return goodsJingle;
    }

    public void setGoodsJingle(String goodsJingle) {
        this.goodsJingle = goodsJingle;
    }

    public String getGcId() {
        return gcId;
    }

    public void setGcId(String gcId) {
        this.gcId = gcId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(String goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public String getGoodsImage() {
        return goodsImage;
    }

    public void setGoodsImage(String goodsImage) {
        this.goodsImage = goodsImage;
    }

    public String getSaleCount() {
        return saleCount == null ? "0" : saleCount;
    }

    public void setSaleCount(String saleCount) {
        this.saleCount = saleCount;
    }

    public String getClickCount() {
        return clickCount == null ? "0" : clickCount;
    }

    public void setClickCount(String clickCount) {
        this.clickCount = clickCount;
    }

    public String getGcId3() {
        return gcId3;
    }

    public void setGcId3(String gcId3) {
        this.gcId3 = gcId3;
    }

    public String getGcId1() {
        return gcId1;
    }

    public void setGcId1(String gcId1) {
        this.gcId1 = gcId1;
    }

    public String getGcId2() {
        return gcId2;
    }

    public void setGcId2(String gcId2) {
        this.gcId2 = gcId2;
    }

    public String getGoodsVerify() {
        return goodsVerify;
    }

    public void setGoodsVerify(String goodsVerify) {
        this.goodsVerify = goodsVerify;
    }

    public String getGoodsState() {
        return goodsState;
    }

    public void setGoodsState(String goodsState) {
        this.goodsState = goodsState;
    }

    public String getIsOwnShop() {
        return isOwnShop;
    }

    public void setIsOwnShop(String isOwnShop) {
        this.isOwnShop = isOwnShop;
    }

    public String getAreaid1() {
        return areaid1;
    }

    public void setAreaid1(String areaid1) {
        this.areaid1 = areaid1;
    }

    public String getFxCommisRate() {
        return fxCommisRate;
    }

    public void setFxCommisRate(String fxCommisRate) {
        this.fxCommisRate = fxCommisRate;
    }

    public boolean isSoleFlag() {
        return soleFlag;
    }

    public void setSoleFlag(boolean soleFlag) {
        this.soleFlag = soleFlag;
    }

    public boolean isGroupFlag() {
        return groupFlag;
    }

    public void setGroupFlag(boolean groupFlag) {
        this.groupFlag = groupFlag;
    }

    public boolean isXianshiFlag() {
        return xianshiFlag;
    }

    public void setXianshiFlag(boolean xianshiFlag) {
        this.xianshiFlag = xianshiFlag;
    }

    public String getGoodsImageUrl() {
        return goodsImageUrl;
    }

    public void setGoodsImageUrl(String goodsImageUrl) {
        this.goodsImageUrl = goodsImageUrl;
    }

    public String getFxCommisM() {
        return fxCommisM;
    }

    public void setFxCommisM(String fxCommisM) {
        this.fxCommisM = fxCommisM;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

}
