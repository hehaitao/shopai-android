package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class GoodsPinGouListBean implements Serializable {

    @SerializedName("pingou_goods_id")
    private String pingouGoodsId = "";
    @SerializedName("pingou_id")
    private String pingouId = "";
    @SerializedName("pingou_name")
    private String pingouName = "";
    @SerializedName("pingou_title")
    private String pingouTitle = "";
    @SerializedName("pingou_explain")
    private String pingouExplain = "";
    @SerializedName("goods_id")
    private String goodsId = "";
    @SerializedName("store_id")
    private String storeId = "";
    @SerializedName("goods_name")
    private String goodsName = "";
    @SerializedName("goods_price")
    private String goodsPrice = "";
    @SerializedName("pingou_price")
    private String pingouPrice = "";
    @SerializedName("goods_image")
    private String goodsImage = "";
    @SerializedName("start_time")
    private String startTime = "";
    @SerializedName("end_time")
    private String endTime = "";
    @SerializedName("min_num")
    private String minNum = "";
    @SerializedName("state")
    private String state = "";
    @SerializedName("goods_maxnum")
    private String goodsMaxnum = "";
    @SerializedName("gc_id")
    private String gcId = "";
    @SerializedName("goods")
    private GoodsBean goods = new GoodsBean();
    @SerializedName("goods_cun")
    private String goodsCun;

    public String getPingouGoodsId() {
        return pingouGoodsId;
    }

    public void setPingouGoodsId(String pingouGoodsId) {
        this.pingouGoodsId = pingouGoodsId;
    }

    public String getPingouId() {
        return pingouId;
    }

    public void setPingouId(String pingouId) {
        this.pingouId = pingouId;
    }

    public String getPingouName() {
        return pingouName;
    }

    public void setPingouName(String pingouName) {
        this.pingouName = pingouName;
    }

    public String getPingouTitle() {
        return pingouTitle;
    }

    public void setPingouTitle(String pingouTitle) {
        this.pingouTitle = pingouTitle;
    }

    public String getPingouExplain() {
        return pingouExplain;
    }

    public void setPingouExplain(String pingouExplain) {
        this.pingouExplain = pingouExplain;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(String goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public String getPingouPrice() {
        return pingouPrice;
    }

    public void setPingouPrice(String pingouPrice) {
        this.pingouPrice = pingouPrice;
    }

    public String getGoodsImage() {
        return goodsImage;
    }

    public void setGoodsImage(String goodsImage) {
        this.goodsImage = goodsImage;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getMinNum() {
        return minNum;
    }

    public void setMinNum(String minNum) {
        this.minNum = minNum;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getGoodsMaxnum() {
        return goodsMaxnum;
    }

    public void setGoodsMaxnum(String goodsMaxnum) {
        this.goodsMaxnum = goodsMaxnum;
    }

    public String getGcId() {
        return gcId;
    }

    public void setGcId(String gcId) {
        this.gcId = gcId;
    }

    public GoodsBean getGoods() {
        return goods;
    }

    public void setGoods(GoodsBean goods) {
        this.goods = goods;
    }

    public String getGoodsCun() {
        return goodsCun;
    }

    public void setGoodsCun(String goodsCun) {
        this.goodsCun = goodsCun;
    }

    public static class GoodsBean {

        @SerializedName("goods_id")
        private String goodsId = "";
        @SerializedName("goods_commonid")
        private String goodsCommonid = "";
        @SerializedName("goods_name")
        private String goodsName = "";
        @SerializedName("goods_jingle")
        private String goodsJingle = "";
        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("store_name")
        private String storeName = "";
        @SerializedName("gc_id")
        private String gcId = "";
        @SerializedName("gc_id_1")
        private String gcId1 = "";
        @SerializedName("gc_id_2")
        private String gcId2 = "";
        @SerializedName("gc_id_3")
        private String gcId3 = "";
        @SerializedName("brand_id")
        private String brandId = "";
        @SerializedName("goods_price")
        private String goodsPrice = "";
        @SerializedName("goods_sale_price")
        private String goodsSalePrice = "";
        @SerializedName("goods_sale_type")
        private String goodsSaleType = "";
        @SerializedName("goods_marketprice")
        private String goodsMarketprice = "";
        @SerializedName("goods_serial")
        private String goodsSerial = "";
        @SerializedName("goods_storage_alarm")
        private String goodsStorageAlarm = "";
        @SerializedName("goods_barcode")
        private String goodsBarcode = "";
        @SerializedName("goods_click")
        private String goodsClick = "";
        @SerializedName("goods_salenum")
        private String goodsSalenum = "";
        @SerializedName("goods_collect")
        private String goodsCollect = "";
        @SerializedName("spec_name")
        private String specName = "";
        @SerializedName("goods_spec")
        private String goodsSpec = "";
        @SerializedName("goods_storage")
        private String goodsStorage = "";
        @SerializedName("goods_image")
        private String goodsImage = "";
        @SerializedName("goods_body")
        private String goodsBody = "";
        @SerializedName("mobile_body")
        private String mobileBody = "";
        @SerializedName("goods_state")
        private String goodsState = "";
        @SerializedName("goods_verify")
        private String goodsVerify = "";
        @SerializedName("goods_addtime")
        private String goodsAddtime = "";
        @SerializedName("goods_edittime")
        private String goodsEdittime = "";
        @SerializedName("areaid_1")
        private String areaid1 = "";
        @SerializedName("areaid_2")
        private String areaid2 = "";
        @SerializedName("areaid_3")
        private String areaid3 = "";
        @SerializedName("color_id")
        private String colorId = "";
        @SerializedName("transport_id")
        private String transportId = "";
        @SerializedName("goods_freight")
        private String goodsFreight = "";
        @SerializedName("goods_trans_v")
        private String goodsTransV = "";
        @SerializedName("goods_vat")
        private String goodsVat = "";
        @SerializedName("goods_commend")
        private String goodsCommend = "";
        @SerializedName("goods_stcids")
        private String goodsStcids = "";
        @SerializedName("evaluation_good_star")
        private String evaluationGoodStar = "";
        @SerializedName("evaluation_count")
        private String evaluationCount = "";
        @SerializedName("is_virtual")
        private String isVirtual = "";
        @SerializedName("virtual_indate")
        private String virtualIndate = "";
        @SerializedName("virtual_limit")
        private String virtualLimit = "";
        @SerializedName("virtual_invalid_refund")
        private String virtualInvalidRefund = "";
        @SerializedName("is_fcode")
        private String isFcode = "";
        @SerializedName("is_presell")
        private String isPresell = "";
        @SerializedName("presell_deliverdate")
        private String presellDeliverdate = "";
        @SerializedName("is_book")
        private String isBook = "";
        @SerializedName("book_down_payment")
        private String bookDownPayment = "";
        @SerializedName("book_final_payment")
        private String bookFinalPayment = "";
        @SerializedName("book_down_time")
        private String bookDownTime = "";
        @SerializedName("book_buyers")
        private String bookBuyers = "";
        @SerializedName("have_gift")
        private String haveGift = "";
        @SerializedName("is_own_shop")
        private String isOwnShop = "";
        @SerializedName("contract_1")
        private String contract1 = "";
        @SerializedName("contract_2")
        private String contract2 = "";
        @SerializedName("contract_3")
        private String contract3 = "";
        @SerializedName("contract_4")
        private String contract4 = "";
        @SerializedName("contract_5")
        private String contract5 = "";
        @SerializedName("contract_6")
        private String contract6 = "";
        @SerializedName("contract_7")
        private String contract7 = "";
        @SerializedName("contract_8")
        private String contract8 = "";
        @SerializedName("contract_9")
        private String contract9 = "";
        @SerializedName("contract_10")
        private String contract10 = "";
        @SerializedName("is_chain")
        private String isChain = "";
        @SerializedName("invite_rate")
        private String inviteRate = "";
        @SerializedName("is_fx")
        private String isFx = "";
        @SerializedName("is_bat")
        private String isBat = "";

        public String getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(String goodsId) {
            this.goodsId = goodsId;
        }

        public String getGoodsCommonid() {
            return goodsCommonid;
        }

        public void setGoodsCommonid(String goodsCommonid) {
            this.goodsCommonid = goodsCommonid;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public String getGoodsJingle() {
            return goodsJingle;
        }

        public void setGoodsJingle(String goodsJingle) {
            this.goodsJingle = goodsJingle;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getGcId() {
            return gcId;
        }

        public void setGcId(String gcId) {
            this.gcId = gcId;
        }

        public String getGcId1() {
            return gcId1;
        }

        public void setGcId1(String gcId1) {
            this.gcId1 = gcId1;
        }

        public String getGcId2() {
            return gcId2;
        }

        public void setGcId2(String gcId2) {
            this.gcId2 = gcId2;
        }

        public String getGcId3() {
            return gcId3;
        }

        public void setGcId3(String gcId3) {
            this.gcId3 = gcId3;
        }

        public String getBrandId() {
            return brandId;
        }

        public void setBrandId(String brandId) {
            this.brandId = brandId;
        }

        public String getGoodsPrice() {
            return goodsPrice;
        }

        public void setGoodsPrice(String goodsPrice) {
            this.goodsPrice = goodsPrice;
        }

        public String getGoodsSalePrice() {
            return goodsSalePrice;
        }

        public void setGoodsSalePrice(String goodsSalePrice) {
            this.goodsSalePrice = goodsSalePrice;
        }

        public String getGoodsSaleType() {
            return goodsSaleType;
        }

        public void setGoodsSaleType(String goodsSaleType) {
            this.goodsSaleType = goodsSaleType;
        }

        public String getGoodsMarketprice() {
            return goodsMarketprice;
        }

        public void setGoodsMarketprice(String goodsMarketprice) {
            this.goodsMarketprice = goodsMarketprice;
        }

        public String getGoodsSerial() {
            return goodsSerial;
        }

        public void setGoodsSerial(String goodsSerial) {
            this.goodsSerial = goodsSerial;
        }

        public String getGoodsStorageAlarm() {
            return goodsStorageAlarm;
        }

        public void setGoodsStorageAlarm(String goodsStorageAlarm) {
            this.goodsStorageAlarm = goodsStorageAlarm;
        }

        public String getGoodsBarcode() {
            return goodsBarcode;
        }

        public void setGoodsBarcode(String goodsBarcode) {
            this.goodsBarcode = goodsBarcode;
        }

        public String getGoodsClick() {
            return goodsClick;
        }

        public void setGoodsClick(String goodsClick) {
            this.goodsClick = goodsClick;
        }

        public String getGoodsSalenum() {
            return goodsSalenum;
        }

        public void setGoodsSalenum(String goodsSalenum) {
            this.goodsSalenum = goodsSalenum;
        }

        public String getGoodsCollect() {
            return goodsCollect;
        }

        public void setGoodsCollect(String goodsCollect) {
            this.goodsCollect = goodsCollect;
        }

        public String getSpecName() {
            return specName;
        }

        public void setSpecName(String specName) {
            this.specName = specName;
        }

        public String getGoodsSpec() {
            return goodsSpec;
        }

        public void setGoodsSpec(String goodsSpec) {
            this.goodsSpec = goodsSpec;
        }

        public String getGoodsStorage() {
            return goodsStorage;
        }

        public void setGoodsStorage(String goodsStorage) {
            this.goodsStorage = goodsStorage;
        }

        public String getGoodsImage() {
            return goodsImage;
        }

        public void setGoodsImage(String goodsImage) {
            this.goodsImage = goodsImage;
        }

        public String getGoodsBody() {
            return goodsBody;
        }

        public void setGoodsBody(String goodsBody) {
            this.goodsBody = goodsBody;
        }

        public String getMobileBody() {
            return mobileBody;
        }

        public void setMobileBody(String mobileBody) {
            this.mobileBody = mobileBody;
        }

        public String getGoodsState() {
            return goodsState;
        }

        public void setGoodsState(String goodsState) {
            this.goodsState = goodsState;
        }

        public String getGoodsVerify() {
            return goodsVerify;
        }

        public void setGoodsVerify(String goodsVerify) {
            this.goodsVerify = goodsVerify;
        }

        public String getGoodsAddtime() {
            return goodsAddtime;
        }

        public void setGoodsAddtime(String goodsAddtime) {
            this.goodsAddtime = goodsAddtime;
        }

        public String getGoodsEdittime() {
            return goodsEdittime;
        }

        public void setGoodsEdittime(String goodsEdittime) {
            this.goodsEdittime = goodsEdittime;
        }

        public String getAreaid1() {
            return areaid1;
        }

        public void setAreaid1(String areaid1) {
            this.areaid1 = areaid1;
        }

        public String getAreaid2() {
            return areaid2;
        }

        public void setAreaid2(String areaid2) {
            this.areaid2 = areaid2;
        }

        public String getAreaid3() {
            return areaid3;
        }

        public void setAreaid3(String areaid3) {
            this.areaid3 = areaid3;
        }

        public String getColorId() {
            return colorId;
        }

        public void setColorId(String colorId) {
            this.colorId = colorId;
        }

        public String getTransportId() {
            return transportId;
        }

        public void setTransportId(String transportId) {
            this.transportId = transportId;
        }

        public String getGoodsFreight() {
            return goodsFreight;
        }

        public void setGoodsFreight(String goodsFreight) {
            this.goodsFreight = goodsFreight;
        }

        public String getGoodsTransV() {
            return goodsTransV;
        }

        public void setGoodsTransV(String goodsTransV) {
            this.goodsTransV = goodsTransV;
        }

        public String getGoodsVat() {
            return goodsVat;
        }

        public void setGoodsVat(String goodsVat) {
            this.goodsVat = goodsVat;
        }

        public String getGoodsCommend() {
            return goodsCommend;
        }

        public void setGoodsCommend(String goodsCommend) {
            this.goodsCommend = goodsCommend;
        }

        public String getGoodsStcids() {
            return goodsStcids;
        }

        public void setGoodsStcids(String goodsStcids) {
            this.goodsStcids = goodsStcids;
        }

        public String getEvaluationGoodStar() {
            return evaluationGoodStar;
        }

        public void setEvaluationGoodStar(String evaluationGoodStar) {
            this.evaluationGoodStar = evaluationGoodStar;
        }

        public String getEvaluationCount() {
            return evaluationCount;
        }

        public void setEvaluationCount(String evaluationCount) {
            this.evaluationCount = evaluationCount;
        }

        public String getIsVirtual() {
            return isVirtual;
        }

        public void setIsVirtual(String isVirtual) {
            this.isVirtual = isVirtual;
        }

        public String getVirtualIndate() {
            return virtualIndate;
        }

        public void setVirtualIndate(String virtualIndate) {
            this.virtualIndate = virtualIndate;
        }

        public String getVirtualLimit() {
            return virtualLimit;
        }

        public void setVirtualLimit(String virtualLimit) {
            this.virtualLimit = virtualLimit;
        }

        public String getVirtualInvalidRefund() {
            return virtualInvalidRefund;
        }

        public void setVirtualInvalidRefund(String virtualInvalidRefund) {
            this.virtualInvalidRefund = virtualInvalidRefund;
        }

        public String getIsFcode() {
            return isFcode;
        }

        public void setIsFcode(String isFcode) {
            this.isFcode = isFcode;
        }

        public String getIsPresell() {
            return isPresell;
        }

        public void setIsPresell(String isPresell) {
            this.isPresell = isPresell;
        }

        public String getPresellDeliverdate() {
            return presellDeliverdate;
        }

        public void setPresellDeliverdate(String presellDeliverdate) {
            this.presellDeliverdate = presellDeliverdate;
        }

        public String getIsBook() {
            return isBook;
        }

        public void setIsBook(String isBook) {
            this.isBook = isBook;
        }

        public String getBookDownPayment() {
            return bookDownPayment;
        }

        public void setBookDownPayment(String bookDownPayment) {
            this.bookDownPayment = bookDownPayment;
        }

        public String getBookFinalPayment() {
            return bookFinalPayment;
        }

        public void setBookFinalPayment(String bookFinalPayment) {
            this.bookFinalPayment = bookFinalPayment;
        }

        public String getBookDownTime() {
            return bookDownTime;
        }

        public void setBookDownTime(String bookDownTime) {
            this.bookDownTime = bookDownTime;
        }

        public String getBookBuyers() {
            return bookBuyers;
        }

        public void setBookBuyers(String bookBuyers) {
            this.bookBuyers = bookBuyers;
        }

        public String getHaveGift() {
            return haveGift;
        }

        public void setHaveGift(String haveGift) {
            this.haveGift = haveGift;
        }

        public String getIsOwnShop() {
            return isOwnShop;
        }

        public void setIsOwnShop(String isOwnShop) {
            this.isOwnShop = isOwnShop;
        }

        public String getContract1() {
            return contract1;
        }

        public void setContract1(String contract1) {
            this.contract1 = contract1;
        }

        public String getContract2() {
            return contract2;
        }

        public void setContract2(String contract2) {
            this.contract2 = contract2;
        }

        public String getContract3() {
            return contract3;
        }

        public void setContract3(String contract3) {
            this.contract3 = contract3;
        }

        public String getContract4() {
            return contract4;
        }

        public void setContract4(String contract4) {
            this.contract4 = contract4;
        }

        public String getContract5() {
            return contract5;
        }

        public void setContract5(String contract5) {
            this.contract5 = contract5;
        }

        public String getContract6() {
            return contract6;
        }

        public void setContract6(String contract6) {
            this.contract6 = contract6;
        }

        public String getContract7() {
            return contract7;
        }

        public void setContract7(String contract7) {
            this.contract7 = contract7;
        }

        public String getContract8() {
            return contract8;
        }

        public void setContract8(String contract8) {
            this.contract8 = contract8;
        }

        public String getContract9() {
            return contract9;
        }

        public void setContract9(String contract9) {
            this.contract9 = contract9;
        }

        public String getContract10() {
            return contract10;
        }

        public void setContract10(String contract10) {
            this.contract10 = contract10;
        }

        public String getIsChain() {
            return isChain;
        }

        public void setIsChain(String isChain) {
            this.isChain = isChain;
        }

        public String getInviteRate() {
            return inviteRate;
        }

        public void setInviteRate(String inviteRate) {
            this.inviteRate = inviteRate;
        }

        public String getIsFx() {
            return isFx;
        }

        public void setIsFx(String isFx) {
            this.isFx = isFx;
        }

        public String getIsBat() {
            return isBat;
        }

        public void setIsBat(String isBat) {
            this.isBat = isBat;
        }

    }

}
