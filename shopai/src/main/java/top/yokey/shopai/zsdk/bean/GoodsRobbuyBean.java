package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class GoodsRobbuyBean implements Serializable {

    @SerializedName("robbuy_id")
    private String robbuyId = "";
    @SerializedName("robbuy_name")
    private String robbuyName = "";
    @SerializedName("start_time")
    private String startTime = "";
    @SerializedName("end_time")
    private String endTime = "";
    @SerializedName("goods_id")
    private String goodsId = "";
    @SerializedName("goods_commonid")
    private String goodsCommonid = "";
    @SerializedName("goods_name")
    private String goodsName = "";
    @SerializedName("store_id")
    private String storeId = "";
    @SerializedName("store_name")
    private String storeName = "";
    @SerializedName("goods_price")
    private String goodsPrice = "";
    @SerializedName("robbuy_price")
    private String robbuyPrice = "";
    @SerializedName("robbuy_rebate")
    private String robbuyRebate = "";
    @SerializedName("virtual_quantity")
    private String virtualQuantity = "";
    @SerializedName("upper_limit")
    private String upperLimit = "";
    @SerializedName("buyer_count")
    private String buyerCount = "";
    @SerializedName("buy_quantity")
    private String buyQuantity = "";
    @SerializedName("robbuy_intro")
    private String robbuyIntro = "";
    @SerializedName("state")
    private String state = "";
    @SerializedName("recommended")
    private String recommended = "";
    @SerializedName("views")
    private String views = "";
    @SerializedName("class_id")
    private String classId = "";
    @SerializedName("s_class_id")
    private String sClassId = "";
    @SerializedName("robbuy_image")
    private String robbuyImage = "";
    @SerializedName("robbuy_image1")
    private String robbuyImage1 = "";
    @SerializedName("remark")
    private String remark = "";
    @SerializedName("is_vr")
    private String isVr = "";
    @SerializedName("vr_city_id")
    private String vrCityId = "";
    @SerializedName("vr_area_id")
    private String vrAreaId = "";
    @SerializedName("vr_mall_id")
    private String vrMallId = "";
    @SerializedName("vr_class_id")
    private String vrClassId = "";
    @SerializedName("vr_s_class_id")
    private String vrSClassId = "";
    @SerializedName("robbuy_url")
    private String robbuyUrl = "";
    @SerializedName("goods_url")
    private String goodsUrl = "";
    @SerializedName("start_time_text")
    private String startTimeText = "";
    @SerializedName("end_time_text")
    private String endTimeText = "";
    @SerializedName("robbuy_state_text")
    private String robbuyStateText = "";
    @SerializedName("reviewable")
    private String reviewable = "";
    @SerializedName("cancelable")
    private String cancelable = "";
    @SerializedName("state_flag")
    private String stateFlag = "";
    @SerializedName("button_text")
    private String buttonText = "";
    @SerializedName("count_down_text")
    private String countDownText = "";
    @SerializedName("count_down")
    private String countDown = "";
    @SerializedName("goods_image_url")
    private String goodsImageUrl = "";
    @SerializedName("endtime")
    private String endtime = "";
    @SerializedName("robbuy_rand")
    private String robbuyRand = "";

    public String getRobbuyId() {
        return robbuyId;
    }

    public void setRobbuyId(String robbuyId) {
        this.robbuyId = robbuyId;
    }

    public String getRobbuyName() {
        return robbuyName;
    }

    public void setRobbuyName(String robbuyName) {
        this.robbuyName = robbuyName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsCommonid() {
        return goodsCommonid;
    }

    public void setGoodsCommonid(String goodsCommonid) {
        this.goodsCommonid = goodsCommonid;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(String goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public String getRobbuyPrice() {
        return robbuyPrice;
    }

    public void setRobbuyPrice(String robbuyPrice) {
        this.robbuyPrice = robbuyPrice;
    }

    public String getRobbuyRebate() {
        return robbuyRebate;
    }

    public void setRobbuyRebate(String robbuyRebate) {
        this.robbuyRebate = robbuyRebate;
    }

    public String getVirtualQuantity() {
        return virtualQuantity;
    }

    public void setVirtualQuantity(String virtualQuantity) {
        this.virtualQuantity = virtualQuantity;
    }

    public String getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(String upperLimit) {
        this.upperLimit = upperLimit;
    }

    public String getBuyerCount() {
        return buyerCount;
    }

    public void setBuyerCount(String buyerCount) {
        this.buyerCount = buyerCount;
    }

    public String getBuyQuantity() {
        return buyQuantity;
    }

    public void setBuyQuantity(String buyQuantity) {
        this.buyQuantity = buyQuantity;
    }

    public String getRobbuyIntro() {
        return robbuyIntro;
    }

    public void setRobbuyIntro(String robbuyIntro) {
        this.robbuyIntro = robbuyIntro;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getRecommended() {
        return recommended;
    }

    public void setRecommended(String recommended) {
        this.recommended = recommended;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getsClassId() {
        return sClassId;
    }

    public void setsClassId(String sClassId) {
        this.sClassId = sClassId;
    }

    public String getRobbuyImage() {
        return robbuyImage;
    }

    public void setRobbuyImage(String robbuyImage) {
        this.robbuyImage = robbuyImage;
    }

    public String getRobbuyImage1() {
        return robbuyImage1;
    }

    public void setRobbuyImage1(String robbuyImage1) {
        this.robbuyImage1 = robbuyImage1;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getIsVr() {
        return isVr;
    }

    public void setIsVr(String isVr) {
        this.isVr = isVr;
    }

    public String getVrCityId() {
        return vrCityId;
    }

    public void setVrCityId(String vrCityId) {
        this.vrCityId = vrCityId;
    }

    public String getVrAreaId() {
        return vrAreaId;
    }

    public void setVrAreaId(String vrAreaId) {
        this.vrAreaId = vrAreaId;
    }

    public String getVrMallId() {
        return vrMallId;
    }

    public void setVrMallId(String vrMallId) {
        this.vrMallId = vrMallId;
    }

    public String getVrClassId() {
        return vrClassId;
    }

    public void setVrClassId(String vrClassId) {
        this.vrClassId = vrClassId;
    }

    public String getVrSClassId() {
        return vrSClassId;
    }

    public void setVrSClassId(String vrSClassId) {
        this.vrSClassId = vrSClassId;
    }

    public String getRobbuyUrl() {
        return robbuyUrl;
    }

    public void setRobbuyUrl(String robbuyUrl) {
        this.robbuyUrl = robbuyUrl;
    }

    public String getGoodsUrl() {
        return goodsUrl;
    }

    public void setGoodsUrl(String goodsUrl) {
        this.goodsUrl = goodsUrl;
    }

    public String getStartTimeText() {
        return startTimeText;
    }

    public void setStartTimeText(String startTimeText) {
        this.startTimeText = startTimeText;
    }

    public String getEndTimeText() {
        return endTimeText;
    }

    public void setEndTimeText(String endTimeText) {
        this.endTimeText = endTimeText;
    }

    public String getRobbuyStateText() {
        return robbuyStateText;
    }

    public void setRobbuyStateText(String robbuyStateText) {
        this.robbuyStateText = robbuyStateText;
    }

    public String getReviewable() {
        return reviewable;
    }

    public void setReviewable(String reviewable) {
        this.reviewable = reviewable;
    }

    public String getCancelable() {
        return cancelable;
    }

    public void setCancelable(String cancelable) {
        this.cancelable = cancelable;
    }

    public String getStateFlag() {
        return stateFlag;
    }

    public void setStateFlag(String stateFlag) {
        this.stateFlag = stateFlag;
    }

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    public String getCountDownText() {
        return countDownText;
    }

    public void setCountDownText(String countDownText) {
        this.countDownText = countDownText;
    }

    public String getCountDown() {
        return countDown;
    }

    public void setCountDown(String countDown) {
        this.countDown = countDown;
    }

    public String getGoodsImageUrl() {
        return goodsImageUrl;
    }

    public void setGoodsImageUrl(String goodsImageUrl) {
        this.goodsImageUrl = goodsImageUrl;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getRobbuyRand() {
        return robbuyRand;
    }

    public void setRobbuyRand(String robbuyRand) {
        this.robbuyRand = robbuyRand;
    }

}
