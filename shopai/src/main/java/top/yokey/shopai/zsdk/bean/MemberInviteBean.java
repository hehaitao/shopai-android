package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class MemberInviteBean implements Serializable {

    @SerializedName("user_name")
    private String userName = "";
    @SerializedName("user_id")
    private String userId = "";
    @SerializedName("avator")
    private String avator = "";
    @SerializedName("point")
    private String point = "";
    @SerializedName("predepoit")
    private String predepoit = "";
    @SerializedName("available_rc_balance")
    private String availableRcBalance = "";
    @SerializedName("myurl")
    private String myurl = "";
    @SerializedName("myurl_src")
    private String myurlSrc = "";
    @SerializedName("mydownurl")
    private String mydownurl = "";
    @SerializedName("html_title")
    private String htmlTitle = "";
    @SerializedName("seo_description")
    private String seoDescription = "";

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAvator() {
        return avator;
    }

    public void setAvator(String avator) {
        this.avator = avator;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getPredepoit() {
        return predepoit;
    }

    public void setPredepoit(String predepoit) {
        this.predepoit = predepoit;
    }

    public String getAvailableRcBalance() {
        return availableRcBalance;
    }

    public void setAvailableRcBalance(String availableRcBalance) {
        this.availableRcBalance = availableRcBalance;
    }

    public String getMyurl() {
        return myurl;
    }

    public void setMyurl(String myurl) {
        this.myurl = myurl;
    }

    public String getMyurlSrc() {
        return myurlSrc;
    }

    public void setMyurlSrc(String myurlSrc) {
        this.myurlSrc = myurlSrc;
    }

    public String getMydownurl() {
        return mydownurl;
    }

    public void setMydownurl(String mydownurl) {
        this.mydownurl = mydownurl;
    }

    public String getHtmlTitle() {
        return htmlTitle;
    }

    public void setHtmlTitle(String htmlTitle) {
        this.htmlTitle = htmlTitle;
    }

    public String getSeoDescription() {
        return seoDescription;
    }

    public void setSeoDescription(String seoDescription) {
        this.seoDescription = seoDescription;
    }

}
