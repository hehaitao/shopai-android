package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("All")
public class OrderEvaluateBean implements Serializable {

    @SerializedName("store_info")
    private StoreInfoBean storeInfo = null;
    @SerializedName("order_goods")
    private ArrayList<OrderGoodsBean> orderGoods = new ArrayList<>();

    public StoreInfoBean getStoreInfo() {
        return storeInfo;
    }

    public void setStoreInfo(StoreInfoBean storeInfo) {
        this.storeInfo = storeInfo;
    }

    public ArrayList<OrderGoodsBean> getOrderGoods() {
        return orderGoods;
    }

    public void setOrderGoods(ArrayList<OrderGoodsBean> orderGoods) {
        this.orderGoods = orderGoods;
    }

    public static class StoreInfoBean {

        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("store_name")
        private String storeName = "";
        @SerializedName("is_own_shop")
        private String isOwnShop = "";

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getIsOwnShop() {
            return isOwnShop;
        }

        public void setIsOwnShop(String isOwnShop) {
            this.isOwnShop = isOwnShop;
        }

    }

    public static class OrderGoodsBean {

        @SerializedName("rec_id")
        private String recId = "";
        @SerializedName("order_id")
        private String orderId = "";
        @SerializedName("goods_id")
        private String goodsId = "";
        @SerializedName("goods_name")
        private String goodsName = "";
        @SerializedName("goods_price")
        private String goodsPrice = "";
        @SerializedName("goods_num")
        private String goodsNum = "";
        @SerializedName("goods_image")
        private String goodsImage = "";
        @SerializedName("goods_pay_price")
        private String goodsPayPrice = "";
        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("buyer_id")
        private String buyerId = "";
        @SerializedName("goods_type")
        private String goodsType = "";
        @SerializedName("promotions_id")
        private String promotionsId = "";
        @SerializedName("commis_rate")
        private String commisRate = "";
        @SerializedName("gc_id")
        private String gcId = "";
        @SerializedName("goods_spec")
        private String goodsSpec = "";
        @SerializedName("goods_contractid")
        private String goodsContractid = "";
        @SerializedName("invite_rates")
        private String inviteRates = "";
        @SerializedName("goods_image_url")
        private String goodsImageUrl = "";

        private String evaluateRating = "5";
        private String evaluateContent = "";
        private String evaluateImage0 = "";
        private String evaluateImage1 = "";
        private String evaluateImage2 = "";
        private String evaluateImage3 = "";
        private String evaluateImage4 = "";
        private String evaluateImage0Name = "";
        private String evaluateImage1Name = "";
        private String evaluateImage2Name = "";
        private String evaluateImage3Name = "";
        private String evaluateImage4Name = "";

        public String getRecId() {
            return recId;
        }

        public void setRecId(String recId) {
            this.recId = recId;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(String goodsId) {
            this.goodsId = goodsId;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public String getGoodsPrice() {
            return goodsPrice;
        }

        public void setGoodsPrice(String goodsPrice) {
            this.goodsPrice = goodsPrice;
        }

        public String getGoodsNum() {
            return goodsNum;
        }

        public void setGoodsNum(String goodsNum) {
            this.goodsNum = goodsNum;
        }

        public String getGoodsImage() {
            return goodsImage;
        }

        public void setGoodsImage(String goodsImage) {
            this.goodsImage = goodsImage;
        }

        public String getGoodsPayPrice() {
            return goodsPayPrice;
        }

        public void setGoodsPayPrice(String goodsPayPrice) {
            this.goodsPayPrice = goodsPayPrice;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getBuyerId() {
            return buyerId;
        }

        public void setBuyerId(String buyerId) {
            this.buyerId = buyerId;
        }

        public String getGoodsType() {
            return goodsType;
        }

        public void setGoodsType(String goodsType) {
            this.goodsType = goodsType;
        }

        public String getPromotionsId() {
            return promotionsId;
        }

        public void setPromotionsId(String promotionsId) {
            this.promotionsId = promotionsId;
        }

        public String getCommisRate() {
            return commisRate;
        }

        public void setCommisRate(String commisRate) {
            this.commisRate = commisRate;
        }

        public String getGcId() {
            return gcId;
        }

        public void setGcId(String gcId) {
            this.gcId = gcId;
        }

        public String getGoodsSpec() {
            return goodsSpec;
        }

        public void setGoodsSpec(String goodsSpec) {
            this.goodsSpec = goodsSpec;
        }

        public String getGoodsContractid() {
            return goodsContractid;
        }

        public void setGoodsContractid(String goodsContractid) {
            this.goodsContractid = goodsContractid;
        }

        public String getInviteRates() {
            return inviteRates;
        }

        public void setInviteRates(String inviteRates) {
            this.inviteRates = inviteRates;
        }

        public String getGoodsImageUrl() {
            return goodsImageUrl;
        }

        public void setGoodsImageUrl(String goodsImageUrl) {
            this.goodsImageUrl = goodsImageUrl;
        }

        public String getEvaluateRating() {
            return evaluateRating;
        }

        public void setEvaluateRating(String evaluateRating) {
            this.evaluateRating = evaluateRating;
        }

        public String getEvaluateContent() {
            return evaluateContent;
        }

        public void setEvaluateContent(String evaluateContent) {
            this.evaluateContent = evaluateContent;
        }

        public String getEvaluateImage0() {
            return evaluateImage0;
        }

        public void setEvaluateImage0(String evaluateImage0) {
            this.evaluateImage0 = evaluateImage0;
        }

        public String getEvaluateImage1() {
            return evaluateImage1;
        }

        public void setEvaluateImage1(String evaluateImage1) {
            this.evaluateImage1 = evaluateImage1;
        }

        public String getEvaluateImage2() {
            return evaluateImage2;
        }

        public void setEvaluateImage2(String evaluateImage2) {
            this.evaluateImage2 = evaluateImage2;
        }

        public String getEvaluateImage3() {
            return evaluateImage3;
        }

        public void setEvaluateImage3(String evaluateImage3) {
            this.evaluateImage3 = evaluateImage3;
        }

        public String getEvaluateImage4() {
            return evaluateImage4;
        }

        public void setEvaluateImage4(String evaluateImage4) {
            this.evaluateImage4 = evaluateImage4;
        }

        public String getEvaluateImage0Name() {
            return evaluateImage0Name;
        }

        public void setEvaluateImage0Name(String evaluateImage0Name) {
            this.evaluateImage0Name = evaluateImage0Name;
        }

        public String getEvaluateImage1Name() {
            return evaluateImage1Name;
        }

        public void setEvaluateImage1Name(String evaluateImage1Name) {
            this.evaluateImage1Name = evaluateImage1Name;
        }

        public String getEvaluateImage2Name() {
            return evaluateImage2Name;
        }

        public void setEvaluateImage2Name(String evaluateImage2Name) {
            this.evaluateImage2Name = evaluateImage2Name;
        }

        public String getEvaluateImage3Name() {
            return evaluateImage3Name;
        }

        public void setEvaluateImage3Name(String evaluateImage3Name) {
            this.evaluateImage3Name = evaluateImage3Name;
        }

        public String getEvaluateImage4Name() {
            return evaluateImage4Name;
        }

        public void setEvaluateImage4Name(String evaluateImage4Name) {
            this.evaluateImage4Name = evaluateImage4Name;
        }

    }

}
