package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class PointGoodsBean implements Serializable {

    @SerializedName("pgoods_id")
    private String pgoodsId = "";
    @SerializedName("pgoods_name")
    private String pgoodsName = "";
    @SerializedName("pgoods_price")
    private String pgoodsPrice = "";
    @SerializedName("pgoods_points")
    private String pgoodsPoints = "";
    @SerializedName("pgoods_image")
    private String pgoodsImage = "";
    @SerializedName("pgoods_tag")
    private String pgoodsTag = "";
    @SerializedName("pgoods_serial")
    private String pgoodsSerial = "";
    @SerializedName("pgoods_storage")
    private String pgoodsStorage = "";
    @SerializedName("pgoods_show")
    private String pgoodsShow = "";
    @SerializedName("pgoods_commend")
    private String pgoodsCommend = "";
    @SerializedName("pgoods_add_time")
    private String pgoodsAddTime = "";
    @SerializedName("pgoods_keywords")
    private String pgoodsKeywords = "";
    @SerializedName("pgoods_description")
    private String pgoodsDescription = "";
    @SerializedName("pgoods_body")
    private String pgoodsBody = "";
    @SerializedName("pgoods_state")
    private String pgoodsState = "";
    @SerializedName("pgoods_close_reason")
    private String pgoodsCloseReason = "";
    @SerializedName("pgoods_salenum")
    private String pgoodsSalenum = "";
    @SerializedName("pgoods_view")
    private String pgoodsView = "";
    @SerializedName("pgoods_islimit")
    private String pgoodsIslimit = "";
    @SerializedName("pgoods_limitnum")
    private String pgoodsLimitnum = "";
    @SerializedName("pgoods_islimittime")
    private String pgoodsIslimittime = "";
    @SerializedName("pgoods_limitmgrade")
    private String pgoodsLimitmgrade = "";
    @SerializedName("pgoods_starttime")
    private String pgoodsStarttime = "";
    @SerializedName("pgoods_endtime")
    private String pgoodsEndtime = "";
    @SerializedName("pgoods_sort")
    private String pgoodsSort = "";
    @SerializedName("pgoods_image_old")
    private String pgoodsImageOld = "";
    @SerializedName("pgoods_image_max")
    private String pgoodsImageMax = "";
    @SerializedName("pgoods_image_small")
    private String pgoodsImageSmall = "";
    @SerializedName("ex_state")
    private String exState = "";

    public String getPgoodsId() {
        return pgoodsId;
    }

    public void setPgoodsId(String pgoodsId) {
        this.pgoodsId = pgoodsId;
    }

    public String getPgoodsName() {
        return pgoodsName;
    }

    public void setPgoodsName(String pgoodsName) {
        this.pgoodsName = pgoodsName;
    }

    public String getPgoodsPrice() {
        return pgoodsPrice;
    }

    public void setPgoodsPrice(String pgoodsPrice) {
        this.pgoodsPrice = pgoodsPrice;
    }

    public String getPgoodsPoints() {
        return pgoodsPoints;
    }

    public void setPgoodsPoints(String pgoodsPoints) {
        this.pgoodsPoints = pgoodsPoints;
    }

    public String getPgoodsImage() {
        return pgoodsImage;
    }

    public void setPgoodsImage(String pgoodsImage) {
        this.pgoodsImage = pgoodsImage;
    }

    public String getPgoodsTag() {
        return pgoodsTag;
    }

    public void setPgoodsTag(String pgoodsTag) {
        this.pgoodsTag = pgoodsTag;
    }

    public String getPgoodsSerial() {
        return pgoodsSerial;
    }

    public void setPgoodsSerial(String pgoodsSerial) {
        this.pgoodsSerial = pgoodsSerial;
    }

    public String getPgoodsStorage() {
        return pgoodsStorage;
    }

    public void setPgoodsStorage(String pgoodsStorage) {
        this.pgoodsStorage = pgoodsStorage;
    }

    public String getPgoodsShow() {
        return pgoodsShow;
    }

    public void setPgoodsShow(String pgoodsShow) {
        this.pgoodsShow = pgoodsShow;
    }

    public String getPgoodsCommend() {
        return pgoodsCommend;
    }

    public void setPgoodsCommend(String pgoodsCommend) {
        this.pgoodsCommend = pgoodsCommend;
    }

    public String getPgoodsAddTime() {
        return pgoodsAddTime;
    }

    public void setPgoodsAddTime(String pgoodsAddTime) {
        this.pgoodsAddTime = pgoodsAddTime;
    }

    public String getPgoodsKeywords() {
        return pgoodsKeywords;
    }

    public void setPgoodsKeywords(String pgoodsKeywords) {
        this.pgoodsKeywords = pgoodsKeywords;
    }

    public String getPgoodsDescription() {
        return pgoodsDescription;
    }

    public void setPgoodsDescription(String pgoodsDescription) {
        this.pgoodsDescription = pgoodsDescription;
    }

    public String getPgoodsBody() {
        return pgoodsBody;
    }

    public void setPgoodsBody(String pgoodsBody) {
        this.pgoodsBody = pgoodsBody;
    }

    public String getPgoodsState() {
        return pgoodsState;
    }

    public void setPgoodsState(String pgoodsState) {
        this.pgoodsState = pgoodsState;
    }

    public String getPgoodsCloseReason() {
        return pgoodsCloseReason;
    }

    public void setPgoodsCloseReason(String pgoodsCloseReason) {
        this.pgoodsCloseReason = pgoodsCloseReason;
    }

    public String getPgoodsSalenum() {
        return pgoodsSalenum;
    }

    public void setPgoodsSalenum(String pgoodsSalenum) {
        this.pgoodsSalenum = pgoodsSalenum;
    }

    public String getPgoodsView() {
        return pgoodsView;
    }

    public void setPgoodsView(String pgoodsView) {
        this.pgoodsView = pgoodsView;
    }

    public String getPgoodsIslimit() {
        return pgoodsIslimit;
    }

    public void setPgoodsIslimit(String pgoodsIslimit) {
        this.pgoodsIslimit = pgoodsIslimit;
    }

    public String getPgoodsLimitnum() {
        return pgoodsLimitnum;
    }

    public void setPgoodsLimitnum(String pgoodsLimitnum) {
        this.pgoodsLimitnum = pgoodsLimitnum;
    }

    public String getPgoodsIslimittime() {
        return pgoodsIslimittime;
    }

    public void setPgoodsIslimittime(String pgoodsIslimittime) {
        this.pgoodsIslimittime = pgoodsIslimittime;
    }

    public String getPgoodsLimitmgrade() {
        return pgoodsLimitmgrade;
    }

    public void setPgoodsLimitmgrade(String pgoodsLimitmgrade) {
        this.pgoodsLimitmgrade = pgoodsLimitmgrade;
    }

    public String getPgoodsStarttime() {
        return pgoodsStarttime;
    }

    public void setPgoodsStarttime(String pgoodsStarttime) {
        this.pgoodsStarttime = pgoodsStarttime;
    }

    public String getPgoodsEndtime() {
        return pgoodsEndtime;
    }

    public void setPgoodsEndtime(String pgoodsEndtime) {
        this.pgoodsEndtime = pgoodsEndtime;
    }

    public String getPgoodsSort() {
        return pgoodsSort;
    }

    public void setPgoodsSort(String pgoodsSort) {
        this.pgoodsSort = pgoodsSort;
    }

    public String getPgoodsImageOld() {
        return pgoodsImageOld;
    }

    public void setPgoodsImageOld(String pgoodsImageOld) {
        this.pgoodsImageOld = pgoodsImageOld;
    }

    public String getPgoodsImageMax() {
        return pgoodsImageMax;
    }

    public void setPgoodsImageMax(String pgoodsImageMax) {
        this.pgoodsImageMax = pgoodsImageMax;
    }

    public String getPgoodsImageSmall() {
        return pgoodsImageSmall;
    }

    public void setPgoodsImageSmall(String pgoodsImageSmall) {
        this.pgoodsImageSmall = pgoodsImageSmall;
    }

    public String getExState() {
        return exState;
    }

    public void setExState(String exState) {
        this.exState = exState;
    }

}
