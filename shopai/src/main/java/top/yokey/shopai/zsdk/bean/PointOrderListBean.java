package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class PointOrderListBean implements Serializable {

    @SerializedName("point_orderid")
    private String pointOrderid = "";
    @SerializedName("point_ordersn")
    private String pointOrdersn = "";
    @SerializedName("point_buyerid")
    private String pointBuyerid = "";
    @SerializedName("point_buyername")
    private String pointBuyername = "";
    @SerializedName("point_buyeremail")
    private String pointBuyeremail = "";
    @SerializedName("point_addtime")
    private String pointAddtime = "";
    @SerializedName("point_shippingtime")
    private String pointShippingtime = "";
    @SerializedName("point_shippingcode")
    private String pointShippingcode = "";
    @SerializedName("point_shipping_ecode")
    private String pointShippingEcode = "";
    @SerializedName("point_finnshedtime")
    private String pointFinnshedtime = "";
    @SerializedName("point_allpoint")
    private String pointAllpoint = "";
    @SerializedName("point_ordermessage")
    private String pointOrdermessage = "";
    @SerializedName("point_orderstate")
    private String pointOrderstate = "";
    @SerializedName("point_orderstatetext")
    private String pointOrderstatetext = "";
    @SerializedName("point_orderstatesign")
    private String pointOrderstatesign = "";
    @SerializedName("point_orderallowship")
    private boolean pointOrderallowship = false;
    @SerializedName("point_orderalloweditship")
    private boolean pointOrderalloweditship = false;
    @SerializedName("point_orderallowcancel")
    private boolean pointOrderallowcancel = false;
    @SerializedName("point_orderallowdelete")
    private boolean pointOrderallowdelete = false;
    @SerializedName("point_orderallowreceiving")
    private boolean pointOrderallowreceiving = false;
    @SerializedName("state_desc")
    private String stateDesc = "";
    @SerializedName("prodlist")
    private ArrayList<ProdlistBean> prodlist = new ArrayList<>();

    public String getPointOrderid() {
        return pointOrderid;
    }

    public void setPointOrderid(String pointOrderid) {
        this.pointOrderid = pointOrderid;
    }

    public String getPointOrdersn() {
        return pointOrdersn;
    }

    public void setPointOrdersn(String pointOrdersn) {
        this.pointOrdersn = pointOrdersn;
    }

    public String getPointBuyerid() {
        return pointBuyerid;
    }

    public void setPointBuyerid(String pointBuyerid) {
        this.pointBuyerid = pointBuyerid;
    }

    public String getPointBuyername() {
        return pointBuyername;
    }

    public void setPointBuyername(String pointBuyername) {
        this.pointBuyername = pointBuyername;
    }

    public String getPointBuyeremail() {
        return pointBuyeremail;
    }

    public void setPointBuyeremail(String pointBuyeremail) {
        this.pointBuyeremail = pointBuyeremail;
    }

    public String getPointAddtime() {
        return pointAddtime;
    }

    public void setPointAddtime(String pointAddtime) {
        this.pointAddtime = pointAddtime;
    }

    public String getPointShippingtime() {
        return pointShippingtime;
    }

    public void setPointShippingtime(String pointShippingtime) {
        this.pointShippingtime = pointShippingtime;
    }

    public String getPointShippingcode() {
        return pointShippingcode;
    }

    public void setPointShippingcode(String pointShippingcode) {
        this.pointShippingcode = pointShippingcode;
    }

    public String getPointShippingEcode() {
        return pointShippingEcode;
    }

    public void setPointShippingEcode(String pointShippingEcode) {
        this.pointShippingEcode = pointShippingEcode;
    }

    public String getPointFinnshedtime() {
        return pointFinnshedtime;
    }

    public void setPointFinnshedtime(String pointFinnshedtime) {
        this.pointFinnshedtime = pointFinnshedtime;
    }

    public String getPointAllpoint() {
        return pointAllpoint;
    }

    public void setPointAllpoint(String pointAllpoint) {
        this.pointAllpoint = pointAllpoint;
    }

    public String getPointOrdermessage() {
        return pointOrdermessage;
    }

    public void setPointOrdermessage(String pointOrdermessage) {
        this.pointOrdermessage = pointOrdermessage;
    }

    public String getPointOrderstate() {
        return pointOrderstate;
    }

    public void setPointOrderstate(String pointOrderstate) {
        this.pointOrderstate = pointOrderstate;
    }

    public String getPointOrderstatetext() {
        return pointOrderstatetext;
    }

    public void setPointOrderstatetext(String pointOrderstatetext) {
        this.pointOrderstatetext = pointOrderstatetext;
    }

    public String getPointOrderstatesign() {
        return pointOrderstatesign;
    }

    public void setPointOrderstatesign(String pointOrderstatesign) {
        this.pointOrderstatesign = pointOrderstatesign;
    }

    public boolean isPointOrderallowship() {
        return pointOrderallowship;
    }

    public void setPointOrderallowship(boolean pointOrderallowship) {
        this.pointOrderallowship = pointOrderallowship;
    }

    public boolean isPointOrderalloweditship() {
        return pointOrderalloweditship;
    }

    public void setPointOrderalloweditship(boolean pointOrderalloweditship) {
        this.pointOrderalloweditship = pointOrderalloweditship;
    }

    public boolean isPointOrderallowcancel() {
        return pointOrderallowcancel;
    }

    public void setPointOrderallowcancel(boolean pointOrderallowcancel) {
        this.pointOrderallowcancel = pointOrderallowcancel;
    }

    public boolean isPointOrderallowdelete() {
        return pointOrderallowdelete;
    }

    public void setPointOrderallowdelete(boolean pointOrderallowdelete) {
        this.pointOrderallowdelete = pointOrderallowdelete;
    }

    public boolean isPointOrderallowreceiving() {
        return pointOrderallowreceiving;
    }

    public void setPointOrderallowreceiving(boolean pointOrderallowreceiving) {
        this.pointOrderallowreceiving = pointOrderallowreceiving;
    }

    public String getStateDesc() {
        return stateDesc;
    }

    public void setStateDesc(String stateDesc) {
        this.stateDesc = stateDesc;
    }

    public ArrayList<ProdlistBean> getProdlist() {
        return prodlist;
    }

    public void setProdlist(ArrayList<ProdlistBean> prodlist) {
        this.prodlist = prodlist;
    }

    public static class ProdlistBean {

        @SerializedName("point_recid")
        private String pointRecid = "";
        @SerializedName("point_orderid")
        private String pointOrderid = "";
        @SerializedName("point_goodsid")
        private String pointGoodsid = "";
        @SerializedName("point_goodsname")
        private String pointGoodsname = "";
        @SerializedName("point_goodspoints")
        private String pointGoodspoints = "";
        @SerializedName("point_goodsnum")
        private String pointGoodsnum = "";
        @SerializedName("point_goodsimage")
        private String pointGoodsimage = "";
        @SerializedName("point_goodsimage_old")
        private String pointGoodsimageOld = "";
        @SerializedName("point_goodsimage_small")
        private String pointGoodsimageSmall = "";

        public String getPointRecid() {
            return pointRecid;
        }

        public void setPointRecid(String pointRecid) {
            this.pointRecid = pointRecid;
        }

        public String getPointOrderid() {
            return pointOrderid;
        }

        public void setPointOrderid(String pointOrderid) {
            this.pointOrderid = pointOrderid;
        }

        public String getPointGoodsid() {
            return pointGoodsid;
        }

        public void setPointGoodsid(String pointGoodsid) {
            this.pointGoodsid = pointGoodsid;
        }

        public String getPointGoodsname() {
            return pointGoodsname;
        }

        public void setPointGoodsname(String pointGoodsname) {
            this.pointGoodsname = pointGoodsname;
        }

        public String getPointGoodspoints() {
            return pointGoodspoints;
        }

        public void setPointGoodspoints(String pointGoodspoints) {
            this.pointGoodspoints = pointGoodspoints;
        }

        public String getPointGoodsnum() {
            return pointGoodsnum;
        }

        public void setPointGoodsnum(String pointGoodsnum) {
            this.pointGoodsnum = pointGoodsnum;
        }

        public String getPointGoodsimage() {
            return pointGoodsimage;
        }

        public void setPointGoodsimage(String pointGoodsimage) {
            this.pointGoodsimage = pointGoodsimage;
        }

        public String getPointGoodsimageOld() {
            return pointGoodsimageOld;
        }

        public void setPointGoodsimageOld(String pointGoodsimageOld) {
            this.pointGoodsimageOld = pointGoodsimageOld;
        }

        public String getPointGoodsimageSmall() {
            return pointGoodsimageSmall;
        }

        public void setPointGoodsimageSmall(String pointGoodsimageSmall) {
            this.pointGoodsimageSmall = pointGoodsimageSmall;
        }

    }

}
