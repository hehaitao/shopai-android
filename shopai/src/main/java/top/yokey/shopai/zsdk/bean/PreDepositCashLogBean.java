package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class PreDepositCashLogBean implements Serializable {

    @SerializedName("pdc_id")
    private String pdcId = "";
    @SerializedName("pdc_sn")
    private String pdcSn = "";
    @SerializedName("pdc_member_id")
    private String pdcMemberId = "";
    @SerializedName("pdc_member_name")
    private String pdcMemberName = "";
    @SerializedName("pdc_amount")
    private String pdcAmount = "";
    @SerializedName("pdc_bank_name")
    private String pdcBankName = "";
    @SerializedName("pdc_bank_no")
    private String pdcBankNo = "";
    @SerializedName("pdc_bank_user")
    private String pdcBankUser = "";
    @SerializedName("pdc_add_time")
    private String pdcAddTime = "";
    @SerializedName("pdc_payment_time")
    private String pdcPaymentTime = "";
    @SerializedName("pdc_payment_state")
    private String pdcPaymentState = "";
    @SerializedName("pdc_payment_admin")
    private String pdcPaymentAdmin = "";
    @SerializedName("pdc_add_time_text")
    private String pdcAddTimeText = "";
    @SerializedName("pdc_payment_time_text")
    private String pdcPaymentTimeText = "";
    @SerializedName("pdc_payment_state_text")
    private String pdcPaymentStateText = "";

    public String getPdcId() {
        return pdcId;
    }

    public void setPdcId(String pdcId) {
        this.pdcId = pdcId;
    }

    public String getPdcSn() {
        return pdcSn;
    }

    public void setPdcSn(String pdcSn) {
        this.pdcSn = pdcSn;
    }

    public String getPdcMemberId() {
        return pdcMemberId;
    }

    public void setPdcMemberId(String pdcMemberId) {
        this.pdcMemberId = pdcMemberId;
    }

    public String getPdcMemberName() {
        return pdcMemberName;
    }

    public void setPdcMemberName(String pdcMemberName) {
        this.pdcMemberName = pdcMemberName;
    }

    public String getPdcAmount() {
        return pdcAmount;
    }

    public void setPdcAmount(String pdcAmount) {
        this.pdcAmount = pdcAmount;
    }

    public String getPdcBankName() {
        return pdcBankName;
    }

    public void setPdcBankName(String pdcBankName) {
        this.pdcBankName = pdcBankName;
    }

    public String getPdcBankNo() {
        return pdcBankNo;
    }

    public void setPdcBankNo(String pdcBankNo) {
        this.pdcBankNo = pdcBankNo;
    }

    public String getPdcBankUser() {
        return pdcBankUser;
    }

    public void setPdcBankUser(String pdcBankUser) {
        this.pdcBankUser = pdcBankUser;
    }

    public String getPdcAddTime() {
        return pdcAddTime;
    }

    public void setPdcAddTime(String pdcAddTime) {
        this.pdcAddTime = pdcAddTime;
    }

    public String getPdcPaymentTime() {
        return pdcPaymentTime;
    }

    public void setPdcPaymentTime(String pdcPaymentTime) {
        this.pdcPaymentTime = pdcPaymentTime;
    }

    public String getPdcPaymentState() {
        return pdcPaymentState;
    }

    public void setPdcPaymentState(String pdcPaymentState) {
        this.pdcPaymentState = pdcPaymentState;
    }

    public String getPdcPaymentAdmin() {
        return pdcPaymentAdmin;
    }

    public void setPdcPaymentAdmin(String pdcPaymentAdmin) {
        this.pdcPaymentAdmin = pdcPaymentAdmin;
    }

    public String getPdcAddTimeText() {
        return pdcAddTimeText;
    }

    public void setPdcAddTimeText(String pdcAddTimeText) {
        this.pdcAddTimeText = pdcAddTimeText;
    }

    public String getPdcPaymentTimeText() {
        return pdcPaymentTimeText;
    }

    public void setPdcPaymentTimeText(String pdcPaymentTimeText) {
        this.pdcPaymentTimeText = pdcPaymentTimeText;
    }

    public String getPdcPaymentStateText() {
        return pdcPaymentStateText;
    }

    public void setPdcPaymentStateText(String pdcPaymentStateText) {
        this.pdcPaymentStateText = pdcPaymentStateText;
    }

}
