package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class RetreatReturnDeliverBean implements Serializable {

    @SerializedName("return_id")
    private String returnId = "";
    @SerializedName("return_delay")
    private String returnDelay = "";
    @SerializedName("return_confirm")
    private String returnConfirm = "";
    @SerializedName("express_list")
    private ArrayList<ExpressListBean> expressList = new ArrayList<>();

    public String getReturnId() {
        return returnId;
    }

    public void setReturnId(String returnId) {
        this.returnId = returnId;
    }

    public String getReturnDelay() {
        return returnDelay;
    }

    public void setReturnDelay(String returnDelay) {
        this.returnDelay = returnDelay;
    }

    public String getReturnConfirm() {
        return returnConfirm;
    }

    public void setReturnConfirm(String returnConfirm) {
        this.returnConfirm = returnConfirm;
    }

    public ArrayList<ExpressListBean> getExpressList() {
        return expressList;
    }

    public void setExpressList(ArrayList<ExpressListBean> expressList) {
        this.expressList = expressList;
    }

    public static class ExpressListBean {

        @SerializedName("express_id")
        private String expressId = "";
        @SerializedName("express_name")
        private String expressName = "";

        public String getExpressId() {
            return expressId;
        }

        public void setExpressId(String expressId) {
            this.expressId = expressId;
        }

        public String getExpressName() {
            return expressName;
        }

        public void setExpressName(String expressName) {
            this.expressName = expressName;
        }

    }

}
