package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class SearchShowBean implements Serializable {

    @SerializedName("area_list")
    private ArrayList<AreaListBean> areaList = new ArrayList<>();
    @SerializedName("contract_list")
    private ArrayList<ContractListBean> contractList = new ArrayList<>();
    @SerializedName("gclist")
    private ArrayList<GcListBean> gcList = new ArrayList<>();

    public ArrayList<AreaListBean> getAreaList() {
        return areaList;
    }

    public void setAreaList(ArrayList<AreaListBean> areaList) {
        this.areaList = areaList;
    }

    public ArrayList<ContractListBean> getContractList() {
        return contractList;
    }

    public void setContractList(ArrayList<ContractListBean> contractList) {
        this.contractList = contractList;
    }

    public ArrayList<GcListBean> getGcList() {
        return gcList;
    }

    public void setGcList(ArrayList<GcListBean> gcList) {
        this.gcList = gcList;
    }

    public static class AreaListBean {

        @SerializedName("area_id")
        private String areaId = "";
        @SerializedName("area_name")
        private String areaName = "";

        public String getAreaId() {
            return areaId;
        }

        public void setAreaId(String areaId) {
            this.areaId = areaId;
        }

        public String getAreaName() {
            return areaName;
        }

        public void setAreaName(String areaName) {
            this.areaName = areaName;
        }
    }

    public static class ContractListBean {

        @SerializedName("id")
        private String id = "";
        @SerializedName("name")
        private String name = "";
        private boolean select;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isSelect() {
            return select;
        }

        public void setSelect(boolean select) {
            this.select = select;
        }

    }

    public static class GcListBean {

        @SerializedName("gc_id")
        private String gcId = "";
        @SerializedName("gc_name")
        private String gcName = "";

        public String getGcId() {
            return gcId;
        }

        public void setGcId(String gcId) {
            this.gcId = gcId;
        }

        public String getGcName() {
            return gcName;
        }

        public void setGcName(String gcName) {
            this.gcName = gcName;
        }

    }

}
