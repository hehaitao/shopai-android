package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class SellerLoginBean implements Serializable {

    @SerializedName("seller_name")
    private String sellerName = "";
    @SerializedName("store_name")
    private String storeName = "";
    @SerializedName("key")
    private String key = "";
    @SerializedName("mem")
    private MemBean mem = null;

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public MemBean getMem() {
        return mem;
    }

    public void setMem(MemBean mem) {
        this.mem = mem;
    }

    public static class MemBean {

        @SerializedName("username")
        private String username = "";
        @SerializedName("userid")
        private String userid = "";
        @SerializedName("key")
        private String key = "";

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

    }

}
