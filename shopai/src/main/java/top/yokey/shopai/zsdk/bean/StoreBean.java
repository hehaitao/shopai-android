package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class StoreBean implements Serializable {

    @SerializedName("store_info")
    private StoreInfoBean storeInfo = null;
    @SerializedName("store_seo")
    private StoreSeoBean storeSeo = null;
    @SerializedName("rec_goods_list_count")
    private String recGoodsListCount = "";
    @SerializedName("rec_goods_list")
    private ArrayList<RecGoodsListBean> recGoodsList = new ArrayList<>();

    public StoreInfoBean getStoreInfo() {
        return storeInfo;
    }

    public void setStoreInfo(StoreInfoBean storeInfo) {
        this.storeInfo = storeInfo;
    }

    public StoreSeoBean getStoreSeo() {
        return storeSeo;
    }

    public void setStoreSeo(StoreSeoBean storeSeo) {
        this.storeSeo = storeSeo;
    }

    public String getRecGoodsListCount() {
        return recGoodsListCount;
    }

    public void setRecGoodsListCount(String recGoodsListCount) {
        this.recGoodsListCount = recGoodsListCount;
    }

    public ArrayList<RecGoodsListBean> getRecGoodsList() {
        return recGoodsList;
    }

    public void setRecGoodsList(ArrayList<RecGoodsListBean> recGoodsList) {
        this.recGoodsList = recGoodsList;
    }

    public static class StoreInfoBean {

        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("store_name")
        private String storeName = "";
        @SerializedName("member_id")
        private String memberId = "";
        @SerializedName("store_qq")
        private String storeQq = "";
        @SerializedName("node_chat")
        private boolean nodeChat = false;
        @SerializedName("mb_store_decoration_switch")
        private String mbStoreDecorationSwitch = "";
        @SerializedName("store_imgname")
        private StoreImgnameBean storeImgname = null;
        @SerializedName("mb_store_menu")
        private MbStoreMenuBean mbStoreMenu = null;
        @SerializedName("store_avatar")
        private String storeAvatar = "";
        @SerializedName("goods_count")
        private String goodsCount = "";
        @SerializedName("store_collect")
        private String storeCollect = "";
        @SerializedName("is_favorate")
        private boolean isFavorate = false;
        @SerializedName("is_own_shop")
        private boolean isOwnShop = false;
        @SerializedName("store_credit_text")
        private String storeCreditText = "";
        @SerializedName("mb_title_img")
        private String mbTitleImg = "";
        @SerializedName("mb_sliders")
        private ArrayList<MbSlidersBean> mbSliders = new ArrayList<>();

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public String getStoreQq() {
            return storeQq;
        }

        public void setStoreQq(String storeQq) {
            this.storeQq = storeQq;
        }

        public boolean isNodeChat() {
            return nodeChat;
        }

        public void setNodeChat(boolean nodeChat) {
            this.nodeChat = nodeChat;
        }

        public String getMbStoreDecorationSwitch() {
            return mbStoreDecorationSwitch;
        }

        public void setMbStoreDecorationSwitch(String mbStoreDecorationSwitch) {
            this.mbStoreDecorationSwitch = mbStoreDecorationSwitch;
        }

        public StoreImgnameBean getStoreImgname() {
            return storeImgname;
        }

        public void setStoreImgname(StoreImgnameBean storeImgname) {
            this.storeImgname = storeImgname;
        }

        public MbStoreMenuBean getMbStoreMenu() {
            return mbStoreMenu;
        }

        public void setMbStoreMenu(MbStoreMenuBean mbStoreMenu) {
            this.mbStoreMenu = mbStoreMenu;
        }

        public String getStoreAvatar() {
            return storeAvatar;
        }

        public void setStoreAvatar(String storeAvatar) {
            this.storeAvatar = storeAvatar;
        }

        public String getGoodsCount() {
            return goodsCount;
        }

        public void setGoodsCount(String goodsCount) {
            this.goodsCount = goodsCount;
        }

        public String getStoreCollect() {
            return storeCollect;
        }

        public void setStoreCollect(String storeCollect) {
            this.storeCollect = storeCollect;
        }

        public boolean isIsFavorate() {
            return isFavorate;
        }

        public void setIsFavorate(boolean isFavorate) {
            this.isFavorate = isFavorate;
        }

        public boolean isIsOwnShop() {
            return isOwnShop;
        }

        public void setIsOwnShop(boolean isOwnShop) {
            this.isOwnShop = isOwnShop;
        }

        public String getStoreCreditText() {
            return storeCreditText;
        }

        public void setStoreCreditText(String storeCreditText) {
            this.storeCreditText = storeCreditText;
        }

        public String getMbTitleImg() {
            return mbTitleImg;
        }

        public void setMbTitleImg(String mbTitleImg) {
            this.mbTitleImg = mbTitleImg;
        }

        public ArrayList<MbSlidersBean> getMbSliders() {
            return mbSliders;
        }

        public void setMbSliders(ArrayList<MbSlidersBean> mbSliders) {
            this.mbSliders = mbSliders;
        }

        public static class StoreImgnameBean {

            @SerializedName("on")
            private String on = "";
            @SerializedName("data")
            private String data = "";

            public String getOn() {
                return on;
            }

            public void setOn(String on) {
                this.on = on;
            }

            public String getData() {
                return data;
            }

            public void setData(String data) {
                this.data = data;
            }

        }

        public static class MbStoreMenuBean {

            @SerializedName("on")
            private String on = "";
            @SerializedName("nou")
            private String nou = "";

            public String getOn() {
                return on;
            }

            public void setOn(String on) {
                this.on = on;
            }

            public String getNou() {
                return nou;
            }

            public void setNou(String nou) {
                this.nou = nou;
            }

        }

        public static class MbSlidersBean {

            @SerializedName("img")
            private String img = "";
            @SerializedName("type")
            private String type = "";
            @SerializedName("link")
            private String link = "";
            @SerializedName("imgUrl")
            private String imgUrl = "";

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getLink() {
                return link;
            }

            public void setLink(String link) {
                this.link = link;
            }

            public String getImgUrl() {
                return imgUrl;
            }

            public void setImgUrl(String imgUrl) {
                this.imgUrl = imgUrl;
            }

        }

    }

    public static class StoreSeoBean {

        @SerializedName("html_title")
        private String htmlTitle = "";
        @SerializedName("seo_keywords")
        private String seoKeywords = "";
        @SerializedName("seo_description")
        private String seoDescription = "";

        public String getHtmlTitle() {
            return htmlTitle;
        }

        public void setHtmlTitle(String htmlTitle) {
            this.htmlTitle = htmlTitle;
        }

        public String getSeoKeywords() {
            return seoKeywords;
        }

        public void setSeoKeywords(String seoKeywords) {
            this.seoKeywords = seoKeywords;
        }

        public String getSeoDescription() {
            return seoDescription;
        }

        public void setSeoDescription(String seoDescription) {
            this.seoDescription = seoDescription;
        }

    }

    public static class RecGoodsListBean {

        @SerializedName("goods_id")
        private String goodsId = "";
        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("store_name")
        private String storeName = "";
        @SerializedName("goods_name")
        private String goodsName = "";
        @SerializedName("goods_price")
        private String goodsPrice = "";
        @SerializedName("goods_marketprice")
        private String goodsMarketprice = "";
        @SerializedName("goods_image")
        private String goodsImage = "";
        @SerializedName("goods_salenum")
        private String goodsSalenum = "";
        @SerializedName("evaluation_good_star")
        private String evaluationGoodStar = "";
        @SerializedName("evaluation_count")
        private String evaluationCount = "";
        @SerializedName("is_virtual")
        private String isVirtual = "";
        @SerializedName("is_presell")
        private String isPresell = "";
        @SerializedName("is_fcode")
        private String isFcode = "";
        @SerializedName("have_gift")
        private String haveGift = "";
        @SerializedName("goods_addtime")
        private String goodsAddtime = "";
        @SerializedName("sole_flag")
        private boolean soleFlag = false;
        @SerializedName("group_flag")
        private boolean groupFlag = false;
        @SerializedName("xianshi_flag")
        private boolean xianshiFlag = false;
        @SerializedName("goods_image_url")
        private String goodsImageUrl = "";

        public String getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(String goodsId) {
            this.goodsId = goodsId;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public String getGoodsPrice() {
            return goodsPrice;
        }

        public void setGoodsPrice(String goodsPrice) {
            this.goodsPrice = goodsPrice;
        }

        public String getGoodsMarketprice() {
            return goodsMarketprice;
        }

        public void setGoodsMarketprice(String goodsMarketprice) {
            this.goodsMarketprice = goodsMarketprice;
        }

        public String getGoodsImage() {
            return goodsImage;
        }

        public void setGoodsImage(String goodsImage) {
            this.goodsImage = goodsImage;
        }

        public String getGoodsSalenum() {
            return goodsSalenum;
        }

        public void setGoodsSalenum(String goodsSalenum) {
            this.goodsSalenum = goodsSalenum;
        }

        public String getEvaluationGoodStar() {
            return evaluationGoodStar;
        }

        public void setEvaluationGoodStar(String evaluationGoodStar) {
            this.evaluationGoodStar = evaluationGoodStar;
        }

        public String getEvaluationCount() {
            return evaluationCount;
        }

        public void setEvaluationCount(String evaluationCount) {
            this.evaluationCount = evaluationCount;
        }

        public String getIsVirtual() {
            return isVirtual;
        }

        public void setIsVirtual(String isVirtual) {
            this.isVirtual = isVirtual;
        }

        public String getIsPresell() {
            return isPresell;
        }

        public void setIsPresell(String isPresell) {
            this.isPresell = isPresell;
        }

        public String getIsFcode() {
            return isFcode;
        }

        public void setIsFcode(String isFcode) {
            this.isFcode = isFcode;
        }

        public String getHaveGift() {
            return haveGift;
        }

        public void setHaveGift(String haveGift) {
            this.haveGift = haveGift;
        }

        public String getGoodsAddtime() {
            return goodsAddtime;
        }

        public void setGoodsAddtime(String goodsAddtime) {
            this.goodsAddtime = goodsAddtime;
        }

        public boolean isSoleFlag() {
            return soleFlag;
        }

        public void setSoleFlag(boolean soleFlag) {
            this.soleFlag = soleFlag;
        }

        public boolean isGroupFlag() {
            return groupFlag;
        }

        public void setGroupFlag(boolean groupFlag) {
            this.groupFlag = groupFlag;
        }

        public boolean isXianshiFlag() {
            return xianshiFlag;
        }

        public void setXianshiFlag(boolean xianshiFlag) {
            this.xianshiFlag = xianshiFlag;
        }

        public String getGoodsImageUrl() {
            return goodsImageUrl;
        }

        public void setGoodsImageUrl(String goodsImageUrl) {
            this.goodsImageUrl = goodsImageUrl;
        }

    }

}
