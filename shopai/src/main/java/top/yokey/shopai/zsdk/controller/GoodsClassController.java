package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.GoodsClassBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class GoodsClassController {

    private static final String ACT = "goods_class";

    public static void getChildAllList(HttpCallBack<ArrayList<GoodsClassBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "get_child_all_list")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, GoodsClassBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
