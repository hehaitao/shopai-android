package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.EvaluateBean;
import top.yokey.shopai.zsdk.bean.GoodsCalcBean;
import top.yokey.shopai.zsdk.bean.GoodsListBean;
import top.yokey.shopai.zsdk.bean.GoodsRobbuyBean;
import top.yokey.shopai.zsdk.bean.GoodsXianshiBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HtmlCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.data.GoodsSearchData;

@SuppressWarnings("ALL")
public class GoodsController {

    private static final String ACT = "goods";

    public static void goodsBody(String goodsId, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "goods_body")
                .add("goods_id", goodsId)
                .getHtml(new HtmlCallBack() {
                    @Override
                    public void onSuccess(String result) {
                        httpCallBack.onSuccess(result, null, result);
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void goodsDetailed(String goodsId, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "goods_detail")
                .add("goods_id", goodsId)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void calc(String goodsId, String areaId, HttpCallBack<GoodsCalcBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "calc")
                .add("goods_id", goodsId)
                .add("area_id", areaId)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(baseBean.getDatas(), GoodsCalcBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void goodsList(GoodsSearchData data, HttpCallBack<ArrayList<GoodsListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "goods_list")
                .add("curpage", data.getPage())
                .add("key", data.getKey())
                .add("page", ShopAISdk.get().getPageNumber())
                .add("order", data.getOrder())
                .add("gc_id", data.getGcId())
                .add("stc_id", data.getStcId())
                .add("b_id", data.getBrandId())
                .add("store_id", data.getStoreId())
                .add("keyword", data.getKeyword())
                .add("price_from", data.getPriceFrom())
                .add("price_to", data.getPriceTo())
                .add("area_id", data.getAreaId())
                .add("gift", data.getGift())
                .add("robbuy", data.getRobbuy())
                .add("xianshi", data.getXianshi())
                .add("virtual", data.getVirtual())
                .add("own_shop", data.getOwnShop())
                .add("ci", data.getCi())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "goods_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, GoodsListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void goodsGBList(GoodsSearchData data, HttpCallBack<ArrayList<GoodsRobbuyBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "goods_gblist")
                .add("curpage", data.getPage())
                .add("key", data.getKey())
                .add("page", ShopAISdk.get().getPageNumber())
                .add("order", data.getOrder())
                .add("gc_id", data.getGcId())
                .add("stc_id", data.getStcId())
                .add("b_id", data.getBrandId())
                .add("store_id", data.getStoreId())
                .add("keyword", data.getKeyword())
                .add("price_from", data.getPriceFrom())
                .add("price_to", data.getPriceTo())
                .add("area_id", data.getAreaId())
                .add("gift", data.getGift())
                .add("robbuy", data.getRobbuy())
                .add("xianshi", data.getXianshi())
                .add("virtual", data.getVirtual())
                .add("own_shop", data.getOwnShop())
                .add("ci", data.getCi())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "goods_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, GoodsRobbuyBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void goodsDZList(GoodsSearchData data, HttpCallBack<ArrayList<GoodsXianshiBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "goods_dzlist")
                .add("curpage", data.getPage())
                .add("key", data.getKey())
                .add("page", ShopAISdk.get().getPageNumber())
                .add("order", data.getOrder())
                .add("gc_id", data.getGcId())
                .add("stc_id", data.getStcId())
                .add("b_id", data.getBrandId())
                .add("store_id", data.getStoreId())
                .add("keyword", data.getKeyword())
                .add("price_from", data.getPriceFrom())
                .add("price_to", data.getPriceTo())
                .add("area_id", data.getAreaId())
                .add("gift", data.getGift())
                .add("robbuy", data.getRobbuy())
                .add("xianshi", data.getXianshi())
                .add("virtual", data.getVirtual())
                .add("own_shop", data.getOwnShop())
                .add("ci", data.getCi())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "goods_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, GoodsXianshiBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void goodsEvaluate(String goodsId, String type, String page, HttpCallBack<ArrayList<EvaluateBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "goods_evaluate")
                .add("goods_id", goodsId)
                .add("type", type)
                .add("curpage", page)
                .add("page", ShopAISdk.get().getPageNumber())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "goods_eval_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, EvaluateBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
