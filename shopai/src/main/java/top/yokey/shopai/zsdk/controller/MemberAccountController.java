package top.yokey.shopai.zsdk.controller;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class MemberAccountController {

    private static final String ACT = "member_account";

    public static void updateAvatar(String img, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "update_avatar")
                .add("img", img)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void updateXingeToken(String xingeToken, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "update_xingetoken")
                .add("xinge_token", xingeToken)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void updateInfo(String item, String value, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "update_info")
                .add("item", item)
                .add("value", value)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void bindMobikeStep1(String mobile, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "bind_mobile_step1")
                .add("mobile", mobile)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.getString(baseBean.getDatas(), "sms_time"));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void bindMobikeStep2(String authCode, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "bind_mobile_step2")
                .add("auth_code", authCode)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void modifyMobileStep2(HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "modify_mobile_step2")
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.getString(baseBean.getDatas(), "sms_time"));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void modifyMobileStep3(String authCode, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "modify_mobile_step3")
                .add("auth_code", authCode)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void modifyPasswordStep2(HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "modify_password_step2")
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.getString(baseBean.getDatas(), "sms_time"));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void modifyPasswordStep3(String authCode, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "modify_password_step3")
                .add("auth_code", authCode)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void modifyPasswordStep4(HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "modify_password_step4")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void modifyPasswordStep5(String password, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "modify_password_step5")
                .add("password", password)
                .add("password1", password)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void modifyPaypwdStep2(HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "modify_paypwd_step2")
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.getString(baseBean.getDatas(), "sms_time"));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void modifyPaypwdStep3(String authCode, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "modify_paypwd_step3")
                .add("auth_code", authCode)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void modifyPaypwdStep4(HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "modify_paypwd_step4")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void modifyPaypwdStep5(String password, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "modify_paypwd_step5")
                .add("password", password)
                .add("password1", password)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void updateArea(String provinceId, String cityId, String areaId, String areaInfo, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "update_area")
                .add("province_id", provinceId)
                .add("city_id", cityId)
                .add("area_id", areaId)
                .add("area_info", areaInfo)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
