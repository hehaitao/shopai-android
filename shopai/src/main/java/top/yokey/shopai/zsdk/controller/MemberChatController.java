package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.MessageListBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class MemberChatController {

    private static final String ACT = "member_chat";

    public static void getMsgCount(HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "get_msg_count")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void delMsg(String tId, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "del_msg")
                .add("t_id", tId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void getUserList(HttpCallBack<ArrayList<MessageListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "get_user_list")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "list");
                        if (data.equals("[]")) {
                            httpCallBack.onSuccess(result, baseBean, new ArrayList<MessageListBean>());
                            return;
                        }
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.object2ArrayList(data, MessageListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
