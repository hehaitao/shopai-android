package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.CouponBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class MemberCouponController {

    private static final String ACT = "member_coupon";

    public static void getcoupon(String tid, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "getcoupon")
                .add("tid", tid)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void couponLst(HttpCallBack<ArrayList<CouponBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "coupon_list")
                .add("page", "999")
                .add("curpage", "1")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "coupon_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, CouponBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void rpPwex(String pwdCode, String captcha, String codeKey, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "rp_pwex")
                .add("pwd_code", pwdCode)
                .add("captcha", captcha)
                .add("codekey", codeKey)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
