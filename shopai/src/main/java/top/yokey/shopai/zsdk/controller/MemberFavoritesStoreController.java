package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.StoreFavoriteBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class MemberFavoritesStoreController {

    private static final String ACT = "member_favorites_store";

    public static void favoritesDel(String storeId, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "favorites_del")
                .add("store_id", storeId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void favoritesAdd(String storeId, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "favorites_add")
                .add("store_id", storeId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void favoritesList(String page, HttpCallBack<ArrayList<StoreFavoriteBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "favorites_list")
                .add("page", ShopAISdk.get().getPageNumber())
                .add("curpage", page)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "favorites_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, StoreFavoriteBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
