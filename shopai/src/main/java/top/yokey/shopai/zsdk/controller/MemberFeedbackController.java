package top.yokey.shopai.zsdk.controller;

import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class MemberFeedbackController {

    private static final String ACT = "member_feedback";

    public static void feedbackAdd(String feedback, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "feedback_add")
                .add("feedback", feedback)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
