package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.PreDepositCashBean;
import top.yokey.shopai.zsdk.bean.PreDepositCashLogBean;
import top.yokey.shopai.zsdk.bean.PreDepositLogBean;
import top.yokey.shopai.zsdk.bean.PreDepositRechargeLogBean;
import top.yokey.shopai.zsdk.bean.RechargeCardLogBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class MemberFundController {

    private static final String ACT = "member_fund";

    public static void pdCashInfo(String id, HttpCallBack<PreDepositCashBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "pdcashinfo")
                .add("pdc_id", id)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "info");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(data, PreDepositCashBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void rcbLog(String page, HttpCallBack<ArrayList<RechargeCardLogBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "rcblog")
                .add("page", ShopAISdk.get().getPageNumber())
                .add("curpage", page)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "log_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, RechargeCardLogBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void preDepositLog(String page, HttpCallBack<ArrayList<PreDepositLogBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "predepositlog")
                .add("page", ShopAISdk.get().getPageNumber())
                .add("curpage", page)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, PreDepositLogBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void pdRechargeList(String page, HttpCallBack<ArrayList<PreDepositRechargeLogBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "pdrechargelist")
                .add("page", ShopAISdk.get().getPageNumber())
                .add("curpage", page)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, PreDepositRechargeLogBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void pdCashList(String page, HttpCallBack<ArrayList<PreDepositCashLogBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "pdcashlist")
                .add("page", ShopAISdk.get().getPageNumber())
                .add("curpage", page)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, PreDepositCashLogBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void rechargecardAdd(String rcSn, String captcha, String codekey, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "rechargecard_add")
                .add("rc_sn", rcSn)
                .add("captcha", captcha)
                .add("codekey", codekey)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
