package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.PointLogBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class MemberPointController {

    private static final String ACT = "member_points";

    public static void pointsLog(String page, HttpCallBack<ArrayList<PointLogBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "pointslog")
                .add("page", ShopAISdk.get().getPageNumber())
                .add("curpage", page)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "log_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, PointLogBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
