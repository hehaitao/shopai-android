package top.yokey.shopai.zsdk.controller;

import java.io.File;
import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.RetreatRefundAllBean;
import top.yokey.shopai.zsdk.bean.RetreatRefundBean;
import top.yokey.shopai.zsdk.bean.RetreatRefundInfoBean;
import top.yokey.shopai.zsdk.bean.RetreatRefundListBean;
import top.yokey.shopai.zsdk.bean.RetreatUploadPicBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.data.RetreatData;

@SuppressWarnings("ALL")
public class MemberRefundController {

    private static final String ACT = "member_refund";

    public static void uploadPic(File file, HttpCallBack<RetreatUploadPicBean> httpListener) {

        ShopAISdk.get().ready(ACT, "upload_pic")
                .add("refund_pic", file)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpListener.onSuccess(result, baseBean, JsonUtil.json2Object(baseBean.getDatas(), RetreatUploadPicBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpListener.onFailure(reason);
                    }
                });

    }

    public static void refundForm(RetreatData data, HttpCallBack<RetreatRefundBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "refund_form")
                .add("order_id", data.getOrderId())
                .add("order_goods_id", data.getOrderGoodsId())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(baseBean.getDatas(), RetreatRefundBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void refundAllForm(RetreatData data, HttpCallBack<RetreatRefundAllBean> httpListener) {

        ShopAISdk.get().ready(ACT, "refund_all_form")
                .add("order_id", data.getOrderId())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpListener.onSuccess(result, baseBean, JsonUtil.json2Object(baseBean.getDatas(), RetreatRefundAllBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpListener.onFailure(reason);
                    }
                });

    }

    public static void refundPost(RetreatData data, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "refund_post")
                .add("order_id", data.getOrderId())
                .add("order_goods_id", data.getOrderGoodsId())
                .add("refund_type", data.getRefundType())
                .add("reason_id", data.getReasonId())
                .add("refund_amount", data.getRefundAmount())
                .add("goods_num", data.getGoodsNum())
                .add("buyer_message", data.getBuyerMessage())
                .add("refund_pic[0]", data.getRefundPic0())
                .add("refund_pic[1]", data.getRefundPic1())
                .add("refund_pic[2]", data.getRefundPic2())
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void refundAllPost(RetreatData data, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "refund_all_post")
                .add("order_id", data.getOrderId())
                .add("buyer_message", data.getBuyerMessage())
                .add("refund_pic[0]", data.getRefundPic0())
                .add("refund_pic[1]", data.getRefundPic1())
                .add("refund_pic[2]", data.getRefundPic2())
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void getRefundList(String page, HttpCallBack<ArrayList<RetreatRefundListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "get_refund_list")
                .add("curpage", page)
                .add("page", ShopAISdk.get().getPageNumber())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "refund_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, RetreatRefundListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void getRefundInfo(String refundId, HttpCallBack<RetreatRefundInfoBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "get_refund_info")
                .add("refund_id", refundId)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = baseBean.getDatas().replace("[]", "null");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(data, RetreatRefundInfoBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
