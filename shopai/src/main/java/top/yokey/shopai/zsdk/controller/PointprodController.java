package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.PointGoodsBean;
import top.yokey.shopai.zsdk.bean.PointGoodsListBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.data.PointSearchData;

@SuppressWarnings("ALL")
public class PointprodController {

    private static final String ACT = "pointprod";

    public static void pinfo(String id, HttpCallBack<PointGoodsBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "pinfo")
                .add("id", id)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "goods_content");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(data, PointGoodsBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void index(PointSearchData data, HttpCallBack<ArrayList<PointGoodsListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "index")
                .add("keyword", data.getKeyword())
                .add("orderby", data.getOrderby())
                .add("isable", data.getIsable())
                .add("points_min", data.getPointsMin())
                .add("points_max", data.getPointsMax())
                .add("page", ShopAISdk.get().getPageNumber())
                .add("curpage", data.getPage())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "goods_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, PointGoodsListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
