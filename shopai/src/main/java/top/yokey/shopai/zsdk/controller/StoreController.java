package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.GoodsListBean;
import top.yokey.shopai.zsdk.bean.StoreBean;
import top.yokey.shopai.zsdk.bean.StoreGoodsClassBean;
import top.yokey.shopai.zsdk.bean.StoreGoodsRankBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.data.GoodsSearchData;

@SuppressWarnings("ALL")
public class StoreController {

    private static final String ACT = "store";

    public static void storeSale(String storeId, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "store_sale")
                .add("store_id", storeId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String sale = JsonUtil.getString(baseBean.getDatas(), "sale");
                        httpCallBack.onSuccess(result, baseBean, sale);
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void storeInfo(String storeId, HttpCallBack<StoreBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "store_info")
                .add("store_id", storeId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(baseBean.getDatas(), StoreBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void storeGoods(GoodsSearchData data, HttpCallBack<ArrayList<GoodsListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "store_goods")
                .add("curpage", data.getPage())
                .add("key", data.getKey())
                .add("page", ShopAISdk.get().getPageNumber())
                .add("order", data.getOrder())
                .add("stc_id", data.getStcId())
                .add("store_id", data.getStoreId())
                .add("keyword", data.getKeyword())
                .add("price_from", data.getPriceFrom())
                .add("price_to", data.getPriceTo())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "goods_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, GoodsListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void storeGoodsClass(String storeId, HttpCallBack<ArrayList<StoreGoodsClassBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "store_goods_class")
                .add("store_id", storeId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "store_goods_class");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, StoreGoodsClassBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void storeNewGoods(String storeId, String page, HttpCallBack<ArrayList<GoodsListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "store_new_goods")
                .add("curpage", page)
                .add("page", ShopAISdk.get().getPageNumber())
                .add("store_id", storeId)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "goods_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, GoodsListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void storeGoodsRank(String storeId, String orderType, String num, HttpCallBack<ArrayList<StoreGoodsRankBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "store_goods_rank")
                .add("store_id", storeId)
                .add("ordertype", orderType)
                .add("num", num)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "goods_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, StoreGoodsRankBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
