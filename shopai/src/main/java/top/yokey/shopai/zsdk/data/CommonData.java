package top.yokey.shopai.zsdk.data;

import java.io.Serializable;
import java.util.ArrayList;

import top.yokey.shopai.zsdk.bean.SpecialBean;

@SuppressWarnings("ALL")
public class CommonData implements Serializable {

    private int pos = 0;
    private int posGoods = 0;
    private String data = "";
    private ArrayList<SpecialBean> specialArrayList;

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public int getPosGoods() {
        return posGoods;
    }

    public void setPosGoods(int posGoods) {
        this.posGoods = posGoods;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public ArrayList<SpecialBean> getSpecialArrayList() {
        return specialArrayList;
    }

    public void setSpecialArrayList(ArrayList<SpecialBean> specialArrayList) {
        this.specialArrayList = specialArrayList;
    }

}
