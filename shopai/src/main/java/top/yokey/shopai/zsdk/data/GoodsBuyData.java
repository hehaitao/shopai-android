package top.yokey.shopai.zsdk.data;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class GoodsBuyData implements Serializable {

    private String ifcart = "";
    private String cartId = "";
    private String addressId = "";
    private String pingou = "";
    private String logId = "";
    private String buyerId = "";
    private String vatHash = "";
    private String offpayHash = "";
    private String offpayHashBatch = "";
    private String payName = "online";
    private String invoiceId = "0";
    private String voucher = "";
    private String pdPay = "0";
    private String password = "";
    private String fcode = "";
    private String rcbPay = "1";
    private String rpt = "";
    private String payMessage = "";
    private String jPointInput = "0";
    private String isPoint = "0";

    public String getIfcart() {
        return ifcart;
    }

    public void setIfcart(String ifcart) {
        this.ifcart = ifcart;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getPingou() {
        return pingou;
    }

    public void setPingou(String pingou) {
        this.pingou = pingou;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getVatHash() {
        return vatHash;
    }

    public void setVatHash(String vatHash) {
        this.vatHash = vatHash;
    }

    public String getOffpayHash() {
        return offpayHash;
    }

    public void setOffpayHash(String offpayHash) {
        this.offpayHash = offpayHash;
    }

    public String getOffpayHashBatch() {
        return offpayHashBatch;
    }

    public void setOffpayHashBatch(String offpayHashBatch) {
        this.offpayHashBatch = offpayHashBatch;
    }

    public String getPayName() {
        return payName;
    }

    public void setPayName(String payName) {
        this.payName = payName;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    public String getPdPay() {
        return pdPay;
    }

    public void setPdPay(String pdPay) {
        this.pdPay = pdPay;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFcode() {
        return fcode;
    }

    public void setFcode(String fcode) {
        this.fcode = fcode;
    }

    public String getRcbPay() {
        return rcbPay;
    }

    public void setRcbPay(String rcbPay) {
        this.rcbPay = rcbPay;
    }

    public String getRpt() {
        return rpt;
    }

    public void setRpt(String rpt) {
        this.rpt = rpt;
    }

    public String getPayMessage() {
        return payMessage;
    }

    public void setPayMessage(String payMessage) {
        this.payMessage = payMessage;
    }

    public String getjPointInput() {
        return jPointInput;
    }

    public void setjPointInput(String jPointInput) {
        this.jPointInput = jPointInput;
    }

    public String getIsPoint() {
        return isPoint;
    }

    public void setIsPoint(String isPoint) {
        this.isPoint = isPoint;
    }

}
