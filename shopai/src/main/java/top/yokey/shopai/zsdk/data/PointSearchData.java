package top.yokey.shopai.zsdk.data;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class PointSearchData implements Serializable {

    private String page = "1";
    private String keyword = "";
    private String orderby = "";
    private String isable = "";
    private String pointsMin = "";
    private String pointsMax = "";

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getOrderby() {
        return orderby;
    }

    public void setOrderby(String orderby) {
        this.orderby = orderby;
    }

    public String getIsable() {
        return isable;
    }

    public void setIsable(String isable) {
        this.isable = isable;
    }

    public String getPointsMin() {
        return pointsMin;
    }

    public void setPointsMin(String pointsMin) {
        this.pointsMin = pointsMin;
    }

    public String getPointsMax() {
        return pointsMax;
    }

    public void setPointsMax(String pointsMax) {
        this.pointsMax = pointsMax;
    }

}
