package top.yokey.shopai.zsdk.data;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class RetreatData implements Serializable {

    private String orderId = "";
    private String orderGoodsId = "";
    private String refundType = "";
    private String reasonId = "";
    private String refundAmount = "";
    private String goodsNum = "";
    private String buyerMessage = "";
    private String refundPic0 = "";
    private String refundPic1 = "";
    private String refundPic2 = "";

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderGoodsId() {
        return orderGoodsId;
    }

    public void setOrderGoodsId(String orderGoodsId) {
        this.orderGoodsId = orderGoodsId;
    }

    public String getRefundType() {
        return refundType;
    }

    public void setRefundType(String refundType) {
        this.refundType = refundType;
    }

    public String getReasonId() {
        return reasonId;
    }

    public void setReasonId(String reasonId) {
        this.reasonId = reasonId;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(String goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getBuyerMessage() {
        return buyerMessage;
    }

    public void setBuyerMessage(String buyerMessage) {
        this.buyerMessage = buyerMessage;
    }

    public String getRefundPic0() {
        return refundPic0;
    }

    public void setRefundPic0(String refundPic0) {
        this.refundPic0 = refundPic0;
    }

    public String getRefundPic1() {
        return refundPic1;
    }

    public void setRefundPic1(String refundPic1) {
        this.refundPic1 = refundPic1;
    }

    public String getRefundPic2() {
        return refundPic2;
    }

    public void setRefundPic2(String refundPic2) {
        this.refundPic2 = refundPic2;
    }

}
